﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class FundInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<FundInfomationUtility> fu = utmcData.FundGetAll();

                ddlFund2.Items.Add("All");
                foreach (FundInfomationUtility f in fu)
                {
                    ddlFund.Items.Add(new ListItem(f.Fund_Name, f.IPD_Fund_Code.ToString()));
                }

                List<Fund> funds = FundService.GetAllFund();

                foreach (Fund fund in funds)
                {
                    if (fund.ID != 1)
                    {
                        ddlFund2.Items.Add(new ListItem(fund.fund_name, fund.ID.ToString()));
                    }
                }
            }


            PriceFunds();
            FundsDistribution();
            FundUnitSplit();
        }

        public void PriceFunds()
        {

            //int count = 1;
            //string innerhtml = "";

            //List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 7 && x.Fund_Id == fundid && x.Status == 1).ToList();

            //List<Fund> fund = FundService.GetAllFund().Where(x => x.Status == 1).ToList();
            //List<DailyNAVFund> dnfs = new List<DailyNAVFund>();
            //List<DailyNAVFund> dnfsDESC = new List<DailyNAVFund>();
            //DailyNAVFund dnf = new DailyNAVFund();

            //foreach (Fund f in fund)
            //{
            //    dnfsDESC = DailyNAVFundData.GetByFundCode(f.fund_code);
            //    dnfsDESC = dnfsDESC.OrderByDescending(x => x.DailyNavDate).ToList();
            //    dnf = dnfsDESC.FirstOrDefault();
            //    dnfs.Add(dnf);
            //}

            //string fundname = "";

            //if (dnfs.Count() != 0)
            //{
            //    foreach (DailyNAVFund d in dnfs)
            //    {
            //        fundname = FundData.getFundNameByCode(d.FundCode);

            //        innerhtml += @"<tr><td>" + count + @"</td>
            //                           <td style='text-align:center'>" + fundname + @"</td>
            //                           <td style='text-align:center'>" + d.DailyUnitPrice + @"</td>
            //                           <td style='text-align:center'>" + d.DailyNavDate.ToString("dd/MM/yyyy") + @"</td>
            //                           </tr>";
            //        count++;
            //    }

            //    tablePrice1.InnerHtml = innerhtml;
            //}

            List<DailyNavFundUtility> navf = new List<DailyNavFundUtility>();
            navf = DailyFundService.GetAllPrice();
            navf = navf.OrderByDescending(x => x.Daily_NAV_Date).ToList();

            string userhtml = "";
            int count = 1;
            foreach (DailyNavFundUtility fdp in navf)
            {
                userhtml = userhtml + @"<tr data-id='" + fdp.id + @"'>
                                              <td  style='text-align:center'>" + count + @"</td>
            	                              <td style='text-align:center'>" + fdp.FundInfomationUtility.Fund_Name + @"</td>
                                              <td style='text-align:center'>" + fdp.Daily_Unit_Price + @"</td>
                                              <td style='text-align:center'>" + fdp.Daily_NAV_Date.ToString(@"dd/MM/yyyy") + @"</td>
            </tr>";
                count++;
            }
            tablePrice1.InnerHtml = userhtml;
        }

        public void FundsDistribution()
        {
            List<Distribution> dl = DistributionService.GetAllDistributionUI().Where(x => x.Status == 1).ToList();

            string userhtml = "";
            int count = 1;
            foreach (Distribution fdp in dl)
            {
                userhtml = userhtml + @"<tr data-id='" + fdp.Id + @"'>
                                            <td  style='text-align:center'>" + count + @"</td>
                                            <td style='text-align:center'>" + fdp.Fund.fund_name + @"</td>
                                            <td style='text-align:center'>" + fdp.EntitlementDate.ToString(@"dd/MM/yyyy") + @"</td>
                                            <td style='text-align:center'>" + fdp.Gross + @"</td>
										</tr>";
                count++;
            }
            show_dis.InnerHtml = userhtml;
        }

        public void FundUnitSplit()
        {
            int count = 1;
            string userhtml = "";
            List<UnitSplit> unitSplit = new List<UnitSplit>();
            unitSplit = UnitSplitService.GetAllUnitSplit().Where(x => x.Status == 1).ToList();

            foreach (UnitSplit us in unitSplit)
            {
                userhtml = userhtml + @"<tr>
                                            <td  style='text-align:center'>" + count + @"</td>
                                            <td>" + us.Fund.fund_name + @"</td>
                                            <td>" + us.ExDate.ToString("dd-MM-yyyy") + @"</td>
                                            <td>" + us.SplitRatio + @"</td>
                                        </tr>";
                count++;
            }
            show_us.InnerHtml = userhtml;
        }

        //public void FundsDistributio()
        //{
        //    List<BindFND> fd = new List<BindFND>();
        //    fd = bindFundDownloadService.bfd2();

        //    List<FundDistributionUtility> fd2 = new List<FundDistributionUtility>();
        //    fd2 = DailyFundService.GetAllDist().Where(x => x.Corporate_Action_Date);

        //    string userhtml = "";
        //    int count = 1;
        //    foreach (BindFND fdp in fd)
        //    {


        //        userhtml = userhtml + @"<tr data-id='" + fdp.fundid + @"'>
        //                                    <td style='text-align:center'>" + fdp.fundName + @"</td>
        //                                    <td style='text-align:center'>" + date.ToString(@"dd/MM/yyyy") + @"</td>
        //                                    <td style='text-align:center'>" + fdp.Distributions + @"</td>
        //		</tr>";
        //        count++;
        //    }
        //    show_dis.InnerHtml = userhtml;
        //}

        protected void search_Click(object sender, EventArgs e)
        {

            if (calendar1.Text != "")
            {
                if (calendar2.Text != "")
                {
                    sf1.Visible = false;
                    sf2.Visible = true;
                    fundname.Text = ddlFund.SelectedItem.Text.ToString();
                    string id = ddlFund.SelectedValue;
                    DateTime c1 = Convert.ToDateTime(calendar1.Text);
                    DateTime c2 = Convert.ToDateTime(calendar2.Text);

                    List<DailyNavFundUtility> navf = new List<DailyNavFundUtility>();
                    navf = DailyFundService.GetPrices(id, c1, c2);
                    navf = navf.OrderByDescending(x => x.Daily_NAV_Date).ToList();
                    string userhtml = "";
                    //int count = 1;


                    foreach (DailyNavFundUtility fdp in navf)
                    {

                        userhtml = userhtml + @"<tr data-id='" + fdp.id + @"'>
                                            <td style='text-align:center'>" + fdp.Daily_NAV_Date.ToString(@"dd/MM/yyyy") + @"</td>
                                            <td style='text-align:center'>" + fdp.Daily_Unit_Price + @"</td>
										</tr>";
                        //   count++;
                    }
                    tablePrice2.InnerHtml = userhtml;
                }
                else
                {
                    Response.Write("<script>window.alert('Please select an end date');</script>");
                }
            }
            else
            {
                Response.Write("<script>window.alert('Please select a start date');</script>");
            }

        }

        //protected void Search2_Click(object sender, EventArgs e)
        //{


        //}

        protected void ddlFund2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFund2.Text == "All")
            {
                List<Distribution> dl = DistributionService.GetAllDistribution().Where(x => x.Status == 1).ToList();

                string userhtml = "";
                int count = 1;
                foreach (Distribution fdp in dl)
                {
                    userhtml = userhtml + @"<tr data-id='" + fdp.Id + @"'>
                                            <td  style='text-align:center'>" + count + @"</td>
                                            <td style='text-align:center'>" + fdp.Fund.fund_name + @"</td>
                                            <td style='text-align:center'>" + fdp.EntitlementDate.ToString(@"dd/MM/yyyy") + @"</td>
                                            <td style='text-align:center'>" + fdp.Gross + @"</td>
										</tr>";
                    count++;
                }
                show_dis.InnerHtml = userhtml;
            }
            else
            {
                int fundid = Convert.ToInt32(ddlFund2.SelectedValue);
                List<Distribution> dl = DistributionService.GetAllDistribution().Where(x => x.Fund_Id == fundid && x.Status == 1).ToList();

                string userhtml = "";
                int count = 1;

                foreach (Distribution fdp in dl)
                {
                    userhtml = userhtml + @"<tr data-id='" + fdp.Id + @"'>
                                            <td  style='text-align:center'>" + count + @"</td>
                                            <td style='text-align:center'>" + fdp.Fund.fund_name + @"</td>
                                            <td style='text-align:center'>" + fdp.EntitlementDate.ToString(@"dd/MM/yyyy") + @"</td>
                                            <td style='text-align:center'>" + fdp.Gross + @"</td>
										</tr>";
                    count++;
                }
                show_dis.InnerHtml = userhtml;
            }

        }
    }
}