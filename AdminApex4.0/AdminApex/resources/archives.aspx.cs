﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class Archives : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_MC();
            Bind_P();
            Bind_A();
        }

        [SuppressUnmanagedCodeSecurity]
        internal static class SafeNativeMethods
        {
            [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
            public static extern int StrCmpLogicalW(string psz1, string psz2);
        }

        public sealed class NaturalStringComparer : IComparer<string>
        {
            public int Compare(string a, string b)
            {
                return SafeNativeMethods.StrCmpLogicalW(a, b);
            }
        }

        public void Bind_MC()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(4).ToList();
            downloadList = downloadList.OrderByDescending(b => b.Id).ToList();

            string userhtml = "";
            int x = 0;
            int y = 1;

            int loopCount = downloadList.Count();
            if (loopCount % 2 != 0)
            {
                loopCount = (loopCount / 2) + 1;
            }
            else
                loopCount = loopCount / 2;

            for (int i = 0; i < loopCount; i++)
            {
                userhtml += @"<tr><td style='border-width:0px;'><ul><li>
                                              <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                            <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
                                            </div>
                                            </a>
                                           </li></ul>
                                         </td>" + (y == downloadList.Count() ?
                                                            "<td style='border-width:0px;'></td></tr>"
                                                            :
                                            "<td style='border-width:0px;'><ul><li><a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
                                             <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                             <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
                                            </div>
                                            </a></td>
                                </tr>");
                x += 2;
                y += 2;
            }

            tableMC.InnerHtml = userhtml;
        }

        public void Bind_P()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(5).ToList();
            downloadList = downloadList.OrderByDescending(b => b.DisplayDate).ToList();

            string html = "";
            int count = 1;
            html += "<table>";
            foreach (Download dl in downloadList)
            {
                html = html + @"
                                    <tr>
                                        <td width='20%' style='text-align:left;padding-bottom:5px'>
                                          <p class='airDate'>" + dl.DisplayDate.ToString("dd MMMM yyyy") + @"</p>
                                        </td>
                                        <td width='80%' style='text-align:left;padding-bottom:5px'><a class='podcast_url' href='" + dl.UrlPath + @"' target='_blank'>" + dl.FileName + @"</a><p class='announcer'>" + dl.FileAuthor + @"</p>
                                        </td>
                                    </tr>
                                ";

                count++;
            }
            html += "</table>";
            podcast.InnerHtml = html;
        }

        public void Bind_A()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(6).ToList();
            downloadList = downloadList.OrderByDescending(b => b.DisplayDate).ToList();

            string html = "";
            int count = 1;

            html += "<p> Note : You will need Adobe Acrobat Reader in order to view or print the electronic articles. If you do not have it, you can download it from <a href ='https://get.adobe.com/reader/' target ='_blank'> Adobe Acrobat Reader</a> which is free of charge.</p>";
                       html += "<table>";
            foreach (Download dl in downloadList)
            {
                html = html + @"
                                      <tr>
                                        <td width='20%' style='text-align:left;padding-bottom:5px'>
                                        <p class='airDate'>" + dl.DisplayDate.ToString("dd MMMM yyyy") + @"</p>
                                        </td>
                                        <td width='80%' style='text-align:left;padding-bottom:5px'><a class='podcast_url' href='" + dl.UrlPath + @"' target='_blank'>" + dl.FileName + @"</a><p class='announcer'>" + dl.FileAuthor + @"</p>
                                        </td>
                                    </tr>
                                ";

                count++;
            }
            html += "</table>";
            articles.InnerHtml = html;
        }

        //public void Bind_MC(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(c => c.DownloadType_Id == 4 && c.Status == 1).ToList();
        //    //downloadList = downloadList.OrderByDescending(b => b.Id).ToList();

        //    string userhtml = "";
        //    int x = 0;
        //    int y = 1;

        //    int loopCount = downloadList.Count();
        //    if (loopCount % 2 != 0)
        //    {
        //        loopCount = (loopCount / 2) + 1;
        //    }
        //    else
        //        loopCount = loopCount / 2;

        //    for (int i = 0; i < loopCount; i++)
        //    {
        //        userhtml += @"<tr><td style='border-width:0px;'><ul><li>
        //                                      <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
        //                                    <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                    <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
        //                                    </div>
        //                                    </a>
        //                                   </li></ul>
        //                                 </td>" + (y == downloadList.Count() ?
        //                                                    "<td style='border-width:0px;'></td></tr>"
        //                                                    :
        //                                    "<td style='border-width:0px;'><ul><li><a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
        //                                     <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                     <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
        //                                    </div>
        //                                    </a></td>
        //                        </tr>");
        //        x += 2;
        //        y += 2;
        //    }

        //    tableMC.InnerHtml = userhtml;
        //}



        //public void Bind_P(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 5 && x.Status == 1).ToList();
        //    //downloadList = downloadList.OrderByDescending(b => b.DisplayDate).ToList();

        //    string html = "";
        //    int count = 1;
        //    html += "<table>";
        //    foreach (Download dl in downloadList)
        //    {
        //        html = html + @"
        //                            <tr>
        //                                <td width='20%' style='text-align:left;padding-bottom:5px'>
        //                                  <p class='airDate'>" + dl.DisplayDate.ToString("dd MMMM yyyy") + @"</p>
        //                                </td>
        //                                <td width='80%' style='text-align:left;padding-bottom:5px'><a class='podcast_url' href='" + dl.UrlPath + @"' target='_blank'>" + dl.FileName + @"</a><p class='announcer'>" + dl.FileAuthor + @"</p>
        //                                </td>
        //                            </tr>
        //                        ";

        //        count++;
        //    }
        //    html += "</table>";
        //    podcast.InnerHtml = html;
        //}

        //public void Bind_A(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 6 && x.Status == 1).ToList();
        //    //downloadList = downloadList.OrderByDescending(b => b.DisplayDate).ToList();

        //    string html = "";
        //    int count = 1;

        //    html += "<p> Note : You will need Adobe Acrobat Reader in order to view or print the electronic articles. If you do not have it, you can download it from <a href ='https://get.adobe.com/reader/' target ='_blank'> Adobe Acrobat Reader</a> which is free of charge.</p>";
        //    html += "<table>";
        //    foreach (Download dl in downloadList)
        //    {
        //        html = html + @"
        //                              <tr>
        //                                <td width='20%' style='text-align:left;padding-bottom:5px'>
        //                                <p class='airDate'>" + dl.DisplayDate.ToString("dd MMMM yyyy") + @"</p>
        //                                </td>
        //                                <td width='80%' style='text-align:left;padding-bottom:5px'><a class='podcast_url' href='" + dl.UrlPath + @"' target='_blank'>" + dl.FileName + @"</a><p class='announcer'>" + dl.FileAuthor + @"</p>
        //                                </td>
        //                            </tr>
        //                        ";

        //        count++;
        //    }
        //    html += "</table>";
        //    articles.InnerHtml = html;
        //}


        public string ConvertBytesToMegaBytes(string bytes)
        {
            double size = (Convert.ToDouble(bytes) / 1024) / 1024;
            return string.Format("{0:0.00} MB", size);
        }
    }
}