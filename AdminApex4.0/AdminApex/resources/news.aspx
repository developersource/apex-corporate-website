﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="AdminApex.news" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <style>
        .content.narrow {
            max-width: 1000px;
            padding: 20px;
        }

        .newscontent img, #news_table img {
            max-width: 100%;
            height: auto;
        }
    </style>
    <div class="resources">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image">
                    <img src="/ICONPIC/APEX_Header_news.jpg"/>
                    <div class="header-copy">
                        <%--<h2>20 years strong, the APEX team is shaped for every investor’s financial success.</h2>--%>
                        <h2>DISTILLING THE BEST IDEAS.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content narrow">
            <h3 class="inner-content">News</h3>
            <table>
                <tbody id="newtable" runat="server" clientidmode="static">
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
