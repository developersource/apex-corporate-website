﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

namespace AdminApex
{
    public partial class Downloads : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindFiles();
            BindForms();
            Bind_MP();
            Bind_SMP();
        }

        private void BindFiles()
        {
            List<bindFundDownloads> bfds = new List<bindFundDownloads>();
            bfds = bindFundDownloadService.bfd();
            
            string userhtml = "";

            foreach (bindFundDownloads b in bfds)
            {
                Download phs = b.downloadFiles.Where(x => x.FileName == "Product Highlights Sheet").FirstOrDefault();
                Download ffs = b.downloadFiles.Where(x => x.FileName == "Fund Fact Sheet").FirstOrDefault();
                Download bro = b.downloadFiles.Where(x => x.FileName == "Brochure").FirstOrDefault();
                Download ir = b.downloadFiles.Where(x => x.FileName == "Interim Report").FirstOrDefault();
                Download ar = b.downloadFiles.Where(x => x.FileName == "Annual Report").FirstOrDefault();
                Download im = b.downloadFiles.Where(x => x.FileName == "Information Memorandum").FirstOrDefault();

                int fundID = b.fundid;

                if (fundID == 11) {
                    userhtml = userhtml + @"<tr data-id='" + b.fundid + @"'>
        			<td>" + b.fundName + @"</td>
        			<td style='text-align: center'><a target='_blank' href='" + (phs != null ? phs.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></a></td>
                    <td style='text-align: center'><a target='_blank' href='" + (ffs != null ? ffs.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (bro != null ? bro.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (ir != null ? ir.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (ar != null ? ar.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
                    <td style='text-align: center'><a target='_blank' href='" + (im != null ? im.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        		</tr>";
                }
                else
                {
                    userhtml = userhtml + @"<tr data-id='" + b.fundid + @"'>
        			<td>" + b.fundName + @"</td>
        			<td style='text-align: center'><a target='_blank' href='" + (phs != null ? phs.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></a></td>
                    <td style='text-align: center'><a target='_blank' href='" + (ffs != null ? ffs.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (bro != null ? bro.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (ir != null ? ir.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
        			<td style='text-align: center'><a target='_blank' href='" + (ar != null ? ar.UrlPath : "/errorMessage.aspx") + @"'><i class='fa fa-file-pdf-o'></i></td>
                    <td style='text-align: center'><a target='_blank'>N/A</td>
        		</tr>";
                }

                 
                
            }
            fundfile.InnerHtml = userhtml;
        }

        private void BindForms()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(3).ToList();
            downloadList = downloadList.OrderBy(b => b.FileName).ToList();

            string userhtml = "";
            int x = 0;
            int y = 1;

            int loopCount = downloadList.Count();
            if (loopCount % 2 != 0)
            {
                loopCount = (loopCount / 2) + 1;
            }
            else
                loopCount = loopCount / 2;

            for (int i = 0; i < loopCount; i++)
            {
                userhtml += @"<tr><td style='border-width:0px;'>
                                            <ul>
                                            <li>
                                            <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                            <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
                                            </div>
                                            </a>
                                            </li>
                                            </ul>
                                         </td>" + (y == downloadList.Count() ?
                                                            "<td style='border-width:0px;'></td></tr>"
                                                            :
                                            "<td style='border-width:0px;'><ul><li>" +
                                            "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
                                             <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                             <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
                                            </div>
                                            </a></li></ul></td>
                                </tr>");
                x += 2;
                y += 2;
            }



            tableForms.InnerHtml = userhtml;


            //foreach (DownloadUtility dl in downloadList)
            //{
            //    userhtml = userhtml + @"<ul>
            //                              <li class=''>
            //                                <a target='_blank' href='" + dl.UrlPath + @"'>
            //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
            //                                <div class='details'>
            //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
            //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
            //                                </div>
            //                                </a>
            //                              </li>
            //                           </ul>";
            //    count++;
            //}
            //forms.InnerHtml = userhtml;
        }

        public void Bind_MP()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(1).ToList();
            downloadList = downloadList.OrderBy(b => b.FileName).ToList();

            string userhtml = "";
            int x = 0;
            int y = 1;

            int loopCount = downloadList.Count();
            if (loopCount % 2 != 0)
            {
                loopCount = (loopCount / 2) + 1;
            }
            else
                loopCount = loopCount / 2;

            for (int i = 0; i < loopCount; i++)
            {
                userhtml += @"<tr><td style='border-width:0px;'>
                                            <ul>
                                            <li>
                                            <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                            <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
                                            </div>
                                            </a>
                                            </li>
                                            </ul>
                                         </td>" + (y == downloadList.Count() ?
                                                            "<td style='border-width:0px;'></td></tr>"
                                                            :
                                            "<td style='border-width:0px;'><ul><li>" +
                                            "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
                                             <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                             <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
                                            </div>
                                            </a></li></ul></td>
                                </tr>");
                x += 2;
                y += 2;
            }

            mptable.InnerHtml = userhtml;
            //int count = 1;
            //foreach (Download dl in downloadList)
            //{
            //    userhtml = userhtml + @"<ul>
            //                              <li class=''>
            //                                <a target='_blank' href='" + dl.UrlPath + @"'>
            //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
            //                                <div class='details'>
            //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
            //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
            //                                </div>
            //                                </a>
            //                              </li>
            //                           </ul>";
            //    count++;
            //}
        }

        public void Bind_SMP()
        {
            List<Download> downloadList = DownloadService.GetDownloadByType(2).ToList();
            downloadList = downloadList.OrderBy(b => b.FileName).ToList();

            string userhtml = "";

            int x = 0;
            int y = 1;

            int loopCount = downloadList.Count();
            if (loopCount % 2 != 0)
            {
                loopCount = (loopCount / 2) + 1;
            }
            else
                loopCount = loopCount / 2;

            for (int i = 0; i < loopCount; i++)
            {
                userhtml += @"<tr><td style='border-width:0px;'>
                                            <ul>
                                            <li>
                                            <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                            <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
                                            </div>
                                            </a>
                                            </li>
                                            </ul>
                                         </td>" + (y == downloadList.Count() ?
                                                            "<td style='border-width:0px;'></td></tr>"
                                                            :
                                            "<td style='border-width:0px;'><ul><li>" +
                                            "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
                                             <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                             <div class='details'>
                                                <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
                                                <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
                                            </div>
                                            </a></li></ul></td>
                                </tr>");
                x += 2;
                y += 2;
            }
            //int count = 1;
            //foreach (Download dl in downloadList)
            //{
            //    userhtml = userhtml + @"<ul>
            //                              <li class=''>
            //                                <a target='_blank' href='" + dl.UrlPath + @"'>
            //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
            //                                <div class='details'>
            //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
            //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
            //                                </div>
            //                                </a>
            //                              </li>
            //                           </ul>";
            //    count++;
            //}
            smptable.InnerHtml = userhtml;
        }

        //private void BindForms(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(c => c.DownloadType_Id == 3 && c.Status == 1).ToList();
        //    //downloadList = downloadList.OrderBy(b => b.FileName).ToList();

        //    string userhtml = "";
        //    int x = 0;
        //    int y = 1;

        //    int loopCount = downloadList.Count();
        //    if (loopCount % 2 != 0)
        //    {
        //        loopCount = (loopCount / 2) + 1;
        //    }
        //    else
        //        loopCount = loopCount / 2;

        //    for (int i = 0; i < loopCount; i++)
        //    {
        //        userhtml += @"<tr><td style='border-width:0px;'>
        //                                    <ul>
        //                                    <li>
        //                                    <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
        //                                    <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                    <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
        //                                    </div>
        //                                    </a>
        //                                    </li>
        //                                    </ul>
        //                                 </td>" + (y == downloadList.Count() ?
        //                                                    "<td style='border-width:0px;'></td></tr>"
        //                                                    :
        //                                    "<td style='border-width:0px;'><ul><li>" +
        //                                    "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
        //                                     <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                     <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
        //                                    </div>
        //                                    </a></li></ul></td>
        //                        </tr>");
        //        x += 2;
        //        y += 2;
        //    }



        //    tableForms.InnerHtml = userhtml;


        //    //foreach (DownloadUtility dl in downloadList)
        //    //{
        //    //    userhtml = userhtml + @"<ul>
        //    //                              <li class=''>
        //    //                                <a target='_blank' href='" + dl.UrlPath + @"'>
        //    //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //    //                                <div class='details'>
        //    //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
        //    //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
        //    //                                </div>
        //    //                                </a>
        //    //                              </li>
        //    //                           </ul>";
        //    //    count++;
        //    //}
        //    //forms.InnerHtml = userhtml;
        //}

        //public void Bind_MP(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(c => c.DownloadType_Id == 1 && c.Status == 1).ToList();
        //    //downloadList = downloadList.OrderBy(b => b.FileName).ToList();

        //    string userhtml = "";
        //    int x = 0;
        //    int y = 1;

        //    int loopCount = downloadList.Count();
        //    if (loopCount % 2 != 0)
        //    {
        //        loopCount = (loopCount / 2) + 1;
        //    }
        //    else
        //        loopCount = loopCount / 2;

        //    for (int i = 0; i < loopCount; i++)
        //    {
        //        userhtml += @"<tr><td style='border-width:0px;'>
        //                                    <ul>
        //                                    <li>
        //                                    <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
        //                                    <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                    <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
        //                                    </div>
        //                                    </a>
        //                                    </li>
        //                                    </ul>
        //                                 </td>" + (y == downloadList.Count() ?
        //                                                    "<td style='border-width:0px;'></td></tr>"
        //                                                    :
        //                                    "<td style='border-width:0px;'><ul><li>" +
        //                                    "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
        //                                     <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                     <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
        //                                    </div>
        //                                    </a></li></ul></td>
        //                        </tr>");
        //        x += 2;
        //        y += 2;
        //    }

        //    mptable.InnerHtml = userhtml;
        //    //int count = 1;
        //    //foreach (Download dl in downloadList)
        //    //{
        //    //    userhtml = userhtml + @"<ul>
        //    //                              <li class=''>
        //    //                                <a target='_blank' href='" + dl.UrlPath + @"'>
        //    //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //    //                                <div class='details'>
        //    //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
        //    //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
        //    //                                </div>
        //    //                                </a>
        //    //                              </li>
        //    //                           </ul>";
        //    //    count++;
        //    //}
        //}

        //public void Bind_SMP(List<Download> downloadList)
        //{
        //    //List<Download> downloadList = DownloadService.GetAllDownload().Where(c => c.DownloadType_Id == 2 && c.Status == 1).ToList();
        //    //downloadList = downloadList.OrderBy(b => b.FileName).ToList();

        //    string userhtml = "";

        //    int x = 0;
        //    int y = 1;

        //    int loopCount = downloadList.Count();
        //    if (loopCount % 2 != 0)
        //    {
        //        loopCount = (loopCount / 2) + 1;
        //    }
        //    else
        //        loopCount = loopCount / 2;

        //    for (int i = 0; i < loopCount; i++)
        //    {
        //        userhtml += @"<tr><td style='border-width:0px;'>
        //                                    <ul>
        //                                    <li>
        //                                    <a target='_blank' href='" + downloadList[x].UrlPath + @"'>
        //                                    <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                    <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[x].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[x].FileSize) + @"</p>
        //                                    </div>
        //                                    </a>
        //                                    </li>
        //                                    </ul>
        //                                 </td>" + (y == downloadList.Count() ?
        //                                                    "<td style='border-width:0px;'></td></tr>"
        //                                                    :
        //                                    "<td style='border-width:0px;'><ul><li>" +
        //                                    "<a target='_blank' href = '" + downloadList[y].UrlPath + @"' >
        //                                     <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //                                     <div class='details'>
        //                                        <p class='font-light-blue'>" + downloadList[y].FileName + @"</p>
        //                                        <p class='font-black'>" + ConvertBytesToMegaBytes(downloadList[y].FileSize) + @"</p>
        //                                    </div>
        //                                    </a></li></ul></td>
        //                        </tr>");
        //        x += 2;
        //        y += 2;
        //    }
        //    //int count = 1;
        //    //foreach (Download dl in downloadList)
        //    //{
        //    //    userhtml = userhtml + @"<ul>
        //    //                              <li class=''>
        //    //                                <a target='_blank' href='" + dl.UrlPath + @"'>
        //    //                                <img class='file-icon' src='/ICONPIC/file-icon.png' />
        //    //                                <div class='details'>
        //    //                                    <p class='font-light-blue'>" + dl.FileName + @"</p>
        //    //                                    <p class='font-black'>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</p>
        //    //                                </div>
        //    //                                </a>
        //    //                              </li>
        //    //                           </ul>";
        //    //    count++;
        //    //}
        //    smptable.InnerHtml = userhtml;
        //}


        public string ConvertBytesToMegaBytes(string bytes)
        {
            double size = (Convert.ToDouble(bytes) / 1024) / 1024;
            return string.Format("{0:0.00} MB", size);
        }


        }
}