﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class news : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindNEWS();
        }
        public void BindNEWS()
        {


            string innerP = "";
            string html = "";
            string content = "";

            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                List<News> newList = NewsService.NewsGetAll().Where(x => x.id == id).ToList();
                News latestNews = newList.FirstOrDefault();

                List<fund_news> fns = FundnewService.GetGetByID(latestNews.id);
                foreach (fund_news fn in fns)
                {
                    innerP += "<p style='color:#71a7b9'>" + fn.fund.fund_name + @"</P>";
                }

                content = makeLink(latestNews.content);
                content = content.Replace("\r\n", "<br />\r\n");

                html = html + @"<table style='width:100%;word-break: break-word;'><tr>
                                     <td style='width:50%;border-width:0px' rowspan='4'><img src='" + latestNews.UrlPath + @"' Width='85%' Height='50%'/></td> 
                                     <td style='width:50%;border-width:0px;font-weight:bold'><p>" + latestNews.title + @"</p></td></tr>
                                     <tr><td style='width:50%;border-width:0px'><p style='text-align:justify;line-height:110%'>" + content + @"</p></td></tr>
                                     <tr><td style='width:50%;border-width:0px'><p style='text-align:justify;line-height:110%'>" + innerP + @"</p></td></tr>
                               </table>
                                <div style='height:1px;background-color:black'></div>";
                innerP = "";

            }
            else
            {
                List<News> newList = NewsService.NewsGetAll().Where(x => x.Status == 1).ToList();
                newList = newList.OrderByDescending(x => x.UploadDate).ToList();

                int count = 1;
                foreach (News n in newList)
                {
                    List<fund_news> fns = FundnewService.GetGetByID(n.id);
                    foreach (fund_news fn in fns)
                    {
                        innerP += "<p style='color:#71a7b9'>" + fn.fund.fund_name + @"</P>";
                    }

                    content = makeLink(n.content);
                    content = content.Replace("\r\n", "<br />\r\n");

                    html = html + @"<table style='width:100%;word-break: break-word;'><tr>
                                     <td style='width:50%;border-width:0px' rowspan='4'><img src='" + n.UrlPath + @"' Width='85%' Height='50%'/></td> 
                                     <td style='width:50%;border-width:0px;font-weight:bold'><p>" + n.title + @"</p></td></tr>
                                     <tr><td style='width:50%;border-width:0px'><p style='text-align:justify;line-height:110%'>" + content + @"</p></td></tr>
                                     <tr><td style='width:50%;border-width:0px'><p style='text-align:justify;line-height:110%'>" + innerP + @"</p></td></tr>
                               </table>
                                <div style='height:1px;background-color:black'></div>";
                    innerP = "";

                    count++;
                    //}
                }
                
            }
            newtable.InnerHtml = html;

        }

        private string makeLink(string txt)
        {
            Regex regx = new Regex("http(s)?://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                txt = txt.Replace(match.Value, "<a href='" + match.Value + "' target='_blank'>" + match.Value + "</a>");
            }

            return txt;
        }
    }
}