﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="unittrust.aspx.cs" Inherits="AdminApex.UnitTrust" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="products unit_trust">
        <div class="content pages">
            <div class="products header bg-white">
                <div class="header-content wide-image">
                    <img src="../ICONPIC/EDM27334.jpg" style="object-position:top"  />
                    <div class="header-copy">
                        <!--<h2>The Apex team has been<br>carefully selected.</h2>-->
                        <h2>YOUR PERFORMANCE,<br>
                            OUR GOAL.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content narrow">
            <h3 class="inner-content">Unit Trust</h3>
        </div>
        <div class="subnav bg-gray">
            <div class="bg-gray-container subnav-wrapper">
                <div class="special_select desktop-hidden">
                    <ul>
                        <li>
                            <a href="javascript:;" id="mobile_page"></a>
                            <img class="" src="http://staging.apexis.com.my/img/down-arrow.png" />
                        </li>
                    </ul>
                </div>
                <div class="inner-content">
                    <ul>
                        <li><a class="active" page="conventional_equities" href="javascript:;">Conventional Funds</a></li>
                        <li><a class="" page="shariah_equities" href="javascript:;">Shariah Funds</a></li>
                        <li><a class="" page="shariah_money_market" href="javascript:;">Shariah Money Market</a></li>
                    </ul>
                    <div class="subnav active-bar"></div>
                </div>
            </div>
        </div>
        <div class="content narrow subpages">
            <!-- ----------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------- -->
            <div class="products inner-content subnav-pages" id="conventional_equities" runat="server" clientidmode="Static">
            </div>
            <!-- ----------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------- -->
            <div class="products inner-content subnav-pages" id="shariah_equities" runat="server" clientidmode="Static">
            </div>
            <!-- ----------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------- -->
            <div class="products inner-content subnav-pages" id="shariah_money_market" runat="server" clientidmode="Static">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
