﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class UnitTrust : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindConventional();
                BindSharie();
                Bindshariahmm();
            }
        }

        private void BindConventional()
        {
            List<Fund> fs = new List<Fund>();
            fs = FundService.GetAllFunds().Where(c => c.Status == 1).ToList();

            List<bindFundDownloads> bfds = new List<bindFundDownloads>();
            bfds = bindFundDownloadService.bfd();

            string userhtml = "";
            int count = 1;

            foreach (Fund f in fs)
            {
                    if (f.DownloadType_Id == 7)
                    {
                        foreach (bindFundDownloads b in bfds)
                        {
                            if (f.fund_name == b.fundName)
                            {

                                Download phs = b.downloadFiles.Where(x => x.FileName == "Product Highlights Sheet").FirstOrDefault();
                                Download ffs = b.downloadFiles.Where(x => x.FileName == "Fund Fact Sheet").FirstOrDefault();
                                Download bro = b.downloadFiles.Where(x => x.FileName == "Brochure").FirstOrDefault();
                                Download ir = b.downloadFiles.Where(x => x.FileName == "Interim Report").FirstOrDefault();
                                Download ar = b.downloadFiles.Where(x => x.FileName == "Annual Report").FirstOrDefault();
                                Download im = b.downloadFiles.Where(x => x.FileName == "Information Memorandum").FirstOrDefault();

                            userhtml = userhtml + @"<ul class='tiles'>
                         <li style = 'background:" + f.backgroundColor + @"'>
                         <div class='info'>
                            <p class='font-light-blue'>" + f.fund_name + @"</p>
                                </div>
                                <div class='expandable-div-holder'>
                                    <div class='expandable-div-cover'>
                                        <div class='expandable-div-content'>
                                            <div class='expandable-div-bg bg-white'></div>
                                                <div class='profile-bio-holder'>
                                                    <div class='product-details'>
                                                        <a href = 'javascript:;' >
                                                        <img class='close-btn' src='/ICONPIC/close-btn-dark.png'/></a>
                                                        <img class='face'/>
                                                        <!-- <img class='face' src='" + f.url_path + @"'/> -->
                                                        <h4 class='font-black'>" + f.fund_name + @"</h4>
                                                        <div class='copy'>
                                                        <h5 class='font-light-blue'>Potential Investors</h5>
                                                        <p>" + f.potential_inves + @"</p>
                                                        <h5 class='font-light-blue'>Investment Strategy</h5>
                                                        <p>" + f.inves_strategy + @"</p>
                                                        <h5 class='font-light-blue'>EPF Approved Fund</h5>
                                                        <p>" + f.epfapproved + @"</p>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Launch Date</h5>
                                                                    <p>" + f.launch_date.ToString(@"dd MMMM yyyy") + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Trustee</h5>
                                                                    <p>" + f.trustee + @"</p>
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Fund Category/type</h5>
                                                                    <p>" + f.fund_category + @"</p>        
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Sales charge</h5>
                                                                    <p>" + f.sales_charge + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Management fee</h5>
                                                                    <p>" + f.manage_fee + @"</p>     
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Trustee fee</h5>
                                                                    <p>" + f.trustee_fee + @"</p>      
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr> 
                                                                <td width = '35%'>   
                                                                <h5 class='font-light-blue'>Minimum initial investment(MYR)</h5>
                                                                <p>" + f.min_ini_inves + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                <h5 class='font-light-blue'>Minimum Subsequent investment(MYR)</h5>
                                                                <p>" + f.min_sub_inv + @"</p> 
                                                                </td>                                                                
                                                                <td width = '35%'> 
                                                                <h5 class='font-light-blue'>Redemption fee</h5>
                                                                <p>" + f.redemp_fee + @"</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                     </div>
                                                     <div class='narrow-copy'>
                                                        <h3> Interested to Invest?</h3>     
                                                        <br>
                                                        <a href='/Contact.aspx' class='rounded-btn bg-light-blue'>Contact Us</a>
                                                     </div>
                                                </div>

                                                <div class='content bg-gray resources'>
                                                <div class='product-details'>
                                                <h4>Resources</h4>  
                                                <ul>
                                                    <li>
                                                        <a href='" + (ar != null ? ar.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png'/>
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Annual Reports</p>
                                                                <p class='font-black'>(" + (ar == null ? "0" : ConvertBytesToMegaBytes(ar.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href='" + (ir != null ? ir.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Interim Reports</p>
                                                                <p class='font-black'>(" + (ir == null ? "0" : ConvertBytesToMegaBytes(ir.FileSize)) + @")</p>
                                                            </div>      
                                                        </a>
                                                    </li>        
                                                    <li>
                                                        <a href='" + (phs != null ? phs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Product Highlights Sheet</p>
                                                                <p class='font-black'>(" + (phs == null ? "0" : ConvertBytesToMegaBytes(phs.FileSize)) + @")</p>     
                                                            </div>        
                                                        </a>       
                                                    </li>       
                                                    <li>
                                                        <a href='" + (ffs != null ? ffs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Fund Fact Sheet</p>
                                                                <p class='font-black'>(" + (ffs == null ? "0" : ConvertBytesToMegaBytes(ffs.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>    
                                                        <a href='" + (bro != null ? bro.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>Brochure</p>
                                                                <p class='font-black'>(" + (bro == null ? "0" : ConvertBytesToMegaBytes(bro.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                         <li>    
                                                        <a href='" + (im != null ? im.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>Information Memorandum</p>
                                                                <p class='font-black'>(" + (im == null ? "0" : ConvertBytesToMegaBytes(im.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <img class='face'/>
                         <!-- <img class='face' src='" + f.url_path + @"'" + (f.url_path == "/FundLogo/ADMF.JPG" ? "style='left:90px !important;'" : "") + @"/> -->
                         <!-- <img class='bg' src='/ICONPIC/profile-bg.png'/> -->            
                         </li>
                         </ul>";
                            }
                        
                    }
                }
                    count++;
            }
            conventional_equities.InnerHtml = userhtml;
        }

        private void BindSharie()
        {
            List<Fund> fs = new List<Fund>();
            fs = FundService.GetAllFunds().Where(c => c.Status == 1).ToList();

            List<bindFundDownloads> bfds = new List<bindFundDownloads>();
            bfds = bindFundDownloadService.bfd();

            string userhtml = "";
            int count = 1;

            foreach (Fund f in fs)
            {
                    if (f.DownloadType_Id == 8)
                    {
                        foreach (bindFundDownloads b in bfds)
                        {
                            if (f.fund_name == b.fundName)
                            {

                                Download phs = b.downloadFiles.Where(x => x.FileName == "Product Highlights Sheet").FirstOrDefault();
                                Download ffs = b.downloadFiles.Where(x => x.FileName == "Fund Fact Sheet").FirstOrDefault();
                                Download bro = b.downloadFiles.Where(x => x.FileName == "Brochure").FirstOrDefault();
                                Download ir = b.downloadFiles.Where(x => x.FileName == "Interim Report").FirstOrDefault();
                                Download ar = b.downloadFiles.Where(x => x.FileName == "Annual Report").FirstOrDefault();
                                Download im = b.downloadFiles.Where(x => x.FileName == "Information Memorandum").FirstOrDefault();

                            userhtml = userhtml + @"<ul class='tiles'>
                    <li style = 'background:" + f.backgroundColor + @"'>
                         <div class='info'>
                            <p class='font-light-blue'>" + f.fund_name + @"</p>
                                </div>
                                <div class='expandable-div-holder'>
                                    <div class='expandable-div-cover'>
                                        <div class='expandable-div-content'>
                                            <div class='expandable-div-bg bg-white'></div>
                                                <div class='profile-bio-holder'>
                                                    <div class='product-details'>
                                                        <a href = 'javascript:;' >
                                                        <img class='close-btn' src='/ICONPIC/close-btn-dark.png'/></a>
                                                        <img class='face'/>
                                                        <!-- <img class='face' src='" + f.url_path + @"'/> -->
                                                        <h4 class='font-black'>" + f.fund_name + @"</h4>
                                                        <div class='copy'>
                                                        <h5 class='font-light-blue'>Potential Investors</h5>
                                                        <p>" + f.potential_inves + @"</p>
                                                        <h5 class='font-light-blue'>Investment Strategy</h5>
                                                        <p>" + f.inves_strategy + @"</p>
                                                        <h5 class='font-light-blue'>EPF Approved Fund</h5>
                                                        <p>" + f.epfapproved + @"</p>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Launch Date</h5>
                                                                    <p>" + f.launch_date.ToString(@"dd MMMM yyyy") + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Trustee</h5>
                                                                    <p>" + f.trustee + @"</p>
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Fund Category/type</h5>
                                                                    <p>" + f.fund_category + @"</p>        
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Sales charge</h5>
                                                                    <p>" + f.sales_charge + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Management fee</h5>
                                                                    <p>" + f.manage_fee + @"</p>     
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Trustee fee</h5>
                                                                    <p>" + f.trustee_fee + @"</p>      
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr> 
                                                                <td width = '35%'>   
                                                                <h5 class='font-light-blue'>Minimum initial investment(MYR)</h5>
                                                                <p>" + f.min_ini_inves + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                <h5 class='font-light-blue'>Minimum Subsequent investment(MYR)</h5>
                                                                <p>" + f.min_sub_inv+ @"</p> 
                                                                </td>                                                                
                                                                <td width = '35%'> 
                                                                <h5 class='font-light-blue'>Redemption fee</h5>
                                                                <p>" + f.redemp_fee + @"</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                     </div>
                                                     <div class='narrow-copy'>
                                                        <h3> Interested to Invest?</h3>     
                                                        <br>
                                                        <a href ='/Contact.aspx' class='rounded-btn bg-light-blue'>Contact Us</a>
                                                     </div>
                                                </div>
                                                <div class='content bg-gray resources'>
                                                <div class='product-details'>
                                                <h4>Resources</h4>
                                                <ul>
                                                    <li>
                                                        <a href='" + (ar != null ? ar.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png'/>
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Annual Reports</p>
                                                                <p class='font-black'>(" + (ar == null ? "0" : ConvertBytesToMegaBytes(ar.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href='" + (ir != null ? ir.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Interim Reports</p>
                                                                <p class='font-black'>(" + (ir == null ? "0" : ConvertBytesToMegaBytes(ir.FileSize)) + @")</p>
                                                            </div>      
                                                        </a>
                                                    </li>        
                                                    <li>
                                                        <a href='" + (phs != null ? phs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Product Highlights Sheet</p>
                                                                <p class='font-black'>(" + (phs == null ? "0" : ConvertBytesToMegaBytes(phs.FileSize)) + @")</p>     
                                                            </div>        
                                                        </a>       
                                                    </li>       
                                                    <li>
                                                        <a  href='" + (ffs != null ? ffs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Fund Fact Sheet</p>
                                                                <p class='font-black'>(" + (ffs == null ? "0" : ConvertBytesToMegaBytes(ffs.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>    
                                                        <a href='" + (bro != null ? bro.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>Brochure</p>
                                                                <p class='font-black'>(" + (bro == null ? "0" : ConvertBytesToMegaBytes(bro.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                        <li>    
                                                        <a href='" + (im != null ? im.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>Information Memorandum</p>
                                                                <p class='font-black'>(" + (im == null ? "0" : ConvertBytesToMegaBytes(im.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class='face'/>
                        <!-- <img class='bg' src='/ICONPIC/profile-bg.png'/> -->            
                        </li>
                    </ul>";
                            }
                        }
                    }
                    count++;
            }
            shariah_equities.InnerHtml = userhtml;
        }

        private void Bindshariahmm()
        {
            List<Fund> fs = new List<Fund>();
            fs = FundService.GetAllFunds().Where(c => c.Status == 1).ToList();

            List<bindFundDownloads> bfds = new List<bindFundDownloads>();
            bfds = bindFundDownloadService.bfd();

            string userhtml = "";
            int count = 1;

            foreach (Fund f in fs)
            {
                    if (f.DownloadType_Id == 9)
                    {
                        foreach (bindFundDownloads b in bfds)
                        {
                            if (f.fund_name == b.fundName)
                            {

                                Download phs = b.downloadFiles.Where(x => x.FileName == "Product Highlights Sheet").FirstOrDefault();
                                Download ffs = b.downloadFiles.Where(x => x.FileName == "Fund Fact Sheet").FirstOrDefault();
                                Download bro = b.downloadFiles.Where(x => x.FileName == "Brochure").FirstOrDefault();
                                Download ir = b.downloadFiles.Where(x => x.FileName == "Interim Report").FirstOrDefault();
                                Download ar = b.downloadFiles.Where(x => x.FileName == "Annual Report").FirstOrDefault();
                                Download im = b.downloadFiles.Where(x => x.FileName == "Information Memorandum").FirstOrDefault();

                            userhtml = userhtml + @"<ul class='tiles'>
                    <li style = 'background:" + f.backgroundColor + @"'>
                         <div class='info'>
                            <p class='font-light-blue'>" + f.fund_name + @"</p>
                                </div>
                                <div class='expandable-div-holder'>
                                    <div class='expandable-div-cover'>
                                        <div class='expandable-div-content'>
                                            <div class='expandable-div-bg bg-white'></div>
                                                <div class='profile-bio-holder'>
                                                    <div class='product-details'>
                                                        <a href = 'javascript:;' >
                                                        <img class='close-btn' src='/ICONPIC/close-btn-dark.png'/></a>
                                                        <img class='face'/>
                                                        <h4 class='font-black'>" + f.fund_name + @"</h4>
                                                        <div class='copy'>
                                                        <h5 class='font-light-blue'>Potential Investors</h5>
                                                        <p>" + f.potential_inves + @"</p>
                                                        <h5 class='font-light-blue'>Investment Strategy</h5>
                                                        <p>" + f.inves_strategy + @"</p>
                                                        <h5 class='font-light-blue'>EPF Approved Fund</h5>
                                                        <p>" + f.epfapproved + @"</p>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Launch Date</h5>
                                                                    <p>" + f.launch_date.ToString(@"dd MMMM yyyy") + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Trustee</h5>
                                                                    <p>" + f.trustee + @"</p>
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Fund Category/type</h5>
                                                                    <p>" + f.fund_category + @"</p>        
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Sales charge</h5>
                                                                    <p>" + f.sales_charge + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                    <h5 class='font-light-blue'>Management fee</h5>
                                                                    <p>" + f.manage_fee + @"</p>     
                                                                </td>
                                                                <td width = '35%'>
                                                                    <h5 class='font-light-blue'>Trustee fee</h5>
                                                                    <p>" + f.trustee_fee + @"</p>      
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class='detail-line'>
                                                            <tr> 
                                                                <td width = '35%'>   
                                                                <h5 class='font-light-blue'>Minimum initial investment(MYR)</h5>
                                                                <p>" + f.min_ini_inves + @"</p>
                                                                </td>
                                                                <td width = '30%'>
                                                                <h5 class='font-light-blue'>Minimum Subsequent investment(MYR)</h5>
                                                                <p>" + f.min_sub_inv + @"</p> 
                                                                </td>                                                                
                                                                <td width = '35%'> 
                                                                <h5 class='font-light-blue'>Redemption fee</h5>
                                                                <p>" + f.redemp_fee + @"</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                     </div>
                                                     <div class='narrow-copy'>
                                                        <h3> Interested to Invest?</h3>     
                                                        <br>
                                                        <a href ='/Contact.aspx' class='rounded-btn bg-light-blue'>Contact Us</a>
                                                     </div>
                                                </div>
                                                <div class='content bg-gray resources'>
                                                <div class='product-details'>
                                                <h4>Resources</h4>
                                                <ul>
                                                    <li>
                                                        <a href='" + (ar != null ? ar.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png'/>
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Annual Reports</p>
                                                                <p class='font-black'>(" + (ar == null ? "0" : ConvertBytesToMegaBytes(ar.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href='" + (ir != null ? ir.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Interim Reports</p>
                                                                <p class='font-black'>(" +(ir == null ? "0" : ConvertBytesToMegaBytes(ir.FileSize))+ @")</p>
                                                            </div>      
                                                        </a>
                                                    </li>        
                                                    <li>
                                                        <a href='" + (phs != null ? phs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Product Highlights Sheet</p>
                                                                <p class='font-black'>(" + (phs == null ? "0" : ConvertBytesToMegaBytes(phs.FileSize)) + @")</p>     
                                                            </div>        
                                                        </a>       
                                                    </li>       
                                                    <li>
                                                        <a  href='" + (ffs != null ? ffs.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class='file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>
                                                                <p class='font-light-blue'>Fund Fact Sheet</p>
                                                                <p class='font-black'>(" + (ffs == null ? "0" : ConvertBytesToMegaBytes(ffs.FileSize)) + @")</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>    
                                                        <a href='" + (bro != null ? bro.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>Brochure</p>
                                                                <p class='font-black'>(" + (bro == null ? "0" : ConvertBytesToMegaBytes(bro.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                    <li>    
                                                        <a href='" + (im != null ? im.UrlPath : "/errorMessage.aspx") + @"' target='_blank'>
                                                            <img class=''file-icon' src='/ICONPIC/file-icon.png' />
                                                            <div class='details'>  
                                                                <p class='font-light-blue'>InFormation Memorandum</p>
                                                                <p class='font-black'>(" + (im == null ? "0" : ConvertBytesToMegaBytes(im.FileSize)) + @")</p>
                                                            </div>
                                                        </a>   
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class='face' />
                        <!-- <img class='bg' src='/ICONPIC/profile-bg.png'/> -->            
                        </li>
                    </ul>";
                            }
                        }
                    }
                    count++;
            }
            shariah_money_market.InnerHtml = userhtml;
        }

        public string ConvertBytesToMegaBytes(string bytes)
        {
            double size = (Convert.ToDouble(bytes) / 1024) / 1024;
            return string.Format("{0:0.00} MB", size);
        }
    }
}