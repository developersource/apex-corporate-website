﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.UI.HtmlControls;

namespace AdminApex
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get Name & Date
            
            List<DailyNavFundUtility> navf = new List<DailyNavFundUtility>();
            navf = DailyFundService.GetAllPrice();
            List<StoredFund> latest_date = new List<StoredFund>();
            latest_date = StoredFundData.GetLatestDate();
            List<StoredFund> latest_time = new List<StoredFund>();
            latest_time = StoredFundData.GetLatestTime();

            //Get Data from utmc
            List<StoredFund> raw_data = new List<StoredFund>();
            raw_data = StoredFundData.GetRAWData();

            //Get Data from stored fund
            List<StoredFund> stored_data = new List<StoredFund>();
            stored_data = StoredFundData.GetStoredData();

            //Get Annoucement from News 
            //List<News> annoucement = new List<News>();
            //annoucement = NewsService.NewsGetAll().Where(x => x.annoucementStatus == 1).ToList();
            //annoucement = annoucement.OrderByDescending(x => x.UploadDate).ToList();
            //var latestAnnoucement = annoucement.FirstOrDefault();

            //string html = "";
            //string desc = "";

            //if (latestAnnoucement != null) {
            //    desc = latestAnnoucement.annoucementDesc.Replace("\r\n", "<br />\r\n");
            //    html = @"<div class=" + "modal-container>" + @"
            //             <div><div class=" + "popup-annoucement-box>" + @"
            //             <h3>" + latestAnnoucement.title + @"</h3>
            //             <p>" + desc + @"</p>
            //             <a href=" + "resources/news.aspx?id=" + latestAnnoucement.id + ">More details" + @"</a></div>
            //             <div class=" + "popup-annoucement-box-copy>" + @"
            //             <a href = " + "javascript:; " + " class=" + " 'rounded-btn bg-gray font-black popup-close' >" + "Close" + @"</a></div></div></div>";
                         
            //    showAnnoucement.InnerHtml = html;
            // }

            //Variable assignation
            int i = 0;
            foreach (DailyNavFundUtility x in navf)
            {
                i++;
            }
            DateTime[] raw_date = new DateTime[i];
            DateTime[] stored_date = new DateTime[i];
            DateTime[] time = new DateTime[i];
            
            decimal[] raw_price = new decimal[i];
            decimal[] stored_price = new decimal[i];
            decimal[] stored_y_price = new decimal[i];
            decimal[] percentage = new decimal[i];
            string[] fund_name = new string[i];
            string[] text_color = new string[i];
            string date;
            string string_time;
            int[] img_decider = new int[i];
            
            i = 0;
            foreach (DailyNavFundUtility x in navf)
            {
                fund_name[i] = x.FundInfomationUtility.Fund_Name;
                i++;
            }
            i = 0;
            foreach (StoredFund x in raw_data)
            {
                raw_date[i] = x.NAVDate;
                raw_price[i] = x.UnitPrice;
                i++;
            }
            i = 0;
            foreach (StoredFund x in stored_data)
            {
                stored_date[i] = x.NAVDate;
                stored_price[i] = x.UnitPrice;
                img_decider[i] = x.ImgDecider;
                stored_y_price[i] = x.YUnitPrice;
                time[i] = x.NAVTime;
               
                i++;
            }
            i = 0;
            //Compare data with storred data
            foreach (StoredFund x in raw_data)
            {
                //Initialazation
                
                //Different Date
                if (raw_date[i] > stored_date[i])
                {
                    if (raw_price[i] == stored_price[i])
                    {
                        //Update Date
                        img_decider[i] = 2;
                        
                        stored_date[i] = raw_date[i];
                        stored_y_price[i] = stored_price[i];
                        stored_price[i] = raw_price[i];
                        
                    }
                    else
                    {
                        //Update Price and Date
                        img_decider[i] = getComparison(raw_price, stored_price, i);

                        stored_date[i] = raw_date[i];
                        stored_y_price[i] = stored_price[i];
                        stored_price[i] = raw_price[i];
                    }
                }
                //Same Date
                else if (raw_date[i] == stored_date[i])
                {
                    if (raw_price[i] == stored_price[i])
                    {
                        //Display
                    }
                    else
                    {
                        //Update Price
                        img_decider[i] = getComparison(raw_price, stored_y_price, i);
                        
                        stored_price[i] = raw_price[i]; 
                    }
                }
                if(stored_price[i] != 0 && stored_y_price[i] != 0)
                percentage[i] = Math.Round(((stored_price[i] - stored_y_price[i]) / stored_y_price[i]) * 100, 2);
                if (percentage[i] < 0)
                {
                    percentage[i] = percentage[i] * -1;
                }
                if (img_decider[i] == 0)
                {
                    text_color[i] = "<b style ='color: #00bf00' >";
                }
                else if (img_decider[i] == 1)
                {
                    text_color[i] = "<b style ='color: red' >";
                }
                else
                {
                    text_color[i] = "<b>";
                }
                i++;
            }
            i = 0;
            
            Display(raw_data, raw_price, img_decider, i, fund_name, percentage, text_color);
            date = (latest_date.FirstOrDefault().NAVDate.ToString("dd/MM/yyyy").Trim()).Substring(0, 10);
            string_time =(latest_time.FirstOrDefault().NAVTime.ToString("hh:mm:ss tt").Trim());
            if (string_time.Substring(0, 1) == "0")
            {
                string_time = string_time.Substring(1);
            }
            dateA.InnerText = "Updated on " + date + " ";
            i = 0;
            //Update data
            foreach (StoredFund x in stored_data)
            {
                x.NAVDate = stored_date[i];
                x.UnitPrice = stored_price[i];
                x.ImgDecider = img_decider[i];
                x.YUnitPrice = stored_y_price[i];
                
                StoredFundData.UpdatePrices(x);
                i++;
            }
        }

        public void imageChange(int i, string img)
        {
            if (i == 0)
                img1.Src = img;
            if (i == 1)
                img2.Src = img;
            if (i == 2)
                img3.Src = img;
            if (i == 3)
                img4.Src = img;
            if (i == 4)
                img5.Src = img;
            if (i == 5)
                img6.Src = img;
            if (i == 6)
                img7.Src = img;
            if (i == 7)
                img8.Src = img;
            if (i == 8)
                img9.Src = img;
        }

        public void spanChange(int i, string text)
        {
            if (i == 0)
                fund1.InnerHtml = text;
            if (i == 1)
                fund2.InnerHtml = text;
            if (i == 2)
                fund3.InnerHtml = text;
            if (i == 3)
                fund4.InnerHtml = text;
            if (i == 4)
                fund5.InnerHtml = text;
            if (i == 5)
                fund6.InnerHtml = text;
            if (i == 6)
                fund7.InnerHtml = text;
            if (i == 7)
                fund8.InnerHtml = text;
            if (i == 8)
                fund9.InnerHtml = text;
        }

        public void Display(List<StoredFund> rd, decimal[] rp, int[] imgd, int i, string[] fn, decimal[] per, string[] txtc)
        {
            foreach (StoredFund x in rd)
            {
                switch (imgd[i])
                {
                    case 0:
                        imageChange(i, "Picture/upp.png");
                        break;
                    case 1:
                        imageChange(i, "Picture/down.png");
                        break;
                    case 2:
                        imageChange(i, "Picture/sameee.png");
                        break;
                }
                spanChange(i, (" &#160 | &#160 " + fn[i] + "  &#160 " + rp[i].ToString() + " &#160 " + txtc[i] + per[i].ToString() + "% &#160 " + "</b>"));
                
                i++;
            }

        }

        public int getComparison(decimal[] rp, decimal[] sp, int i)
        {
             int imgd = 0;
            if (rp[i] > sp[i])
            {
                imgd = 0;
            }
            else if (rp[i] < sp[i])
            {
                imgd = 1;
            }
            else
            {
                imgd = 2;
            }
            return (imgd);
        }
    }
}