﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class UserLog
    {
        public Int32 Id { get; set; }
        public string Title { get; set; }
        public string UrlPath { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
    }
}