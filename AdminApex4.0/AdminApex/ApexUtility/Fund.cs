﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class Fund
    {
        public Int32 ID { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
        public string fund_name { get; set; }
        public string fund_code { get; set; }
        public string potential_inves { get; set; }
        public string inves_strategy { get; set; }
        public DateTime launch_date { get; set; }
        public string trustee { get; set; }
        public string fund_category { get; set; }
        public string sales_charge { get; set; }
        public string manage_fee { get; set; }
        public string trustee_fee { get; set; }
        public string min_ini_inves { get; set; }
        public string redemp_fee { get; set; }
        public List<Download> Download { get; set; }
        public int DownloadType_Id { get; set; }
        public DownloadType DownloadType { get; set; }
        public string url_path { get; set; }
        public string min_sub_inv { get; set; }
        public string epfapproved { get; set; }
        public string backgroundColor { get; set; }
    }

    public class ddlFund
    {
        public string ipd_fund_code { get; set; }
        public string fund_name { get; set; }
    }

    public class customHistoricalNav
    {
        public Int32 fundId { get; set; }
        public string fundCode { get; set; }
        public string fundName { get; set; }
        public DateTime navDate { get; set; }
        public Decimal navPrice { get; set; }
    }
}