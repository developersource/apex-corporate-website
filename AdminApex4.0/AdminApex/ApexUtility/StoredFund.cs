﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class StoredFund
    {
        public string IPDFundCode { get; set; }
        public DateTime NAVDate { get; set; }
        public Decimal UnitPrice { get; set; }
        public int ImgDecider { get; set; }
        public Decimal YUnitPrice { get; set; }
        public DateTime NAVTime { get; set; }
    }
}