﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class Download
    {
        public int Id { get; set; }
        public DateTime UploadDate { get; set; }
        public int CreatedBy { get; set; }
        public int DownloadType_Id { get; set; }
        public DownloadType DownloadType { get; set; }
        public int Fund_Id { get; set; }
        public Fund Fund { get; set; }
        public string FileName { get; set; }
        public string FileAuthor { get; set; }
        public string UrlPath { get; set; }
        public string FileSize { get; set; }
        public DateTime DisplayDate { get; set; }
        public int Status { get; set; }
    }
}