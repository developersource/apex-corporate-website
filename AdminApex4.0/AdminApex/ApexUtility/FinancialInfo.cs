﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class FinancialInfo
    {
        public Int32 ID { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
        public Int32 year { get; set; }
        public Decimal issue_paidup_capital { get; set; }
        public Decimal shareholder_fund { get; set; }
        public Decimal revenue { get; set; }
        public string profit_loss_before_tax { get; set; }
        public string profit_loss_after_tax { get; set; }
        public Decimal cash_bank_deposits { get; set; }
        public string bank_borrowings { get; set; }
    }
}