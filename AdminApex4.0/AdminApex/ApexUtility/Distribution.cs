﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class Distribution
    {
        public Int32 Id { get; set; }
        public int Fund_Id { get; set; }
        public Fund Fund { get; set; }
        public DateTime EntitlementDate { get; set; }
        public decimal Gross { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
    }
}