﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class DownloadType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public int Status { get; set; }
        public int IsFund { get; set; }
        public List<Download> Download { get; set; }
        public List<Fund> Fund { get; set; }
        //public List<Achives> Achives { get; set; }
    }
}