﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class Admin
    {
        public Int32 Id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string Password { get; set; }        
        public string Department { get; set; }
        public string Email { get; set; }
        public Int32 Status { get; set; }
        public Int32 locked { get; set; }
    }
}