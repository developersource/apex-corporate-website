﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class DailyNAVFund
    {
        public string FundCode { get; set; }
        public DateTime DailyNavDate { get; set; }
        public Decimal DailyUnitPrice { get; set; }
    }
}