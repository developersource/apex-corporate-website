﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class News
    {
        public Int32 id { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime UploadDate { get; set; }
        public Int32 Status { get; set; }
        public string UrlPath { get; set; }
        public string content { get; set; }
        public string title { get; set; }

        //For future announcement upgrade
        //public Int32 annoucementStatus { get; set; }
        //public string annoucementDesc { get; set; }

    }
}