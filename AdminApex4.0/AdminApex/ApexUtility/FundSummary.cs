﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class FundSummary
    {
        public Int32 ID { get; set; }
        public string fund_categ { get; set; }
        public Int32 no_of_fund { get; set; }
        public Decimal total_value { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
    }
}