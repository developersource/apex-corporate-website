﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class StoredUnitPrice
    {
        public string IPD_Fund_Code { get; set; }
        public string Fund_Name { get; set; }
        public DateTime NAV_Date { get; set; }
        public Decimal Unit_Price { get; set; }
    }
}