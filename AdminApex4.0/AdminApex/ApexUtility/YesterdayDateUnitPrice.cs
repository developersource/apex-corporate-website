﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class YesterdayDateUnitPrice
    {
        public string IPD { get; set; }
        public DateTime CompareDate { get; set; }
        public Decimal CompareUnitPrice { get; set; }
    }
}