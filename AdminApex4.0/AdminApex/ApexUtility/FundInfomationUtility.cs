﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class FundInfomationUtility
    {
        public Int32 id { get; set; }
        public string EPF_IPD_Code { get; set; }
        public string IPD_Fund_Code { get; set; }
        public string Fund_Code { get; set; }
        public string Fund_Name { get; set; }
        public DateTime Effective_Date { get; set; }
        public string LIPPER_Category_Of_Fund { get; set; }
        public string Conventional { get; set; }
        public string Status { get; set; }
        public string Foreign_Fund { get; set; }
        public DateTime Report_Date { get; set; }
        public string Fund_Base_Currency { get; set; }
        public Int32 IS_EMIS { get; set; }
        public string FUND_CLS { get; set; }
        public Int32 IS_RETAIL { get; set; }
    }
}