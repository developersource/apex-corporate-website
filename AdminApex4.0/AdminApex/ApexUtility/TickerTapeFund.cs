﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class TickerTapeFund
    {
        public Int32 id { get; set; }
        public string Fund_Name { get; set; }
        public Decimal Daily_Unit_Price { get; set; }
        public DateTime Daily_Date { get; set; }
        public Int32 IsPublicHoliday { get; set; }
        
    }
}