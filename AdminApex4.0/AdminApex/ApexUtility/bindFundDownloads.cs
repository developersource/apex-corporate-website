﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class bindFundDownloads
    {
        public int fundid { get; set; }
        public string fundName { get; set; }
        public List<Download> downloadFiles { get; set; }
    }
}