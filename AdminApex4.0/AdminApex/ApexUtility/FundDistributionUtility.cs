﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class FundDistributionUtility
    {
        public Int32 id { get; set; }
        public FundInfomationUtility FundInfomationUtility { get; set; }
        public string EPF_IPD_Code { get; set; }
        public string IPD_Fund_Code { get; set; }
        public DateTime Corporate_Action_Date { get; set; }
        public Decimal Distributions { get; set; }
        public Decimal Unit_Splits { get; set; }
        public DateTime Report_Date { get; set; }
        public Decimal Net_Distribution { get; set; }
    }
}