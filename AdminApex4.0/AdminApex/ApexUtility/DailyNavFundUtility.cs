﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class DailyNavFundUtility
    {
        public Int32 id { get; set; }
        public Fund Fund { get; set; }
        public FundInfomationUtility FundInfomationUtility { get; set; }
        public string EPF_IPD_Code { get; set; }
        public string IPD_Fund_Code { get; set; }
        public DateTime Daily_NAV_Date { get; set; }
        public DateTime Daily_NAV_Time { get; set; }
        public Decimal Daily_NAV { get; set; }
        public DateTime Report_Date { get; set; }
        public Decimal Daily_Unit_Created { get; set; }
        public Decimal Daily_NAV_EPF { get; set; }
        public Decimal Daily_Unit_Created_EPF { get; set; }
        public Decimal Daily_Unit_Price { get; set; }
        public Decimal Daily_Unit_Created_EPF_Adj { get; set; }
    }
}