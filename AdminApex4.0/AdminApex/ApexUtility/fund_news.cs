﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class fund_news
    {
        public Int32 fund_news_news_id { get; set; }
        public Int32 fund_news_fund_id { get; set; }
        public Int32 ID { get; set; }
        public Fund fund { get; set; }
    }
}