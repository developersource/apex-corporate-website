﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexUtility
{
    public class UnitSplit
    {
        public Int32 Id { get; set; }
        public int Fund_Id { get; set; }
        public Fund Fund { get; set; }
        public DateTime ExDate { get; set; }
        public string SplitRatio { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
        public FundInfomationUtility FundInfomationUtility { get; set; }
    }
}