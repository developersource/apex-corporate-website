﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewFund : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindFunds();
        }

        public void BindFunds()
        {
            List<Fund> fs = new List<Fund>();

            fs = FundService.GetAllFund();

            string userhtml = "";
            int count = 1;
            int flag = 1;

            foreach (Fund f in fs)
            {
                if (flag != 1)
                {
                    userhtml = userhtml + @"<tr data-id='" + f.ID + @"'>
                                            <td  style='text-align:center'>" + count + @"</td>
											<td>" + f.fund_name + @"</td>
											<td style='text-align: center;'>
                                                " + (f.Status == 1 ?
                                                 "<i class='fa fa-circle  txt-success' style='color:#64DF6F' title='Activate'></i>"
                                                 :
                                                 "<i class='fa fa-circle  txt-danger' style='color:red' title='Suspended'></i>"
                                                 ) + @"
                                            </td>
                                            <td style='text-align: center;'>
                                                " +
                                                  "<a href='EditFund.aspx?id=" + f.ID + @"' class='btn-sm btn-info' data-toggle='tooltip' title='edit'><i class='fa fa-edit'> Edit</i></a>"
                                                + @"
                                            </td>
                                            <td style='text-align: center;'>" + "<a href='ViewResource.aspx' class='btn-sm btn-info'  title='view'><i class='fa fa-file'> Resources</i></a>" + @"</td>
										</tr>";

                    count++;
                }
                flag++;
            }
            tableFund.InnerHtml = userhtml;

        }

    }
}