﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AddDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<DownloadType> downloadTypeList = DownloadTypeService.GetAllDownloadType();
                ddlFileType.Items.Clear();
                //ddlFileType.Items.Add(new ListItem("Please select an item", "0"));
                foreach (DownloadType downloadType in downloadTypeList)
                {
                    ddlFileType.Items.Add(new ListItem(downloadType.TypeName, downloadType.Id.ToString()));
                }
            }
        }

        protected void lblSubmit_Click(object sender, EventArgs e)
        {
            int val = 1;
            //1 can run, 0 gt error

            try
            {
                if (!txtFileName.Text.Equals(""))
                {
                    if (!(ddlFileType.SelectedValue == "0"))
                    {
                        if (fuFileType.HasFile)
                        {
                            Admin admin = (Admin)Session["admin"];
                            List<Download> dl = DownloadService.GetAllDownload();
                           
                            //update path
                            string dlType = ddlFileType.SelectedItem.Text;
                            string fileName = "";

                            if (txtFileName.Text != "-")
                                fileName = txtFileName.Text;
                            else
                            {
                                if (ddlFileName.SelectedItem.ToString() != "Please select an item")
                                {
                                    fileName = ddlFileName.SelectedItem.ToString();
                                    val = 1;
                                }
                                else
                                    val = 0;
                            }

                            if (val == 1)
                            {

                                int downloadTypeId = Convert.ToInt32(ddlFileType.SelectedValue);
                                string fileSize = Convert.ToString(fuFileType.PostedFile.ContentLength);
                                string extension = Path.GetExtension(fuFileType.FileName);
                                string url = "/Download/" + dlType;
                                string path = Server.MapPath(url);
                                path = path + "/" + ddlFileType.SelectedItem.Text + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                                //fuFileType.SaveAs(path);

                                //update db
                                Download download = new Download();
                                download.UploadDate = DateTime.Now;
                                download.CreatedBy = admin.Id;
                                download.DownloadType_Id = downloadTypeId;
                                if (hdntype.Value == "1")
                                {
                                    if (rblFund.SelectedIndex != -1)
                                    {
                                        download.Fund_Id = Convert.ToInt32(rblFund.SelectedItem.Value.ToString());
                                        val = 1;
                                    }
                                    else
                                        val = 0;
                                }
                                else
                                {
                                    download.Fund_Id = 1;
                                    val = 1;
                                }
                                if (val == 1)
                                {

                                    fuFileType.SaveAs(path);
                                    download.FileName = fileName;
                                    if (hdntype.Value == "2")
                                        if (!(txtFileAuthor.Text.Equals("")))
                                        {
                                            if (!txtDate.Text.Equals(""))
                                            {
                                                //if text date is empty
                                                string fileAuthor = txtFileAuthor.Text;
                                                DateTime displayDate = Convert.ToDateTime(txtDate.Text);

                                                download.FileAuthor = fileAuthor;
                                                download.UrlPath = url + "/" + ddlFileType.SelectedItem.Text + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                                                download.FileSize = fileSize;
                                                download.DisplayDate = displayDate;
                                                download.Status = 1;
                                                DownloadService.Insert(download);

                                                //user log   
                                                WriteTextFile wtf = new WriteTextFile();
                                                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                                                string[] data = { DateTime.Now.ToString(), admin.name, "Add New Download - " + download.FileName, "Success" };
                                                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                                                //Response.Redirect(Request.RawUrl);
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter date!')", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter file author!')", true);
                                        }

                                    else
                                    {
                                        download.FileAuthor = "-";
                                        download.UrlPath = url + "/" + ddlFileType.SelectedItem.Text + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                                        download.FileSize = fileSize;
                                        download.DisplayDate = DateTime.Now;
                                        download.Status = 1;
                                        DownloadService.Insert(download);

                                        //user log   
                                        WriteTextFile wtf = new WriteTextFile();
                                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                                        string[] data = { DateTime.Now.ToString(), admin.name, "Add New Download - " + download.FileName, "Success" };
                                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);

                                    }
                                }
                                else
                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select fund!')", true);
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select a file name!')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select a file to upload!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select file type!')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill in file name!')", true);
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rblFund.Items.Clear();
            ddlFileName.Items.Clear();
            txtFileAuthor.Visible = false;
            lblFileAuthor.Visible = false;
            txtDate.Visible = false;
            lblDate.Visible = false;
            ddlFileName.Visible = false;
            txtFileName.Visible = true;
            txtFileName.Text = "";
            DownloadType dlList = DownloadTypeService.GetAllDownloadType().Where(x => x.Id == Convert.ToInt32(ddlFileType.SelectedValue)).FirstOrDefault();

            //hiddentype = 0 no fund, 1 fund, 2 author      
            if (dlList != null)
            {
                if (dlList.IsFund == 1)
                {
                    BindFund();
                    hdntype.Value = "1";
                }
                else if (dlList.TypeName.Equals("Podcast") || dlList.TypeName.Equals("Articles"))
                {
                    hdntype.Value = "2";
                    txtFileAuthor.Visible = true;
                    lblFileAuthor.Visible = true;
                    txtDate.Visible = true;
                    lblDate.Visible = true;
                }
                else
                {
                    hdntype.Value = "0";
                }
            }
        }

        public void BindFund()
        {
            int downloadTypeId = Convert.ToInt32(ddlFileType.SelectedValue);
            List<Fund> fundList = FundService.GetAllFund();

            if (downloadTypeId == 7)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 7 && x.Status == 1).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
                txtFileName.Visible = false;
                ddlFileName.Visible = true;
                ddlFileName.Items.Add("Please select an item");
                ddlFileName.Items.Add("Brochure");
                ddlFileName.Items.Add("Product Highlights Sheet");
                ddlFileName.Items.Add("Fund Fact Sheet");
                ddlFileName.Items.Add("Interim Report");
                ddlFileName.Items.Add("Annual Report");
                ddlFileName.Items.Add("Information Memorandum");
                txtFileName.Text = "-";
            }
            else if (downloadTypeId == 8)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 8 && x.Status == 1).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
                txtFileName.Visible = false;
                ddlFileName.Visible = true;
                ddlFileName.Items.Add("Please select an item");
                ddlFileName.Items.Add("Brochure");
                ddlFileName.Items.Add("Product Highlights Sheet");
                ddlFileName.Items.Add("Fund Fact Sheet");
                ddlFileName.Items.Add("Interim Report");
                ddlFileName.Items.Add("Annual Report");
                ddlFileName.Items.Add("Information Memorandum");
                txtFileName.Text = "-";
            }
            else if (downloadTypeId == 9)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 9 && x.Status == 1).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
                txtFileName.Visible = false;
                ddlFileName.Visible = true;
                ddlFileName.Items.Add("Please select an item");
                ddlFileName.Items.Add("Brochure");
                ddlFileName.Items.Add("Product Highlights Sheet");
                ddlFileName.Items.Add("Fund Fact Sheet");
                ddlFileName.Items.Add("Interim Report");
                ddlFileName.Items.Add("Annual Report");
                ddlFileName.Items.Add("Information Memorandum");
                txtFileName.Text = "-";
            }
            rblFund.Visible = true;
        }
    }
}