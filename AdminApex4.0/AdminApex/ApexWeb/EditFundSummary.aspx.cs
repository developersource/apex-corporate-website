﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditFundSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = Request.QueryString["id"];

                List<FundSummary> fiList = new List<FundSummary>();
                FundSummary fs = FundSummaryService.FsGetById(id);

                ddlfunc.SelectedValue = fs.fund_categ;
                txt_nooffund.Text = Convert.ToString(fs.no_of_fund);
                txt_totalfund.Text = (fs.total_value).ToString("n0");
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                string id = Request.QueryString["id"];

                List<FundSummary> fiList = new List<FundSummary>();
                FundSummary fs = FundSummaryService.FsGetById(id);

                string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                DateTime currenttime = DateTime.Parse(time);

                fs.CreatedBy = admin.Id;
                fs.CreatedDate = currenttime;
                fs.fund_categ = ddlfunc.SelectedValue;
                fs.no_of_fund = Convert.ToInt32(txt_nooffund.Text);
                fs.total_value = Convert.ToDecimal(txt_totalfund.Text);
                fs = FundSummaryService.Update(fs,id);

                //user log   
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Edit Financial Info - " + fs.fund_categ, "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                Response.Write("<script>window.alert('The Fund Summary was successfully updated !');</script>");
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}