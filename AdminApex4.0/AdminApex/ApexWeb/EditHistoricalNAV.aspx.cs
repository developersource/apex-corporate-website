﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static AdminApex.ApexData.utmcData;
using AdminApex.ApexService;

namespace AdminApex.ApexWeb
{
    public partial class EditHistoricalNAV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                //load ddl fund list
                getFunds();
                ddlFundType.SelectedIndex = 0;
            }
            else
            {

                //populate table data
            }
            if(Session["admin"] != null)
            {
                
                //Query to get data
                StringBuilder mainQuery = new StringBuilder();
                mainQuery.Append(@"SELECT dnf.ID as fundId, dnf.IPD_FUND_CODE as fundCode, ufi.FUND_NAME as fundName, dnf.Daily_NAV_Date as navDate, dnf.Daily_Unit_Price as navPrice 
                                    from utmc_daily_nav_fund dnf
                                    join utmc_fund_information ufi on dnf.IPD_FUND_CODE = ufi.IPD_Fund_Code 
                                    where 1=1 ");
                StringBuilder queryFilter = new StringBuilder();

                if (ddlFundType.SelectedIndex != 0)
                {
                    queryFilter.Append(@" and dnf.IPD_FUND_CODE = '" +ddlFundType.SelectedValue+"' ");
                }
                else
                {
                    queryFilter.Append(@" and dnf.IPD_FUND_CODE in ('01','02','03','04','05','06','07','08','09') ");
                }

                if(!String.IsNullOrEmpty(fromDate.Text.ToString()))
                {
                    queryFilter.Append(@"and dnf.Daily_NAV_Date >='" + fromDate.Text.ToString()+"' ");

                }

                if (!String.IsNullOrEmpty(toDate.Text.ToString()))
                {
                    queryFilter.Append(@"and dnf.Daily_NAV_Date <='" + toDate.Text.ToString() + "' ");
                }

                queryFilter.Append(@"Order By dnf.id desc, dnf.daily_nav_date desc Limit 0,100");

                Response responseGetFundList = utmcData.GetDataByQuery(mainQuery.ToString() + queryFilter.ToString(),true);
                if(responseGetFundList.IsSuccess){
                    var FundListDyn = responseGetFundList.Data;
                    var JsonData = JsonConvert.SerializeObject(FundListDyn);
                    List<customHistoricalNav> fundList = JsonConvert.DeserializeObject<List<customHistoricalNav>>(JsonData);

                    //Create Table here
                    StringBuilder sb = new StringBuilder();
                    int index = 1;
                    fundList.ForEach(x => {
                        sb.Append(@"<tr>
                                    <th><input type='checkbox'name='check[]' runat='server' value='"+x.fundId+"'/>"+index+@"</th>
                                    <th>"+x.fundName+@"</th>
                                    <th>"+x.navDate.ToString("dd/MM/yyyy")+@"</th>
                                    <th>"+x.navPrice+@"</th>
                                </tr>");
                        index++;
                    });
                    tablePrice1.InnerHtml = sb.ToString();
                    
                }
                else
                {

                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Session Timeout, Please Login to again to Proceed.');window.location.href='Login.aspx';", true);
            }
        }

        public void getFunds() {
            //Load fund ddl
            String queryGetFund = "select IPD_Fund_Code, FUND_NAME from utmc_fund_information;";
            Response responseGetFund = utmcData.GetDataByQuery(queryGetFund, true);
            if (responseGetFund.IsSuccess)
            {
                var FundDyn = responseGetFund.Data;
                var JsonData = JsonConvert.SerializeObject(FundDyn);
                List<ddlFund> fundList = JsonConvert.DeserializeObject<List<ddlFund>>(JsonData);

                ddlFundType.Items.Add(new ListItem("Select", "0"));
                fundList.ForEach(x =>
                {

                    ddlFundType.Items.Add(new ListItem(x.fund_name, x.ipd_fund_code));
                });                 
            }
            else
            {
                //error msg here
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(string fundId, double newNavPrice)
        {
            Admin loginUser = (Admin)HttpContext.Current.Session["admin"];
            Response response = new Response();
            if(newNavPrice == 0)
            {
                response.IsSuccess = false;
                response.Message = "Please enter a valid NAV price";
                return response;
            }

            try
            {
                //Get Old Price
                string queryGetDnf = @"SELECT dnf.ID as fundId, dnf.IPD_FUND_CODE as fundCode, ufi.FUND_NAME as fundName, dnf.Daily_NAV_Date as navDate, dnf.Daily_Unit_Price as navPrice 
                                    from utmc_daily_nav_fund dnf
                                    join utmc_fund_information ufi on dnf.IPD_FUND_CODE = ufi.IPD_Fund_Code where dnf.ID = " +fundId;

                Response responseGetDnf = utmcData.GetDataByQuery(queryGetDnf, true);
                if (responseGetDnf.IsSuccess)
                {
                    var FundListDyn = responseGetDnf.Data;
                    var JsonData = JsonConvert.SerializeObject(FundListDyn);
                    List<customHistoricalNav> fundList = JsonConvert.DeserializeObject<List<customHistoricalNav>>(JsonData);
                    customHistoricalNav singlednf = fundList.FirstOrDefault();

                    //New Price
                    Response responseGet = utmcData.UpdateSingleHistoricalNav(fundId, newNavPrice);
                    if (responseGet.IsSuccess)
                    {

                        //user log
                        string path = System.Web.HttpContext.Current.Server.MapPath("/Log/");
                        //Server.MapPath("/Log/");
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), loginUser.name, "Edit Historical NAV - Selected Fund(" + singlednf.fundName + ") Initial price(" +singlednf.navPrice+") New Price("+newNavPrice+")", "Success" };
                        wtf.Write(System.Web.HttpContext.Current.Server.MapPath(userLog.UrlPath), data);
                    }
                    else {
                        response.IsSuccess = false;
                        response.Message = "Failed to Edit Historical NAV Price";
                        return response;
                    }

                    response.IsSuccess = true;
                    response.Message = "Successfuly updated NAV price";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Selected Item not found in database";
                    return response;
                }

                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}