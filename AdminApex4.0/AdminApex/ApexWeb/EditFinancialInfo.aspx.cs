﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditFinancialInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = Request.QueryString["id"];

                List<FinancialInfo> fiList = new List<FinancialInfo>();
                FinancialInfo fi = FinancialInfoService.FiGetById(id);

                ddlyear.SelectedValue = Convert.ToString(fi.year);
                txt_ipcap.Text = Convert.ToString(fi.issue_paidup_capital);
                txt_shareholder.Text = Convert.ToString(fi.shareholder_fund);
                txt_revenue.Text = Convert.ToString(fi.revenue);
                txt_plbeforetax.Text = fi.profit_loss_before_tax;
                txt_plaftertax.Text = fi.profit_loss_after_tax;
                txt_cbdeposits.Text = Convert.ToString(fi.cash_bank_deposits);
                txt_bankborrow.Text = Convert.ToString(fi.bank_borrowings);
            }

        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                string id = Request.QueryString["id"];

                List<FinancialInfo> filist = new List<FinancialInfo>();
                FinancialInfo fi = FinancialInfoService.FiGetById(id);

                string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                DateTime currenttime = DateTime.Parse(time);

                fi.CreatedBy = admin.Id;
                fi.CreatedDate = currenttime;
                fi.year = Convert.ToInt32(ddlyear.SelectedValue);
                fi.issue_paidup_capital = Convert.ToDecimal(txt_ipcap.Text);
                fi.shareholder_fund = Convert.ToDecimal(txt_shareholder.Text);
                fi.revenue = Convert.ToDecimal(txt_revenue.Text);
                fi.profit_loss_before_tax = txt_plbeforetax.Text;
                fi.profit_loss_after_tax = txt_plaftertax.Text;
                fi.cash_bank_deposits = Convert.ToDecimal(txt_cbdeposits.Text);
                fi.bank_borrowings = txt_bankborrow.Text;
                fi = FinancialInfoService.Update(fi, id);

                //user log   
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Edit Financial Info - " + fi.year, "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                Response.Write("<script>window.alert('The Key Financial Info was successfully updated !');</script>");
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}