﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditDistribution : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);

                List<Fund> fundList = FundService.GetAllFund().Where(x => x.Status == 1).ToList();
                foreach (Fund fund in fundList)
                {
                    ddlFund.Items.Add(new ListItem(fund.fund_name, fund.ID.ToString()));
                }

                //set default value
                Distribution distribution = DistributionService.GetById(id);

                ddlFund.SelectedValue = distribution.Fund_Id.ToString();
                txtDate.Text = distribution.EntitlementDate.ToString("yyyy-MM-dd");
                txtGrossDistribution.Text = distribution.Gross.ToString();
            }
        }

        protected void lblSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Admin admin = (Admin)Session["admin"];
                int id = Convert.ToInt32(Request.QueryString["id"]);

                if (!txtDate.Text.Equals(""))
                {
                    if (!txtGrossDistribution.Text.Equals(""))
                    {
                        //insert db
                        int fundId = Convert.ToInt32(ddlFund.SelectedItem.Value.ToString());
                        DateTime entitlementDate = Convert.ToDateTime(txtDate.Text);
                        decimal gross = Convert.ToDecimal(txtGrossDistribution.Text);

                        Distribution distribution = DistributionService.GetById(id);
                        distribution.Fund_Id = fundId;
                        distribution.EntitlementDate = entitlementDate;
                        distribution.Gross = gross;
                        DistributionService.Update(distribution);

                        //user log   
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Edit Fund Distribution - " + FundService.FundGetById(distribution.Fund_Id).fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                        //Response.Redirect(Request.RawUrl);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter gross distribution!')", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select date!')", true);
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewFundInformation.aspx");

        }
    }
}