﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewFinancialInfo.aspx.cs" Inherits="AdminApex.ApexWeb.ViewFinancialInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--      <div class="kfi container-fluid mt-30 mb-30">
         <h2>Key Financial Infomation</h2>
        <div class="row">
            <div class="col">
                <table id="dataTable" class="table table-hover display pb-30">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Paid-Up Capital</th>
                            <th>Sharehoder's Fund</th>
                            <th>Revenue</th>
                            <th>Before-Tax</th>
                            <th>After-Tax</th>
                            <th style="text-align: center">Publish</th>
                            <th style="text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tableFund" runat="server" clientidmode="static">
                        <tr>
                            <th>ID</th>
                            <th>Fund Name</th>
                            <th>Status</th>
                            <th>Modify</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>--%>

    <div class="kfi container-fluid mt-30 mb-30">
        <h2>Financial Info</h2>
        <ul class="nav nav-tabs">
            <asp:HiddenField ID="hidTAB" runat="server" />
            <li id="kfiTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#kfim">Key Financial Information</a></li>
            <li id="fsTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#fs">Fund Summary</a></li>
        </ul>

        <div class="tab-content table-responsive">
            <div id="kfim" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover  table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Paid-Up Capital</th>
                            <th>Sharehoder's Fund</th>
                            <th>Revenue</th>
                            <th>Before-Tax</th>
                            <th>After-Tax</th>
                            <th style="text-align: center">Publish</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tablekfi" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID1" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction1" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction1" runat="server" OnClick="btnAction1_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />

            </div>
            <div id="fs" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Fund</th>
                            <th style="text-align: center">No.of Fund</th>
                            <th style="text-align: center">Total Value</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tablefs" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID2" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction2" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction2" runat="server" OnClick="btnAction2_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="../Content/js/dataTables-data.js"></script>
    <script>
        $(document).ready(function () {
            $('#tablekfi a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID1').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction1').val(title);
                $('#btnAction1').click();
            });
            $('#tablefs a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID2').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction2').val(title);
                $('#btnAction2').click();
            });
        });
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to delete your data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
