﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                conventionalTab.Attributes.Add("class", "active");
                ce.Attributes.Remove("class");
                ce.Attributes.Add("class", "tab-pane fade active in");

                ddlFund1.Items.Clear();
                ddlFund2.Items.Clear();
                ddlFund3.Items.Clear();

                ddlFund1.Items.Add(new ListItem("Select Fund Name - ", "0"));
                ddlFund2.Items.Add(new ListItem("Select Fund Name - ", "0"));
                ddlFund3.Items.Add(new ListItem("Select Fund Name - ", "0"));

                List<Fund> funds = FundService.GetAllFund();
                List<Fund> CE = funds.Where(x => x.DownloadType_Id == 7 && x.Status == 1).ToList();
                foreach (Fund ce in CE)
                {
                    ddlFund1.Items.Add(new ListItem(ce.fund_name, ce.ID.ToString()));
                }


                List<Fund> SE = funds.Where(x => x.DownloadType_Id == 8 && x.Status == 1).ToList();
                foreach (Fund se in SE)
                {
                    ddlFund2.Items.Add(new ListItem(se.fund_name, se.ID.ToString()));
                }


                List<Fund> SMM = funds.Where(x => x.DownloadType_Id == 9 && x.Status == 1).ToList();
                foreach (Fund smm in SMM)
                {
                    ddlFund3.Items.Add(new ListItem(smm.fund_name, smm.ID.ToString()));
                }

                if (Request.QueryString["tab"] != null)
                {
                    int tab = Convert.ToInt32(Request.QueryString["tab"]);

                    CleanTab();
                    CleanTabContent();

                    if (tab == 1)
                    {
                        conventionalTab.Attributes.Add("class", "active");
                        ce.Attributes.Add("class", "tab-pane fade in active");
                        se.Attributes.Add("class", "tab-pane fade");
                        smm.Attributes.Add("class", "tab-pane fade");

                    }
                    else if(tab == 2)
                    {
                        shariahEqTab.Attributes.Add("class", "active");
                        ce.Attributes.Add("class", "tab-pane fade");
                        se.Attributes.Add("class", "tab-pane fade in active");
                        smm.Attributes.Add("class", "tab-pane fade");
                    }
                    else if(tab == 3)
                    {
                        shariahMMTab.Attributes.Add("class", "active");
                        ce.Attributes.Add("class", "tab-pane fade");
                        se.Attributes.Add("class", "tab-pane fade");
                        smm.Attributes.Add("class", "tab-pane fade in active");
                    }
                }
            }
        }

        private void CleanTab()
        {
            conventionalTab.Attributes.Clear();
            shariahEqTab.Attributes.Clear();
            shariahMMTab.Attributes.Clear();
        }

        private void CleanTabContent()
        {
            ce.Attributes.Clear();
            se.Attributes.Clear();
            smm.Attributes.Clear();
        }


        public string ConvertBytesToMegaBytes(string bytes)
        {
            double size = (Convert.ToDouble(bytes) / 1024) / 1024;
            return string.Format("{0:0.00} MB", size);
        }

        public void Clear()
        {
            ddlFund1.ClearSelection();
            ddlFund2.ClearSelection();
            ddlFund3.ClearSelection();
            ddlFund1.Items.Clear();
            ddlFund2.Items.Clear();
            ddlFund3.Items.Clear();
        }

        protected void ddlFund1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetTabs();
            conventionalTab.Attributes.Add("class", "active");
            ce.Attributes.Remove("class");
            ce.Attributes.Add("class", "tab-pane fade active in");
            Bind_CE();
        }

        public void Bind_CE()
        {
            int count = 1;
            string innerhtml = "";
            int fundid = Convert.ToInt32(ddlFund1.SelectedValue);
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 7 && x.Fund_Id == fundid && x.Status == 1).ToList();
            if (downloadList.Count() != 0)
            {
                foreach (Download dl in downloadList)
                {
                    innerhtml += @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                            <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                            <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
                                            <td>" + dl.UploadDate + @"</td>											
                                            <td style='text-align: center;'>" + "<a href='EditResource.aspx?id=" + dl.Id + @"&tab=1'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                            + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                    count++;
                }

                tableManageDownload1.InnerHtml = innerhtml;
            }
            else
                tableManageDownload1.InnerHtml = "";
        }

        public void ResetTabs()
        {
            conventionalTab.Attributes.Remove("class");
            shariahEqTab.Attributes.Remove("class");
            shariahMMTab.Attributes.Remove("class");

            ce.Attributes.Remove("class");
            se.Attributes.Remove("class");
            smm.Attributes.Remove("class");

            ce.Attributes.Add("class", "tab-pane fade");
            se.Attributes.Add("class", "tab-pane fade");
            smm.Attributes.Add("class", "tab-pane fade");
        }

        protected void ddlFund2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetTabs();
            shariahEqTab.Attributes.Add("class", "active");
            se.Attributes.Remove("class");
            se.Attributes.Add("class", "tab-pane fade active in");
            Bind_SE();
        }

        public void Bind_SE()
        {
            int count = 1;
            string innerhtml = "";
            int fundid = Convert.ToInt32(ddlFund2.SelectedValue);
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 8 && x.Fund_Id == fundid && x.Status == 1).ToList();
            if (downloadList.Count() != 0)
            {
                foreach (Download dl in downloadList)
                {
                    innerhtml += @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                            <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                            <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
                                            <td>" + dl.UploadDate + @"</td>											
                                            <td style='text-align: center;'>" + "<a href='EditResource.aspx?id=" + dl.Id + @"&tab=2'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                            + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                    count++;
                }

                tableManageDownload2.InnerHtml = innerhtml;
            }
            else
                tableManageDownload2.InnerHtml = "";
        }

        protected void ddlFund3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetTabs();
            shariahMMTab.Attributes.Add("class", "active");
            smm.Attributes.Remove("class");
            smm.Attributes.Add("class", "tab-pane fade active in");
            Bind_SMM();
        }

        public void Bind_SMM()
        {
            int count = 1;
            string innerhtml = "";
            int fundid = Convert.ToInt32(ddlFund3.SelectedValue);
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 9 && x.Fund_Id == fundid && x.Status == 1).ToList();
            if (downloadList.Count() != 0)
            {
                foreach (Download dl in downloadList)
                {
                    innerhtml += @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                            <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                            <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
                                            <td>" + dl.UploadDate + @"</td>											
                                            <td style='text-align: center;'>" + "<a href='EditResource.aspx?id=" + dl.Id + @"&tab=3'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                            + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                    count++;
                }

                tableManageDownload3.InnerHtml = innerhtml;
            }
            else
                tableManageDownload3.InnerHtml = "";
        }

        protected void btnAction1_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID1.Value);
                string Action = hdnAction1.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                conventionalTab.Attributes.Add("class", "active");
                ce.Attributes.Remove("class");
                ce.Attributes.Add("class", "tab-pane fade active in");
                Bind_CE();
            }
            else
            {
                ResetTabs();
                conventionalTab.Attributes.Add("class", "active");
                ce.Attributes.Remove("class");
                ce.Attributes.Add("class", "tab-pane fade active in");
                Bind_CE();
            }
        }

        protected void btnAction2_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID2.Value);
                string Action = hdnAction2.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                shariahEqTab.Attributes.Add("class", "active");
                se.Attributes.Remove("class");
                se.Attributes.Add("class", "tab-pane fade active in");
                Bind_SE();
            }
            else
            {
                ResetTabs();
                shariahEqTab.Attributes.Add("class", "active");
                se.Attributes.Remove("class");
                se.Attributes.Add("class", "tab-pane fade active in");
                Bind_SE();
            }
        }

        protected void btnAction3_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID3.Value);
                string Action = hdnAction3.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                shariahMMTab.Attributes.Add("class", "active");
                smm.Attributes.Remove("class");
                smm.Attributes.Add("class", "tab-pane fade active in");
                Bind_SMM();
            }
            else
            {
                ResetTabs();
                shariahMMTab.Attributes.Add("class", "active");
                smm.Attributes.Remove("class");
                smm.Attributes.Add("class", "tab-pane fade active in");
                Bind_SMM();
            }
        }
    }
}