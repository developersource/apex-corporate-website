﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditUnitSplit.aspx.cs" Inherits="AdminApex.ApexWeb.EditUnitSplit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Edit Unit Split</h2>
                 <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlFund" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Ex Date</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtDate" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                           <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDate"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Split Ratio</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtSplitRatio" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>               
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="lblSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="lblSubmit_Click" />
                    &nbsp
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
