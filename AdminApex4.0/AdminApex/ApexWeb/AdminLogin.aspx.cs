﻿using AdminApex.ApexUtility;
using AdminApex.ApexData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void login_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtusername.Text != "" || txtpassword.Text != "")
                {
                    string username = txtusername.Text;
                    string password = txtpassword.Text;
                    List<ApexUtility.Admin> admins = AdminData.GetAllAdmin();
                    ApexUtility.Admin admin = new ApexUtility.Admin();

                    admin = admins.Where(x => x.username == username && x.Password == password).FirstOrDefault();

                    if (admin != null)
                    {
                        Session["admin"] = admin;

                        //user log
                        WriteTextFile wtf = new WriteTextFile();
                        string path = Server.MapPath("/Log/");
                        string[] data = { DateTime.Now.ToString(), admin.name, "Login", "Success" };
                        wtf.Write(path, data);

                        Response.Redirect("ViewFinancialInfo.aspx");
                    }
                    else
                    {
                        Response.Write("<script>window.alert('Invalid Username or Password');</script>");
                    }
                }
                else
                {
                    Response.Write("<script>window.alert('<i class='fa fa-close icon'></i> Please enter (<sup>*</sup>) required fields.');</script>");
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

    }
}