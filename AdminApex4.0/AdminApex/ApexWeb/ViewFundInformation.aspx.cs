﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewDistribution : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NAVTab.Attributes.Add("class", "active");
                nav.Attributes.Remove("class");
                nav.Attributes.Add("class", "tab-pane fade active in");

                //ddlFund1.Items.Clear();
                ddlFund2.Items.Clear();
                ddlFund3.Items.Clear();

                //ddlFund1.Items.Add(new ListItem("Select Fund Name - ", "0"));
                ddlFund2.Items.Add(new ListItem("Select Fund Name - ", "0"));
                ddlFund3.Items.Add(new ListItem("Select Fund Name - ", "0"));

                List<Fund> funds = FundService.GetAllFund();
                foreach (Fund fund in funds)
                {
                    if (fund.ID != 1)
                    {
                        //ddlFund1.Items.Add(new ListItem(fund.fund_name, fund.ID.ToString()));
                        ddlFund2.Items.Add(new ListItem(fund.fund_name, fund.ID.ToString()));
                        ddlFund3.Items.Add(new ListItem(fund.fund_name, fund.ID.ToString()));
                    }
                }
                Bind_DailyNAV();

            }
        }

        //protected void ddlFund1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int count = 1;
        //    string innerhtml = "";

        //    List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 7 && x.Fund_Id == fundid && x.Status == 1).ToList();
        //    if (downloadList.Count() != 0)
        //    {
        //        foreach (Download dl in downloadList)
        //        {
        //            innerhtml += @"<tr><td>" + count + @"</td>
        //                                             <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
        //                                             <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
        //                                            <td>" + dl.UploadDate + @"</td>											
        //                                            <td>" + "<a href='EditResource.aspx?id=" + dl.Id + @"'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-check'>Edit</i></a>"
        //                                        + @"</td></tr>";
        //            count++;
        //        }

        //        tableManageDownload1.InnerHtml = innerhtml;
        //    }
        //    else
        //        tableManageDownload1.InnerHtml = "";

        //}

        public void Bind_DailyNAV()
        {
            try
            {
                int count = 1;
                string innerhtml = "";

                //List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 7 && x.Fund_Id == fundid && x.Status == 1).ToList();

                List<Fund> fund = FundService.GetAllFund().Where(x => x.Status == 1).ToList();
                List<DailyNAVFund> dnfs = new List<DailyNAVFund>();
                List<DailyNAVFund> dnfsDESC = new List<DailyNAVFund>();
                DailyNAVFund dnf = new DailyNAVFund();

                foreach (Fund f in fund)
                {
                    dnfsDESC = DailyNAVFundData.GetByFundCode(f.fund_code);
                    dnfsDESC = dnfsDESC.OrderByDescending(x => x.DailyNavDate).ToList();
                    dnf = dnfsDESC.FirstOrDefault();
                    dnfs.Add(dnf);
                }

                string fundname = "";

                if (dnfs.Count() != 0)
                {
                    foreach (DailyNAVFund d in dnfs)
                    {
                        fundname = FundData.getFundNameByCode(d.FundCode);

                        innerhtml += @"<tr><td>" + count + @"</td>
                                                         <td>" + fundname + @"</td>
                                                         <td>" + d.DailyNavDate.ToString("dd/MM/yyyy") + @"</td>
                                                        <td>" + d.DailyUnitPrice + @"</td></tr>";
                        count++;
                    }

                    tableManageDownload1.InnerHtml = innerhtml;
                }
                else
                    tableManageDownload1.InnerHtml = "";
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('Database is empty, please grab csv');</script>");
            }

        }

        public void ResetTabs()
        {
            NAVTab.Attributes.Remove("class");
            DistributionTab.Attributes.Remove("class");
            UnitSplitTab.Attributes.Remove("class");

            nav.Attributes.Remove("class");
            d.Attributes.Remove("class");
            us.Attributes.Remove("class");

            nav.Attributes.Add("class", "tab-pane fade");
            d.Attributes.Add("class", "tab-pane fade");
            us.Attributes.Add("class", "tab-pane fade");
        }

        protected void ddlFund2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetTabs();
            DistributionTab.Attributes.Add("class", "active");
            d.Attributes.Remove("class");
            d.Attributes.Add("class", "tab-pane fade active in");
            Bind_D();
        }

        public void Bind_D()
        {
            int count = 1;
            string innerhtml = "";
            int fundid = Convert.ToInt32(ddlFund2.SelectedValue);

            List<Distribution> distributionList = DistributionService.GetAllDistribution().Where(x => x.Fund_Id == fundid && x.Status == 1).ToList();
            if (distributionList.Count() != 0)
            {
                foreach (Distribution d in distributionList)
                {
                    innerhtml = innerhtml + @"<tr data-id='" + d.Id + @"'><td>" + count + @"</td>
                                            <td>" + d.EntitlementDate.ToString("dd/MM/yyyy") + @"</td>
                                            <td>" + d.Gross + @"</td>
									        <td>" + d.CreatedDate.ToString("dd/MM/yyyy") + @"</td>											
									        <td style='text-align: center;'>" + "<a href='EditDistribution.aspx?id=" + d.Id + @"'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-check'>Edit</i></a>"
                                            + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                    count++;
                }

                tableManageDownload2.InnerHtml = innerhtml;
            }
            else
                tableManageDownload2.InnerHtml = "";
        }



        protected void ddlFund3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetTabs();
            UnitSplitTab.Attributes.Add("class", "active");
            us.Attributes.Remove("class");
            us.Attributes.Add("class", "tab-pane fade active in");
            Bind_US();
        }

        public void Bind_US()
        {
            int count = 1;
            string innerhtml = "";
            int fundid = Convert.ToInt32(ddlFund3.SelectedValue);
            List<UnitSplit> unitSplitList = UnitSplitService.GetAllUnitSplit().Where(x => x.Fund_Id == fundid && x.Status == 1).ToList();
            if (unitSplitList.Count() != 0)
            {
                foreach (UnitSplit us in unitSplitList)
                {
                    innerhtml += @"<tr data-id='" + us.Id + @"'><td>" + count + @"</td>
                                            <td>" + us.ExDate.ToString("dd/MM/yyyy") + @"</td>
                                            <td>" + us.SplitRatio + @"</td>
                                            <td>" + us.CreatedDate.ToString("dd/MM/yyyy") + @"</td>											
                                            <td style='text-align: center;'>" + "<a href='EditUnitSplit.aspx?id=" + us.Id + @"'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-check'>Edit</i></a>"
                                            + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                    count++;
                }

                tableManageDownload3.InnerHtml = innerhtml;
            }
            else
                tableManageDownload3.InnerHtml = "";
        }

        protected void btnAction1_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID1.Value);
                string Action = hdnAction1.Value;

                Distribution dl = DistributionService.GetAllDistribution().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DistributionService.Update(dl);
                        break;
                }
                ResetTabs();
                DistributionTab.Attributes.Add("class", "active");
                d.Attributes.Remove("class");
                d.Attributes.Add("class", "tab-pane fade active in");
                Bind_D();
            }
            else
            {
                ResetTabs();
                DistributionTab.Attributes.Add("class", "active");
                d.Attributes.Remove("class");
                d.Attributes.Add("class", "tab-pane fade active in");
                Bind_D();
            }
        }

        protected void btnAction2_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID2.Value);
                string Action = hdnAction2.Value;

                UnitSplit ul = UnitSplitService.GetAllUnitSplit().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        ul.Status = 0;
                        UnitSplitService.Update(ul);
                        break;
                }
                ResetTabs();
                UnitSplitTab.Attributes.Add("class", "active");
                us.Attributes.Remove("class");
                us.Attributes.Add("class", "tab-pane fade active in");
                Bind_US();
            }
            else
            {
                ResetTabs();
                UnitSplitTab.Attributes.Add("class", "active");
                us.Attributes.Remove("class");
                us.Attributes.Add("class", "tab-pane fade active in");
                Bind_US();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            /*
             Upload_Daily_NAV_CSV ob = new Upload_Daily_NAV_CSV();
             ob.Upload_DailyNav_CSV(1);
             ResetTabs();
             NAVTab.Attributes.Add("class", "active");
             nav.Attributes.Remove("class");
             nav.Attributes.Add("class", "tab-pane fade active in");   
             Bind_DailyNAV();
             */
            //  Delete_Record();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            bool validExt = true;

            for (int i=0; i < FileUpload1.PostedFiles.Count; i++)
            {
                if (Path.GetExtension(FileUpload1.PostedFiles[i].FileName) != ".xlsx")
                {
                    validExt = false;
                }
            }

            if (FileUpload1.HasFiles && validExt) 
            {
                int updated = 0;
                foreach (var file in FileUpload1.PostedFiles) {

                    using (var excel = new ExcelPackage(file.InputStream))
                    {
                        var tbl = new DataTable();
                        var ws = excel.Workbook.Worksheets.First();
                        var hasHeader = true;  // adjust accordingly
                        foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            tbl.Columns.Add(hasHeader ? firstRowCell.Text
                                : String.Format("Column {0}", firstRowCell.Start.Column));

                        // add DataRows to DataTable
                        int startRow = hasHeader ? 2 : 1;
                        for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                        {
                            var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                            DataRow row = tbl.NewRow();
                            foreach (var cell in wsRow)
                                row[cell.Start.Column - 1] = cell.Text;
                            tbl.Rows.Add(row);
                        }

                        for (int i = 0; i < tbl.Rows.Count; i++)
                        {
                            string FundCode = tbl.Rows[i].ItemArray[1].ToString().Trim();
                            string Hour = "00";
                            string Minute = "00";
                            string Second = "00";
                            string Daily_NAV_Time = Hour + ":" + Minute + ":" + Second;
                            string Daily_NAV_Date = tbl.Rows[i].ItemArray[2].ToString().Trim();

                            string Year = Convert.ToDateTime(Daily_NAV_Date).ToString("yyyy");
                            string Month = Convert.ToDateTime(Daily_NAV_Date).ToString("MM");
                            string Day = Convert.ToDateTime(Daily_NAV_Date).ToString("dd");
                            string Daily_NAV_Date1 = Year + "-" + Month + "-" + Day;

                            decimal Unit_Per_Price = Convert.ToDecimal(tbl.Rows[i].ItemArray[3].ToString().Trim());
                            Unit_Per_Price = decimal.Round(Unit_Per_Price, 4);
                            Insert_DailyNAV(FundCode, Daily_NAV_Date1, Unit_Per_Price, Daily_NAV_Time);
                            updated++;
                        }
                        
                    }
                }
                var msg = String.Format("Total of [{0}] NAV price uploaded successfully",
                                        updated);
                Label1.Text = msg;
                Label1.ForeColor = System.Drawing.Color.Green;

            }
            else
            {
                Label1.Text = "Unsupported file format";
                Label1.ForeColor = System.Drawing.Color.Red;
            }
                ResetTabs();
                NAVTab.Attributes.Add("class", "active");
                nav.Attributes.Remove("class");
                nav.Attributes.Add("class", "tab-pane fade active in");
                Bind_DailyNAV();
            
            // OLD file (.out)
            //if (FileUpload1.HasFiles)
            //{
            //    foreach (HttpPostedFile uploadedFile in FileUpload1.PostedFiles)
            //    {

            //        StreamReader csvreader = new StreamReader(uploadedFile.InputStream);

            //        while (!csvreader.EndOfStream)
            //        {
            //            var line = csvreader.ReadLine();


            //            foreach (string row in line.Split('\n'))
            //            {
            //                if (!string.IsNullOrEmpty(row))
            //                {
            //                    var cell = row.Split('|');
            //                    string FundCode = cell[5].ToString().Trim();
            //                    string Time = cell[1].ToString().Trim();
            //                    string Hour = Time.Substring(0, 2);
            //                    string Minute = Time.Substring(3, 2);
            //                    string Second = "00";
            //                    string Daily_NAV_Time = Hour + ":" + Minute + ":" + Second;
            //                    string Daily_NAV_Date = cell[7].ToString().Trim();
            //                    string Year = Daily_NAV_Date.Substring(Daily_NAV_Date.Length - 4);
            //                    string Month = Daily_NAV_Date.Substring(3, 2);
            //                    string Day = Daily_NAV_Date.Substring(0, 2);
            //                    string Daily_NAV_Date1 = Year + "-" + Month + "-" + Day;

            //                    decimal Unit_Per_Price = Convert.ToDecimal(cell[9].ToString().Trim());
            //                    Unit_Per_Price = decimal.Round(Unit_Per_Price, 4);
            //                    Insert_DailyNAV(FundCode, Daily_NAV_Date1, Unit_Per_Price, Daily_NAV_Time);
            //                }
            //            }
            //        }
            //    }
            //}
            //ResetTabs();
            //NAVTab.Attributes.Add("class", "active");
            //nav.Attributes.Remove("class");
            //nav.Attributes.Add("class", "tab-pane fade active in");
            //Bind_DailyNAV();
        }

        public void Insert_DailyNAV(string IPD_FundCode, string Date, Decimal DailyNAV, string Time)
        {
            string Sqlconnstring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();


                string EPF_IPD_Code = "IPD033";

                string IPD_Fund_Code = IPD_FundCode;
                DateTime dt = Convert.ToDateTime(Date);
                DateTime dt2 = Convert.ToDateTime(Time);
                string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                string Daily_NAV_Time = dt2.ToString("HH:mm:ss");
                string Daily_NAV = "0.00";
                string Daily_Unit_created = "0.00";
                string Daily_Unit_Price = DailyNAV.ToString();

                decimal daily_Unit_Created_EPF = 0.4M;
                decimal Daily_NAV_EPF = 0.2M;


                string Query = "Insert into utmc_daily_nav_fund("
                    + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV_Time, Daily_NAV, Report_Date,"
                       + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                         + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV_Time + "','" + Daily_NAV + "','" + Daily_NAV_Date + "','"
                         + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";



                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                {

                    int i = Record_exsiting(Daily_NAV_Date, IPD_Fund_Code);
                    if (i == 0)
                        mysql_comm.ExecuteNonQuery();
                    else
                        Insert_Update_Daily_NAV(Daily_NAV_Date, Daily_NAV_Time, IPD_Fund_Code, Daily_Unit_Price);

                }
                mysql_conn.Close();
            }
        }


        protected void Delete_Record()
        {
            string Sqlconnstring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                string Query = "DELETE FROM utmc_daily_nav_fund";

                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                {

                    mysql_comm.ExecuteNonQuery();
                }
                mysql_conn.Close();
            }
        }

        protected int Record_exsiting(string Date, string IPDFundCode)
        {
            int count = 0;
            string Sqlconnstring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                string Query = "select count(*) FROM utmc_daily_nav_fund where Daily_NAV_Date='" + Date + "' and IPD_Fund_Code='" + IPDFundCode + "' ";

                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                {

                    count = Convert.ToUInt16(mysql_comm.ExecuteScalar());
                }
                mysql_conn.Close();
            }
            return count;
        }


        protected void Insert_Update_Daily_NAV(string Date, string Daily_NAV_Time, string IPDFundCode, string dailyprice)
        {
            string Sqlconnstring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                string Query = "update utmc_daily_nav_fund set Daily_NAV_Date ='" + Date + "', Daily_NAV_Time ='" + Daily_NAV_Time + "', IPD_Fund_Code ='" + IPDFundCode + "', Daily_Unit_Price ='" + dailyprice + "' where Daily_NAV_Date='" + Date + "' and IPD_Fund_Code='" + IPDFundCode + "' ";

                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                {

                    mysql_comm.ExecuteNonQuery();
                }
                mysql_conn.Close();
            }
        }


    }
}