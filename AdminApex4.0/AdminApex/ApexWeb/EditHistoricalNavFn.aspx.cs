﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static AdminApex.ApexData.utmcData;

namespace AdminApex.ApexWeb
{
    public partial class EditHistoricalNavFn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Session Timeout, Please Login to again to Proceed.');window.location.href='Login.aspx';", true);
            }
            else
            {
                String idString = Request.QueryString["id"];
                //Do Edit
                if(!String.IsNullOrEmpty(idString))
                {
                    //Get Data based on passed value.
                    String query = @"SELECT dnf.ID as fundId, dnf.IPD_FUND_CODE as fundCode, ufi.FUND_NAME as fundName, dnf.Daily_NAV_Date as navDate, dnf.Daily_Unit_Price as navPrice 
                                    from utmc_daily_nav_fund dnf
                                    join utmc_fund_information ufi on dnf.IPD_FUND_CODE = ufi.IPD_Fund_Code 
                                    where dnf.ID="+idString;
                    Response responseGetSingle = utmcData.GetDataByQuery(query,true);
                    if (responseGetSingle.IsSuccess)
                    {
                        var FundListDyn = responseGetSingle.Data;
                        var JsonData = JsonConvert.SerializeObject(FundListDyn);
                        List<customHistoricalNav> fundList = JsonConvert.DeserializeObject<List<customHistoricalNav>>(JsonData);
                        customHistoricalNav singleFund = fundList.FirstOrDefault();

                        Id.Value = idString;
                     
                        FundNameTxt.Value = singleFund.fundName.ToString();
                        NAVDateTxt.Value = singleFund.navDate.ToString("dd/MM/yyyy");
                        NAVPriceTxt.Value = singleFund.navPrice.ToString();
                    }
                }

            }
        }
    }
}