﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFileAuthor.Enabled = false;
                lblFileAuthor.Enabled = false;
                txtDate.Enabled = false;
                lblDate.Enabled = false;

                int id = Convert.ToInt32(Request.QueryString["id"]);

                List<DownloadType> downloadTypeList = DownloadTypeService.GetAllDownloadType().Where(x => x.IsFund == 0).ToList();
                foreach (DownloadType dt in downloadTypeList)
                {
                    ddlFileType.Items.Add(new ListItem(dt.TypeName, dt.Id.ToString()));
                }

                //set default value
                Download download = DownloadService.GetById(id);
                DownloadType downloadType = DownloadTypeService.GetById(download.DownloadType_Id);
                txtFileName.Text = download.FileName;
                if (downloadType.TypeName.Equals("Podcast") || downloadType.TypeName.Equals("Articles"))
                {
                    txtFileAuthor.Enabled = true;
                    lblFileAuthor.Enabled = true;
                    txtDate.Enabled = true;
                    lblDate.Enabled = true;
                    txtFileAuthor.Text = download.FileAuthor;
                    txtDate.Text = download.DisplayDate.ToString("yyyy-MM-dd");
                    hdntype.Value = "2";
                }
                ddlFileType.SelectedValue = downloadType.Id.ToString();
                hlFile.NavigateUrl = download.UrlPath;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateFileUpload();
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        public void UpdateFileUpload()
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            string fileName = txtFileName.Text;
            int downloadTypeId = Convert.ToInt32(ddlFileType.SelectedValue);
            string downloadTypeName = ddlFileType.SelectedItem.Text;


            Download oldDownload = DownloadService.GetById(id);
            Admin admin = (Admin)Session["admin"];
            string url = "/Download/" + downloadTypeName;
            string path = Server.MapPath(url);

            if (fuFile.HasFile)
            {
                string fileSize = Convert.ToString(fuFile.PostedFile.ContentLength);
                string extension = Path.GetExtension(fuFile.FileName);
                path = path + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                fuFile.SaveAs(path);

                //update db old record
                oldDownload.Status = 0;
                DownloadService.Update(oldDownload);

                //insert db new record
                Download dl = new Download();
                dl.UploadDate = DateTime.Now;
                dl.CreatedBy = admin.Id;
                dl.DownloadType_Id = downloadTypeId;
                dl.FileName = fileName;
                if (hdntype.Value == "2")
                {
                    if (!(txtFileAuthor.Text.Equals("")))
                    {
                        if (!(txtDate.Text.Equals("")))
                        {
                            //if text date is empty
                            string fileAuthor = txtFileAuthor.Text;
                            DateTime displayDate = Convert.ToDateTime(txtDate.Text);

                            dl.FileAuthor = fileAuthor;
                            dl.Fund_Id = 1;
                            dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                            dl.FileSize = fileSize;
                            dl.DisplayDate = displayDate;
                            dl.Status = 1;
                            DownloadService.Insert(dl);

                            //user log   
                            WriteTextFile wtf = new WriteTextFile();
                            UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                            string[] data = { DateTime.Now.ToString(), admin.name, "Edit Download - " + DownloadTypeService.GetById(dl.DownloadType_Id).TypeName + ", " + dl.FileName, "Success" };
                            wtf.Write(Server.MapPath(userLog.UrlPath), data);

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                            Response.Redirect("ViewDownload.aspx");

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter date!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter file author!')", true);
                    }
                }
                else
                {
                    dl.FileAuthor = "-";
                    dl.Fund_Id = 1;
                    dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                    dl.FileSize = fileSize;
                    dl.Status = 1;
                    DownloadService.Insert(dl);

                }
            }
            else
            {
                string extension = Path.GetExtension(oldDownload.UrlPath);
                path = path + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                File.Copy(Server.MapPath(oldDownload.UrlPath), path);

                //update db old record
                oldDownload.Status = 0;
                DownloadService.Update(oldDownload);

                //insert db new record
                Download dl = new Download();
                dl.UploadDate = DateTime.Now;
                dl.CreatedBy = admin.Id;
                dl.DownloadType_Id = downloadTypeId;
                dl.FileName = fileName;
                if (hdntype.Value == "2")
                {
                    if (!(txtFileAuthor.Text.Equals("")))
                    {
                        if (!txtDate.Text.Equals(""))
                        {
                            //if text date is empty
                            string fileAuthor = txtFileAuthor.Text;
                            DateTime displayDate = Convert.ToDateTime(txtDate.Text);

                            dl.FileAuthor = fileAuthor;
                            dl.Fund_Id = 1;
                            dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                            dl.FileSize = oldDownload.FileSize;
                            dl.DisplayDate = displayDate;
                            dl.Status = 1;
                            DownloadService.Insert(dl);

                            //user log   
                            WriteTextFile wtf = new WriteTextFile();
                            UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                            string[] data = { DateTime.Now.ToString(), admin.name, "Edit Download - " + DownloadTypeService.GetById(dl.DownloadType_Id).TypeName + ", " + dl.FileName, "Success" };
                            wtf.Write(Server.MapPath(userLog.UrlPath), data);

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                            Response.Redirect("ViewDownload.aspx");

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter date!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter file author!')", true);
                    }
                }
                else
                {
                    dl.FileAuthor = "-";
                    dl.Fund_Id = 1;
                    dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                    dl.FileSize = oldDownload.FileSize;
                    dl.Status = 1;
                    DownloadService.Insert(dl);

                    //user log   
                    WriteTextFile wtf = new WriteTextFile();
                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                    string[] data = { DateTime.Now.ToString(), admin.name, "Edit Download - " + DownloadTypeService.GetById(dl.DownloadType_Id).TypeName + ", " + dl.FileName, "Success" };
                    wtf.Write(Server.MapPath(userLog.UrlPath), data);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                    Response.Redirect("ViewDownload.aspx");
                }
            }
        }

        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblFileAuthor.Enabled = false;
            txtFileAuthor.Enabled = false;
            lblDate.Enabled = false;
            txtDate.Enabled = false;
            DownloadType dlList = DownloadTypeService.GetAllDownloadType().Where(x => x.Id == Convert.ToInt32(ddlFileType.SelectedValue)).FirstOrDefault();

            if (dlList != null)
            {
                if (dlList.TypeName.Equals("Podcast") || dlList.TypeName.Equals("Articles"))
                {
                    hdntype.Value = "2";
                    txtFileAuthor.Enabled = true;
                    lblFileAuthor.Enabled = true;
                    lblDate.Enabled = true;
                    txtDate.Enabled = true;
                }
                else
                {
                    hdntype.Value = "0";
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            int tab = Convert.ToInt32(Request.QueryString["tab"]);
            Response.Redirect("ViewDownload.aspx?tab=" +tab);
        }
    }
}
