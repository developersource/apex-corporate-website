﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdminApex._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Key Financial Information</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Year</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="DropDownList1" runat="server" class="border">
                            <asp:ListItem>2018</asp:ListItem>
                            <asp:ListItem>2019</asp:ListItem>
                            <asp:ListItem>2020</asp:ListItem>
                            <asp:ListItem>2021</asp:ListItem>
                            <asp:ListItem>2022</asp:ListItem>
                            <asp:ListItem>2023</asp:ListItem>
                            <asp:ListItem>2024</asp:ListItem>
                            <asp:ListItem>2025</asp:ListItem>
                            <asp:ListItem>2026</asp:ListItem>
                            <asp:ListItem>2027</asp:ListItem>
                            <asp:ListItem>2028</asp:ListItem>
                            <asp:ListItem>2029</asp:ListItem>
                            <asp:ListItem>2030</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Issued/Paid-up Capital</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="ipcap" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Shareholders’ Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="shareholder" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Revenue</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="revenue" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Profit or Loss Before Tax</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="plbeforetax" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Profit or Loss After Tax</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="plaftertax" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Cash & Bank Deposits</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="cbdeposits" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Bank Borrowings</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="bankborrow" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnsubmit1" runat="server" Text="Save" CssClass="btn" OnClick="btnsubmit1_Click" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h2>Fund Summary</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="DropDownList2" runat="server">
                            <asp:ListItem>Unit Trust Fund</asp:ListItem>
                            <asp:ListItem>Wholesale Fund</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>No. of Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="nooffund" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Total Value of Fund(RM)</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="totalfund" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnsubmit2" runat="server" Text="Save" CssClass="btn" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

