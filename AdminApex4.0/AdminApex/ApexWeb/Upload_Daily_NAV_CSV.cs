﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace AdminApex.ApexWeb
{
    public class Upload_Daily_NAV_CSV
    {

        public void Read_Value_CSV(string csv_file_path)
        {

            try
            {
                string csvData = File.ReadAllText(csv_file_path);
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        var cell = row.Split('|');
                        string FundCode = cell[5].ToString().Trim();

                        string Daily_NAV_Date = cell[7].ToString().Trim();
                        string Year = Daily_NAV_Date.Substring(Daily_NAV_Date.Length - 4);
                        string Month = Daily_NAV_Date.Substring(3, 2);
                        string Day = Daily_NAV_Date.Substring(0, 2);
                        string Daily_NAV_Date1 = Year + "-" + Month + "-" + Day;

                        decimal Unit_Per_Price = Convert.ToDecimal(cell[9].ToString().Trim());
                        Unit_Per_Price = decimal.Round(Unit_Per_Price, 4);
                        Insert_DailyNAV(FundCode, Daily_NAV_Date1, Unit_Per_Price);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void Insert_DailyNAV(string IPD_FundCode, string Date, Decimal DailyNAV)
        {
            string Sqlconnstring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();


                string EPF_IPD_Code = "IPD033";

                string IPD_Fund_Code = IPD_FundCode;
                DateTime dt = Convert.ToDateTime(Date);
                string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                string Daily_NAV = "0.00";
                string Daily_Unit_created = "0.00";
                string Daily_Unit_Price = DailyNAV.ToString();

                decimal daily_Unit_Created_EPF = 0.4M;
                decimal Daily_NAV_EPF = 0.2M;


                string Query = "Insert into utmc_daily_nav_fund("
                    + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV, Report_Date,"
                       + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                         + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + Daily_NAV_Date + "','"
                         + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";



                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                {

                    mysql_comm.ExecuteNonQuery();
                }
                mysql_conn.Close();
            }
        }
        public void Upload_DailyNav_CSV(int x)
        {
            try
            {
                string UploadTime = ConfigurationManager.AppSettings["UploadTime_HH:MM"].ToString();

                UploadTime = UploadTime.Substring(0, 2);
                if ((DateTime.Now.Hour == Convert.ToInt16(UploadTime)) || (DateTime.Now.Minute == 00) || x == 1)
                {

                    string CSV_Path = ConfigurationManager.AppSettings["Upload_CSV_Path"].ToString();
                    

                    DirectoryInfo di = new DirectoryInfo(CSV_Path);
                    FileInfo[] files = di.GetFiles("*.OUT");
                    foreach (var file in files)
                    {
                        string FilePath = file.FullName.ToString();
                        Read_Value_CSV(FilePath);
                        string sourceFile = @"" + FilePath;
                        if (!File.Exists(sourceFile))
                            return;

                        string Todaysdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm tt");
                        Todaysdate = Todaysdate.Replace("-", "");
                        Todaysdate = Todaysdate.Replace(" ", "");
                        Todaysdate = Todaysdate.Replace(":", "");
                        string newPath = Path.Combine(@"" + CSV_Path + "/Uploaded/", Todaysdate);

                        if (!Directory.Exists(newPath))
                            Directory.CreateDirectory(newPath);

                        try
                        {
                            //if(!File.Exists(sourceFile))
                            File.Move(sourceFile, Path.Combine(newPath, Path.GetFileName(sourceFile)));
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                    x = 2;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}