﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewUserLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_UserLog();
        }

        public void Bind_UserLog()
        {
            List<UserLog> userLog = UserLogService.GetAllUserLog().Where(x => x.Status != 99).ToList();
            userLog = userLog.OrderByDescending(x => x.CreatedDate).ToList();


            string html = "";
            int count = 1;

            foreach (UserLog ul in userLog)
            {
                html = html + @"<tr><td>" + count + @"</td>
                                    <td>" + (Translate(ul.Month) + "-" + ul.Year) + @"</td>
                                    <td>" + ul.Title + @"</td>
                                    <td style='text-align: center;'><a target='_blank' href='" + ul.UrlPath + @"' download>" + "View" + @"</td></tr>";
                count++;
            }
            tableManageUserLog.InnerHtml = html;
        }

        public string Translate(string month)
        {
            string monthName = "";

            switch (month)
            {
                case "1":
                    monthName = "Jan";
                    break;
                case "2":
                    monthName = "Feb";
                    break;
                case "3":
                    monthName = "Mar";
                    break;
                case "4":
                    monthName = "Apr";
                    break;
                case "5":
                    monthName = "May";
                    break;
                case "6":
                    monthName = "Jun";
                    break;
                case "7":
                    monthName = "July";
                    break;
                case "8":
                    monthName = "Aug";
                    break;
                case "9":
                    monthName = "Sept";
                    break;
                case "10":
                    monthName = "Oct";
                    break;
                case "11":
                    monthName = "Nov";
                    break;
                case "12":
                    monthName = "Dec";
                    break;
            }
            return monthName;
        }
    }
}