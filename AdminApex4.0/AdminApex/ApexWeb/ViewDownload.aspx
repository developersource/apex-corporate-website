﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewDownload.aspx.cs" Inherits="AdminApex.ApexWeb.ViewDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <h2>Files</h2>
        <ul class="nav nav-tabs">
            <asp:HiddenField ID="hidTAB" runat="server" />
            <li id="MPTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#mp">Master Prospectus</a></li>
            <li id="SMPTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#smp">Supplemental Master Prospectus</a></li>
            <li id="FTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#f">Forms</a></li>
            <li id="MCTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#mc">Market Commentary</a></li>
            <li id="PTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#p">Podcast</a></li>
            <li id="ATab" runat="server" clientidmode="static"><a data-toggle="tab" href="#a">Articles</a></li>
        </ul>

        <div class="tab-content table-responsive">
            <div id="mp" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>File Size</th>
                            <th>Upload Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload1" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID1" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction1" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction1" runat="server" OnClick="btnAction1_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="smp" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>File Size</th>
                            <th>Upload Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload2" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID2" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction2" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction2" runat="server" OnClick="btnAction2_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="f" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>File Size</th>
                            <th>Upload Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload3" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID3" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction3" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction3" runat="server" OnClick="btnAction3_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="mc" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover  table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>File Size</th>
                            <th>Upload Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload4" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID4" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction4" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction4" runat="server" OnClick="btnAction4_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="p" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover  table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>Author</th>
                            <th>File Size</th>
                            <th>Display Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload5" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID5" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction5" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction5" runat="server" OnClick="btnAction5_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="a" class="tab-pane fade" runat="server" clientidmode="static">
                <table class="table table-hover  table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>File Name</th>
                            <th>Author</th>
                            <th>File Size</th>
                            <th>Display Date</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="tableManageDownload6" runat="server" clientidmode="static">
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnUserID6" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnAction6" runat="server" ClientIDMode="Static" />
                <asp:Button ID="btnAction6" runat="server" OnClick="btnAction6_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="../Content/js/dataTables-data.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableManageDownload1 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID1').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction1').val(title);
                $('#btnAction1').click();
            });
            $('#tableManageDownload2 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID2').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction2').val(title);
                $('#btnAction2').click();
            });
            $('#tableManageDownload3 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID3').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction3').val(title);
                $('#btnAction3').click();
            });
            $('#tableManageDownload4 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID4').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction4').val(title);
                $('#btnAction4').click();
            });
            $('#tableManageDownload5 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID5').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction5').val(title);
                $('#btnAction5').click();
            });
            $('#tableManageDownload6 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID6').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction6').val(title);
                $('#btnAction6').click();
            });
        });
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
        function Confirm() {
            var confirm_value = document.createElement("input");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to delete your data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
