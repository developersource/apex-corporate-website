﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AddDistribution : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Fund> fundList = FundService.GetAllFund().Where(x => x.Status == 1).ToList();

            foreach (Fund f in fundList)
            {
                ddlFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
            }

        }

        protected void lblSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtDate.Text.Equals(""))
                {
                    if (!txtGrossDistribution.Text.Equals(""))
                    {
                        Admin admin = (Admin)Session["admin"];
                        //insert db
                        int fundId = Convert.ToInt32(ddlFund.SelectedItem.Value.ToString());
                        DateTime entitlementDate = Convert.ToDateTime(txtDate.Text);
                        decimal gross = Convert.ToDecimal(txtGrossDistribution.Text);

                        Distribution distribution = new Distribution();
                        distribution.Fund_Id = fundId;
                        distribution.EntitlementDate = entitlementDate;
                        distribution.Gross = gross;
                        distribution.CreatedBy = admin.Id;
                        distribution.CreatedDate = DateTime.Now;
                        distribution.Status = 1;
                        DistributionService.Insert(distribution);

                        //user log
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Add New Fund Distribution - " + FundService.FundGetById(fundId).fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                        //Response.Redirect(Request.RawUrl);
                        txtDate.Text = "";
                        txtGrossDistribution.Text = "";
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter gross distribution!')", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select date!')", true);
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}