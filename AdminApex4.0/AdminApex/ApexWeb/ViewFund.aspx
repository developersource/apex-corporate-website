﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewFund.aspx.cs" Inherits="AdminApex.ApexWeb.ViewFund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <h2>Fund</h2>
        <div class="row">
            <div class="col table-responsive">
                <table id="dataTable" class="table table-hover table-striped table-bordered nowrap display pb-30">
                    <thead>
                        <tr>
                            <th style="text-align: center">ID</th>
                            <th>Fund Name</th>
                            <th style="text-align: center">Status</th>
                            <th style="text-align: center">Action</th>
                            <th style="text-align: center">Files</th>
                        </tr>
                    </thead>
                    <tbody id="tableFund" runat="server" clientidmode="static">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
