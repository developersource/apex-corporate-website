﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            if (!IsPostBack)
            {
                List<DownloadType> downloadTypeList = DownloadTypeService.GetAllDownloadType().Where(x => x.IsFund == 1).ToList();
                foreach (DownloadType dt in downloadTypeList)
                {
                    ddlFileType.Items.Add(new ListItem(dt.TypeName, dt.Id.ToString()));
                }

                //ddl file name
                // ddlFileName.Items.Add("Please select an item");
                ddlFileName.Items.Add(new ListItem("Brochure", "Brochure"));
                ddlFileName.Items.Add(new ListItem("Product Highlights Sheet", "Product Highlights Sheet"));
                ddlFileName.Items.Add(new ListItem("Fund Fact Sheet", "Fund Fact Sheet"));
                ddlFileName.Items.Add(new ListItem("Interim Report", "Interim Report"));
                ddlFileName.Items.Add(new ListItem("Annual Report", "Annual Report"));
                ddlFileName.Items.Add(new ListItem("Information Memorandum", "Information Memorandum"));

                //set default value
                Download download = DownloadService.GetById(id);
                DownloadType downloadType = DownloadTypeService.GetById(download.DownloadType_Id);
                ddlFileName.SelectedValue = ddlFileName.Items.FindByText(download.FileName).Value;
                ddlFileType.SelectedValue = downloadType.Id.ToString();
                hlFile.NavigateUrl = download.UrlPath;
                BindFund();
                rblFund.SelectedValue = download.Fund_Id.ToString();

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateFileUpload();
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        public void UpdateFileUpload()
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            string fileName = ddlFileName.SelectedItem.Text;
            int downloadTypeId = Convert.ToInt32(ddlFileType.SelectedValue);
            string downloadTypeName = ddlFileType.SelectedItem.Text;
            int fundId = Convert.ToInt32(rblFund.SelectedValue);

            Download oldDownload = DownloadService.GetById(id);
            Admin admin = (Admin)Session["admin"];
            string url = "/Download/" + downloadTypeName;
            string path = Server.MapPath(url);

            if (fuFile.HasFile)
            {
                string fileSize = Convert.ToString(fuFile.PostedFile.ContentLength);
                string extension = Path.GetExtension(fuFile.FileName);
                path = path + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                fuFile.SaveAs(path);

                //update db old record
                oldDownload.Status = 0;
                DownloadService.Update(oldDownload);

                //insert db new record
                Download dl = new Download();
                dl.UploadDate = DateTime.Now;
                dl.CreatedBy = admin.Id;
                dl.DownloadType_Id = downloadTypeId;
                dl.Fund_Id = fundId;
                dl.FileName = fileName;
                dl.FileAuthor = "-";
                dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                dl.FileSize = fileSize;
                dl.Status = 1;
                DownloadService.Insert(dl);

                //user log   
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Edit Download - " + DownloadTypeService.GetById(dl.DownloadType_Id).TypeName + ", " + dl.FileName, "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                Response.Redirect("ViewResource.aspx");
            }
            else
            {
                string extension = Path.GetExtension(oldDownload.UrlPath);
                path = path + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                fuFile.SaveAs(path);

                //update db old record
                oldDownload.Status = 0;
                DownloadService.Update(oldDownload);

                //insert db new record
                Download dl = new Download();
                dl.UploadDate = DateTime.Now;
                dl.CreatedBy = admin.Id;
                dl.DownloadType_Id = downloadTypeId;
                dl.Fund_Id = fundId;
                dl.FileName = fileName;
                dl.FileAuthor = "-";
                dl.UrlPath = url + "/" + downloadTypeName + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                dl.FileSize = oldDownload.FileSize;
                dl.Status = 1;
                DownloadService.Insert(dl);

                //user log   
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Edit Download - " + DownloadTypeService.GetById(dl.DownloadType_Id).TypeName + ", " + dl.FileName, "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                Response.Redirect("ViewResource.aspx");
            }
        }

        public void BindFund()
        {
            int downloadTypeId = Convert.ToInt32(ddlFileType.SelectedValue);
            List<Fund> fundList = FundService.GetAllFund();

            if (downloadTypeId == 7)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 7).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
            }
            else if (downloadTypeId == 8)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 8).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
            }
            else if (downloadTypeId == 9)
            {
                List<Fund> fundCheck = fundList.Where(x => x.DownloadType_Id == 9).ToList();
                foreach (Fund f in fundCheck)
                {
                    rblFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                }
            }

        }

        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Clear();
            BindFund();
        }

        public void Clear()
        {
            rblFund.Items.Clear();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            int tab = Convert.ToInt32(Request.QueryString["tab"]);
            Response.Redirect("ViewResource.aspx?tab=" + tab);
        }

    }
}