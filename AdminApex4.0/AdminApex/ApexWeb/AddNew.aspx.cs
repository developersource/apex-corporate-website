﻿using AdminApex.ApexUtility;
using AdminApex.ApexService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AdminApex.ApexData;

namespace AdminApex.ApexWeb
{
    public partial class AddNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<Fund> fundlist = new List<Fund>();
                List<Fund> fund = NewsService.FundGetAll();

                foreach (Fund f in fund)
                {
                    if (f.ID != 1)
                    {
                        chkboxfundname.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                    }
                }
            }
        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                string url = "/Picture/";
                string path = Server.MapPath(url);
                string filename = Path.GetFileName(FileUpload1.FileName);

                path = path + filename;
                FileUpload1.PostedFile.SaveAs(path);

                News news = new News();
                Fund fund = new Fund();

                string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                DateTime currenttime = DateTime.Parse(time);
                news.CreatedBy = admin.Id;
                news.UploadDate = currenttime;
                news.Status = 1;
                news.title = txt_title.Text;
                news.UrlPath = url + filename;
                news.content = txt_Content.InnerText;

                //FOR ANNOUNCEMENTS UPGRADE
                //if (chkboxAnnoucement.Checked)
                //{
                //    news.annoucementStatus = 1;
                //    news.annoucementDesc = txt_Desc.InnerText;
                //}
                //else
                //{
                //    news.annoucementStatus = 0;
                //}

                //if any url filter found in the text
                //if (urlFilters.Contains(news.content))
                //{
                //    news.content = ConvertStringToURL(news.content);
                //}
                news = NewsService.Insert(news);

                List<string> fundSelected = chkboxfundname.Items.Cast<ListItem>()
                                   .Where(li => li.Selected)
                                   .Select(li => li.Value)
                                   .ToList();

                fund_news fundnews = new fund_news();

                foreach (String y in fundSelected)
                {
                    fundnews.fund_news_news_id = news.id;
                    fundnews.fund_news_fund_id = Int32.Parse(y);

                    FundnewService.Insert(fundnews);
                }
                //user log   
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Add New News - " + news.title, "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                Response.Write("<script>window.alert('The News was successfully added !'); window.location='ViewNews.aspx';</script>");
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        
    }
}