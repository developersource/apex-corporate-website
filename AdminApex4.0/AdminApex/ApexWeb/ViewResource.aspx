﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewResource.aspx.cs" Inherits="AdminApex.ApexWeb.ViewResource" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <h2>Fund Resources</h2>
        <ul class="nav nav-tabs">
            <li id="conventionalTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#ce">Conventional Equities</a></li>
            <li id="shariahEqTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#se">Shariah Equities</a></li>
            <li id="shariahMMTab" runat="server" clientidmode="static"><a data-toggle="tab" href="#smm">Shariah Money Market</a></li>
        </ul>
        <br />

        <div class="tab-content table-responsive">
            <div id="ce" class="tab-pane fade" runat="server" clientidmode="static">
                        <asp:DropDownList ID="ddlFund1" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlFund1_SelectedIndexChanged">
                        </asp:DropDownList>
                        <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>File Name</th>
                                    <th>File Size</th>
                                    <th>Upload Date</th>
                                    <th style="text-align: center">Action</th>
                                    <th style="text-align: center">Delete</th>
                                </tr>
                            </thead>
                            <tbody id="tableManageDownload1" runat="server" clientidmode="static">
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdnUserID1" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnAction1" runat="server" ClientIDMode="Static" />
                        <asp:Button ID="btnAction1" runat="server" OnClick="btnAction1_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="se" class="tab-pane fade" runat="server" clientidmode="static">
                        <asp:DropDownList ID="ddlFund2" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlFund2_SelectedIndexChanged">
                        </asp:DropDownList>
                        <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>File Name</th>
                                    <th>File Size</th>
                                    <th>Upload Date</th>
                                    <th style="text-align: center">Action</th>
                                    <th style="text-align: center">Delete</th>
                                </tr>
                            </thead>
                            <tbody id="tableManageDownload2" runat="server" clientidmode="static">
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdnUserID2" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnAction2" runat="server" ClientIDMode="Static" />
                        <asp:Button ID="btnAction2" runat="server" OnClick="btnAction2_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
            <div id="smm" class="tab-pane fade" runat="server" clientidmode="static">
                        <asp:DropDownList ID="ddlFund3" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlFund3_SelectedIndexChanged">
                        </asp:DropDownList>
                        <table class="table table-hover table-striped table-bordered nowrap display pb-30">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>File Name</th>
                                    <th>File Size</th>
                                    <th>Upload Date</th>
                                    <th style="text-align: center">Action</th>
                                    <th style="text-align: center">Delete</th>
                                </tr>
                            </thead>
                            <tbody id="tableManageDownload3" runat="server" clientidmode="static">
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdnUserID3" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnAction3" runat="server" ClientIDMode="Static" />
                        <asp:Button ID="btnAction3" runat="server" OnClick="btnAction3_Click" OnClientClick="Confirm()" ClientIDMode="Static" CssClass="hide" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="../Content/js/dataTables-data.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableManageDownload1 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID1').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction1').val(title);
                $('#btnAction1').click();
            });
            $('#tableManageDownload2 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID2').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction2').val(title);
                $('#btnAction2').click();
            });
            $('#tableManageDownload3 a[title="Delete"]').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID3').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction3').val(title);
                $('#btnAction3').click();
            });
        });
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to delete your data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
