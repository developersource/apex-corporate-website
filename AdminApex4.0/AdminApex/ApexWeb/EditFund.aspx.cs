﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AdminApex.ApexWeb
{
    public partial class EditFund : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<DownloadType> downloadTypeList = DownloadTypeService.GetAllDownloadType();
                ddlFileType.Items.Clear();
                foreach (DownloadType downloadType in downloadTypeList)
                {
                    if (downloadType.Id > 6)
                    {
                        ddlFileType.Items.Add(new ListItem(downloadType.TypeName, downloadType.Id.ToString()));
                    }
                }
                int id = Convert.ToInt32(Request.QueryString["id"]);

                List<Fund> fundlist = new List<Fund>();
                Fund fund = FundService.FundGetById(id);

                ImgPhoto.ImageUrl = fund.url_path;
                txt_fund_code.Text = fund.fund_code;
                txt_fundname.Text = fund.fund_name;
                ddlFileType.SelectedValue = fund.DownloadType_Id.ToString();
                txt_epf_approved.Text = fund.epfapproved;
                txt_pot_inves.InnerText = fund.potential_inves;
                txt_inves_stra.InnerText = fund.inves_strategy;
                txt_launch_date.Text = fund.launch_date.ToString("yyyy-MM-dd");
                txt_trustee.Text = fund.trustee;
                txt_fund_category.Text = fund.fund_category;
                txt_sales_charge.Text = fund.sales_charge;
                txt_manage_fee.Text = fund.manage_fee;
                txt_trustee_fee.Text = fund.trustee_fee;
                txt_min_ini_inves.Text = fund.min_ini_inves;
                txt_min_sub_inves.Text = fund.min_sub_inv;
                txt_redemp_fee.Text = fund.redemp_fee;
                if (fund.Status == 1)
                {
                    chkact.Checked = true;
                }
                else
                {
                    chksus.Checked = true;
                }
            }

        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (fundlogo.HasFile)
                {
                    if (txt_fund_code.Text != "" && txt_epf_approved.Text != "" && txt_fund_category.Text != "" && txt_fundname.Text != "" && txt_inves_stra.InnerText != "" &&
                       txt_launch_date.Text != "" && txt_manage_fee.Text != "" && txt_min_ini_inves.Text != "" && txt_min_sub_inves.Text != "" && txt_pot_inves.InnerText != "" &&
                       txt_redemp_fee.Text != "" && txt_sales_charge.Text != "" && txt_trustee.Text != "" && txt_trustee_fee.Text != "")
                    {
                        Admin admin = (Admin)Session["admin"];
                        int id = Convert.ToInt32(Request.QueryString["id"]);

                        List<Fund> fundlist = new List<Fund>();
                        Fund fund = FundService.FundGetById(id);

                        string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                        DateTime currenttime = DateTime.Parse(time);

                        string url = "/FundLogo/";
                        string path = Server.MapPath(url);
                        string filename = Path.GetFileName(fundlogo.FileName);

                        path = path + filename;
                        fundlogo.PostedFile.SaveAs(path);

                        int numbers = Convert.ToInt32(Regex.Match(txt_min_sub_inves.Text, @"\d+").Value);
                        var letters = new String(txt_min_sub_inves.Text.Where(c => Char.IsLetter(c) || Char.IsPunctuation(c)).ToArray());



                        fund.CreatedBy = admin.Id;
                        fund.CreatedDate = currenttime;
                        fund.url_path = url + filename;
                        fund.fund_code = txt_fund_code.Text;
                        fund.fund_name = txt_fundname.Text;
                        fund.DownloadType_Id = Convert.ToInt32(ddlFileType.SelectedValue);
                        fund.epfapproved = txt_epf_approved.Text;
                        fund.potential_inves = txt_pot_inves.InnerText;
                        fund.inves_strategy = txt_inves_stra.InnerText;
                        fund.launch_date = Convert.ToDateTime(txt_launch_date.Text);
                        fund.trustee = txt_trustee.Text;
                        fund.fund_category = txt_fund_category.Text;
                        fund.sales_charge = txt_sales_charge.Text;
                        fund.manage_fee = txt_manage_fee.Text;
                        fund.trustee_fee = txt_trustee_fee.Text;
                        fund.min_ini_inves = txt_min_ini_inves.Text;
                        fund.min_sub_inv = numbers.ToString("n0") + letters;
                        fund.redemp_fee = txt_redemp_fee.Text;
                        if (chkact.Checked == true)
                        {
                            fund.Status = 1;
                        }
                        else
                        {
                            fund.Status = 0;
                        }
                        fund = FundService.Update(fund, id);

                        //user log   
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Edit Fund - " + fund.fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        Response.Write("<script>window.alert('The Fund was successfully updated !');</script>");
                    }
                    else
                    {
                        Response.Write("<script>window.alert('The text field cannot be empty !');</script>");
                    }
                }
                else
                {
                    if (txt_fund_code.Text != "" && txt_epf_approved.Text != "" && txt_fund_category.Text != "" && txt_fundname.Text != "" && txt_inves_stra.InnerText != "" &&
                        txt_launch_date.Text != "" && txt_manage_fee.Text != "" && txt_min_ini_inves.Text != "" && txt_min_sub_inves.Text != "" && txt_pot_inves.InnerText != "" &&
                        txt_redemp_fee.Text != "" && txt_sales_charge.Text != "" && txt_trustee.Text != "" && txt_trustee_fee.Text != "")
                    {
                        Admin admin = (Admin)Session["admin"];
                        int id = Convert.ToInt32(Request.QueryString["id"]);

                        List<Fund> fundlist = new List<Fund>();
                        Fund fund = FundService.FundGetById(id);
                        Fund oldurl = FundService.FundGetById(id);
                        string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                        DateTime currenttime = DateTime.Parse(time);

                        fund.CreatedBy = admin.Id;
                        fund.CreatedDate = currenttime;
                        fund.url_path = oldurl.url_path;
                        fund.fund_code = txt_fund_code.Text;
                        fund.fund_name = txt_fundname.Text;
                        fund.DownloadType_Id = Convert.ToInt32(ddlFileType.SelectedValue);
                        fund.epfapproved = txt_epf_approved.Text;
                        fund.potential_inves = txt_pot_inves.InnerText;
                        fund.inves_strategy = txt_inves_stra.InnerText;
                        fund.launch_date = Convert.ToDateTime(txt_launch_date.Text);
                        fund.trustee = txt_trustee.Text;
                        fund.fund_category = txt_fund_category.Text;
                        fund.sales_charge = txt_sales_charge.Text;
                        fund.manage_fee = txt_manage_fee.Text;
                        fund.trustee_fee = txt_trustee_fee.Text;
                        fund.min_ini_inves = txt_min_ini_inves.Text;
                        fund.min_sub_inv = txt_min_sub_inves.Text;
                        fund.redemp_fee = txt_redemp_fee.Text;
                        if (chkact.Checked == true)
                        {
                            fund.Status = 1;
                        }
                        else
                        {
                            fund.Status = 0;
                        }
                        fund = FundService.Update(fund, id);

                        //user log   
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Edit Fund - " + fund.fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        Response.Write("<script>window.alert('The Fund was successfully updated !');</script>");
                    }
                    else
                    {
                        Response.Write("<script>window.alert('The text field cannot be empty !');</script>");
                    }
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        //protected void ddlfund_name_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int id = Convert.ToInt32(ddlfund_name.SelectedValue);
        //    string ID = ddlfund_name.Text;
        //    List<Fund> fundlist = new List<Fund>();
        //    Fund fund = FundData.FundGetByID(id);

        //    txt_pot_inves.Text = fund.potential_inves;
        //    txt_inves_stra.Text = fund.inves_strategy;
        //    txt_launch_date.Text = fund.launch_date.ToString("yyyy-MM-dd");
        //    txt_trustee.Text = fund.trustee;
        //    txt_fund_category.Text = fund.fund_category;
        //    txt_sales_charge.Text = fund.sales_charge;
        //    txt_manage_fee.Text = fund.manage_fee;
        //    txt_trustee_fee.Text = fund.trustee_fee;
        //    txt_min_ini_inves.Text = fund.min_ini_inves;
        //    txt_redemp_fee.Text = fund.redemp_fee;
        //    if (fund.Status == 1)
        //    {
        //        chkact.Checked = true;
        //    }
        //    else
        //    {
        //        chksus.Checked = true;
        //    }

        //    txt_pot_inves.Enabled = true;
        //    txt_inves_stra.Enabled = true;
        //    txt_launch_date.Enabled = true;
        //    txt_trustee.Enabled = true;
        //    txt_fund_category.Enabled = true;
        //    txt_sales_charge.Enabled = true;
        //    txt_manage_fee.Enabled = true;
        //    txt_trustee_fee.Enabled = true;
        //    txt_min_ini_inves.Enabled = true;
        //    txt_redemp_fee.Enabled = true;

        //}

        protected void chkact_CheckedChanged(object sender, EventArgs e)
        {
            chksus.Checked = !chkact.Checked;
        }

        protected void chksus_CheckedChanged(object sender, EventArgs e)
        {
            chkact.Checked = !chksus.Checked;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewFund.aspx");
        }
    }
}