﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditFundSummary.aspx.cs" Inherits="AdminApex.ApexWeb.EditFundSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Fund Summary</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlfunc" runat="server" CssClass="ddlbox2">
                            <asp:ListItem>Unit Trust Fund</asp:ListItem>
                            <asp:ListItem>Wholesale Fund</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>No. of Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_nooffund" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Total Value of Fund(RM)</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_totalfund" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnsubmit" runat="server" Text="Save" CssClass="btn" OnClick="btnsubmit_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
