﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewFinancialInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            kfiTab.Attributes.Add("class", "active");
            kfim.Attributes.Remove("class");
            kfim.Attributes.Add("class", "tab-pane fade active in");

            Bindkfi();
            Bindfs();
        }

        public void ResetTabs()
        {
            kfiTab.Attributes.Remove("class");
            fsTab.Attributes.Remove("class");

            kfim.Attributes.Remove("class");
            fs.Attributes.Remove("class");

            kfim.Attributes.Add("class", "tab-pane fade");
            fs.Attributes.Add("class", "tab-pane fade");
        }

        public void Bindkfi()
        {
            List<FinancialInfo> fi = new List<FinancialInfo>();
            fi = FinancialInfoService.GetAllFi();

            string userhtml = "";

            foreach (FinancialInfo f in fi)
            {
                userhtml = userhtml + @"<tr data-id='" + f.ID + @"'>
                                            <td>" + f.year + @"</td>
											<td>" + f.issue_paidup_capital + @"</td>
                                            <td>" + f.shareholder_fund + @"</td>
                                            <td>" + f.revenue + @"</td>
                                            <td>" + f.profit_loss_before_tax + @"</td>
                                            <td>" + f.profit_loss_after_tax + @"</td>
											<td style='text-align: center;'>
                                                " + (f.Status == 1 ?
                                                "<i class='fa fa-circle  txt-success' style='color:#64DF6F' title='Activate'></i>"
                                                :
                                                "<i class='fa fa-circle  txt-danger' style='color:red' title='Suspended'></i>"
                                                ) + @"
                                            </td>
                                            <td style='text-align: center;'>
                                                " +
                                                 "<a href='EditFinancialInfo.aspx?id=" + f.ID + @"' class='btn-sm btn-info' data-toggle='tooltip' title='edit'><i class='fa fa-edit'> Edit</i></a>"
                                               + @"
                                            </td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
										</tr>";
            }
            tablekfi.InnerHtml = userhtml;
        }

        public void Bindfs()
        {
            List<FundSummary> fi = new List<FundSummary>();
            fi = FundSummaryService.GetAllFs().Where(x => x.Status == 1).ToList();

            string userhtml = "";

            foreach (FundSummary f in fi)
            {
                if (f.Status != 99)
                {
                    userhtml = userhtml + @"<tr data-id='" + f.ID + @"'>
                                            <td>" + f.fund_categ + @"</td>
											<td style='text-align: center;'>" + f.no_of_fund + @"</td>
                                            <td style='text-align: cetner;'>" + f.total_value.ToString("n0") + @"</td>
                                            <td style='text-align: center;'>
                                                " +
                                                     "<a href='EditFundSummary.aspx?id=" + f.ID + @"' class='btn-sm btn-info' data-toggle='tooltip' title='edit'><i class='fa fa-edit'> Edit</i></a>"
                                                   + @"
                                            </td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
										</tr>";
                }
            }
            tablefs.InnerHtml = userhtml;
        }

        protected void btnAction1_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID1.Value);
                string Action = hdnAction1.Value;

                FinancialInfo fi = FinancialInfoService.GetAllFi().Where(x => x.ID == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        fi.Status = 0;
                        FinancialInfoData.Update2(fi);
                        break;
                }
                ResetTabs();
                kfiTab.Attributes.Add("class", "active");
                kfim.Attributes.Remove("class");
                kfim.Attributes.Add("class", "tab-pane fade active in");
                Bindkfi();
            }
            else
            {
                ResetTabs();
                kfiTab.Attributes.Add("class", "active");
                kfim.Attributes.Remove("class");
                kfim.Attributes.Add("class", "tab-pane fade active in");
                Bindkfi();
            }
        }

        protected void btnAction2_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID2.Value);
                string Action = hdnAction2.Value;

                FundSummary fss = FundSummaryService.GetAllFs().Where(x => x.ID == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        fss.Status = 0;
                        FundSummaryData.Update2(fss);
                        break;
                }
                ResetTabs();
                fsTab.Attributes.Add("class", "active");
                fs.Attributes.Remove("class");
                fs.Attributes.Add("class", "tab-pane fade active in");
                Bindfs();
            }
            else
            {
                ResetTabs();
                fsTab.Attributes.Add("class", "active");
                fs.Attributes.Remove("class");
                fs.Attributes.Add("class", "tab-pane fade active in");
                Bindfs();
            }
        }
    }
}