﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddDistribution.aspx.cs" Inherits="AdminApex.ApexWeb.AddDistribution" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Add Distribution</h2>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>Fund</label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlFund" runat="server" CssClass="form-control" Width="500"></asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Entitlement Date</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtDate" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                    </div>
                       <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDate"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Gross Distribution</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtGrossDistribution" runat="server" onkeypress="CheckNumeric(event);" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="lblSubmit" runat="server" Text="Save" CssClass="btn" OnClick="lblSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
    <script>
        function CheckNumeric(e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8 & e.keyCode != 46) {
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.which < 48 || e.which > 57) & e.which != 8 & e.which != 46) {
                    e.preventDefault();
                    return false;
                }
            }
        }
    </script>
</asp:Content>
