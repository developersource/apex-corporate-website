﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            newsTab.Attributes.Add("class", "active");
            news.Attributes.Remove("class");
            news.Attributes.Add("class", "tab-pane fade active in");
            BindNews();
            //BindAnn();
        }

        public void BindNews()
        {
            List<News> fn = new List<News>();

            fn = NewsService.NewsGetAll().Where(x => x.Status ==1).ToList();

            string userhtml = "";
            int count = 1;

            foreach (News n in fn)
            {
                userhtml = userhtml + @"<tr data-id='" + n.id + @"'>
                                            <td  style='text-align:center'>" + count + @"</td>
											<td>" + n.title + @"</td>
                                            <td>" + n.UploadDate + @"</td>
                                            <td style='text-align: center;'>
                                                " +
                                                  "<a href='EditNews.aspx?id=" + n.id + @"' class='btn-sm btn-info' data-toggle='tooltip' title='edit'><i class='fa fa-edit'> Edit</i></a>"
                                                + @"
                                            </td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";

                count++;
            }
            tableManageDownload1.InnerHtml = userhtml;

        }

        protected void btnAction1_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID1.Value);
                string Action = hdnAction1.Value;

                News n = NewsService.NewsGetAll().Where(x => x.id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        n.Status = 0;
                        NewsService.UpdateS(n);
                        break;
                }
                BindNews();
            }
            else
            {
            }
        }

        //public void BindAnn()
        //{
        //    List<News> fn = new List<News>();

        //    fn = NewsService.NewsGetAll().Where(x => x.annoucementStatus == 1).ToList();
        //    fn = fn.OrderBy(x => x.UploadDate).ToList();

        //    string userhtml = "";
        //    int count = 1;

        //    foreach (News n in fn)
        //    {
        //        userhtml = userhtml + @"<tr data-id='" + n.id + @"'>
        //                                    <td  style='text-align:center'>" + count + @"</td>
								//			<td>" + n.title + @"</td>
        //                                    <td>" + n.UploadDate + @"</td>
        //                                    <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Deactivate'><i class='fa fa-remove'></i></a></td>
        //                                    </tr>";

        //        count++;
        //    }
        //    tableManageDownload2.InnerHtml = userhtml;

        //}

        protected void btnAction2_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID2.Value);
                string Action = hdnAction2.Value;

                News n = NewsService.NewsGetAll().Where(x => x.id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Deactivate":
                        //n.annoucementStatus = 0;
                        NewsService.UpdateS(n);
                        break;
                }
                //BindAnn();
            }
            else
            {
            }
        }

    }
}