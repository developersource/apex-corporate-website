﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AddAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
        }

        protected void lblSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(txtName.Text.Equals("") || txtDepartment.Text.Equals("") || txtEmail.Text.Equals("") || txtUsername.Text.Equals("") || txtPassword.Text.Equals("")))
                {
                    List<Admin> adminList = AdminData.GetAllAdmin();
                    Admin adminCheck = adminList.Where(x => x.username == txtUsername.Text).FirstOrDefault();
                    if (adminCheck == null)
                    {
                        string name = txtName.Text;
                        string department = txtDepartment.Text;
                        string email = txtEmail.Text;
                        string username = txtUsername.Text;
                        string password = txtPassword.Text;

                        //insert db
                        Admin adminNew = new Admin();
                        adminNew.name = name;
                        adminNew.Department = department;
                        adminNew.Email = email;
                        adminNew.username = username;
                        adminNew.Password = password;
                        adminNew.Status = 1;
                        adminNew.locked = 0;
                        AdminData.Insert(adminNew);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                        //Response.Redirect(Request.RawUrl);
                        Clear();

                        //user log
                        Admin admin = (Admin)Session["admin"];
                        string path = Server.MapPath("/Log/");
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Add Admin - " + adminNew.name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Duplicate username!')", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Fill in the blank!')", true);

            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed!')", true);
            }

        }

        public void Clear()
        {
            txtName.Text = "";
            txtEmail.Text = "";
            txtDepartment.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
        }
    }

}



