﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewUserLog.aspx.cs" Inherits="AdminApex.ApexWeb.ViewUserLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30 table-responsive">
        <h2>User Log</h2>
        <table class="table table-hover table-striped table-bordered nowrap display pb-30">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>File Name</th>
                    <th style="text-align: center">Action</th>
                </tr>
            </thead>
            <tbody id="tableManageUserLog" runat="server" clientidmode="Static">
            </tbody>
        </table>
         <p style="color: red; font-size: smaller">
                    *Right click to save the user log file
                </p>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
