﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class EditNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<Fund> fundlist = new List<Fund>();
                List<Fund> fund = NewsService.FundGetAll();

                foreach (Fund f in fund)
                {
                    if (f.ID != 1)
                    {
                        chkboxfundname.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
                    }
                }

                int id = Convert.ToInt32(Request.QueryString["id"]);
                News news = NewsService.NewsGetById(id);

                ImgPhoto.ImageUrl = news.UrlPath;
                txt_title.Text = news.title;
                txt_Content.InnerText = news.content;
                //txt_Desc.InnerText = news.annoucementDesc;

                //if (news.annoucementStatus == 1)
                //{
                //    chkboxAnnoucement.Checked = true;
                //}

                List<fund_news> fns = FundnewService.GetGetByID(id);

                foreach (fund_news n in fns)
                {
                    chkboxfundname.Items.FindByValue(n.fund_news_fund_id.ToString()).Selected = true;
                }
            }
        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (newslogo.HasFile)
                {
                    ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                    int id = Convert.ToInt32(Request.QueryString["id"]);

                    string url = "/Picture/";
                    string path = Server.MapPath(url);
                    string filename = Path.GetFileName(newslogo.FileName);

                    path = path + filename;
                    newslogo.PostedFile.SaveAs(path);

                    News news = NewsService.NewsGetById(id);

                    string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime currenttime = DateTime.Parse(time);
                    news.CreatedBy = admin.Id;
                    //news.UploadDate = currenttime;
                    news.Status = 1;
                    news.title = txt_title.Text;
                    news.UrlPath = url + filename;
                    news.content = txt_Content.InnerText;
                    //news.annoucementDesc = txt_Desc.InnerText;

                    //if (chkboxAnnoucement.Checked == true)
                    //{
                    //    news.annoucementStatus = 1;
                    //}
                    //else
                    //{
                    //    news.annoucementStatus = 0;
                    //}
                    news = NewsService.Update(news, id);

                    List<string> fundSelected = chkboxfundname.Items.Cast<ListItem>()
                                       .Where(li => li.Selected)
                                       .Select(li => li.Value)
                                       .ToList();

                    int newsid = news.id;
                    List<fund_news> fundnews = new List<fund_news>();
                    fundnews = FundnewService.GetGetByID(newsid);

                    foreach (String y in fundSelected)
                    {
                        fundnews.Where(x => x.fund_news_news_id == news.id);
                        fundnews.Where(x => x.fund_news_fund_id == Int32.Parse(y));

                        //FundnewService.Update(fundnews, news.id);
                    }

                    //user log   
                    WriteTextFile wtf = new WriteTextFile();
                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                    string[] data = { DateTime.Now.ToString(), admin.name, "Add New News - " + news.title, "Success" };
                    wtf.Write(Server.MapPath(userLog.UrlPath), data);

                    Response.Write("<script>window.alert('The News was successfully updated !'); window.location='ViewNews.aspx';</script>");
                }
                else
                {
                    ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];
                    int id = Convert.ToInt32(Request.QueryString["id"]);

                    News news = NewsService.NewsGetById(id);
                    News oldnews = NewsService.NewsGetById(id);
                    Fund fund = new Fund();

                    string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime currenttime = DateTime.Parse(time);
                    news.CreatedBy = admin.Id;
                    //news.UploadDate = currenttime;
                    news.Status = 1;
                    news.title = txt_title.Text;
                    news.UrlPath = oldnews.UrlPath;
                    news.content = txt_Content.InnerText;
                    //news.annoucementDesc = txt_Desc.InnerText;

                    //if (chkboxAnnoucement.Checked == true)
                    //{
                    //    news.annoucementStatus = 1;
                    //}
                    //else
                    //{
                    //    news.annoucementStatus = 0;
                    //}
                    news = NewsService.Update(news, id);

                    List<string> fundSelected = chkboxfundname.Items.Cast<ListItem>()
                                       .Where(li => li.Selected)
                                       .Select(li => li.Value)
                                       .ToList();

                    fund_news fundnews = new fund_news();

                    foreach (String y in fundSelected)
                    {
                        fundnews.fund_news_news_id = news.id;
                        fundnews.fund_news_fund_id = Int32.Parse(y);

                        //FundnewService.Update(fundnews, news.id);
                    }
                    //user log   
                    WriteTextFile wtf = new WriteTextFile();
                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                    string[] data = { DateTime.Now.ToString(), admin.name, "Edit News - " + news.title, "Success" };
                    wtf.Write(Server.MapPath(userLog.UrlPath), data);

                    Response.Write("<script>window.alert('The News was successfully updated !'); window.location='ViewNews.aspx';</script>");
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}