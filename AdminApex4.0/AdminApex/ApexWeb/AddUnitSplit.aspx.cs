﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AddUnitSplit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Fund> fundList = FundService.GetAllFund().Where(x => x.Status == 1).ToList();

            foreach (Fund f in fundList)
            {
                ddlFund.Items.Add(new ListItem(f.fund_name, f.ID.ToString()));
            }
        }

        protected void lblSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Admin admin = (Admin)Session["admin"];

                if (!txtDate.Text.Equals(""))
                {
                    if (!txtSplitRatio.Text.Equals(""))
                    {
                        //insert db
                        int fundId = Convert.ToInt32(ddlFund.SelectedItem.Value.ToString());
                        DateTime exDate = Convert.ToDateTime(txtDate.Text);
                        string splitRatio = txtSplitRatio.Text;

                        UnitSplit unitSplit = new UnitSplit();
                        unitSplit.Fund_Id = fundId;
                        unitSplit.ExDate = exDate;
                        unitSplit.SplitRatio = splitRatio;
                        unitSplit.CreatedBy = admin.Id;
                        unitSplit.CreatedDate = DateTime.Now;
                        unitSplit.Status = 1;
                        UnitSplitService.Insert(unitSplit);

                        //user log   
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Add New Fund Unit Split - " + FundService.FundGetById(unitSplit.Fund_Id).fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);

                        txtDate.Text = "";
                        txtSplitRatio.Text = "";
                        //Response.Redirect(Request.RawUrl);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter split ratio!')", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select date!')", true);
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}