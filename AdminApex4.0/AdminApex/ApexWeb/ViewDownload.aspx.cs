﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MPTab.Attributes.Add("class", "active");
                mp.Attributes.Remove("class");
                mp.Attributes.Add("class", "tab-pane fade active in");

                Bind_MP();
                Bind_SMP();
                Bind_F();
                Bind_MC();
                Bind_P();
                Bind_A();

                if (Request.QueryString["tab"] != null)
                {
                    int tab = Convert.ToInt32(Request.QueryString["tab"]);

                    CleanTab();
                    CleanTabContent();

                    if (tab == 1)
                    {
                        MPTab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade in active");
                        smp.Attributes.Add("class", "tab-pane fade");
                        f.Attributes.Add("class", "tab-pane fade");
                        mc.Attributes.Add("class", "tab-pane fade");
                        p.Attributes.Add("class", "tab-pane fade");
                        a.Attributes.Add("class", "tab-pane fade");
                    }
                    else if (tab ==2 )
                    {
                        SMPTab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade");
                        smp.Attributes.Add("class", "tab-pane fade in active");
                        f.Attributes.Add("class", "tab-pane fade");
                        mc.Attributes.Add("class", "tab-pane fade");
                        p.Attributes.Add("class", "tab-pane fade");
                        a.Attributes.Add("class", "tab-pane fade");
                    }else if (tab ==3)
                    {
                        FTab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade");
                        smp.Attributes.Add("class", "tab-pane fade");
                        f.Attributes.Add("class", "tab-pane fade in active");
                        mc.Attributes.Add("class", "tab-pane fade");
                        p.Attributes.Add("class", "tab-pane fade");
                        a.Attributes.Add("class", "tab-pane fade");
                    }
                    else if(tab == 4)
                    {
                        MCTab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade");
                        smp.Attributes.Add("class", "tab-pane fade");
                        f.Attributes.Add("class", "tab-pane fade");
                        mc.Attributes.Add("class", "tab-pane fade in active");
                        p.Attributes.Add("class", "tab-pane fade");
                        a.Attributes.Add("class", "tab-pane fade");
                    }
                    else if (tab == 5)
                    {
                        PTab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade");
                        smp.Attributes.Add("class", "tab-pane fade");
                        f.Attributes.Add("class", "tab-pane fade");
                        mc.Attributes.Add("class", "tab-pane fade");
                        p.Attributes.Add("class", "tab-pane fade in active");
                        a.Attributes.Add("class", "tab-pane fade");
                    }
                    else if(tab == 6)
                    {
                        ATab.Attributes.Add("class", "active");
                        mp.Attributes.Add("class", "tab-pane fade");
                        smp.Attributes.Add("class", "tab-pane fade");
                        f.Attributes.Add("class", "tab-pane fade");
                        mc.Attributes.Add("class", "tab-pane fade");
                        p.Attributes.Add("class", "tab-pane fade");
                        a.Attributes.Add("class", "tab-pane fade in active");
                    }
                }
            }
        }

        private void CleanTab()
        {
            MPTab.Attributes.Clear();
            SMPTab.Attributes.Clear();
            FTab.Attributes.Clear();
            MCTab.Attributes.Clear();
            PTab.Attributes.Clear();
            ATab.Attributes.Clear();
        }

        private void CleanTabContent()
        {
            mp.Attributes.Clear();
            smp.Attributes.Clear();
            f.Attributes.Clear();
            mc.Attributes.Clear();
            p.Attributes.Clear();
            a.Attributes.Clear();
        }

        public void ResetTabs()
        {
            MPTab.Attributes.Remove("class");
            SMPTab.Attributes.Remove("class");
            FTab.Attributes.Remove("class");
            MCTab.Attributes.Remove("class");
            PTab.Attributes.Remove("class");
            ATab.Attributes.Remove("class");

            mp.Attributes.Remove("class");
            smp.Attributes.Remove("class");
            f.Attributes.Remove("class");
            mc.Attributes.Remove("class");
            p.Attributes.Remove("class");
            a.Attributes.Remove("class");

            mp.Attributes.Add("class", "tab-pane fade");
            smp.Attributes.Add("class", "tab-pane fade");
            f.Attributes.Add("class", "tab-pane fade");
            mc.Attributes.Add("class", "tab-pane fade");
            p.Attributes.Add("class", "tab-pane fade");
            a.Attributes.Add("class", "tab-pane fade");
        }

        public void Bind_MP()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 1 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                    <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                    <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
								    <td>" + dl.UploadDate + @"</td>											
								    <td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=1'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                + @"</td>
                                    <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                    </tr>";
                count++;
            }
            tableManageDownload1.InnerHtml = html;
        }

        public void Bind_SMP()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 2 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                    <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                    <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
									<td>" + dl.UploadDate + @"</td>											
									<td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=2'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                + @"</td>
                                    <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                    </tr>";
                count++;
            }
            tableManageDownload2.InnerHtml = html;
        }

        public void Bind_F()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 3 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                    <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                    <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
									<td>" + dl.UploadDate + @"</td>											
									<td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=3'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                + @"</td>
                                    <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                    </tr>";
                count++;
            }
            tableManageDownload3.InnerHtml = html;
        }

        public void Bind_MC()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 4 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                        <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                        <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
            	                        <td>" + dl.UploadDate + @"</td>											
            	                        <td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=4'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                    + @"</td>
                                        <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                        </tr>";
                count++;
            }
            tableManageDownload4.InnerHtml = html;
        }

        public void Bind_P()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 5 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                        <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                        <td>" + dl.FileAuthor + @"</td>	
                                        <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
            	                        <td>" + dl.DisplayDate.ToString("dd-MM-yyyy") + @"</td>	
            	                        <td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=5'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                    + @"</td>
                                        <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                        </tr>";
                count++;
            }
            tableManageDownload5.InnerHtml = html;
        }

        public void Bind_A()
        {
            List<Download> downloadList = DownloadService.GetAllDownload().Where(x => x.DownloadType_Id == 6 && x.Status == 1).ToList();
            string html = "";
            int count = 1;
            foreach (Download dl in downloadList)
            {
                html = html + @"<tr data-id='" + dl.Id + @"'><td>" + count + @"</td>
                                            <td><a target='_blank' href='" + dl.UrlPath + @"'>" + dl.FileName + @"</td>
                                            <td>" + dl.FileAuthor + @"</td>	    
                                            <td>" + ConvertBytesToMegaBytes(dl.FileSize) + @"</td>
            		                        <td>" + dl.DisplayDate.ToString("dd-MM-yyyy") + @"</td>											
            		                        <td style='text-align: center;'>" + "<a href='EditDownload.aspx?id=" + dl.Id + @"&tab=6'class='btn-sm btn-info' data-toggle='tooltip' title='Edit'><i class='fa fa-edit'>Edit</i></a>"
                                        + @"</td>
                                            <td style='text-align: center;'><a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Delete'><i class='fa fa-remove'></i></a></td>
                                            </tr>";
                count++;
            }
            tableManageDownload6.InnerHtml = html;
        }


        public string ConvertBytesToMegaBytes(string bytes)
        {
            double size = (Convert.ToDouble(bytes) / 1024) / 1024;
            return string.Format("{0:0.00} MB", size);
        }

        protected void btnAction1_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID1.Value);
                string Action = hdnAction1.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                MPTab.Attributes.Add("class", "active");
                mp.Attributes.Remove("class");
                mp.Attributes.Add("class", "tab-pane fade active in");
                Bind_MP();
            }
            else
            {
                ResetTabs();
                MPTab.Attributes.Add("class", "active");
                mp.Attributes.Remove("class");
                mp.Attributes.Add("class", "tab-pane fade active in");
                Bind_MP();
            }
        }

        protected void btnAction2_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID2.Value);
                string Action = hdnAction2.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                SMPTab.Attributes.Add("class", "active");
                smp.Attributes.Remove("class");
                smp.Attributes.Add("class", "tab-pane fade active in");
                Bind_SMP();
            }
            else
            {
                ResetTabs();
                SMPTab.Attributes.Add("class", "active");
                smp.Attributes.Remove("class");
                smp.Attributes.Add("class", "tab-pane fade active in");
                Bind_SMP();
            }
        }

        protected void btnAction3_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID3.Value);
                string Action = hdnAction3.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                FTab.Attributes.Add("class", "active");
                f.Attributes.Remove("class");
                f.Attributes.Add("class", "tab-pane fade active in");
                Bind_F();
            }
            else
            {
                ResetTabs();
                FTab.Attributes.Add("class", "active");
                f.Attributes.Remove("class");
                f.Attributes.Add("class", "tab-pane fade active in");
                Bind_F();
            }
        }

        protected void btnAction4_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID4.Value);
                string Action = hdnAction4.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                MCTab.Attributes.Add("class", "active");
                mc.Attributes.Remove("class");
                mc.Attributes.Add("class", "tab-pane fade active in");
                Bind_MC();
            }
            else
            {
                ResetTabs();
                MCTab.Attributes.Add("class", "active");
                mc.Attributes.Remove("class");
                mc.Attributes.Add("class", "tab-pane fade active in");
                Bind_MC();
            }


        }

        protected void btnAction5_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID5.Value);
                string Action = hdnAction5.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                PTab.Attributes.Add("class", "active");
                p.Attributes.Remove("class");
                p.Attributes.Add("class", "tab-pane fade active in");
                Bind_P();
            }
            else
            {
                ResetTabs();
                PTab.Attributes.Add("class", "active");
                p.Attributes.Remove("class");
                p.Attributes.Add("class", "tab-pane fade active in");
                Bind_P();
            }
        }

        protected void btnAction6_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = Convert.ToInt32(hdnUserID6.Value);
                string Action = hdnAction6.Value;

                Download dl = DownloadService.GetAllDownload().Where(x => x.Id == userID).FirstOrDefault();

                switch (Action)
                {
                    case "Delete":
                        dl.Status = 0;
                        DownloadService.Update(dl);
                        break;
                }
                ResetTabs();
                ATab.Attributes.Add("class", "active");
                a.Attributes.Remove("class");
                a.Attributes.Add("class", "tab-pane fade active in");
                Bind_A();
            }
            else
            {
                ResetTabs();
                ATab.Attributes.Add("class", "active");
                a.Attributes.Remove("class");
                a.Attributes.Add("class", "tab-pane fade active in");
                Bind_A();
            }
        }
    }
}