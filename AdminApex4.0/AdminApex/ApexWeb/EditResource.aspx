﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditResource.aspx.cs" Inherits="AdminApex.ApexWeb.EditResource" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Download</h2>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>File Type</label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlFileType" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged"></asp:DropDownList><br />
                        <asp:RadioButtonList ID="rblFund" runat="server" AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>                    
                </div>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>File Name</label>
                    </div>
                    <div class="col-md-9">
                        <%--<asp:TextBox ID="txtFileName" runat="server" CssClass="form-control"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlFileName" runat="server" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                        <br />
                    </div>                    
                </div>             
                <div class="row mt-10">
                    <div class="col-md-9 col-md-offset-3">                        
                        <asp:HyperLink ID="hlFile" runat="server" Target="_blank">Click here to view</asp:HyperLink>
                        <asp:FileUpload ID="fuFile" runat="server" />
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                     &nbsp <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click"  />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
