﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class ViewAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Admin();
        }

        public void Bind_Admin()
        {
            List<Admin> adminList = AdminData.GetAllAdmin();
            string html = "";
            int count = 1;
            foreach (Admin admin in adminList)
            {
                html = html + @"<tr data-id='" + admin.Id + @"'>
                                    <td>" + count + @"</td>                                   
                                    <td>" + admin.name + @"</td>
                                    <td>" + admin.Department + @"</td>
                                    <td>" + admin.Email + @"</td>
                                    <td style='text-align: center;'>" + (admin.Status == 1 ?
                                                 "<i class='fa fa-circle  txt-success' style='color:#64DF6F' title='Activate'></i>"
                                                 :
                                                 "<i class='fa fa-circle  txt-danger' style='color:red' title='Suspended'></i>"
                                                 ) + @"
                                            </td>
                                    <td style='text-align: center;'>
                                                " + ((admin.Status == 1) ?
                                            "<a href='javascript:;' class='btn-sm btn-danger' data-toggle='tooltip' title='Block'><i class='fa fa-ban'></i></a>"
                                            :
                                            "<a href='javascript:;' class='btn-sm btn-success' data-toggle='tooltip' title='Unblock'><i class='fa fa-check'></i></a>"
                                            )
                                            + @"</td>
								    </tr>";
                count++;
            }
            tableManageAdmin.InnerHtml = html;
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(hdnUserID.Value);
            string Action = hdnAction.Value;

            Admin admin = AdminData.GetAllAdmin().Where(x => x.Id == userID).FirstOrDefault();
            switch (Action)
            {
                case "Block":
                    admin.Status = 0;
                    AdminData.Update(admin);
                    break;
                case "Unblock":
                    admin.Status = 1;
                    AdminData.Update(admin);
                    break;
            }
            Bind_Admin();
        }
    }
}