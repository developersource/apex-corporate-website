﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="AdminApex.ApexWeb.AddNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Add News</h2>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>Title</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txt_title" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Picture</label>
                    </div>
                    <div class="col-md-9">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Content</label>
                    </div>
                    <div class="col-md-9">
                        <textarea id="txt_Content" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:CheckBoxList ID="chkboxfundname" runat="server"></asp:CheckBoxList>
                    </div>
                </div>
               <%-- For announcement--%>
                <%--<div class="row mt-10 hide">
                          <div class="col-md-3">
                        <label>Annoucement Status</label>
                        </div>
                    <div class="col-md-9">
                        <asp:CheckBox id="chkboxAnnoucement" runat="server" text="Activate"/>
                    </div>
                </div>--%>
                <div class="row mt-10" id="annoucement_txt" style="display:none;">
                    <div class="col-md-3">
                        <label>Description</label>
                    </div>
                    <div class="col-md-9">
                        <textarea id="txt_Desc" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="btnsubmit1" runat="server" Text="Save" CssClass="btn" OnClick="btnsubmit1_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        <%--$(document).ready(function () {
            $('#<%=chkboxAnnoucement.ClientID %>').change(function () {
                if (this.checked) {
                    $('#annoucement_txt').show();
                }
                else {
                    $('#annoucement_txt').hide();
                }
            });
        });--%>
    </script>
</asp:Content>
