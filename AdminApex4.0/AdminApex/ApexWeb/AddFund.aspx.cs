﻿using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AdminApex.ApexService;
using AdminApex.ApexData;
using System.Text.RegularExpressions;

namespace AdminApex
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<DownloadType> downloadTypeList = DownloadTypeService.GetAllDownloadType();
                ddlFileType.Items.Clear();
                foreach (DownloadType downloadType in downloadTypeList)
                {
                    if (downloadType.Id > 6)
                    {
                        ddlFileType.Items.Add(new ListItem(downloadType.TypeName, downloadType.Id.ToString()));
                    }
                }
            }
        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (fundlogo.HasFile)
                {
                    if (txt_fund_code.Text != "" && txt_epf_approved.Text != "" && txt_fund_category.Text != "" && txt_fund_name.Text != "" && txt_inves_stra.InnerText != "" &&
                        txt_launch_date.Text != "" && txt_manage_fee.Text != "" && txt_min_ini_inves.Text != "" && txt_min_sub_inves.Text != "" && txt_pot_inves.InnerText != "" &&
                        txt_redemp_fee.Text != "" && txt_sales_charge.Text != "" && txt_trustee.Text != "" && txt_trustee_fee.Text != "")
                    {

                        ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                        Fund fund = new Fund();

                        string url = "/FundLogo/";
                        string path = Server.MapPath(url);
                        string filename = Path.GetFileName(fundlogo.FileName);

                        path = path + filename;
                        fundlogo.PostedFile.SaveAs(path);

                        //int numbers = Convert.ToInt32(Regex.Match(txt_min_sub_inves.Text, @"(\d+)$").Value);
                        //var letters = new String(txt_min_sub_inves.Text.Where(c => Char.IsLetter(c) || Char.IsPunctuation(c)).ToArray());

                        string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                        DateTime currenttime = DateTime.Parse(time);
                        fund.CreatedBy = admin.Id;
                        fund.CreatedDate = currenttime;
                        fund.Status = 1;
                        fund.url_path = url + filename;
                        fund.fund_code = txt_fund_code.Text;
                        fund.fund_name = txt_fund_name.Text;
                        fund.DownloadType_Id = Convert.ToInt32(ddlFileType.SelectedValue);
                        fund.epfapproved = txt_epf_approved.Text;
                        fund.potential_inves = txt_pot_inves.InnerText;
                        fund.inves_strategy = txt_inves_stra.InnerText;
                        fund.launch_date = Convert.ToDateTime(txt_launch_date.Text);
                        fund.trustee = txt_trustee.Text;
                        fund.fund_category = txt_fund_category.Text;
                        fund.sales_charge = txt_sales_charge.Text;
                        fund.manage_fee = txt_manage_fee.Text;
                        fund.trustee_fee = txt_trustee_fee.Text;
                        fund.min_ini_inves = txt_min_ini_inves.Text;
                        fund.min_sub_inv = txt_min_sub_inves.Text;
                        fund.redemp_fee = txt_redemp_fee.Text;
                        fund = FundService.Insert(fund);

                        //user log   
                        WriteTextFile wtf = new WriteTextFile();
                        UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                        string[] data = { DateTime.Now.ToString(), admin.name, "Add New Fund - " + fund.fund_name, "Success" };
                        wtf.Write(Server.MapPath(userLog.UrlPath), data);

                        Response.Write("<script>window.alert('The Fund was successfully added !');</script>");
                    }
                    else
                    {
                        Response.Write("<script>window.alert('The text field cannot be empty !');</script>");
                    }
                }
                else
                {
                    Response.Write("<script>window.alert('Please select an image !');</script>");
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }
    }
}