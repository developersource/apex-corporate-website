﻿using AdminApex.ApexData;
using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex.ApexWeb
{
    public partial class AddFinancialInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_ipcap.Text != "" && txt_shareholder.Text != "" && txt_revenue.Text != "" && txt_plaftertax.Text != "" && txt_plbeforetax.Text != "" &&
                    txt_cbdeposits.Text != "" && txt_bankborrow.Text != "")
                {

                    ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                    FinancialInfo fi = new FinancialInfo();

                    string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime currenttime = DateTime.Parse(time);
                    fi.CreatedBy = admin.Id;
                    fi.CreatedDate = currenttime;
                    fi.Status = 1;
                    fi.year = Convert.ToInt32(ddlyear.SelectedValue);
                    fi.issue_paidup_capital = Convert.ToDecimal(txt_ipcap.Text);
                    fi.shareholder_fund = Convert.ToDecimal(txt_shareholder.Text);
                    fi.revenue = Convert.ToDecimal(txt_revenue.Text);
                    fi.profit_loss_before_tax = txt_plbeforetax.Text;
                    fi.profit_loss_after_tax = txt_plaftertax.Text;
                    fi.cash_bank_deposits = Convert.ToDecimal(txt_cbdeposits.Text);
                    fi.bank_borrowings = txt_bankborrow.Text;
                    fi = FinancialInfoService.Insert(fi);

                    //user log   
                    WriteTextFile wtf = new WriteTextFile();
                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                    string[] data = { DateTime.Now.ToString(), admin.name, "Add New Financial Info - " + fi.year, "Success" };
                    wtf.Write(Server.MapPath(userLog.UrlPath), data);

                    Response.Write("<script>window.alert('The Info was successfully added !');</script>");
                }
                else
                {
                    Response.Write("<script>window.alert('The text field cannot be empty !');</script>");
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        protected void btnsubmit2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_nooffund.Text != "" && txt_totalfund.Text != "")
                {
                    ApexUtility.Admin admin = (ApexUtility.Admin)Session["admin"];

                    FundSummary fs = new FundSummary();

                    string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime currenttime = DateTime.Parse(time);
                    fs.CreatedBy = admin.Id;
                    fs.CreatedDate = currenttime;
                    fs.Status = 1;
                    fs.fund_categ = ddlfunc.Text;
                    fs.no_of_fund = Convert.ToInt32(txt_nooffund.Text);
                    fs.total_value = Convert.ToDecimal(txt_totalfund.Text);
                    fs = FundSummaryService.Insert(fs);

                    //user log
                    WriteTextFile wtf = new WriteTextFile();
                    string path = Server.MapPath("/Log/");
                    string[] data = { DateTime.Now.ToString(), admin.name, "Add Fund Summary", "Success" };
                    wtf.Write(path, data);

                    Response.Write("<script>window.alert('The Summary was successfully added !');</script>");
                }
                else
                {
                    Response.Write("<script>window.alert('The text field cannot be empty !');</script>");
                }
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        protected void btnsubmit3_Click(object sender, EventArgs e)
        {
            try
            {
                Admin admin = (Admin)Session["admin"];

                DateTime NavDate = Convert.ToDateTime(txtNavDate.Text);

                //update db
                FundSummary fs = FundSummaryService.GetAllFinancialSummary().Where(x => x.Status == 99).FirstOrDefault();
                fs.CreatedDate = NavDate;
                FundSummaryService.UpdateDate(fs);

                //user log
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                string[] data = { DateTime.Now.ToString(), admin.name, "Edit NAV Date - " + fs.CreatedDate.ToString("dd-MM-yyyy"), "Success" };
                wtf.Write(Server.MapPath(userLog.UrlPath), data);

                Response.Write("<script>window.alert('The NAV Date was successfully added !');</script>");
                Clear();
            }
            catch (Exception a)
            {
                Console.WriteLine("Exception: " + a.Message);
            }
        }

        public void Clear()
        {
            txtNavDate.Text = "";
        }

        //protected void txt_totalfund_TextChanged(object sender, EventArgs e)
        //{
        //    Double value;
        //    if (Double.TryParse(txt_totalfund.Text, out value))
        //        txt_totalfund.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0:C2}", value);
        //    else
        //        txt_totalfund.Text = String.Empty;
        //}
    }
}