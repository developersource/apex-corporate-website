﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class FundnewService
    {
        public static fund_news Insert(fund_news news)
        {
            return fundnewsData.Insert(news);
        }

        public static List<fund_news> GetGetByID(Int32 id)
        {
            List<fund_news> fn = fundnewsData.GetByID(id);  
            List<fund_news> fnl = new List<fund_news>();


            foreach (fund_news f in fn)
            {
                f.fund = FundData.FundGetByID(f.fund_news_fund_id);
                fnl.Add(f);
            }
            return fnl;
        }

        //public static List<fund_news> Update(fund_news fn, int id)
        //{
        //    return fundnewsData.Update(fn, id);
        //}

    }
}