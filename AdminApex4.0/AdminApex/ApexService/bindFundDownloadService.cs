﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class bindFundDownloadService
    {
        public static List<bindFundDownloads> bfd()
        {
            List<bindFundDownloads> bfds = bindFundDownloadData.bfdGetAll();
            List<bindFundDownloads> bfdsN = new List<bindFundDownloads>();
            foreach (bindFundDownloads bfd in bfds)
            {
                bfd.downloadFiles = utmcData.GetFundResourceByFundid(bfd.fundid);
                bfdsN.Add(bfd);
            }

            return bfdsN;
        }
    }
}