﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class DownloadTypeService
    {
        public static DownloadType Insert(DownloadType downloadType)
        {
            return DownloadTypeData.Insert(downloadType);
        }

        public static List<DownloadType> GetAllDownloadType()
        {
            List<DownloadType> downloadTypeData = DownloadTypeData.GetAllDownloadType();
            List<DownloadType> downloadTypeDataNew = new List<DownloadType>();

            foreach (DownloadType downloadType in downloadTypeData)
            {
                downloadType.Download = DownloadData.GetAllDownload().Where(x => x.DownloadType_Id == downloadType.Id).ToList();
                downloadType.Fund = FundData.FundGetAll().Where(x => x.DownloadType_Id == downloadType.Id).ToList();
                //downloadType.Achives = AchivesData.GetAllAchives().Where(x => x.DownloadType_Id == downloadType.Id).ToList();

                downloadTypeDataNew.Add(downloadType);

            }
            return downloadTypeDataNew;
        }

        public static DownloadType GetById(int Id)
        {
            DownloadType downloadType = DownloadTypeData.GetById(Id);

            downloadType.Download = DownloadData.GetAllDownload().Where(x => x.DownloadType_Id == downloadType.Id).ToList();
            downloadType.Fund = FundData.FundGetAll().Where(x => x.DownloadType_Id == downloadType.Id).ToList();
            //downloadType.Achives = AchivesData.GetAllAchives().Where(x => x.DownloadType_Id == downloadType.Id).ToList();

            return downloadType;
        }

        public static DownloadType Update(DownloadType downloadType)
        {
            return DownloadTypeData.Update(downloadType);
        }

        public static void Delete(int id)
        {
            DownloadTypeData.Delete(id);
        }
    }
}