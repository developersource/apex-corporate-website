﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class UserLogService
    {
        public static UserLog Insert(UserLog userLog)
        {
            return UserLogData.Insert(userLog);
        }

        public static List<UserLog> GetAllUserLog()
        {
            List<UserLog> userLogData = UserLogData.GetAllUserLog();
            List<UserLog> userLogDataNew = new List<UserLog>();

            foreach (UserLog userLog in userLogData)
            {
                userLogDataNew.Add(userLog);
            }
            return userLogDataNew;
        }

        public static UserLog GetById(int Id)
        {
            UserLog userLogData = UserLogData.GetById(Id);

            return userLogData;
        }

        public static UserLog Update(UserLog userLog)
        {
            return UserLogData.Update(userLog);
        }

        public static void Update()
        {
            UserLogData.Update();
        }

        public static void UpdateException()
        {
            UserLogData.UpdateException();
        }

    }
}