﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class DailyFundService
    {
        public static List<DailyNavFundUtility> GetAllPrice()
        {
            List<DailyNavFundUtility> DPrice = utmcData.PriceGetAll();
            List<DailyNavFundUtility> DP = new List<DailyNavFundUtility>();

            foreach (DailyNavFundUtility price in DPrice)
            {
                price.FundInfomationUtility = utmcData.GetByid(price.IPD_Fund_Code);
                DP.Add(price);
            }

            return DP;
        }

        public static List<FundDistributionUtility> GetAllDist()
        {
            List<FundDistributionUtility> FDist = utmcData.DistGetAll();
            List<FundDistributionUtility> DP = new List<FundDistributionUtility>();

            foreach (FundDistributionUtility dis in FDist)
            {
                dis.FundInfomationUtility = utmcData.GetByid(dis.IPD_Fund_Code);
                DP.Add(dis);
            }
            return DP;
        }

        public static List<DailyNavFundUtility> GetPrices(string id, DateTime c1, DateTime c2)
        {
            List<DailyNavFundUtility> DPrice = utmcData.GetPrice(id, c1, c2);
            return DPrice;
        }

        public static List<FundDistributionUtility> GetDistribution(string id)
        {
            List<FundDistributionUtility> fd = utmcData.GetDist(id);
            List<FundDistributionUtility> fdd = new List<FundDistributionUtility>();

            foreach (FundDistributionUtility dis in fd)
            {
                dis.FundInfomationUtility = utmcData.GetByid(dis.IPD_Fund_Code);
                fdd.Add(dis);
            }
            return fdd;
        }

    }
}