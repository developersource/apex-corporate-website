﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class FundService
    {
        public static Fund Insert(Fund fund)
        {
            return FundData.Insert(fund);
        }

        public static Fund FundGetById(int id)
        {
            //Fund fund = FundData.FundGetByID(id);
            ////if(fund != null)
            ////{
            ////    fund.ID = FundData.FundGetAll().Where(x => x.ID == fund.ID).ToList();   
            ////}
            //return fund;

            Fund fund = FundData.FundGetByID(id);

            fund.Download = DownloadData.GetAllDownload().Where(x => x.Fund_Id == fund.ID).ToList();
            fund.DownloadType = DownloadTypeData.GetById(fund.DownloadType_Id);

            return fund;
        }

        public static List<Fund> GetAllFund()
        {
            List<Fund> fundData = FundData.FundGetAll();
            List<Fund> fundDataNew = new List<Fund>();

            foreach (Fund fund in fundData)
            {
                fund.Download = DownloadData.GetAllDownload().Where(x => x.Fund_Id == fund.ID).ToList();
                fund.DownloadType = DownloadTypeData.GetById(fund.DownloadType_Id);
                fundDataNew.Add(fund);

            }
            return fundDataNew;
        }


        public static Fund Update(Fund fund, int id)
        {
            return FundData.Update(fund,id);
        }

        public static List<Fund> GetAllFunds()
        {
            List<Fund> fd = FundData.FundGetAll();
            return fd;
        }

        public static List<FundInfomationUtility> GetAllFunds2()
        {
            List<FundInfomationUtility> fd = utmcData.FundGetAll();
            return fd;
        }
    }
}