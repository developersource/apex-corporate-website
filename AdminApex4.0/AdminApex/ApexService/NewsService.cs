﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class NewsService
    {
        public static News Insert(News news)
        {
            return NewsData.Insert(news);
        }

        public static News Update(News n, int id)
        {
            return NewsData.Update(n, id);
        }

        public static News UpdateS(News n)
        {
            return NewsData.UpdateS(n);
        }

        public static List<News> NewsGetAll()
        {
            List<News> n = NewsData.NewsGetAll();
            return n;
        }

        public static List<Fund> FundGetAll()
        {
            List<Fund> n = NewsData.FundGetAll();
            return n;
        }

        public static News NewsGetById(int id)
        {
            News fund = NewsData.NewsGetByID(id);
            return fund;
        }
    }
}