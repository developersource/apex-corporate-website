﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class FinancialInfoService
    {
        public static FinancialInfo Insert(FinancialInfo fi)
        {
            return FinancialInfoData.Insert(fi);
        }

        public static FinancialInfo Update(FinancialInfo fi, string id)
        {
            return FinancialInfoData.Update(fi, id);
        }

        public static FinancialInfo FiGetById(string id)
        {
            FinancialInfo fund = FinancialInfoData.FiGetByID(id);
            //if(fund != null)
            //{
            //    fund.ID = FundData.FundGetAll().Where(x => x.ID == fund.ID).ToList();   
            //}
            return fund;
        }

        public static List<FinancialInfo> GetAllFi()
        {
            List<FinancialInfo> fd = FinancialInfoData.FIGetAll();
            return fd;
        }
    }
}