﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class DistributionService
    {
        public static Distribution Insert(Distribution distribution)
        {
            return DistributionData.Insert(distribution);
        }

        public static List<Distribution> GetAllDistribution()
        {
            List<Distribution> distributionData = DistributionData.GetAllDistribution();
            List<Distribution> distributionDataNew = new List<Distribution>();

            foreach (Distribution distribution in distributionData)
            {                
                distribution.Fund = FundData.FundGetByID(distribution.Fund_Id);
                distributionDataNew.Add(distribution);

            }
            return distributionDataNew;
        }

        public static List<Distribution> GetAllDistributionUI()
        {
            List<Distribution> distributionData = DistributionData.GetAllDistributionUI();
            List<Distribution> distributionDataNew = new List<Distribution>();

            foreach (Distribution distribution in distributionData)
            {
                distribution.Fund = FundData.FundGetByID(distribution.Fund_Id);
                distributionDataNew.Add(distribution);

            }
            return distributionDataNew;
        }

        public static Distribution GetById(int Id)
        {
            Distribution distributionData = DistributionData.GetById(Id);
                        
            distributionData.Fund = FundData.FundGetByID(distributionData.Fund_Id);
            return distributionData;
        }

        public static Distribution Update(Distribution distribution)
        {
            return DistributionData.Update(distribution);
        }

        public static void Delete(int id)
        {
            DistributionData.Delete(id);
        }
    }
}