﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class UnitSplitService
    {
        public static UnitSplit Insert(UnitSplit unitSplit)
        {
            return UnitSplitData.Insert(unitSplit);
        }

        public static List<UnitSplit> GetAllUnitSplit()
        {
            List<UnitSplit> unitSplitData = UnitSplitData.GetAllUnitSplit();
            List<UnitSplit> unitSplitDataNew = new List<UnitSplit>();

            foreach (UnitSplit unitSplit in unitSplitData)
            {
                unitSplit.Fund = FundData.FundGetByID(unitSplit.Fund_Id);
                unitSplitDataNew.Add(unitSplit);

            }

            return unitSplitDataNew;
        }

        public static UnitSplit GetById(int Id)
        {
            UnitSplit unitSplitData = UnitSplitData.GetById(Id);

            unitSplitData.Fund = FundData.FundGetByID(unitSplitData.Fund_Id);
            return unitSplitData;
        }

        public static UnitSplit Update(UnitSplit unitSplit)
        {
            return UnitSplitData.Update(unitSplit);
        }

        public static void Delete(int id)
        {
            UnitSplitData.Delete(id);
        }
    }
}