﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class DownloadService
    {
        public static Download Insert(Download download)
        {
            return DownloadData.Insert(download);
        }

        public static List<Download> GetAllDownload()
        {
            List<Download> downloadData = DownloadData.GetAllDownload();
            List<Download> downloadDataNew = new List<Download>();

            foreach (Download download in downloadData)
            {
                download.DownloadType = DownloadTypeData.GetById(download.DownloadType_Id);
                download.Fund = FundData.FundGetByID(download.Fund_Id);
                downloadDataNew.Add(download);

            }
            return downloadDataNew;
        }

        public static List<Download> GetDownloadByType(int typeid)
        {
            List<Download> downloadData = DownloadData.GetDownloadByType(typeid);
            List<Download> downloadDataNew = new List<Download>();

            foreach (Download download in downloadData)
            {
                download.DownloadType = DownloadTypeData.GetById(download.DownloadType_Id);
                download.Fund = FundData.FundGetByID(download.Fund_Id);
                downloadDataNew.Add(download);

            }
            return downloadDataNew;
        }

        public static Download GetById(int Id)
        {
            Download downloadData = DownloadData.GetById(Id);

            downloadData.DownloadType = DownloadTypeData.GetById(downloadData.DownloadType_Id);
            downloadData.Fund = FundData.FundGetByID(downloadData.Fund_Id);
            return downloadData;
        }

        public static Download Update(Download download)
        {
            return DownloadData.Update(download);
        }

        public static void Delete(int id)
        {
            DownloadData.Delete(id);
        }
    }
}