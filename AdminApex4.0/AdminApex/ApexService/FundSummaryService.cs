﻿using AdminApex.ApexData;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexService
{
    public class FundSummaryService
    {
        public static FundSummary Insert(FundSummary fs)
        {
            return FundSummaryData.Insert(fs);
        }

        public static FundSummary Update(FundSummary fs, string id)
        {
            return FundSummaryData.Update(fs,id);
        }

        public static List<FundSummary> GetAllFs()
        {
            List<FundSummary> fs = FundSummaryData.FsGetAll();
            return fs;
        }

        public static FundSummary FsGetById(string id)
        {
            FundSummary fs = FundSummaryData.FsGetByID(id);
            return fs;
        }

        public static List<FundSummary> GetAllFinancialSummary()
        {
            List<FundSummary> financialSummaryData = FundSummaryData.GetAllFinancialSummmary();
            List<FundSummary> financialSummaryDataaNew = new List<FundSummary>();

            foreach (FundSummary financialSummary in financialSummaryData)
            {
                financialSummaryDataaNew.Add(financialSummary);
            }
            return financialSummaryDataaNew;
        }
        
        public static FundSummary UpdateDate(FundSummary fundSummary)
        {
            return FundSummaryData.UpdateDate(fundSummary);
        }


    }
}