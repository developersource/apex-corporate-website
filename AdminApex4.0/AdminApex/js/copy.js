var nav_about_col_1_url = "/abouts/ourvalues";
var nav_about_col_1_title = "Our Values";
var nav_about_col_1_desc = "Discover our vision and core values.";
var nav_about_col_2_url = "/abouts/financialposition";
var nav_about_col_2_title = "Financial Position";
var nav_about_col_2_desc = "Our financials and information on our Funds.";
var nav_about_col_3_url = "/abouts/timeline";
var nav_about_col_3_title = "Corporate Timeline";
var nav_about_col_3_desc = "Learn the inception and background of our company.";
var nav_about_col_4_url = "/abouts/people";
var nav_about_col_4_title = "Our People";
var nav_about_col_4_desc = "Get to know the key individuals in our organisation and their expertise.";

var nav_products_col_1_url = "/products/unittrust";
var nav_products_col_1_title = "Unit Trust";
var nav_products_col_1_desc = "Explore various unit trusts that are tailored to your personal choice of investment.";
//var nav_products_col_1_desc = "Conventional<br/>Apex Malaysia Growth Trust (Equities, Growth)<br/> Apex Quantum Fund (Equities, Mixed) <br/> Apex Dynamic Fund (Equities, Growth) <br/> Apex Asian (Ex Japan) Fund (Equities, Growth) <br/> <br/> Islamic <br/> Apex Dana Aslah (Equities, Mixed) <br/> Apex Dana Al-Faiz-I (Equities, Balanced) <br/> Apex Dana Al-Sofi-I (Equities, Growth) <br/> Apex Dana Al-Kanz (Money Market) <br/>";
var nav_products_col_2_url = "/products/privatemandates";
var nav_products_col_2_title = "Private Mandates";
//var nav_products_col_2_desc = "Learn the flexibilities of our portfolio management that are suited for each of our client’s investment goals.";
var nav_products_col_2_desc = "Equities <br/> Fixed income <br/>";


var nav_news_col_1_url = "/resources/fundinfo";
var nav_news_col_1_title = "Fund Information";
var nav_news_col_1_desc = "Obtain Fund prices, distribution and splits.";
var nav_news_col_2_url = "/resources/downloads";
var nav_news_col_2_title = "Downloads";
var nav_news_col_2_desc = "Master and supplemental prospectuses, our different forms and reports.";
/*var nav_news_col_3_url = "/resources/archives";
var nav_news_col_3_title = "Archives";
var nav_news_col_3_desc = "Newsletters, podcasts and media articles.";
var nav_news_col_4_url = "/resources/news";
var nav_news_col_4_title = "News";
var nav_news_col_4_desc = "View updates and announcements.";*/