//---------------------------------------------------------------------------------
//Some global function.js variables
//---------------------------------------------------------------------------------
var navbar_height = 80;
var expandedNav_height = 480;
var globalEasing = "easeInOutQuad";
var initChart = 0;
var segmenter;
var tickerWidth = 0;
var loopTicker;
window.cycled = 0;
var elements;
var arrowLoad = 0;
var arrowTrigger = 0;

var mobileBreak = 768;

// var mDevice() = false;
// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() <= mobileBreak ) {
// 	mDevice() = true;
// }

function mDevice() {
    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return 1;
    } else if ($(window).width() <= mobileBreak) {
        return 1;
    } else {
        return 0;
    }

}

//---------------------------------------------------------------------------------
//Show/hide the navbar using true/false variables
//---------------------------------------------------------------------------------
function showNav(a) {
    if (!$(".main-nav-container").is(":animated")) {
        if (mDevice()) {
            if (a) {
                $(".nav-left-bg.bg-transparent").stop().animate({ "top": "0" }, 400, globalEasing)
                $(".nav-right").stop().animate({ "right": "0" }, 400, globalEasing)
                $("body").mCustomScrollbar("disable");
                $("#hamburger-text").html("CLOSE")
            } else {
                $(".nav-left-bg.bg-transparent").stop().animate({ "top": "-80px" }, 400, globalEasing)
                $(".nav-right").stop().animate({ "right": "-100%" }, 400, globalEasing, function () {
                    $("body").mCustomScrollbar("update");
                })
                $("#hamburger-text").html("MENU")
            }
        } else {
            if (a) {
                $(".main-nav-container").stop().animate({ "top": "0px" }, 400, globalEasing)
            } else {
                $(".main-nav-container").stop().animate({ "top": "-" + navbar_height + "px" }, 400, globalEasing)
            }
        }
    }
}

//---------------------------------------------------------------------------------
//Show/hide the expanded nav using true/false variables
//---------------------------------------------------------------------------------
function triggerExpandedNav(elem) {
    $(".login-nav").hide();
    var thisid = $(elem).attr("id");
    // var sublink = window.location.origin+"/"+thisid.split("-")[1];
    //sublink = host+"/"+thisid.split("-")[1];
    sublink = "";
    $(".nav-right ul li a.expandable img").removeClass("flipV")
    switch (thisid) {
        case "nav-about":
            $(".expanded-nav-columns .expanded-nav-columns-box").css({ "background": "white" })
            $(".expanded-nav-columns.one a").attr("href", sublink + nav_about_col_1_url)
            $(".expanded-nav-columns.one .expanded-nav-columns-box h3").html(nav_about_col_1_title)
            $(".expanded-nav-columns.one .expanded-nav-columns-box p").html(nav_about_col_1_desc)
            $(".expanded-nav-columns.two a").attr("href", sublink + nav_about_col_2_url)
            $(".expanded-nav-columns.two .expanded-nav-columns-box h3").html(nav_about_col_2_title)
            $(".expanded-nav-columns.two .expanded-nav-columns-box p").html(nav_about_col_2_desc)
            $(".expanded-nav-columns.three a").attr("href", sublink + nav_about_col_3_url)
            $(".expanded-nav-columns.three .expanded-nav-columns-box h3").html(nav_about_col_3_title)
            $(".expanded-nav-columns.three .expanded-nav-columns-box p").html(nav_about_col_3_desc)
            $(".expanded-nav-columns.four a").attr("href", sublink + nav_about_col_4_url)
            $(".expanded-nav-columns.four .expanded-nav-columns-box h3").html(nav_about_col_4_title)
            $(".expanded-nav-columns.four .expanded-nav-columns-box p").html(nav_about_col_4_desc)

            $(".expanded-nav-columns.one").show()
            $(".expanded-nav-columns.two").show()
            $(".expanded-nav-columns.three").show()
            $(".expanded-nav-columns.four").show()

            $(this).children("img").addClass("flipV")
            showExpandedNav(true)
            break;
        case "nav-invest":
            showExpandedNav(false)
            break;
        case "nav-products":
            $(".expanded-nav-columns .expanded-nav-columns-box").css({ "background": "white" })
            $(".expanded-nav-columns.one a").attr("href", sublink + nav_products_col_1_url)
            $(".expanded-nav-columns.one .expanded-nav-columns-box h3").html(nav_products_col_1_title)
            $(".expanded-nav-columns.one .expanded-nav-columns-box p").html(nav_products_col_1_desc)
            $(".expanded-nav-columns.two a").attr("href", sublink + nav_products_col_2_url)
            $(".expanded-nav-columns.two .expanded-nav-columns-box h3").html(nav_products_col_2_title)
            $(".expanded-nav-columns.two .expanded-nav-columns-box p").html(nav_products_col_2_desc)

            $(".expanded-nav-columns.one").show()
            $(".expanded-nav-columns.two").show()
            $(".expanded-nav-columns.three").hide()
            $(".expanded-nav-columns.four").hide()


            $(this).children("img").addClass("flipV")
            showExpandedNav(true)
            break;
        case "nav-resources":
            $(".expanded-nav-columns .expanded-nav-columns-box").css({ "background": "white" })
            $(".expanded-nav-columns.one a").attr("href", sublink + nav_news_col_1_url)
            $(".expanded-nav-columns.one .expanded-nav-columns-box h3").html(nav_news_col_1_title)
            $(".expanded-nav-columns.one .expanded-nav-columns-box p").html(nav_news_col_1_desc)
            $(".expanded-nav-columns.two a").attr("href", sublink + nav_news_col_2_url)
            $(".expanded-nav-columns.two .expanded-nav-columns-box h3").html(nav_news_col_2_title)
            $(".expanded-nav-columns.two .expanded-nav-columns-box p").html(nav_news_col_2_desc)
            /*$(".expanded-nav-columns.three a").attr("href", sublink + nav_news_col_3_url)
            $(".expanded-nav-columns.three .expanded-nav-columns-box h3").html(nav_news_col_3_title)
            $(".expanded-nav-columns.three .expanded-nav-columns-box p").html(nav_news_col_3_desc)
            $(".expanded-nav-columns.four a").attr("href", sublink + nav_news_col_4_url)
            $(".expanded-nav-columns.four .expanded-nav-columns-box h3").html(nav_news_col_4_title)
            $(".expanded-nav-columns.four .expanded-nav-columns-box p").html(nav_news_col_4_desc)*/

            $(".expanded-nav-columns.one").show()
            $(".expanded-nav-columns.two").show()
            $(".expanded-nav-columns.three").hide()
            $(".expanded-nav-columns.four").hide()

            $(this).children("img").addClass("flipV")
            showExpandedNav(true)
            break;
        case "nav-contact":
            showExpandedNav(false)
            break;
        case "nav-careers":

            break;
        default:
            $(".expanded-nav-columns-box").css({ "background": "white" })
            $(".expanded-nav-columns h2").html("")
            $(".expanded-nav-columns p").html("")
            showExpandedNav(false)
    }
}

function showExpandedNav(a) {
    // if(!$(".expanded-nav").is(":animated")){
    if (a) {
        if (mDevice()) {
            $(".expanded-nav").stop().animate({ "right": "0" }, 400, globalEasing)
            $(".expanded-nav-tint, .close-expanded-nav").stop().fadeIn(400, globalEasing)
        } else {
            $(".expanded-nav").finish().animate({ "top": "0" }, 400, globalEasing)
        }
        // $(".nav-right a, .nav-right a:hover").css({"color":"black"})
        // $(".nav-hover").css({"background":"black"})
        // $(".nav-logo").attr("src", logo_dark_url)
        // $("#nav-login img").attr("src", user_dark_url)
        // $(".nav-bg").removeClass("bg-dark-blue").addClass("bg-transparent")
        // $(".nav-right ul li a.expandable img").removeClass("invert")
    } else {
        if (mDevice()) {
            $(".expanded-nav").stop().animate({ "right": "-100%" }, 400, globalEasing)
            $(".expanded-nav-tint, .close-expanded-nav").stop().fadeOut(400, globalEasing)
        } else {
            $(".expanded-nav").finish().animate({ "top": "-" + (expandedNav_height + 80) + "px" }, 400, globalEasing)
        }
        // $(".nav-right a, .nav-right a:hover").css({"color":"white"})
        // $(".nav-hover").css({"background":"white"})
        // $(".nav-logo").attr("src", logo_url)
        // $("#nav-login img").attr("src", user_url)
        // $(".nav-bg").addClass("bg-dark-blue").removeClass("bg-transparent")
        // $(".nav-right ul li a.expandable img").addClass("invert")
        $(".nav-right ul li a img").removeClass("flipV")
    }
    // }
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//---------------------------------------------------------------------------------
//Subnav engine
//---------------------------------------------------------------------------------
function initSubnav() {
    if (getCookie("popup") != "" && getCookie("popup") != null && getCookie("popup") != undefined) {
        popup = parseInt(getCookie("popup"));

        if (popup == 0) {
            $("body").mCustomScrollbar("update");
            $(".popup-tnc").fadeOut(500, globalEasing);
        }
    }
    if (window.location.pathname.toLowerCase().indexOf("/people") > 0 || window.location.pathname.toLowerCase().indexOf("/unittrust") > 0
        || (window.location.pathname.toLowerCase().indexOf("resources") > 0 && window.location.pathname.toLowerCase().indexOf("news") < 0)) {



        //initialize
        var page = $(".subnav ul li").find("a.active").attr("page");
        $("#mobile_page").html($(".subnav ul li").find("a.active").html())
        $(".subnav-pages").not($("#" + page)).fadeOut(800);
        $("#" + page).stop().fadeIn(800);
        if (!mDevice()) {
            $(".subnav-wrapper .inner-content").show()
            var subpage = $(".subnav ul li").find("a.active");
            var leftPosition = subpage.position().left;
            var liWidth = subpage.width();
            $(".subnav.active-bar").css({ "left": leftPosition, "width": liWidth }, 400, globalEasing)
            //hover around to move the bar
            $(".subnav ul li").hover(function () {
                var leftPosition = $(this).position().left;
                var liWidth = $(this).children().width();
                if (!$(".login-nav").is(":animated")) {
                    $(".subnav.active-bar").stop().animate({ "left": leftPosition, "width": liWidth }, 400, globalEasing)
                }
            })

            //resets position of bar when mouse leaves the subnav
            $(".subnav ul").mouseleave(function () {
                var leftPosition = $(".subnav ul li a.active").position().left;
                var liWidth = $(".subnav ul li a.active").width();
                if (!$(".login-nav").is(":animated")) {
                    $(".subnav.active-bar").stop().animate({ "left": leftPosition, "width": liWidth }, 400, globalEasing)
                }
            })

            $(".subnav.active-bar").show()
        } else {
            $(".subnav-wrapper .inner-content").hide()
            $(".subnav.active-bar").hide()
        }

        //setting active sub navs
        $(".subnav ul li").not($(".special_select ul li")).click(function () {
            $(".subnav ul li a").removeClass("active");
            $(this).children().addClass("active");
            var page = $(this).children().attr("page");
            $(".subnav-pages").not("#" + page).hide()
            $("#" + page).stop().show()
            var thisURL1 = window.location.pathname.split("/")[1];
            var thisURL2 = window.location.pathname.split("/")[2];
            console.log("/" + thisURL1 + "/" + thisURL2 + "/" + page)
            window.history.replaceState("object or string", page, "/" + thisURL1 + "/" + thisURL2 + "/" + page);
            if (mDevice()) {
                $("#mobile_page").html($(".subnav ul li").find("a.active").html())
                $(".subnav .inner-content").slideUp();
            }
        })

        $('a[page=' + window.location.pathname.split('/')[3] + ']').parent('li').click();
        $(".subnav ul").mouseleave();

        //for mobile selector
        $(".special_select").click(function () {
            if (!$(".subnav .inner-content").is(":animated")) {
                if (!$(".subnav .inner-content").is(":visible")) {
                    $(".subnav .inner-content").slideDown();
                } else {
                    $(".subnav .inner-content").slideUp();
                }
            }
        });

    }
}

//---------------------------------------------------------------------------------
//Segmenter
//---------------------------------------------------------------------------------
function segmenter1() {
    $(".second-slider").hide();
    $(".third-slider").hide();
    var headline = document.querySelector('.trigger-headline'),
        segmenter = new Segmenter(document.querySelector('.segmenter.one'), {
            pieces: 4,
            animation: {
                duration: 10000,
                easing: globalEasing,
                delay: 0,
                translateZ: 100
            },
            parallax: true,
            parallaxMovement: { min: 10, max: 20 },
            positions: "random",
            // [
            // 	{top: 0, left: 0, width: 45, height: 45},
            // 	{top: 55, left: 0, width: 45, height: 45},
            // 	{top: 0, left: 55, width: 45, height: 45},
            // 	{top: 55, left: 55, width: 45, height: 45}
            // ],
            onReady: function () {
                // setTimeout(function(){
                segmenter.animate();
                // }, 1000)
            }
        });
    var segmenter2 = new Segmenter(document.querySelector('.segmenter.two'), {});
    var segmenter3 = new Segmenter(document.querySelector('.segmenter.three'), {});
}

function segmenter2() {
    $(".second-slider").show();
    $(".third-slider").hide();
    var headline = document.querySelector('.trigger-headline'),
        segmenter2 = new Segmenter(document.querySelector('.segmenter.two'), {
            pieces: 8,
            animation: {
                duration: 10000,
                easing: globalEasing,
                delay: 100,
                translateZ: 100
            },
            parallax: true,
            parallaxMovement: { min: 10, max: 20 },
            positions: "random",
            onReady: function () {
                // setTimeout(function(){
                segmenter2.animate();
                // }, 1000)
            }
        });
    var segmenter1 = new Segmenter(document.querySelector('.segmenter.one'), {});
    var segmenter3 = new Segmenter(document.querySelector('.segmenter.three'), {});
}

function segmenter3() {
    $(".third-slider").show();
    $(".second-slider").hide();
    var headline = document.querySelector('.trigger-headline'),
        segmenter3 = new Segmenter(document.querySelector('.segmenter.three'), {
            pieces: 5,
            animation: {
                duration: 10000,
                easing: globalEasing,
                delay: 100,
                translateZ: 100
            },
            parallax: true,
            parallaxMovement: { min: 10, max: 20 },
            positions: "random",
            onReady: function () {
                // setTimeout(function(){
                segmenter3.animate();
                // }, 1000)
            }
        });
    var segmenter1 = new Segmenter(document.querySelector('.segmenter.one'), {});
    var segmenter2 = new Segmenter(document.querySelector('.segmenter.two'), {});
}

//---------------------------------------------------------------------------------
//Historical Fund Prices Function
//---------------------------------------------------------------------------------
function getPrices(fundid, sDate, eDate, url, latest, location) {
    var sDay = sDate.getDate(),
        sMonth = sDate.getMonth() + 1,
        sYear = sDate.getFullYear(),
        eDay = eDate.getDate(),
        eMonth = eDate.getMonth() + 1,
        eYear = eDate.getFullYear();

    //give preceeding 0
    if (sMonth < 10) {
        sMonth = "0" + sMonth;
    }
    if (eMonth < 10) {
        eMonth = "0" + eMonth;
    }
    if (sDay < 10) {
        sDay = "0" + sDay;
    }
    if (eDay < 10) {
        eDay = "0" + eDay;
    }

    var newSDate = sYear + "-" + sMonth + "-" + sDay;
    var newEDate = eYear + "-" + eMonth + "-" + eDay;

    // console.log(newEDate)

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: url,
        data: {
            latest: latest,
            fundid: fundid,
            sDate: newSDate,
            eDate: newEDate,
            location: location,
            _method: $("#historical_method").val(),
            _token: document.head.querySelector("[name=csrf-token]").content
        },
        cache: true,
        success: function (data) {
            $("#show_funds").html(data)
            $("#priceTicker").html(data)
            // console.log(data)
        }
    })

}

//---------------------------------------------------------------------------------
//Distribution Prices Function
//---------------------------------------------------------------------------------
function getDist(fundid, url) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: url,
        data: {
            fundid: fundid,
            _method: $("#dist_method").val(),
            _token: $("#dist_csrf").val()
        },
        cache: true,
        success: function (data) {
            $("#distribution_table").html(data)
            // console.log(data)
        }
    })
}

function getWidth() {
    arrowLoad++;
    //console.log(arrowLoad)
    //console.log($("#priceTicker").children(".title").length)
    if (arrowLoad >= $("#priceTicker").children(".title").length && arrowTrigger == 0) {
        $('#priceTicker li').each(function () {
            tickerWidth += $(this).outerWidth(true);
            //console.log($(this))
            //console.log(tickerWidth)
            priceTicker(1, 32)
            arrowTrigger = 1;
        })
    }
}

function loopEngine(left, cycled, elements, speed, interval) {
    clearInterval(loopTicker);
    loopTicker = setInterval(function () {
        // console.log(cycled)
        if (-$("#priceTicker li:first-child").position().left >= (tickerWidth - $("#priceTicker").outerWidth()) && window.cycled == 0) {
            $("#priceTicker").children().clone().appendTo("#priceTicker")
            // console.log("passed")
            window.cycled = 1;
        }
        if (-$("#priceTicker li:first-child").position().left >= tickerWidth) {
            // $("#priceTicker li:nth-child()").html("")
            left = 0;
        }
        $("#priceTicker li").css({ "left": left })
        left = left - speed;
    }, interval)
}

function priceTicker(speed, interval) {
    if (window.location.pathname == "/") {
        var left = 0;
        cycled = 0;
        elements = $("#priceTicker").children(".title").length;
        loopEngine(left, cycled, elements, speed, interval);
    }
}

function resetAnim() {
    $(".slider-item").removeClass("animate-next").removeClass("animate-prev").css({ "z-index": "1", "display": "none" })
    $(".segmenter").removeClass("animate");


    //$(".slider-item .font-white").css('background', '#ff0000');
    //console.log('reset anim')

}

function animLand(page) {
    resetAnim();
    switch (page) {
        case 1:
            $(".first-slider").addClass("animate-next").css({ "z-index": "2", "display": "block" })
            $(".third-slider").addClass("animate-prev").css({ "z-index": "3", "display": "block" })
            $(".one.segmenter").addClass("animate")
            break;
        case 2:
            $(".second-slider").addClass("animate-next").css({ "z-index": "2", "display": "block" })
            $(".first-slider").addClass("animate-prev").css({ "z-index": "3", "display": "block" })
            $(".two.segmenter").addClass("animate")
            break;
        case 3:
            $(".third-slider").addClass("animate-next").css({ "z-index": "2", "display": "block" })
            $(".second-slider").addClass("animate-prev").css({ "z-index": "3", "display": "block" })
            $(".three.segmenter").addClass("animate")
            break;
    }
}

function resetTable() {
    if (mDevice()) {
        if ($(document).width() > 768) {
            $(".year-1, .year-2, .year-3").show()
            console.log("reach")
        } else {
            $(".year-1, .year-2, .year-3").hide()
            $(".mobile-years:nth-child(1)").trigger("click")
        }
    } else {
        $(".year-1, .year-2, .year-3").show()
    }
}