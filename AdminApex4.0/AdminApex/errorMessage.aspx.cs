﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class errorMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("<script>window.alert('No file for this Fund !');</script>");
            string closeWindowScript = "<script language=javascript>window.top.close();</script>";
            if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "clientScript", closeWindowScript);
            }
        }
    }
}