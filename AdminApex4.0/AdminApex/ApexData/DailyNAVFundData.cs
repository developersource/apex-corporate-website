﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class DailyNAVFundData
    {
        public static List<DailyNAVFund> GetByFundCode(string fundCode)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select * from utmc_daily_nav_fund WHERE IPD_Fund_Code = @IPD_Fund_Code";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", fundCode);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<DailyNAVFund> dnf = new List<DailyNAVFund>();
            while (mdr.Read())
            {
                DailyNAVFund dailyNAVFund = new DailyNAVFund();
                dailyNAVFund.FundCode = mdr["IPD_Fund_Code"].ToString();
                dailyNAVFund.DailyNavDate = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
                dailyNAVFund.DailyUnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                dnf.Add(dailyNAVFund);
            }
            con.Close();
            return dnf;
        }

        public static List<DailyNAVFund> GetFundByFundId(string fundId)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select * from utmc_daily_nav_fund WHERE Id = @FundId";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@FundId", fundId);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<DailyNAVFund> dnf = new List<DailyNAVFund>();
            while (mdr.Read())
            {
                DailyNAVFund dailyNAVFund = new DailyNAVFund();
                dailyNAVFund.FundCode = mdr["IPD_Fund_Code"].ToString();
                dailyNAVFund.DailyNavDate = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
                dailyNAVFund.DailyUnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                dnf.Add(dailyNAVFund);
            }
            con.Close();
            return dnf;
        }
    }
}