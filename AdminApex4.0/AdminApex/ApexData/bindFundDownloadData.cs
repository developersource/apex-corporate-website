﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class bindFundDownloadData
    {
        public static List<bindFundDownloads> bfdGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund where id != 1";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<bindFundDownloads> bfds = new List<bindFundDownloads>();

            while (mdr.Read())
            {
                bindFundDownloads bfd = new bindFundDownloads();

                bfd.fundid = Convert.ToInt32(mdr["id"].ToString());
                bfd.fundName = mdr["fund_name"].ToString();

                bfds.Add(bfd);
            }
            con.Close();
            return bfds;
        }
    }
}