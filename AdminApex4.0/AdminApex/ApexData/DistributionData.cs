﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class DistributionData
    {
        public static Distribution Insert(Distribution distribution)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO distribution(
                            distribution_fund_id,
                            entitlement_date,
                            gross,
                            created_by, 
                            created_date, 
                            status) VALUES(
                            @distribution_fund_id,
                            @entitlement_date,
                            @gross,
                            @created_by, 
                            @created_date,
                            @status)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@distribution_fund_id", distribution.Fund_Id);
            cmd.Parameters.AddWithValue("@entitlement_date", distribution.EntitlementDate);
            cmd.Parameters.AddWithValue("@gross", distribution.Gross);
            cmd.Parameters.AddWithValue("@created_by", distribution.CreatedBy);
            cmd.Parameters.AddWithValue("@created_date", distribution.CreatedDate);
            cmd.Parameters.AddWithValue("@status", distribution.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return distribution;
        }

        public static List<Distribution> GetAllDistribution()
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"SELECT * FROM distribution";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Distribution> distributionList = new List<Distribution>();

            while (mdr.Read())
            {
                Distribution distribution = new Distribution();
                distribution.Id = Convert.ToInt32(mdr["id"].ToString());
                distribution.Fund_Id = Convert.ToInt32(mdr["distribution_fund_id"].ToString());
                distribution.EntitlementDate = Convert.ToDateTime(mdr["entitlement_date"].ToString());
                distribution.Gross = Convert.ToDecimal(mdr["gross"].ToString());
                distribution.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                distribution.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                distribution.Status = Convert.ToInt32(mdr["status"].ToString());

                distributionList.Add(distribution);
            }
            conn.Close();
            return distributionList;
        }

        public static List<Distribution> GetAllDistributionUI()
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"select * from distribution A 
                             inner join (select distribution_fund_id, max(entitlement_date) as DateMAx
                             from distribution group by distribution_fund_id) B 
                             on A.distribution_fund_id=B.distribution_fund_id and A.entitlement_date=B.DateMAx
                             where A.entitlement_date >= DATE_SUB(DATE_FORMAT(CURRENT_DATE,'%Y-%m-01'),INTERVAL 1 YEAR)";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Distribution> distributionList = new List<Distribution>();

            while (mdr.Read())
            {
                Distribution distribution = new Distribution();
                distribution.Id = Convert.ToInt32(mdr["id"].ToString());
                distribution.Fund_Id = Convert.ToInt32(mdr["distribution_fund_id"].ToString());
                distribution.EntitlementDate = Convert.ToDateTime(mdr["entitlement_date"].ToString());
                distribution.Gross = Convert.ToDecimal(mdr["gross"].ToString());
                distribution.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                distribution.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                distribution.Status = Convert.ToInt32(mdr["status"].ToString());

                distributionList.Add(distribution);
            }
            conn.Close();
            return distributionList;
        }

        public static Distribution GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from distribution where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            Distribution distribution = new Distribution();
            if (mdr.Read())
            {
                distribution.Id = Convert.ToInt32(mdr["id"].ToString());
                distribution.Fund_Id = Convert.ToInt32(mdr["distribution_fund_id"].ToString());
                distribution.EntitlementDate = Convert.ToDateTime(mdr["entitlement_date"].ToString());
                distribution.Gross = Convert.ToDecimal(mdr["gross"].ToString());
                distribution.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                distribution.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                distribution.Status = Convert.ToInt32(mdr["status"].ToString());

            }
            conn.Close();
            return distribution;
        }

        public static Distribution Update(Distribution distribution)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE distribution SET distribution_fund_id = @distribution_fund_id,
                           entitlement_date = @entitlement_date,
                           gross = @gross,
                           created_by = @created_by,
                           created_date = @created_date,
                           status = @status 
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", distribution.Id);
            cmd.Parameters.AddWithValue("@distribution_fund_id", distribution.Fund_Id);
            cmd.Parameters.AddWithValue("@entitlement_date", distribution.EntitlementDate);
            cmd.Parameters.AddWithValue("@created_by", distribution.CreatedBy);
            cmd.Parameters.AddWithValue("@created_date", distribution.CreatedDate);
            cmd.Parameters.AddWithValue("@gross", distribution.Gross);
            cmd.Parameters.AddWithValue("@status", distribution.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return distribution;
        }

        public static void Delete(int id)
        {
            using (MySqlConnection sqlCon = DBconnection.GetConnection())
            {
                using (MySqlCommand cmdGet = new MySqlCommand())
                {
                    cmdGet.CommandText = @"DELETE FROM distribution where id = @id";

                    cmdGet.Parameters.AddWithValue("@id", id);
                    cmdGet.Connection = sqlCon;
                    sqlCon.Open();
                    int n = cmdGet.ExecuteNonQuery();
                    sqlCon.Close();
                }
            }

        }
    }
}