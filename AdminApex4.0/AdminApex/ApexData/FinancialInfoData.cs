﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class FinancialInfoData
    {
        public static FinancialInfo Insert(FinancialInfo fi)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"insert into key_financial_info(year, issue_paidup_capital ,shareholder_fund, revenue, profit_loss_before_tax, profit_loss_after_tax, cash_bank_deposits, bank_borrowings, created_date, created_by, status) values
                         (@year, @issue_paidup_capital, @shareholder_fund, @revenue, @profit_loss_before_tax, @profit_loss_after_tax, @cash_bank_deposits, @bank_borrowings, @created_date, @created_by, @status)";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@year", fi.year);
            cmd.Parameters.AddWithValue("@issue_paidup_capital", fi.issue_paidup_capital);
            cmd.Parameters.AddWithValue("@shareholder_fund", fi.shareholder_fund);
            cmd.Parameters.AddWithValue("@revenue", fi.revenue);
            cmd.Parameters.AddWithValue("@profit_loss_before_tax", fi.profit_loss_before_tax);
            cmd.Parameters.AddWithValue("@profit_loss_after_tax", fi.profit_loss_after_tax);
            cmd.Parameters.AddWithValue("@cash_bank_deposits", fi.cash_bank_deposits);
            cmd.Parameters.AddWithValue("@bank_borrowings", fi.bank_borrowings);
            cmd.Parameters.AddWithValue("@created_date", fi.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fi.CreatedBy);
            cmd.Parameters.AddWithValue("@status", fi.Status);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return fi;
        }

        public static FinancialInfo Update(FinancialInfo fi, string id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update key_financial_info set year = @year, 
                                             issue_paidup_capital = @issue_paidup_capital,
                                             shareholder_fund = @shareholder_fund,
                                             revenue = @revenue, 
                                             profit_loss_before_tax = @profit_loss_before_tax, 
                                             profit_loss_after_tax = @profit_loss_after_tax, 
                                             cash_bank_deposits = @cash_bank_deposits, 
                                             bank_borrowings = @bank_borrowings, 
                                             created_date =@created_date, 
                                             created_by = @created_by
                                             where id=@id";

            con.Open();
            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@year", fi.year);
            cmd.Parameters.AddWithValue("@issue_paidup_capital", fi.issue_paidup_capital);
            cmd.Parameters.AddWithValue("@shareholder_fund", fi.shareholder_fund);
            cmd.Parameters.AddWithValue("@revenue", fi.revenue);
            cmd.Parameters.AddWithValue("@profit_loss_before_tax", fi.profit_loss_before_tax);
            cmd.Parameters.AddWithValue("@profit_loss_after_tax", fi.profit_loss_after_tax);
            cmd.Parameters.AddWithValue("@cash_bank_deposits", fi.cash_bank_deposits);
            cmd.Parameters.AddWithValue("@bank_borrowings", fi.bank_borrowings);
            cmd.Parameters.AddWithValue("@created_date", fi.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fi.CreatedBy);

            cmd.ExecuteNonQuery();
            con.Close();
            return fi;

        }

        public static FinancialInfo Update2(FinancialInfo fi)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update key_financial_info set status = @status
                                             where id=@id";

            con.Open();
            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", fi.ID);
            cmd.Parameters.AddWithValue("@status", fi.Status);
            cmd.ExecuteNonQuery();
            con.Close();
            return fi;

        }

        public static List<FinancialInfo> FIGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from key_financial_info";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FinancialInfo> filist = new List<FinancialInfo>();

            while (mdr.Read())
            {
                FinancialInfo fi = new FinancialInfo()
                {
                    ID = Convert.ToInt32(mdr["id"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString()),
                    Status = Convert.ToInt32(mdr["status"].ToString()),
                    year = Convert.ToInt32(mdr["year"].ToString()),
                    issue_paidup_capital = Convert.ToDecimal(mdr["issue_paidup_capital"].ToString()),
                    shareholder_fund = Convert.ToDecimal(mdr["shareholder_fund"].ToString()),
                    revenue = Convert.ToDecimal(mdr["revenue"].ToString()),
                    profit_loss_before_tax = mdr["profit_loss_before_tax"].ToString(),
                    profit_loss_after_tax = mdr["profit_loss_after_tax"].ToString(),
                    cash_bank_deposits = Convert.ToDecimal(mdr["cash_bank_deposits"].ToString()),
                    bank_borrowings = mdr["bank_borrowings"].ToString()
                };
                filist.Add(fi);
            }
            con.Close();
            return filist;
        }

        public static FinancialInfo FiGetByID(string id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from key_financial_info where id=@id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader mdr = cmd.ExecuteReader();

            FinancialInfo fi = new FinancialInfo();

            if (mdr.Read())
            {
                fi.ID = Convert.ToInt32(mdr["id"].ToString());
                fi.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                fi.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                fi.Status = Convert.ToInt32(mdr["status"].ToString());
                fi.year = Convert.ToInt32(mdr["year"].ToString());
                fi.issue_paidup_capital = Convert.ToDecimal(mdr["issue_paidup_capital"].ToString());
                fi.shareholder_fund = Convert.ToDecimal(mdr["shareholder_fund"].ToString());
                fi.revenue = Convert.ToDecimal(mdr["revenue"].ToString());
                fi.profit_loss_before_tax = mdr["profit_loss_before_tax"].ToString();
                fi.profit_loss_after_tax = mdr["profit_loss_after_tax"].ToString();
                fi.cash_bank_deposits = Convert.ToDecimal(mdr["cash_bank_deposits"].ToString());
                fi.bank_borrowings = mdr["bank_borrowings"].ToString();
            }
            con.Close();
            return fi;
        }
    }
}