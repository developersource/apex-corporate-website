﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class ResouresData
    {
        public static List<Download> GetFundResourceByFundid(int fundid)
        {

            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from download where (download_download_type_id = 7 || download_download_type_id = 8 || download_download_type_id = 9) and status = 1 and download_Fund_id = @download_Fund_id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@download_Fund_id", fundid);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<Download> funddlist = new List<Download>();

            while (mdr.Read())
            {
                Download fdl = new Download()
                {
                    Id = Convert.ToInt32(mdr["id"].ToString()),
                    UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    DownloadType_Id = Convert.ToInt32(mdr["download_download_type_id"].ToString()),
                    Fund_Id = Convert.ToInt32(mdr["download_Fund_id"].ToString()),
                    FileName = mdr["file_name"].ToString(),
                    FileAuthor = mdr["file_author"].ToString(),
                    FileSize = mdr["file_size"].ToString(),
                    UrlPath = mdr["url_path"].ToString(),
                    Status = Convert.ToInt32(mdr["status"].ToString())
                };
                funddlist.Add(fdl);
            }
            con.Close();
            return funddlist;
        }
    }
}