﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class FundSummaryData
    {
        public static FundSummary Insert(FundSummary fs)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"insert into fund_summary(fund_categ ,no_of_fund, total_value,  created_date, created_by, status) values
                         (@fund_categ, @no_of_fund, @total_value,  @created_date, @created_by, @status)";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@fund_categ", fs.fund_categ);
            cmd.Parameters.AddWithValue("@no_of_fund", fs.no_of_fund);
            cmd.Parameters.AddWithValue("@total_value", fs.total_value);
            cmd.Parameters.AddWithValue("@created_date", fs.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fs.CreatedBy);
            cmd.Parameters.AddWithValue("@status", fs.Status);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return fs;
        }

        public static FundSummary Update(FundSummary fs, string id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update fund_summary set fund_categ = @fund_categ, 
                                             no_of_fund = @no_of_fund,
                                             total_value = @total_value,
                                             status = @status, 
                                             created_date =@created_date, 
                                             created_by = @created_by
                                             where id=@id";

            con.Open();
            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@fund_categ", fs.fund_categ);
            cmd.Parameters.AddWithValue("@no_of_fund", fs.no_of_fund);
            cmd.Parameters.AddWithValue("@total_value", fs.total_value);
            cmd.Parameters.AddWithValue("@created_date", fs.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fs.CreatedBy);
            cmd.Parameters.AddWithValue("@status", fs.Status);

            cmd.ExecuteNonQuery();
            con.Close();
            return fs;

        }

        public static FundSummary Update2(FundSummary fs)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update fund_summary set status = @status
                                             where id=@id";

            con.Open();
            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", fs.ID);
            cmd.Parameters.AddWithValue("@status", fs.Status);

            cmd.ExecuteNonQuery();
            con.Close();
            return fs;

        }

        public static List<FundSummary> FsGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund_summary";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FundSummary> fslist = new List<FundSummary>();

            while (mdr.Read())
            {
                FundSummary fs = new FundSummary()
                {
                    ID = Convert.ToInt32(mdr["id"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString()),
                    Status = Convert.ToInt32(mdr["status"].ToString()),
                    fund_categ = mdr["fund_categ"].ToString(),
                    no_of_fund = Convert.ToInt32(mdr["no_of_fund"].ToString()),
                    total_value = Convert.ToDecimal(mdr["total_value"].ToString())
                };
                fslist.Add(fs);
            }
            con.Close();
            return fslist;
        }

        public static FundSummary FsGetByID(string id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund_summary where id=@id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader mdr = cmd.ExecuteReader();

            FundSummary fs = new FundSummary();

            if (mdr.Read())
            {
                fs.ID = Convert.ToInt32(mdr["id"].ToString());
                fs.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                fs.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                fs.Status = Convert.ToInt32(mdr["status"].ToString());
                fs.fund_categ = mdr["fund_categ"].ToString();
                fs.no_of_fund = Convert.ToInt32(mdr["no_of_fund"].ToString());
                fs.total_value = Convert.ToDecimal(mdr["total_value"].ToString());
            }
            con.Close();
            return fs;
        }
        public static List<FundSummary> GetAllFinancialSummmary()
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select * from fund_summary";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<FundSummary> financialSummaryList = new List<FundSummary>();
            while (mdr.Read())
            {
                FundSummary fundSummary = new FundSummary();
                fundSummary.ID = Convert.ToInt32(mdr["id"].ToString());
                fundSummary.fund_categ = mdr["fund_categ"].ToString();
                fundSummary.no_of_fund = Convert.ToInt32(mdr["no_of_fund"].ToString());
                fundSummary.total_value = Convert.ToDecimal(mdr["total_value"].ToString());
                fundSummary.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                fundSummary.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                fundSummary.Status = Convert.ToInt32(mdr["status"].ToString());

                financialSummaryList.Add(fundSummary);
            }
            con.Close();
            return financialSummaryList;
        }


        public static FundSummary GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from fund_summary where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            FundSummary financial_summary = new FundSummary();
            if (mdr.Read())
            {
                financial_summary.ID = Convert.ToInt32(mdr["id"].ToString());
                financial_summary.fund_categ = mdr["fund_categ"].ToString();
                financial_summary.no_of_fund = Convert.ToInt32(mdr["no_of_fund"].ToString());
                financial_summary.total_value = Convert.ToDecimal(mdr["total_value"].ToString());
                financial_summary.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                financial_summary.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                financial_summary.Status = Convert.ToInt32(mdr["status"].ToString());
            }
            conn.Close();
            return financial_summary;
        }

        public static FundSummary UpdateDate(FundSummary financialSummary)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE fund_summary SET created_date = @created_date
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", financialSummary.ID);
            cmd.Parameters.AddWithValue("@created_date", financialSummary.CreatedDate);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return financialSummary;
        }
    }
}