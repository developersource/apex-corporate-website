﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using AdminApex.ApexUtility;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class FundNameData
    {
        public static List<FundName> GetName()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT fund_name FROM fund LIMIT 10 OFFSET 1;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FundName> FundNameList = new List<FundName>();

            while (mdr.Read())
            {
                FundName fn = new FundName();

                fn.fund_name = mdr["fund_name"].ToString();
                


                FundNameList.Add(fn);
            }
            con.Close();
            return FundNameList;
        }
    }
}