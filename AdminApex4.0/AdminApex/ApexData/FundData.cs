﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using AdminApex.ApexUtility;


namespace AdminApex.ApexData
{
    public class FundData
    {
        public static Fund Insert(Fund fund)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"insert into fund(fund_name, fund_code, potential_inves, inves_strategy, launch_date, trustee, fund_category, sales_charge, manage_fee, trustee_fee, min_ini_inves, redemp_fee, created_date, created_by, status, fund_download_type_id, url_path, min_sub_inves, epfapproved) values
                         (@fund_name, @fund_code, @potential_inves, @inves_strategy, @launch_date, @trustee, @fund_category, @sales_charge, @manage_fee, @trustee_fee, @min_ini_inves, @redemp_fee, @created_date, @created_by, @status, @fund_download_type_id, @url_path, @min_sub_inves, @epfapproved)";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@fund_name", fund.fund_name);
            cmd.Parameters.AddWithValue("@fund_code", fund.fund_code);
            cmd.Parameters.AddWithValue("@potential_inves", fund.potential_inves);
            cmd.Parameters.AddWithValue("@inves_strategy", fund.inves_strategy);
            cmd.Parameters.AddWithValue("@launch_date", fund.launch_date);
            cmd.Parameters.AddWithValue("@trustee", fund.trustee);
            cmd.Parameters.AddWithValue("@fund_category", fund.fund_category);
            cmd.Parameters.AddWithValue("@sales_charge", fund.sales_charge);
            cmd.Parameters.AddWithValue("@manage_fee", fund.manage_fee);
            cmd.Parameters.AddWithValue("@trustee_fee", fund.trustee_fee);
            cmd.Parameters.AddWithValue("@min_ini_inves", fund.min_ini_inves);
            cmd.Parameters.AddWithValue("@redemp_fee", fund.redemp_fee);
            cmd.Parameters.AddWithValue("@created_date", fund.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fund.CreatedBy);
            cmd.Parameters.AddWithValue("@status", fund.Status);
            cmd.Parameters.AddWithValue("@fund_download_type_id", fund.DownloadType_Id);
            cmd.Parameters.AddWithValue("@url_path", fund.url_path);
            cmd.Parameters.AddWithValue("@min_sub_inves", fund.min_sub_inv);
            cmd.Parameters.AddWithValue("@epfapproved", fund.epfapproved);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return fund;
        }

        public static Fund Update(Fund fund, Int32 id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update fund set fund_code = @fund_code,
                                             fund_name = @fund_name,
                                             fund_download_type_id = @fund_download_type_id,
                                             epfapproved = @epfapproved,
                                             potential_inves = @potential_inves,
                                             inves_strategy = @inves_strategy,
                                             launch_date = @launch_date,
                                             trustee = @trustee, 
                                             fund_category = @fund_category, 
                                             sales_charge = @sales_charge, 
                                             manage_fee = @manage_fee, 
                                             trustee_fee = @trustee_fee, 
                                             min_ini_inves = @min_ini_inves, 
                                             redemp_fee = @redemp_fee, 
                                             created_date =@created_date, 
                                             created_by = @created_by, 
                                             status = @status,
                                             url_path = @url_path,
                                             min_sub_inves = @min_sub_inves
                                             where id=@id";

            con.Open();
            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@fund_code", fund.fund_code);
            cmd.Parameters.AddWithValue("@fund_name", fund.fund_name);
            cmd.Parameters.AddWithValue("@fund_download_type_id", fund.DownloadType_Id);
            cmd.Parameters.AddWithValue("@epfapproved", fund.epfapproved);
            cmd.Parameters.AddWithValue("@potential_inves", fund.potential_inves);
            cmd.Parameters.AddWithValue("@inves_strategy", fund.inves_strategy);
            cmd.Parameters.AddWithValue("@launch_date", fund.launch_date);
            cmd.Parameters.AddWithValue("@trustee", fund.trustee);
            cmd.Parameters.AddWithValue("@fund_category", fund.fund_category);
            cmd.Parameters.AddWithValue("@sales_charge", fund.sales_charge);
            cmd.Parameters.AddWithValue("@manage_fee", fund.manage_fee);
            cmd.Parameters.AddWithValue("@trustee_fee", fund.trustee_fee);
            cmd.Parameters.AddWithValue("@min_ini_inves", fund.min_ini_inves);
            cmd.Parameters.AddWithValue("@redemp_fee", fund.redemp_fee);
            cmd.Parameters.AddWithValue("@created_date", fund.CreatedDate);
            cmd.Parameters.AddWithValue("@created_by", fund.CreatedBy);
            cmd.Parameters.AddWithValue("@status", fund.Status);
            cmd.Parameters.AddWithValue("@url_path", fund.url_path);
            cmd.Parameters.AddWithValue("@min_sub_inves", fund.min_sub_inv);

            cmd.ExecuteNonQuery();
            con.Close();
            return fund;

        }

        public static List<Fund> FundGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<Fund> fundlist = new List<Fund>();

            while (mdr.Read())
            {
                Fund fd = new Fund();

                fd.ID = Convert.ToInt32(mdr["id"].ToString());
                fd.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                fd.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                fd.Status = Convert.ToInt32(mdr["status"].ToString());
                fd.fund_name = mdr["fund_name"].ToString();
                fd.fund_code = mdr["fund_code"].ToString();
                fd.epfapproved = mdr["epfapproved"].ToString();
                fd.potential_inves = mdr["potential_inves"].ToString();
                fd.inves_strategy = mdr["inves_strategy"].ToString();
                fd.launch_date = Convert.ToDateTime(mdr["launch_date"].ToString());
                fd.trustee = mdr["trustee"].ToString();
                fd.fund_category = mdr["fund_category"].ToString();
                fd.sales_charge = mdr["sales_charge"].ToString();
                fd.manage_fee = mdr["manage_fee"].ToString();
                fd.trustee_fee = mdr["trustee_fee"].ToString();
                fd.min_ini_inves = mdr["min_ini_inves"].ToString();
                fd.redemp_fee = mdr["redemp_fee"].ToString();
                fd.DownloadType_Id = Convert.ToInt32(mdr["fund_download_type_id"].ToString());
                fd.url_path = mdr["url_path"].ToString();
                fd.min_sub_inv = mdr["min_sub_inves"].ToString();
                fd.backgroundColor = mdr["backgroundcolor"].ToString();

                fundlist.Add(fd);
            }
            con.Close();
            return fundlist;
        }

        public static Fund FundGetByID(Int32 id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund where id=@id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            MySqlDataReader mdr = cmd.ExecuteReader();

            Fund fd = new Fund();

            if (mdr.Read())
            {
                fd.ID = Convert.ToInt32(mdr["id"].ToString());
                fd.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                fd.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                fd.Status = Convert.ToInt32(mdr["status"].ToString());
                fd.fund_name = mdr["fund_name"].ToString();
                fd.fund_code = mdr["fund_code"].ToString();
                fd.epfapproved = mdr["epfapproved"].ToString();
                fd.potential_inves = mdr["potential_inves"].ToString();
                fd.inves_strategy = mdr["inves_strategy"].ToString();
                fd.launch_date = Convert.ToDateTime(mdr["launch_date"].ToString());
                fd.trustee = mdr["trustee"].ToString();
                fd.fund_category = mdr["fund_category"].ToString();
                fd.sales_charge = mdr["sales_charge"].ToString();
                fd.manage_fee = mdr["manage_fee"].ToString();
                fd.trustee_fee = mdr["trustee_fee"].ToString();
                fd.min_ini_inves = mdr["min_ini_inves"].ToString();
                fd.redemp_fee = mdr["redemp_fee"].ToString();
                fd.DownloadType_Id = Convert.ToInt32(mdr["fund_download_type_id"].ToString());
                fd.url_path = mdr["url_path"].ToString();
                fd.min_sub_inv = mdr["min_sub_inves"].ToString();
            }
            con.Close();
            return fd;
        }

        public static string getFundNameByCode(string FundCode)
        {
            string fundName = "";

            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select fund_name from fund where fund_code=@FundCode";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@FundCode", FundCode);

            MySqlDataReader mdr = cmd.ExecuteReader();

            Fund fd = new Fund();

            if (mdr.Read())
            {
                fundName = mdr["fund_name"].ToString();
            }
            con.Close();
            return fundName;
        }
    }
}