﻿using System;
using System.Collections.Generic;
using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class TickerTapeFundData
    {
        public static List<TickerTapeFund> PriceGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT * FROM ticker_tape_compare_data";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<TickerTapeFund> tickerfundlist = new List<TickerTapeFund>();

            while (mdr.Read())
            {
                TickerTapeFund ttf = new TickerTapeFund();

                ttf.id = Convert.ToInt32(mdr["id"].ToString());
                ttf.Fund_Name = mdr["Fund_Name"].ToString();
                ttf.Daily_Unit_Price = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                ttf.Daily_Date = Convert.ToDateTime(mdr["Daily_Unit_Price"].ToString());


                tickerfundlist.Add(ttf);
            }
            con.Close();
            return tickerfundlist;
        }
    }
}