﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class UnitSplitData
    {
        public static UnitSplit Insert(UnitSplit unitSplit)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO unit_split(
                            unit_split_fund_id,
                            ex_date,
                            split_ratio,
                            created_by, 
                            created_date, 
                            status) VALUES(
                            @unit_split_fund_id,
                            @ex_date,
                            @split_ratio,
                            @created_by, 
                            @created_date,
                            @status)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@unit_split_fund_id", unitSplit.Fund_Id);
            cmd.Parameters.AddWithValue("@ex_date", unitSplit.ExDate);
            cmd.Parameters.AddWithValue("@split_ratio", unitSplit.SplitRatio);
            cmd.Parameters.AddWithValue("@created_by", unitSplit.CreatedBy);
            cmd.Parameters.AddWithValue("@created_date", unitSplit.CreatedDate);
            cmd.Parameters.AddWithValue("@status", unitSplit.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return unitSplit;
        }

        public static List<UnitSplit> GetAllUnitSplit()
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"SELECT * FROM unit_split";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<UnitSplit> unitSplitList = new List<UnitSplit>();

            while (mdr.Read())
            {
                UnitSplit unitSplit = new UnitSplit();
                unitSplit.Id = Convert.ToInt32(mdr["id"].ToString());
                unitSplit.Fund_Id = Convert.ToInt32(mdr["unit_split_fund_id"].ToString());
                unitSplit.ExDate = Convert.ToDateTime(mdr["ex_date"].ToString());
                unitSplit.SplitRatio = mdr["split_ratio"].ToString();
                unitSplit.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                unitSplit.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                unitSplit.Status = Convert.ToInt32(mdr["status"].ToString());

                unitSplitList.Add(unitSplit);
            }
            conn.Close();
            return unitSplitList;
        }

        public static UnitSplit GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from unit_split where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            UnitSplit unitSplit = new UnitSplit();
            if (mdr.Read())
            {
                unitSplit.Id = Convert.ToInt32(mdr["id"].ToString());
                unitSplit.Fund_Id = Convert.ToInt32(mdr["unit_split_fund_id"].ToString());
                unitSplit.ExDate = Convert.ToDateTime(mdr["ex_date"].ToString());
                unitSplit.SplitRatio = mdr["split_ratio"].ToString();
                unitSplit.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                unitSplit.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                unitSplit.Status = Convert.ToInt32(mdr["status"].ToString());

            }
            conn.Close();
            return unitSplit;
        }

        public static UnitSplit Update(UnitSplit unitSplit)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE unit_split SET unit_split_fund_id = @unit_split_fund_id,
                           ex_date = @ex_date,
                           split_ratio = @split_ratio,
                           created_by = @created_by,
                           created_date = @created_date,
                           status = @status 
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", unitSplit.Id);
            cmd.Parameters.AddWithValue("@unit_split_fund_id", unitSplit.Fund_Id);
            cmd.Parameters.AddWithValue("@ex_date", unitSplit.ExDate);
            cmd.Parameters.AddWithValue("@split_ratio", unitSplit.SplitRatio);
            cmd.Parameters.AddWithValue("@created_by", unitSplit.CreatedBy);
            cmd.Parameters.AddWithValue("@created_date", unitSplit.CreatedDate);
            cmd.Parameters.AddWithValue("@status", unitSplit.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return unitSplit;
        }

        public static void Delete(int id)
        {
            using (MySqlConnection sqlCon = DBconnection.GetConnection())
            {
                using (MySqlCommand cmdGet = new MySqlCommand())
                {
                    cmdGet.CommandText = @"DELETE FROM unit_split where id = @id";

                    cmdGet.Parameters.AddWithValue("@id", id);
                    cmdGet.Connection = sqlCon;
                    sqlCon.Open();
                    int n = cmdGet.ExecuteNonQuery();
                    sqlCon.Close();
                }
            }

        }
    }
}