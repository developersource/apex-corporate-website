﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminApex.ApexUtility;

namespace AdminApex.ApexData
{
    public class NewsData
    {

        public static News Insert(News news)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"insert into news(content_title, url_path, content, upload_date, created_by, status) values
                         (@content_title, @url_path, @content, @upload_date, @created_by, @status)";

            //For announcement upgrade
            //string query = @"insert into news(content_title, url_path, content, upload_date, created_by, status, annoucement_status, annoucement_desc) values
            //             (@content_title, @url_path, @content, @upload_date, @created_by, @status, @annoucement_status, @annoucement_desc)";

            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@content_title", news.title);
            cmd.Parameters.AddWithValue("@url_path", news.UrlPath);
            cmd.Parameters.AddWithValue("@content", news.content);
            cmd.Parameters.AddWithValue("@upload_date", news.UploadDate);
            cmd.Parameters.AddWithValue("@created_by", news.CreatedBy);
            cmd.Parameters.AddWithValue("@status", news.Status);
            //cmd.Parameters.AddWithValue("@annoucement_status", news.annoucementStatus);
            //cmd.Parameters.AddWithValue("@annoucement_desc", news.annoucementDesc);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            con.Open();
            String query2 = @"SELECT ID FROM news where content_title=@title";
            MySqlCommand cmdselect = new MySqlCommand(query2, con);

            cmdselect.Parameters.AddWithValue("@title", news.title);
            MySqlDataReader mdr = cmdselect.ExecuteReader();
            if (mdr.Read())
            {
                news.id = Convert.ToInt32(mdr["ID"].ToString());
            }

            con.Close();

            return news;
        }

        public static News Update(News news, int id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update news set content_title = @content_title,
                                             content = @content,
                                             upload_date =@upload_date, 
                                             created_by = @created_by, 
                                             status = @status,
                                             url_path = @url_path
                                             where id=@id";

            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@content_title", news.title);
            cmd.Parameters.AddWithValue("@url_path", news.UrlPath);
            cmd.Parameters.AddWithValue("@content", news.content);
            cmd.Parameters.AddWithValue("@upload_date", news.UploadDate);
            cmd.Parameters.AddWithValue("@created_by", news.CreatedBy);
            cmd.Parameters.AddWithValue("@status", news.Status);
            //cmd.Parameters.AddWithValue("@annoucement_status", news.annoucementStatus);
            //cmd.Parameters.AddWithValue("@annoucement_desc", news.annoucementDesc);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return news;
        }

        public static News UpdateS(News news)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"update news set status = @status
                                             where id=@id";

            MySqlCommand cmd = new MySqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", news.id);
            cmd.Parameters.AddWithValue("@status", news.Status);
            //cmd.Parameters.AddWithValue("@annoucement_status", news.annoucementStatus);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return news;
        }

        public static List<Fund> FundGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<Fund> fundlist = new List<Fund>();

            while (mdr.Read())
            {
                Fund fd = new Fund()
                {
                    ID = Convert.ToInt32(mdr["id"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString()),
                    Status = Convert.ToInt32(mdr["status"].ToString()),
                    fund_name = mdr["fund_name"].ToString(),
                    potential_inves = mdr["potential_inves"].ToString(),
                    inves_strategy = mdr["inves_strategy"].ToString(),
                    launch_date = Convert.ToDateTime(mdr["launch_date"].ToString()),
                    trustee = mdr["trustee"].ToString(),
                    fund_category = mdr["fund_category"].ToString(),
                    sales_charge = mdr["sales_charge"].ToString(),
                    manage_fee = mdr["manage_fee"].ToString(),
                    trustee_fee = mdr["trustee_fee"].ToString(),
                    min_ini_inves = mdr["min_ini_inves"].ToString(),
                    redemp_fee = mdr["redemp_fee"].ToString()
                };
                fundlist.Add(fd);
            }
            con.Close();
            return fundlist;
        }

        public static List<News> NewsGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from news";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<News> newslist = new List<News>();

            while (mdr.Read())
            {
                News n = new News()
                {
                    id = Convert.ToInt32(mdr["id"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString()),
                    Status = Convert.ToInt32(mdr["status"].ToString()),
                    content = mdr["content"].ToString(),
                    UrlPath = mdr["url_path"].ToString(),
                    title = mdr["content_title"].ToString(),
                    //annoucementStatus = Convert.ToInt32(mdr["annoucement_status"].ToString()),
                    //annoucementDesc = mdr["annoucement_desc"].ToString()
                };
                newslist.Add(n);
            }
            con.Close();
            return newslist;
        }

        public static News NewsGetByID(Int32 id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from news where id=@id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            MySqlDataReader mdr = cmd.ExecuteReader();

            News nl = new News();

            if (mdr.Read())
            {
                nl.id = Convert.ToInt32(mdr["id"].ToString());
                nl.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                nl.UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString());
                nl.Status = Convert.ToInt32(mdr["status"].ToString());
                nl.content = mdr["content"].ToString();
                nl.UrlPath = mdr["url_path"].ToString();
                nl.title = mdr["content_title"].ToString();
                //nl.annoucementStatus = Convert.ToInt32(mdr["annoucement_status"].ToString());
                //nl.annoucementDesc = mdr["annoucement_desc"].ToString();
            }
            con.Close();
            return nl;
        }

    }
}