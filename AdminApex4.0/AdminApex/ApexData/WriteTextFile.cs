﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class WriteTextFile
    {
        public void Write(string path, string[] data)
        {
            try
            {
                //    StreamReader sr = new StreamReader(path);
                if (new FileInfo(path).Length != 0)
                {
                    //string firstLine = sr.ReadLine();
                    //sr.Close();
                    StreamWriter sw = new StreamWriter(path, true);
                    string textData = string.Format("{0,-25} {1,-15} {2,-20} {3}", data[0], data[1], data[3], data[2]);
                    sw.WriteLine(textData);
                    sw.Close();

                }
                else
                {
                    //sr.Close();
                    StreamWriter sw = new StreamWriter(path + "UserLog.txt", true);
                    string header = string.Format("{0,-25} {1,-15} {2,-20} {3}", "DateTime", "Name", "Status", "Action");
                    string textData = string.Format("{0,-25} {1,-15} {2,-20} {3}", data[0], data[1], data[3], data[2]);
                    sw.WriteLine(header);
                    sw.WriteLine(textData);
                    sw.Close();
                    //Create();
                }
            }
            catch (Exception e)
            {
                //create new file(physical file deleted + db record still there)
                //create new text file
                string url = Create(path, data);

                //Server.MapPath("/Log/")

                //update db
                UserLogService.UpdateException();
                UserLog newUserLog = new UserLog();
                newUserLog.Title = "UserLog-" + DateTime.Now.ToString("MMMMyyyy");
                newUserLog.UrlPath = "/Log/" + url;
                newUserLog.Month = DateTime.Now.Month.ToString();
                newUserLog.Year = DateTime.Now.Year.ToString();
                newUserLog.CreatedDate = DateTime.Now;
                newUserLog.Status = 1;
                newUserLog = UserLogService.Insert(newUserLog);



                Console.WriteLine("Exception: " + e.Message);
            }
        }

        public string Create(string path, string[] data)
        {
            string filepath = path;
            string dbPath = "";

            if (path.Contains("UserLog"))
            {
                filepath = filepath.Substring(0, filepath.Length - 19);
                dbPath = "UserLog" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            }
            else
            {
                dbPath = "UserLog" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
                //myString.Substring(str.Length - 3)

            }

            string url = filepath + dbPath;
            try
            {
                StreamWriter sw = new StreamWriter(url, true);
                string header = string.Format("{0,-25} {1,-15} {2,-20} {3}", "DateTime", "Name", "Status", "Action");
                string textData = string.Format("{0,-25} {1,-15} {2,-20} {3}", data[0], data[1], data[3], data[2]);
                sw.WriteLine(header);
                sw.WriteLine(textData);

                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }

            return dbPath;
        }
    }
}