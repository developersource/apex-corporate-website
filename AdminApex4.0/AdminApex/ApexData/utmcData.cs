﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class utmcData
    {
        public static List<DailyNavFundUtility> PriceGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select A.id,A.IPD_Fund_Code,A.Daily_NAV_Date,A.Daily_NAV_Time,A.Daily_Unit_Price from 
                             utmc_daily_nav_fund A inner join (select 
                             IPD_Fund_Code,max(Daily_NAV_Date) as DateMAx from utmc_daily_nav_fund
                             group by IPD_Fund_Code) B on A.IPD_Fund_Code=B.IPD_Fund_Code and A.Daily_NAV_Date=B.DateMAx 
                             order by IPD_Fund_Code;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<DailyNavFundUtility> fundlist = new List<DailyNavFundUtility>();

            while (mdr.Read())
            {
                DailyNavFundUtility dnf = new DailyNavFundUtility();

                dnf.id = Convert.ToInt32(mdr["id"].ToString());
                dnf.IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString();
                dnf.Daily_NAV_Date = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
                dnf.Daily_NAV_Time = Convert.ToDateTime(mdr["Daily_NAV_Time"].ToString());
                dnf.Daily_Unit_Price = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());

                fundlist.Add(dnf);
            }
            con.Close();
            return fundlist;
        }

        public static Response GetDataByQuery(string query,bool maintainColumnName = true)
        {
            Response responseObject = new Response();
            List<dynamic> objs = new List<dynamic>();
            try
            {
                MySqlConnection con = DBconnection.GetConnection();

                con.Open();
                MySqlCommand cmd = new MySqlCommand(query, con);
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                dt.AsEnumerable().Where(row => row.ItemArray.All(field => field == DBNull.Value)).ToList().ForEach(row => row.Delete());
                dt.AcceptChanges();
                objs = dt.ToDynamicObject<dynamic>(maintainColumnName);
                con.Close();
                responseObject.IsSuccess = true;
                responseObject.Data = objs;
            }
            catch (Exception ex)
            {
                responseObject.IsSuccess = false;
                responseObject.Message = "Error Found: " + ex.Message;
            }

            return responseObject;
        }

        public static Response UpdateSingleHistoricalNav(string fundId, double newNavPrice)
        {
            Response responseObject = new Response();
            try
            {
                MySqlConnection con = DBconnection.GetConnection();
                con.Open();
                String query = @"Update utmc_daily_nav_fund
                                 Set Daily_Unit_Price = @NavPrice
                                 Where ID = @FundID";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Parameters.AddWithValue("@NavPrice",newNavPrice);
                cmd.Parameters.AddWithValue("@FundID",fundId);
                cmd.ExecuteNonQuery();

                con.Close();
                responseObject.IsSuccess = true;
                responseObject.Message = "Successfully Updated Historical NAV Price based on ID("+fundId+") with new price "+newNavPrice;
            }
            catch (Exception ex)
            {
                responseObject.IsSuccess = false;
                responseObject.Message = "Error Found: " + ex.Message;
            }
            return responseObject;
        }

        public static List<DailyNavFundUtility> GetPrice(string id, DateTime c1, DateTime c2)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select id,IPD_Fund_Code,Daily_NAV_Date,Daily_Unit_Price from utmc_daily_nav_fund where IPD_Fund_Code = @IPD_Fund_Code && Daily_NAV_Date >= @Daily_NAV_Date1 && Daily_NAV_Date <= @Daily_NAV_Date2";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", id);
            cmd.Parameters.AddWithValue("@Daily_NAV_Date1", c1);
            cmd.Parameters.AddWithValue("@Daily_NAV_Date2", c2);
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<DailyNavFundUtility> fundlist = new List<DailyNavFundUtility>();

            while (mdr.Read())
            {
                DailyNavFundUtility dnf = new DailyNavFundUtility();

                dnf.id = Convert.ToInt32(mdr["id"].ToString());
                dnf.IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString();
                dnf.Daily_NAV_Date = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
                dnf.Daily_Unit_Price = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());

                fundlist.Add(dnf);
            }
            con.Close();
            return fundlist;
        }



        public static FundInfomationUtility GetByid(string id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from utmc_fund_information where id = @id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            FundInfomationUtility finfo = new FundInfomationUtility();
            if (mdr.Read())
            {
                finfo.id = Convert.ToInt32(mdr["id"].ToString());
                finfo.EPF_IPD_Code = mdr["EPF_IPD_Code"].ToString();
                finfo.IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString();
                finfo.Fund_Code = mdr["Fund_Code"].ToString();
                finfo.Fund_Name = mdr["Fund_Name"].ToString();
                finfo.Effective_Date = DateTime.Parse(mdr["Effective_Date"].ToString());
                finfo.LIPPER_Category_Of_Fund = mdr["LIPPER_Category_Of_Fund"].ToString();
                finfo.Conventional = mdr["Conventional"].ToString();
                finfo.Status = mdr["Status"].ToString();
                finfo.Foreign_Fund = mdr["Foreign_Fund"].ToString();
                finfo.Report_Date = DateTime.Parse(mdr["Report_Date"].ToString());
                finfo.Fund_Base_Currency = mdr["Fund_Base_Currency"].ToString();
                finfo.IS_EMIS = Convert.ToInt32(mdr["IS_EMIS"].ToString());
                finfo.FUND_CLS = mdr["FUND_CLS"].ToString();
                finfo.IS_RETAIL = Convert.ToInt32(mdr["IS_RETAIL"].ToString());

            }
            conn.Close();
            return finfo;
        }

        public static List<FundInfomationUtility> FundGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from utmc_fund_information";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FundInfomationUtility> fundlist = new List<FundInfomationUtility>();

            while (mdr.Read())
            {
                FundInfomationUtility fd = new FundInfomationUtility()
                {
                    id = Convert.ToInt32(mdr["id"].ToString()),
                    EPF_IPD_Code = mdr["EPF_IPD_Code"].ToString(),
                    IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString(),
                    Fund_Code = mdr["Fund_Code"].ToString(),
                    Fund_Name = mdr["Fund_Name"].ToString(),
                    Effective_Date = DateTime.Parse(mdr["Effective_Date"].ToString()),
                    LIPPER_Category_Of_Fund = mdr["LIPPER_Category_Of_Fund"].ToString(),
                    Conventional = mdr["Conventional"].ToString(),
                    Status = mdr["Status"].ToString(),
                    Foreign_Fund = mdr["Foreign_Fund"].ToString(),
                    Report_Date = DateTime.Parse(mdr["Report_Date"].ToString()),
                    Fund_Base_Currency = mdr["Fund_Base_Currency"].ToString(),
                    IS_EMIS = Convert.ToInt32(mdr["IS_EMIS"].ToString()),
                    FUND_CLS = mdr["FUND_CLS"].ToString(),
                    IS_RETAIL = Convert.ToInt32(mdr["IS_RETAIL"].ToString())
                };
                fundlist.Add(fd);
            }
            con.Close();
            return fundlist;
        }

        public static List<FundDistributionUtility> GetDist(string id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select id,IPD_Fund_Code,Corporate_Action_Date,Distributions from utmc_fund_corporate_actions where IPD_Fund_Code = @IPD_Fund_Code";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", id);
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<FundDistributionUtility> Distlist = new List<FundDistributionUtility>();

            while (mdr.Read())
            {
                FundDistributionUtility fd = new FundDistributionUtility()
                {
                    id = Convert.ToInt32(mdr["id"].ToString()),
                    IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString(),
                    Corporate_Action_Date = DateTime.Parse(mdr["Corporate_Action_Date"].ToString()),
                    Distributions = Convert.ToDecimal(mdr["Distributions"].ToString())
                };
                Distlist.Add(fd);
            }
            con.Close();
            return Distlist;
        }

        public static List<FundDistributionUtility> DistGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from utmc_fund_corporate_actions";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FundDistributionUtility> fundlist = new List<FundDistributionUtility>();

            while (mdr.Read())
            {
                FundDistributionUtility fd = new FundDistributionUtility()
                {
                    id = Convert.ToInt32(mdr["id"].ToString()),
                    EPF_IPD_Code = mdr["EPF_IPD_Code"].ToString(),
                    IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString(),
                    Corporate_Action_Date = DateTime.Parse(mdr["Corporate_Action_Date"].ToString()),
                    Distributions = Convert.ToDecimal(mdr["Distributions"].ToString()),
                    Unit_Splits = Convert.ToDecimal(mdr["Unit_Splits"].ToString()),
                    Report_Date = Convert.ToDateTime(mdr["Report_Date"].ToString()),
                    Net_Distribution = Convert.ToDecimal(mdr["Net_Distribution"].ToString())
                };
                fundlist.Add(fd);
            }
            con.Close();
            return fundlist;
        }

        //public static List<DownloadUtility> GetIsFund()
        //{

        //    MySqlConnection con = DBC.GetConnection();

        //    con.Open();
        //    string query = @"select * from download where download_download_type_id = 7 || download_download_type_id = 8 || download_download_type_id = 9";
        //    MySqlCommand cmd = new MySqlCommand(query, con);
        //    MySqlDataReader mdr = cmd.ExecuteReader();

        //    List<DownloadUtility> funddlist = new List<DownloadUtility>();

        //    while (mdr.Read())
        //    {
        //        DownloadUtility fdl = new DownloadUtility()
        //        {
        //            id = Convert.ToInt32(mdr["id"].ToString()),
        //            UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString()),
        //            CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
        //            DownloadType_id = Convert.ToInt32(mdr["download_download_type_id"].ToString()),
        //            Fund_id = Convert.ToInt32(mdr["download_Fund_id"].ToString()),
        //            FileName = mdr["file_name"].ToString(),
        //            FileAuthor = mdr["file_author"].ToString(),
        //            FileSize = mdr["file_size"].ToString(),
        //            UrlPath = mdr["url_path"].ToString(),
        //            Status = Convert.ToInt32(mdr["status"].ToString())
        //        };
        //        funddlist.Add(fdl);
        //    }
        //    con.Close();
        //    return funddlist;
        //}

        public static List<Download> GetFundResourceByFundid(int fundid)
        {

            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from download where (download_download_type_id = 7 || download_download_type_id = 8 || download_download_type_id = 9) and status = 1 and download_Fund_id = @download_Fund_id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@download_Fund_id", fundid);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<Download> funddlist = new List<Download>();

            while (mdr.Read())
            {
                Download fdl = new Download()
                {
                    Id = Convert.ToInt32(mdr["id"].ToString()),
                    UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString()),
                    CreatedBy = Convert.ToInt32(mdr["created_by"].ToString()),
                    DownloadType_Id = Convert.ToInt32(mdr["download_download_type_id"].ToString()),
                    Fund_Id = Convert.ToInt32(mdr["download_Fund_id"].ToString()),
                    FileName = mdr["file_name"].ToString(),
                    FileAuthor = mdr["file_author"].ToString(),
                    FileSize = mdr["file_size"].ToString(),
                    UrlPath = mdr["url_path"].ToString(),
                    Status = Convert.ToInt32(mdr["status"].ToString())
                };
                funddlist.Add(fdl);
            }
            con.Close();
            return funddlist;
        }

        public static List<FundDistributionUtility> DistGetByID(int fundid)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from utmc_fund_corporate_actions where download_Fund_id = @download_Fund_id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@download_Fund_id", fundid);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<FundDistributionUtility> fundlist = new List<FundDistributionUtility>();

            while (mdr.Read())
            {
                FundDistributionUtility fd = new FundDistributionUtility()
                {
                    id = Convert.ToInt32(mdr["id"].ToString()),
                    EPF_IPD_Code = mdr["EPF_IPD_Code"].ToString(),
                    IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString(),
                    Corporate_Action_Date = DateTime.Parse(mdr["Corporate_Action_Date"].ToString()),
                    Distributions = Convert.ToDecimal(mdr["Distributions"].ToString()),
                    Unit_Splits = Convert.ToDecimal(mdr["Unit_Splits"].ToString()),
                    Report_Date = Convert.ToDateTime(mdr["Report_Date"].ToString()),
                    Net_Distribution = Convert.ToDecimal(mdr["Net_Distribution"].ToString())
                };
                fundlist.Add(fd);
            }
            con.Close();
            return fundlist;
        }

        public class Response
        {
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
            public object Data { get; set; }
        }
    }
}