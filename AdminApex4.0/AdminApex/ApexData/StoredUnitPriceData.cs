﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using AdminApex.ApexUtility;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class StoredUnitPriceData
    {
        public static List<StoredUnitPrice> PriceGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT * FROM stored_unit_price";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredUnitPrice> storedUnitPriceList = new List<StoredUnitPrice>();

            while (mdr.Read())
            {
                StoredUnitPrice stp = new StoredUnitPrice();

                stp.IPD_Fund_Code = mdr["IPD_Fund_Code"].ToString();
                stp.Unit_Price = Convert.ToDecimal(mdr["Unit_Price"].ToString());
                stp.NAV_Date = Convert.ToDateTime(mdr["NAV_Date"].ToString());


                storedUnitPriceList.Add(stp);
            }
            con.Close();
            return storedUnitPriceList;
        }

        public static List<StoredUnitPrice> UpdatePrice(YesterdayDateUnitPrice yesterdayDateUnitPrice)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"update stored_unit_price set NAV_Date = @NAV_Date,
                                             Unit_Price = @Unit_Price
                                             where IPD_Fund_Code= @IPD_Fund_Code";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@NAV_Date", yesterdayDateUnitPrice.CompareDate);
            cmd.Parameters.AddWithValue("@Unit_Price", yesterdayDateUnitPrice.CompareUnitPrice);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", yesterdayDateUnitPrice.IPD);


            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredUnitPrice> storedUnitPriceList = new List<StoredUnitPrice>();

            while (mdr.Read())
            {
                StoredUnitPrice stp = new StoredUnitPrice();

                stp.IPD_Fund_Code = mdr["id"].ToString();
                stp.Unit_Price = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                stp.NAV_Date = Convert.ToDateTime(mdr["Daily_Unit_Price"].ToString());


                storedUnitPriceList.Add(stp);
            }
            con.Close();
            return storedUnitPriceList;
            
            

            //con.Open();
            //MySqlCommand cmd = new MySqlCommand(query, con);

            

            //cmd.ExecuteNonQuery();
            //con.Close();
            //return fund;
        }
        public static List<YesterdayDateUnitPrice> UpdatePrices(YesterdayDateUnitPrice yesterdayDateUnitPrice)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"update stored_unit_price set NAV_Date = @NAV_Date,
                                             Unit_Price = @Unit_Price
                                             where IPD_Fund_Code= @IPD_Fund_Code";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@NAV_Date", yesterdayDateUnitPrice.CompareDate);
            cmd.Parameters.AddWithValue("@Unit_Price", yesterdayDateUnitPrice.CompareUnitPrice);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", yesterdayDateUnitPrice.IPD);


            MySqlDataReader mdr = cmd.ExecuteReader();

            List<YesterdayDateUnitPrice> storedUnitPriceList = new List<YesterdayDateUnitPrice>();

            while (mdr.Read())
            {
                YesterdayDateUnitPrice stp = new YesterdayDateUnitPrice();

                stp.IPD = mdr["id"].ToString();
                stp.CompareUnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                stp.CompareDate = Convert.ToDateTime(mdr["Daily_Unit_Price"].ToString());


                storedUnitPriceList.Add(stp);
            }
            con.Close();
            return storedUnitPriceList;
        }

    }
}