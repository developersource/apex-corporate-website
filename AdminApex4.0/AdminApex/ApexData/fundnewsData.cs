﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class fundnewsData
    {
        public static fund_news Insert(fund_news fundnews)
        {

            MySqlConnection con = DBconnection.GetConnection();

            string sql = @"insert into fund_news(fund_news_news_id, fund_news_fund_id) values 
                (@fund_news_news_id, @fund_news_fund_id)";

            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@fund_news_news_id", fundnews.fund_news_news_id);
            cmd.Parameters.AddWithValue("@fund_news_fund_id", fundnews.fund_news_fund_id);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();


            return fundnews;
        }

        //public static fund_news Update(fund_news fundnews, int id)
        //{
        //    MySqlConnection con = DBconnection.GetConnection();

        //    string query = @"update news set fund_news_news_id = @fund_news_news_id,
        //                                     fund_news_fund_id = @fund_news_fund_id,
        //                                     where fund_news_news_id=@fund_news_news_id";

        //    MySqlCommand cmd = new MySqlCommand(query, con);
            
        //    cmd.Parameters.AddWithValue("@fund_news_news_id", id);
        //    cmd.Parameters.AddWithValue("@fund_news_fund_id", fundnews.fund_news_fund_id);

        //    con.Open();
        //    cmd.ExecuteNonQuery();
        //    con.Close();

        //    return fundnews;
        //}

        public static List<fund_news> GetByID(Int32 id)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from fund_news where fund_news_news_id=@fund_news_news_id";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@fund_news_news_id", id);

            MySqlDataReader mdr = cmd.ExecuteReader();

            List<fund_news> fnl = new List<fund_news>();


            while (mdr.Read())
            {
                fund_news fn = new fund_news()
                {
                    ID = Convert.ToInt32(mdr["id"].ToString()),
                    fund_news_news_id = Convert.ToInt32(mdr["fund_news_news_id"].ToString()),
                    fund_news_fund_id = Convert.ToInt32(mdr["fund_news_fund_id"].ToString())
                };
                fnl.Add(fn);
            }
            con.Close();
            return fnl;
        }
    }
}