﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class DownloadTypeData
    {
        public static DownloadType Insert(DownloadType downloadType)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO download_type(
                            download_type_name,
                            status, 
                            is_fund) VALUES(
                            @download_type_name,
                            @status,
                            @is_fund)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@download_type_name", downloadType.TypeName);
            cmd.Parameters.AddWithValue("@status", downloadType.Status);
            cmd.Parameters.AddWithValue("@is_fund", downloadType.IsFund);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return downloadType;
        }

        public static List<DownloadType> GetAllDownloadType()
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"SELECT * FROM download_type";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<DownloadType> downloadTypeList = new List<DownloadType>();

            while (mdr.Read())
            {
                DownloadType downloadType = new DownloadType()
                {
                    Id = Convert.ToInt32(mdr["id"].ToString()),
                    TypeName = mdr["download_type_name"].ToString(),
                    Status = Convert.ToInt32(mdr["status"].ToString()),
                    IsFund = Convert.ToInt32(mdr["is_fund"].ToString())
                };

                downloadTypeList.Add(downloadType);
            }
            conn.Close();
            return downloadTypeList;
        }

        public static DownloadType GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from download_type where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            DownloadType downloadType = new DownloadType();
            if (mdr.Read())
            {
                downloadType.Id = Convert.ToInt32(mdr["id"].ToString());
                downloadType.TypeName = mdr["download_type_name"].ToString();
                downloadType.Status = Convert.ToInt32(mdr["status"].ToString());
                downloadType.IsFund = Convert.ToInt32(mdr["is_fund"].ToString());
            }
            conn.Close();
            return downloadType;
        }

        public static DownloadType Update(DownloadType downloadType)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE download_type SET download_type_name = @download_type_name,
                           status = @status,
                           is_fund = @is_fund,
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", downloadType.Id);
            cmd.Parameters.AddWithValue("@download_type_name", downloadType.TypeName);
            cmd.Parameters.AddWithValue("@status", downloadType.Status);
            cmd.Parameters.AddWithValue("@is_fund", downloadType.IsFund);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return downloadType;
        }

        public static void Delete(int id)
        {
            using (MySqlConnection sqlCon = DBconnection.GetConnection())
            {
                using (MySqlCommand cmdGet = new MySqlCommand())
                {
                    cmdGet.CommandText = @"DELETE FROM download_type where id = @id";

                    cmdGet.Parameters.AddWithValue("@id", id);
                    cmdGet.Connection = sqlCon;
                    sqlCon.Open();
                    int n = cmdGet.ExecuteNonQuery();
                    sqlCon.Close();
                }
            }

        }
    }
}