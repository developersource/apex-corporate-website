﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using AdminApex.ApexUtility;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class StoredFundData
    {
        public static List<StoredFund> GetRAWData() 
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select A.id,A.IPD_Fund_Code,A.Daily_NAV_Date,A.Daily_NAV_Time,A.Daily_Unit_Price from 
                             utmc_daily_nav_fund A inner join (select 
                             IPD_Fund_Code,max(Daily_NAV_Date) as DateMAx from utmc_daily_nav_fund
                             group by IPD_Fund_Code) B on A.IPD_Fund_Code=B.IPD_Fund_Code and A.Daily_NAV_Date=B.DateMAx 
                             order by IPD_Fund_Code;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredFund> storedFundList = new List<StoredFund>();

            while (mdr.Read())
            {
                StoredFund sf = new StoredFund();

                sf.IPDFundCode = mdr["IPD_Fund_Code"].ToString();
                sf.UnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                sf.NAVDate = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());


                storedFundList.Add(sf);
            }
            con.Close();
            return storedFundList;
        }
        public static List<StoredFund> GetStoredData()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT Fund_IPD_Code, NAV_Date, Unit_Price, Img_Decider, 
                            Y_Unit_Price FROM stored_fund;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredFund> storedFundList = new List<StoredFund>();

            while (mdr.Read())
            {
                StoredFund sf = new StoredFund();

                sf.IPDFundCode = mdr["Fund_IPD_Code"].ToString();
                sf.UnitPrice = Convert.ToDecimal(mdr["Unit_Price"].ToString());
                sf.NAVDate = Convert.ToDateTime(mdr["NAV_Date"].ToString());
                sf.ImgDecider = Convert.ToInt16(mdr["Img_Decider"].ToString());
                sf.YUnitPrice = Convert.ToDecimal(mdr["Y_Unit_Price"].ToString());

                storedFundList.Add(sf);
            }
            con.Close();
            return storedFundList;
        }
        public static List<StoredFund> UpdatePrices(StoredFund storedFund)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"update stored_fund set NAV_Date = @NAV_Date,
                                             Unit_Price = @Unit_Price,
                                            Img_Decider = @Img_Decider,
                                            Y_Unit_Price = @Y_Unit_Price
                                            
                                             where Fund_IPD_Code= @Fund_IPD_Code";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@NAV_Date", storedFund.NAVDate);
            cmd.Parameters.AddWithValue("@Unit_Price", storedFund.UnitPrice);
            cmd.Parameters.AddWithValue("@Fund_IPD_Code", storedFund.IPDFundCode);
            cmd.Parameters.AddWithValue("@Img_Decider", storedFund.ImgDecider);
            cmd.Parameters.AddWithValue("@Y_Unit_Price", storedFund.YUnitPrice);
            //cmd.Parameters.AddWithValue("@Time", storedFund.Time);

            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredFund> storedFundList = new List<StoredFund>();

            while (mdr.Read())
            {
                StoredFund sf = new StoredFund();

                sf.IPDFundCode = mdr["Fund_IPD_Code"].ToString();
                sf.UnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                sf.NAVDate = Convert.ToDateTime(mdr["Daily_Unit_Price"].ToString());
                sf.ImgDecider = Convert.ToInt16(mdr["Img_Deceider"].ToString());
                sf.YUnitPrice = Convert.ToDecimal(mdr["Y_Unit_Price"].ToString());
                //sf.YUnitPrice = Convert.ToDecimal(mdr["Time"].ToString());

                storedFundList.Add(sf);
            }
            con.Close();
            return storedFundList;
        }
        public static List<StoredFund> GetLatestDate()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select A.Daily_NAV_Date from 
                             utmc_daily_nav_fund A inner join (select 
                             IPD_Fund_Code,max(Daily_NAV_Date) as DateMAx from utmc_daily_nav_fund
                             group by IPD_Fund_Code) B on A.IPD_Fund_Code=B.IPD_Fund_Code and A.Daily_NAV_Date=B.DateMAx
                             order by Daily_NAV_Date DESC;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredFund> storedFundList = new List<StoredFund>();

            while (mdr.Read())
            {
                StoredFund sf = new StoredFund();

                sf.NAVDate = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
               
                
                storedFundList.Add(sf);
            }
            con.Close();
            return storedFundList;
        }
        public static List<StoredFund> GetLatestTime()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select A.Daily_NAV_Time from 
                             utmc_daily_nav_fund A inner join (select 
                             IPD_Fund_Code,max(Daily_NAV_Date) as DateMAx from utmc_daily_nav_fund
                             group by IPD_Fund_Code) B on A.IPD_Fund_Code=B.IPD_Fund_Code and A.Daily_NAV_Date=B.DateMAx 
                             order by Daily_NAV_Time DESC;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<StoredFund> storedFundList = new List<StoredFund>();

            while (mdr.Read())
            {
                StoredFund sf = new StoredFund();

                sf.NAVTime = Convert.ToDateTime(mdr["Daily_NAV_Time"].ToString());


                storedFundList.Add(sf);
            }
            con.Close();
            return storedFundList;
        }
    }
}