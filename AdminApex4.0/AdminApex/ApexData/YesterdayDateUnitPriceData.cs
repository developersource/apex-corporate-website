﻿using System;
using System.Collections.Generic;
using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class YesterdayDateUnitPriceData
    {
        public static List<YesterdayDateUnitPrice> PriceGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT IPD_Fund_Code, Daily_NAV_Date, Daily_Unit_Price FROM utmc_daily_nav_fund WHERE Daily_NAV_Date = DATE_SUB((SELECT Daily_NAV_Date FROM utmc_daily_nav_fund ORDER BY Daily_NAV_Date DESC LIMIT 1), INTERVAL 1 DAY) ORDER BY IPD_Fund_Code;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<YesterdayDateUnitPrice> fundlist = new List<YesterdayDateUnitPrice>();

            while (mdr.Read())
            {
                YesterdayDateUnitPrice dnf = new YesterdayDateUnitPrice();

                
                dnf.IPD = mdr["IPD_Fund_Code"].ToString();
                dnf.CompareDate = Convert.ToDateTime(mdr["Daily_NAV_Date"].ToString());
                dnf.CompareUnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());


                fundlist.Add(dnf);
            }
            con.Close();
            return fundlist;
        }
        public static List<YesterdayDateUnitPrice> StoredPriceGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"SELECT * FROM stored_unit_price";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<YesterdayDateUnitPrice> storedUnitPriceList = new List<YesterdayDateUnitPrice>();

            while (mdr.Read())
            {
                YesterdayDateUnitPrice stp = new YesterdayDateUnitPrice();

                stp.IPD = mdr["IPD_Fund_Code"].ToString();
                stp.CompareUnitPrice = Convert.ToDecimal(mdr["Unit_Price"].ToString());
                stp.CompareDate = Convert.ToDateTime(mdr["NAV_Date"].ToString());


                storedUnitPriceList.Add(stp);
            }
            con.Close();
            return storedUnitPriceList;
        }
        public static List<YesterdayDateUnitPrice> UpdatePrices(YesterdayDateUnitPrice yesterdayDateUnitPrice)
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"update stored_unit_price set NAV_Date = @NAV_Date,
                                             Unit_Price = @Unit_Price
                                             where IPD_Fund_Code= @IPD_Fund_Code";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Parameters.AddWithValue("@NAV_Date", yesterdayDateUnitPrice.CompareDate);
            cmd.Parameters.AddWithValue("@Unit_Price", yesterdayDateUnitPrice.CompareUnitPrice);
            cmd.Parameters.AddWithValue("@IPD_Fund_Code", yesterdayDateUnitPrice.IPD);


            MySqlDataReader mdr = cmd.ExecuteReader();

            List<YesterdayDateUnitPrice> storedUnitPriceList = new List<YesterdayDateUnitPrice>();

            while (mdr.Read())
            {
                YesterdayDateUnitPrice stp = new YesterdayDateUnitPrice();

                stp.IPD = mdr["id"].ToString();
                stp.CompareUnitPrice = Convert.ToDecimal(mdr["Daily_Unit_Price"].ToString());
                stp.CompareDate = Convert.ToDateTime(mdr["Daily_Unit_Price"].ToString());


                storedUnitPriceList.Add(stp);
            }
            con.Close();
            return storedUnitPriceList;
        }
    }
}