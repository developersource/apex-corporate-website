﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class DownloadData
    {
        public static Download Insert(Download download)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO download(
                            upload_date, 
                            created_by, 
                            download_download_type_id,
                            download_fund_id,
                            file_name,
                            file_author,
                            url_path, 
                            file_size,
                            display_date,
                            status) VALUES(
                            @upload_date, 
                            @created_by, 
                            @download_download_type_id,
                            @download_fund_id,
                            @file_name,
                            @file_author,
                            @url_path, 
                            @file_size,
                            @display_date,
                            @status)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@upload_date", download.UploadDate);
            cmd.Parameters.AddWithValue("@created_by", download.CreatedBy);
            cmd.Parameters.AddWithValue("@download_download_type_id", download.DownloadType_Id);
            cmd.Parameters.AddWithValue("@download_fund_id", download.Fund_Id);
            cmd.Parameters.AddWithValue("@file_name", download.FileName);
            cmd.Parameters.AddWithValue("@file_author", download.FileAuthor);
            cmd.Parameters.AddWithValue("@url_path", download.UrlPath);
            cmd.Parameters.AddWithValue("@file_size", download.FileSize);
            cmd.Parameters.AddWithValue("@display_date", download.DisplayDate);
            cmd.Parameters.AddWithValue("@status", download.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return download;
        }

        public static List<Download> GetAllDownload()
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"SELECT * FROM download  order by upload_date desc";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Download> downloadList = new List<Download>();

            while (mdr.Read())
            {
                Download download = new Download();
                download.Id = Convert.ToInt32(mdr["id"].ToString());
                download.UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString());
                download.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                download.DownloadType_Id = Convert.ToInt32(mdr["download_download_type_id"].ToString());
                download.Fund_Id = Convert.ToInt32(mdr["download_fund_id"].ToString());
                download.FileName = mdr["file_name"].ToString();
                download.FileAuthor = mdr["file_author"].ToString();
                download.UrlPath = mdr["url_path"].ToString();
                download.FileSize = mdr["file_size"].ToString();
                download.DisplayDate = Convert.ToDateTime(mdr["display_date"].ToString());
                download.Status = Convert.ToInt32(mdr["status"].ToString());

                downloadList.Add(download);
            }
            conn.Close();
            return downloadList;
        }

        public static List<Download> GetDownloadByType(int typeid)
        {
            MySqlConnection conn = DBconnection.GetConnection();
            string query = @"SELECT * FROM download  WHERE download_download_type_id = @download_download_type_id AND status = 1 order by upload_date desc";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@download_download_type_id", typeid);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Download> downloadList = new List<Download>();

            while (mdr.Read())
            {
                Download download = new Download();
                download.Id = Convert.ToInt32(mdr["id"].ToString());
                download.UploadDate = Convert.ToDateTime(mdr["upload_date"].ToString());
                download.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                download.DownloadType_Id = Convert.ToInt32(mdr["download_download_type_id"].ToString());
                download.Fund_Id = Convert.ToInt32(mdr["download_fund_id"].ToString());
                download.FileName = mdr["file_name"].ToString();
                download.FileAuthor = mdr["file_author"].ToString();
                download.UrlPath = mdr["url_path"].ToString();
                download.FileSize = mdr["file_size"].ToString();
                download.DisplayDate = Convert.ToDateTime(mdr["display_date"].ToString());
                download.Status = Convert.ToInt32(mdr["status"].ToString());

                downloadList.Add(download);
            }
            conn.Close();
            return downloadList;
        }

        public static Download GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from download where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            Download download = new Download();
            if (mdr.Read())
            {
                download.Id = Convert.ToInt32(mdr["id"].ToString());
                download.UploadDate = DateTime.Parse(mdr["upload_date"].ToString());
                download.CreatedBy = Convert.ToInt32(mdr["created_by"].ToString());
                download.DownloadType_Id = Convert.ToInt32(mdr["download_download_type_id"].ToString());
                download.Fund_Id = Convert.ToInt32(mdr["download_fund_id"].ToString());
                download.FileName = mdr["file_name"].ToString();
                download.FileAuthor = mdr["file_author"].ToString();
                download.UrlPath = mdr["url_path"].ToString();
                download.FileSize = mdr["file_size"].ToString();
                download.DisplayDate = DateTime.Parse(mdr["display_date"].ToString());
                download.Status = Convert.ToInt32(mdr["status"].ToString());

            }
            conn.Close();
            return download;
        }

        public static Download Update(Download download)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE download SET upload_date = @upload_date,
                           created_by = @created_by, 
                           download_download_type_id = @download_download_type_id,
                           download_fund_id = @download_fund_id,
                           file_name = @file_name,
                           file_author = @file_author,
                           url_path = @url_path,
                           file_size = @file_size,
                           display_date = @display_date,
                           status = @status 
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", download.Id);
            cmd.Parameters.AddWithValue("@upload_date", download.UploadDate);
            cmd.Parameters.AddWithValue("@created_by", download.CreatedBy);
            cmd.Parameters.AddWithValue("@download_download_type_id", download.DownloadType_Id);
            cmd.Parameters.AddWithValue("@download_fund_id", download.Fund_Id);
            cmd.Parameters.AddWithValue("@file_name", download.FileName);
            cmd.Parameters.AddWithValue("@file_author", download.FileAuthor);
            cmd.Parameters.AddWithValue("@url_path", download.UrlPath);
            cmd.Parameters.AddWithValue("@file_size", download.FileSize);
            cmd.Parameters.AddWithValue("@display_date", download.DisplayDate);
            cmd.Parameters.AddWithValue("@status", download.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return download;
        }

        public static void Delete(int id)
        {
            using (MySqlConnection sqlCon = DBconnection.GetConnection())
            {
                using (MySqlCommand cmdGet = new MySqlCommand())
                {
                    cmdGet.CommandText = @"DELETE FROM admin where id = @id";

                    cmdGet.Parameters.AddWithValue("@id", id);
                    cmdGet.Connection = sqlCon;
                    sqlCon.Open();
                    int n = cmdGet.ExecuteNonQuery();
                    sqlCon.Close();
                }
            }

        }
    }
}