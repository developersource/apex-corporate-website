﻿using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace AdminApex.ApexData
{
    public class AdminData
    {
        public static Admin Insert(Admin admin)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO admin(
                            username, 
                            password, 
                            name,
                            department,
                            email,
                            status,
                            locked) VALUES(
                            @username, 
                            @password, 
                            @name,
                            @department,
                            @email,
                            @status,
                            @locked)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@username", admin.username);
            cmd.Parameters.AddWithValue("@password", admin.Password);
            cmd.Parameters.AddWithValue("@name", admin.name);
            cmd.Parameters.AddWithValue("@department", admin.Department);
            cmd.Parameters.AddWithValue("@email", admin.Email);
            cmd.Parameters.AddWithValue("@status", admin.Status);
            cmd.Parameters.AddWithValue("@locked", admin.locked);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return admin;
        }

        public static List<Admin> GetAllAdmin()
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select * from admin";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<Admin> admins = new List<Admin>();
            while (mdr.Read())
            {
                Admin admin = new Admin();

                admin.Id = Convert.ToInt32(mdr["id"].ToString());
                admin.username = mdr["username"].ToString();
                admin.Password = mdr["password"].ToString();
                admin.name = mdr["name"].ToString();
                admin.Department = mdr["department"].ToString();
                admin.Email = mdr["email"].ToString();
                admin.Status = Convert.ToInt32(mdr["status"].ToString());
                admin.locked = Convert.ToInt32(mdr["locked"].ToString());

                admins.Add(admin);
            }
            con.Close();
            return admins;
        }

        public static Admin GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from admin where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            Admin admin = new Admin();
            if (mdr.Read())
            {
                admin.Id = Convert.ToInt32(mdr["id"].ToString());
                admin.username = mdr["username"].ToString();
                admin.Password = mdr["password"].ToString();
                admin.name = mdr["name"].ToString();
                admin.Department = mdr["department"].ToString();
                admin.Email = mdr["email"].ToString();
                admin.Status = Convert.ToInt32(mdr["status"].ToString());
                admin.locked = Convert.ToInt32(mdr["locked"].ToString());
            }
            conn.Close();
            return admin;
        }

        public static Admin Update(Admin admin)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE admin SET username = @username,
                           password = @password, 
                           name = @name,
                           department = @department,
                           email = @email,
                           status = @status
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", admin.Id);
            cmd.Parameters.AddWithValue("@username", admin.username);
            cmd.Parameters.AddWithValue("@password", admin.Password);
            cmd.Parameters.AddWithValue("@name", admin.name);
            cmd.Parameters.AddWithValue("@department", admin.Department);
            cmd.Parameters.AddWithValue("@email", admin.Email);
            cmd.Parameters.AddWithValue("@status", admin.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return admin;
        }

        public static Admin UpdateLOCK(Admin admin)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE admin SET locked = @locked
                           where username = @username";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@username", admin.username);
            cmd.Parameters.AddWithValue("@locked", admin.locked);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return admin;
        }
    }
}