﻿using AdminApex.ApexUtility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminApex.ApexData
{
    public class UserLogData
    {
        public static UserLog Insert(UserLog userLog)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO user_log(
                            title, 
                            url_path, 
                            month,
                            year,  
                            created_date,
                            status) VALUES(
                            @title, 
                            @url_path, 
                            @month,
                            @year,
                            @created_date,
                            @status)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@title", userLog.Title);
            cmd.Parameters.AddWithValue("@url_path", userLog.UrlPath);
            cmd.Parameters.AddWithValue("@month", userLog.Month);
            cmd.Parameters.AddWithValue("@year", userLog.Year);
            cmd.Parameters.AddWithValue("@created_date", userLog.CreatedDate);
            cmd.Parameters.AddWithValue("@status", userLog.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            conn.Open();
            String query = @"SELECT ID FROM user_log where  month= @month and year = @year";
            MySqlCommand cmdselect = new MySqlCommand(query, conn);
            cmdselect.Parameters.AddWithValue("@month", userLog.Month);
            cmdselect.Parameters.AddWithValue("@year", userLog.Year);

            MySqlDataReader mdr = cmdselect.ExecuteReader(CommandBehavior.CloseConnection);
            if (mdr.Read())
            {
                userLog.Id = Convert.ToInt32(mdr["id"].ToString());
            }
            conn.Close();

            return userLog;
        }

        public static List<UserLog> GetAllUserLog()
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select * from user_log";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<UserLog> userLogList = new List<UserLog>();
            while (mdr.Read())
            {
                UserLog userLog = new UserLog();

                userLog.Id = Convert.ToInt32(mdr["id"].ToString());
                userLog.Title = mdr["title"].ToString();
                userLog.UrlPath = mdr["url_path"].ToString();
                userLog.Month = mdr["month"].ToString();
                userLog.Year = mdr["year"].ToString();
                userLog.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                userLog.Status = Convert.ToInt32(mdr["status"].ToString());

                userLogList.Add(userLog);
            }
            con.Close();
            return userLogList;
        }

        public static UserLog GetById(int Id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from user_log where Id = @Id";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            UserLog userLog = new UserLog();
            if (mdr.Read())
            {
                userLog.Id = Convert.ToInt32(mdr["id"].ToString());
                userLog.Title = mdr["title"].ToString();
                userLog.UrlPath = mdr["url_path"].ToString();
                userLog.Month = mdr["month"].ToString();
                userLog.Year = mdr["year"].ToString();
                userLog.CreatedDate = Convert.ToDateTime(mdr["created_date"].ToString());
                userLog.Status = Convert.ToInt32(mdr["status"].ToString());
            }
            conn.Close();
            return userLog;
        }

        public static UserLog Update(UserLog userLog)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE user_log SET title = @title,
                           url_path = @url_path, 
                           month = @month,
                           year = @year,
                           created_date = @created_date,
                           status = @status
                           where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", userLog.Id);
            cmd.Parameters.AddWithValue("@title", userLog.Title);
            cmd.Parameters.AddWithValue("@url_path", userLog.UrlPath);
            cmd.Parameters.AddWithValue("@month", userLog.Month);
            cmd.Parameters.AddWithValue("@year", userLog.Year);
            cmd.Parameters.AddWithValue("@created_date", userLog.CreatedDate);
            cmd.Parameters.AddWithValue("@status", userLog.Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return userLog;
        }

        public static int readlastrow()
        {
            int id = 0;

            MySqlConnection con = DBconnection.GetConnection();
            //SELECT TOP 1 * FROM table_Name ORDER BY unique_column DESC
            string query = @"select MAX(id) as id from user_log";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();

            if (mdr.Read())
            {
                id = Convert.ToInt32(mdr["id"].ToString());
            }
            con.Close();

            return id;
        }

        public static void UpdateException()
        {
            int lastRowId = readlastrow();

            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE user_log SET status = 99 where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", lastRowId);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static void Update()
        {
            int lastRowId = readlastrow();

            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"UPDATE user_log SET status = 0 where id = @id";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@id", lastRowId);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}