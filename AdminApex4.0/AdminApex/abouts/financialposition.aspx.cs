﻿using AdminApex.ApexService;
using AdminApex.ApexUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class FinancialPosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            List<FinancialInfo> fpu = new List<FinancialInfo>();

            fpu = FinancialInfoService.GetAllFi().Where(x => x.Status == 1).ToList();
            fpu = fpu.OrderByDescending(x => x.year).ToList();
            FinancialInfo firstfi = fpu.FirstOrDefault();

            myear1.Text = Convert.ToString(firstfi.year);
            year1.Text = Convert.ToString(firstfi.year);
            issue1.Text = Convert.ToString(firstfi.issue_paidup_capital);
            share1.Text = Convert.ToString(firstfi.shareholder_fund);
            revenue1.Text = Convert.ToString(firstfi.revenue);
            before1.Text = Convert.ToString(firstfi.profit_loss_before_tax);
            after1.Text = Convert.ToString(firstfi.profit_loss_after_tax);
            cash1.Text = Convert.ToString(firstfi.cash_bank_deposits);

            FinancialInfo secondfi = fpu.Skip(1).FirstOrDefault();

            myear2.Text = Convert.ToString(secondfi.year);
            year2.Text = Convert.ToString(secondfi.year);
            issue2.Text = Convert.ToString(secondfi.issue_paidup_capital);
            share2.Text = Convert.ToString(secondfi.shareholder_fund);
            revenue2.Text = Convert.ToString(secondfi.revenue);
            before2.Text = Convert.ToString(secondfi.profit_loss_before_tax);
            after2.Text = Convert.ToString(secondfi.profit_loss_after_tax);
            cash2.Text = Convert.ToString(secondfi.cash_bank_deposits);

            FinancialInfo thridfi = fpu.Skip(2).FirstOrDefault();

            myear3.Text = Convert.ToString(thridfi.year);
            year3.Text = Convert.ToString(thridfi.year);
            issue3.Text = Convert.ToString(thridfi.issue_paidup_capital);
            share3.Text = Convert.ToString(thridfi.shareholder_fund);
            revenue3.Text = Convert.ToString(thridfi.revenue);
            before3.Text = Convert.ToString(thridfi.profit_loss_before_tax);
            after3.Text = Convert.ToString(thridfi.profit_loss_after_tax);
            cash3.Text = Convert.ToString(thridfi.cash_bank_deposits);




            List<FundSummary> fsu = new List<FundSummary>();

            fsu = FundSummaryService.GetAllFs();
            FundSummary firstfs = fsu.Where(x => x.fund_categ == "Unit Trust Fund" && x.Status == 1).FirstOrDefault();

            fund1.Text = firstfs.fund_categ;
            no1.Text = Convert.ToString(firstfs.no_of_fund);
            value1.Text = (firstfs.total_value).ToString("n0");

            FundSummary secondfs = fsu.Where(x => x.fund_categ == "Wholesale Fund" && x.Status == 1).FirstOrDefault();

            fund2.Text = secondfs.fund_categ;
            no2.Text = Convert.ToString(secondfs.no_of_fund);
            value2.Text = (secondfs.total_value).ToString("n0");

            totalno.Text = Convert.ToString(firstfs.no_of_fund + secondfs.no_of_fund);
            totalvalue.Text = (firstfs.total_value + secondfs.total_value).ToString("n0");




            List<FundSummary> fi = new List<FundSummary>();
            fi = FundSummaryService.GetAllFs().Where(x => x.Status == 99).ToList();

            FundSummary fss = fi.FirstOrDefault();

            navdate.Text = fss.CreatedDate.ToString("dd MMMM yyyy");

        }
    }
}