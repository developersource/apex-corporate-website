﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="OurValues.aspx.cs" Inherits="AdminApex.ourvalues" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <div class="ourvalues">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image">
                    <img src="/ICONPIC/APEX_Header_aboutus.jpg" />
                    <div class="header-copy">
                        <h2>20 years strong.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content narrow">
            <div class="inner-content">
                <h3 class="">Vision Statement</h3>
                <p style="font-size:20px">A trusted investment partner, committed to excellence and sustainability in our performance.</p>
            </div>
        </div>
        <div class="content">
            <div class="pointers-frame">
                <img src="/ICONPIC/pointers.jpg" />
            </div>
        </div>
        <div class="content narrow vision">
            <div class="inner-content">
                <h3>Our Values</h3>
                <!--<h4 style="width: 100%; text-align: center;">WE,</h4>-->
                <div class="pointers-info">
                    <ul>
                        <li>
                            <p>We believe that <b>trust</b> is the bedrock of a successful relationship.</p>
                        </li>
                        <li>
                            <p>We consider our stakeholders as
                                <br>
                                <b>long-term partners.</b></p>
                        </li>
                        <li>
                            <p>We are <b>committed</b> to living our values and fulfilling our vision.</p>
                        </li>
                        <li>
                            <p>We strive for <b>excellence</b> in our business.</p>
                        </li>
                        <li>
                            <p>We believe in <b>fairness</b> and <b>sustainability</b> in our dealings.</p>
                        </li>
                        <li>
                            <p>We have a duty to <b>perform</b> our best for our investors.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
