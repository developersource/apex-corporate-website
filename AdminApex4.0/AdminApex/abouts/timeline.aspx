﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="timeline.aspx.cs" Inherits="AdminApex.Timeline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="profile">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image">
                    <img src="/ICONPIC/APEX_Header_aboutus.jpg" />
                    <div class="header-copy">
                        <h2>20 years strong.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content bg-gray z-0">
            <h3 class="inner-content">Corporate Timeline</h3>
            <p class="desc">The Company is a licensed Unit Trust Management Company (UTMC) and a licensed Asset Management Company (AMC). It is also an Institutional Unit Trust Adviser (IUTA) which allows the company to distribute and deal in third-party unit trust Funds.</p>
            <p class="desc">The Company is regulated by Securities Commission Malaysia (SC) and Federation of Investment Managers Malaysia (FIMM).</p>
            <div class="timeline">
                <div class="dot start"></div>
                <div class="backbone"></div>
                <div class="dot first"></div>
                <div class="card first">
                    <p>The Company was incorporated on February 21, 1997 as Jardine Fleming Apex Unit Trusts Berhad (JFAUTB). It was subsequently registered as a Unit Trust Management Company (UTMC) with FIMM. The Company’s first open-ended unit trust Fund was launched in 1997.</p>
                </div>
                <div class="dot second"></div>
                <div class="card second float-right">
                    <p>In 1999, Apex Equity Holdings Group acquired the 15% interest in JFAUTB held by Jardine Fleming Asset Management (HK). The Company was subsequently renamed to Apex Unit Trusts Berhad.</p>
                </div>
                <div class="dot third"></div>
                <div class="card third">
                    <p>In 2000, the Company was granted an Institutional Unit Trust Adviser (IUTA) status by FIMM.</p>
                </div>
                <div class="dot fourth"></div>
                <div class="card fourth float-right">
                    <p>In 2004, the Company was renamed to Astute Fund Management Berhad ("AFMB").</p>
                </div>
                <div class="dot fifth"></div>
                <div class="card fifth">
                    <p>In 2008, the Company was issued a Capital Markets Services License (CMSL) for the regulated activities of Fund management and dealing in securities restricted to unit trust products. This came under the single licensing regime introduced by the SC in 2007.</p>
                </div>
                <div class="dot sixth"></div>
                <div class="card sixth float-right">
                    <p>In 2014, Med-Bumikar Mara Sdn. Bhd. acquired a 43% stake in the Company. At the same time, two former employees of J.P. Morgan joined the Company’s senior management. Apex Equity Holdings Group retained a 43% stake in the Company.</p>
                </div>
                <div class="dot seventh"></div>
                <div class="card seventh">
                    <p>In March 2022, the 43% stake held by JF Apex Securities Berhad was acquired by several investors. To reflect the entity's independent identity, the name of the Company was changed from Apex Investments Services Berhad to Astute Fund Management Berhad.</p>
                </div>
                <div class="dot last"></div>
            </div>
        </div>
        <div class="content narrow z-1">
            <div class="shareholding_structure">
                <div class="copy">
                    <p>The Company manages 8 equity unit trust Funds. These Funds are Astute Malaysia Growth Trust, Astute Quantum Fund, Astute Dynamic Fund, Astute Dana Al-Sofi-I, Astute Dana Al-Faiz-I, Astute Dana Aslah, Astute Asian (Ex Japan) Fund and Astute Maximiser Dividend Fund. It manages 1 money market Funds: Astute Dana Al-Kanz.</p>
                </div>
                <h3>SHAREHOLDING STRUCTURE</h3>
                <div class="pie-holder">
                    <img src="../ICONPIC/png.jpg" />
                    <canvas id="shareholdChart"></canvas>
                   <%-- <h4 class="apex">43%<br>
                        <span class="mobile-hidden">JF Apex<br>
                            Securities Berhad<sup>1</sup></span><div class="line mobile-hidden"></div>
                    </h4>--%>
                    <h4 class="med">43%<br>
                        <span class="mobile-hidden">Med-Bumikar Mara<br>
                            Sdn. Bhd.<sup>1</sup></span><div class="line mobile-hidden"></div>
                    </h4>
                    <h4 class="others">57%<br>
                        <span class="mobile-hidden">Management<br>
                            &amp; Others</span><div class="line mobile-hidden"></div>
                    </h4>
                    <ul class="desktop-hidden">
                        <%--<li>
                            <div class="legend legend-apex"></div>
                            <h4><span>JF Apex<br>
                                Securities Berhad<sup>1</sup></span></h4>
                        </li>--%>
                        <li>
                            <div class="legend legend-med"></div>
                            <h4><span>Med-Bumikar Mara<br>
                                Sdn. Bhd.<sup>1</sup></span></h4>
                        </li>
                        <li>
                            <div class="legend legend-others"></div>
                            <h4><span>Management<br>
                                &amp; Others</span></h4>
                        </li>
                    </ul>
                </div>
                <div class="pie-explanation">
                    <%--<div class="left">
                        <p><b>1. JF Apex Securities Berhad (JFAS)</b></p>
                        <p>JFAS is a wholly owned subsidiary of Apex Equity Holdings Berhad.</p>
                        <p>For details, see <a href="//www.apexequity.com.my" target="_blank">www.apexequity.com.my</a>.</p>
                    </div>--%>
                    <div class="right">
                        <p><b>1. Med-Bumikar Mara Sdn. Bhd.</b></p>
                        <p>Med-Bumikar Mara Sdn. Bhd. – The parent company of MBM Resources Berhad, a company listed on Bursa Malaysia. (Bloomberg code: MBMMK).</p>
                        <p>For details, see <a href="http://www.mbmr.com.my/" target="_blank">www.mbmr.com.my</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
