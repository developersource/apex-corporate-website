﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminApex
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static object SendEmail(string name, string email, string tel, string comment)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            string SMTPHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            string FromEmail = ConfigurationManager.AppSettings["FromEmail"].ToString();
            string FromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();


            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(FromEmail);
            //msg.To.Add("ida@astutefm.com.my");
            //msg.To.Add("devi@astutefm.com.my");
            //msg.To.Add("suhana@astutefm.com.my");
            msg.To.Add(ToEmail);
            msg.Subject = "Enquiry & FeedBack";

            msg.Body = "Name : " + name + "\n" +
                       "<br /><br />Contact Number : " + tel + "\n" +
                       "<br /><br />Email Address  : " + email + "\n" +
                       "<br /><br />Enquiry / Feedback : " + comment + "\n" +
                       "<br /><br />";
            msg.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = SMTPHost;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            //NetworkCred.UserName = "feedback@astutefm.com.my";
            //NetworkCred.Password = "Apex1234!";
            NetworkCred.UserName = FromEmail;
            NetworkCred.Password = FromEmailPassword;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(msg);
            return "";
            //Response.Write("<script>window.alert('Your feedback had successfully send !');</script>");
        }
    }
}