﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AdminApex.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/Apex.css" rel="stylesheet" />
    <style>
        body {
            background-color: grey;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Login</h1>
        <div id="divuserid" style="text-align: center">
            <asp:TextBox ID="txtusername" placeholder="User Name" runat="server" CssClass="box"></asp:TextBox>
            <span class="help-block"></span>
            <br />
            <br />
            <asp:TextBox ID="txtpassword" TextMode="Password" placeholder="User Password" runat="server" CssClass="box" onkeypress="return isNumberKey(event)"></asp:TextBox>
            <span class="help-block"></span>
            <br />
        </div>
        <br />
        <div style="text-align: center">
            <asp:Button ID="login" runat="server" Text="Login" CssClass="btn" OnClick="login_Click" />
            <br />
            <asp:Label ID="locked" runat="server" Text="" ForeColor="White"></asp:Label>
        </div>
    </form>
</body>
</html>
