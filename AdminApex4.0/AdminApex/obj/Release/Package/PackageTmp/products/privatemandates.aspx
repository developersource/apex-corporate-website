﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="PrivateMandates.aspx.cs" Inherits="AdminApex.PrivateMandates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
			
		.header-content.wide-image {background-image: url(../ICONPIC/APEX_Header_ourproducts.jpg) !important;}
	</style>

<div class="products private_mandate">
    <div class="content pages">
    	<div class="products header bg-white">
            <div class="header-content wide-image">
                <img src="ICONPIC/APEX_Header_ourproducts.jpg" />
                <div class="header-copy">
                    <h2>BUILT ON VALUES,<br /> DRIVEN BY PARTNERSHIPS.</h2>
                    <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content narrow">
        <div class="inner-content">
			<h3>Private Mandates</h3><p><strong>Discretionary Portfolio Management</strong></p><p>The Company offers discretionary portfolio management services in equities and fixed income. This is commonly referred to as private mandates. This is available for an initial investment above a certain threshold.</p><p>This service is suitable for investors who prefer their own customised portfolios. These investors may specify certain requirements for their portfolios.</p><p>For discretionary mandates, we work closely with our clients to understand their risk profiles and investment objectives.</p><p>For this service, investors inform the Manager the parameters and objectives for their investments. This is documented in the Investment Management Agreement (IMA). Our portfolio manager then adopts an asset allocation strategy and security selection process that fit the investor's objectives.</p><p>In order to keep our investors informed, we ensure that our investors receive a monthly update on their investments.</p>			        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
