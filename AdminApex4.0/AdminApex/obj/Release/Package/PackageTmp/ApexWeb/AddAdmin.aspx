﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddAdmin.aspx.cs" Inherits="AdminApex.ApexWeb.AddAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Add Admin</h2>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>Name</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Department</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>                    
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Email</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>                    
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Username</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>                    
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Password</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>                      
                    </div>                    
                </div>
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="lblSubmit" runat="server" Text="Save" CssClass="btn" OnClick="lblSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
