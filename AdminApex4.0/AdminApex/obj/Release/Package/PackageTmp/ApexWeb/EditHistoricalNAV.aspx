﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="EditHistoricalNAV.aspx.cs" Inherits="AdminApex.ApexWeb.EditHistoricalNAV" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .genericBtn {
            background: #71a7b9;
            border: 0;
            color: white;
            height: 34px;
            width: fit-content;
            vertical-align: top;
            margin-left: 5px;
            padding: 0 10px;
            cursor: pointer;
        }

        .label {
            color: #71a7b9;
            font-size: 30px;
            font-weight: bold;
    
        }

        .ddlFundType {
            width: 75%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
        
        .anchorNoLink , .anchorNoLink:hover{
            text-decoration:none;
            color:#3c96b7;
        }

        .datebox {
            width: 75%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }

        .editButton {
              display: inline-block;
              padding: 6px 12px;
              margin-bottom: 0;
              font-size: 14px;
              font-weight: normal;
              line-height: 1.42857143;
              text-align: center;
              white-space: nowrap;
              vertical-align: middle;
              -ms-touch-action: manipulation;
                  touch-action: manipulation;
              cursor: pointer;
              -webkit-user-select: none;
                 -moz-user-select: none;
                  -ms-user-select: none;
                      user-select: none;
              background-image: none;
              border: 1px solid transparent;
              border-radius: 4px;
        }
        .txtbox {
            padding: 14px 15px;
            width: 100%;
            background: #f8f9f9;
            border: solid 1px #8c9098;
            box-sizing: border-box;
        }

        .thColored{
            background:#71a7b9;
            color:white;
        }



    </style>
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h3 style="padding-left:10px;"><u>Edit Historical NAV</u></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <span class="anchorNoLink"style="padding-left:8px;">Fund :</span>
                <asp:DropDownList ID="ddlFundType" runat="server" CssClass="ddlFundType">
                </asp:DropDownList>
            </div>
            <div class="col-md-3">
                <span class="anchorNoLink"style="padding-left:8px;">From :</span>
                <asp:TextBox ID="fromDate" TextMode="Date" runat="server" ClientIDMode="Static" CssClass="datebox"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <span class="anchorNoLink"style="padding-left:8px;">To :</span>
                <asp:TextBox ID="toDate" TextMode="Date" runat="server" ClientIDMode="Static" CssClass="datebox"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <button id="searchBtn" runat="server" clientidmode="static" class="genericBtn" data-toggle="tooltip" data-placement="bottom" title="Search"><i class="fa fa-search"></i></button>
<%--                <button data-original-title="Edit" data-trigger="hover" data-placement="bottom" data-content="Select one record" type="button" class="popovers btn action-button pull-right" data-action="Edit" data-url="EditHistoricalNavFn.aspx" data-title="Edit Material"><i class="fa fa-edit"></i></button>--%>
                <a id="editBtn" class="btn btn-primary genericBtn" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                <%--<a href="checkedCheckBox()"data-toggle="modal" data-target="#commonEditModal"><button id="editBtn" class="genericBtn" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></button></a>--%>
                  <div class="modal fade bs-example-modal-lg" id="commonEditModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                      
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="sf1" runat="server" visible="true" style="width: 100%; margin: 0 auto;padding-top:20px;padding-bottom:20px;">
                        <table id="show_funds_1" class="table table-bordered table-striped">
                            <thead class="thColored">
                                <tr>
                                    <th>Index</th>
                                    <th>Fund Name</th>
                                    <th>NAV Date</th>
                                    <th>NAV Price</th>
                                </tr>
                            </thead>
                            <tbody id="tablePrice1" runat="server" clientidmode="Static">
                                <%--<tr>
                                    <th><input type="checkbox" runat="server" value="0"/>1</th>
                                    <th>Apex Dana Aslah</th>
                                    <th>22/01/2022</th>
                                    <th>1.5677</th>
                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script>


    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#show_funds_1').DataTable({
                searching: false

            });


            $('#editBtn').click(function () {
                if ($('input[name="check[]"]:checked').length <= 0)
                {
                    showSnackBar("Please select a fund to edit!");
                    //alert("Please select a fund to edit!");
                    $('#commonEditModal').modal('hide');
                }
                else if ($('input[name="check[]"]:checked').length > 1)
                {
                    showSnackBar("Please select only a single fund to edit!");
                    //alert("Please select only a single fund to edit!");
                    $('#commonEditModal').modal('hide');
                }
                else
                {
                    //ajax call here

                    var path = loadModalContent();
                    //document.getElementById("editBtn").innerHTML += "href='" + path + "'";
                    //document.getElementById("editBtn").getAttribute("href").link(path);
                    //$('#editBtn').attr("href", path);
                    $('.modal-content').load(path, function ()
                    {
                        $('#commonEditModal').modal('show');
                    });
                    console.log(loadModalContent());
                }
            });

          
            function loadModalContent() {
                var id = $('input[name="check[]"]:checked').val();
                var modalUrlPath = "EditHistoricalNavFn.aspx?id=" + id ;
                return modalUrlPath;
            }

            function showSnackBar(message) {
                var x = $("#snackbar");
                x.html(message);
                x.addClass('show');
                setTimeout(function () {
                    x.className = x.removeClass('show');
                }, 3000); // 3000 = 3 seconds
            }
                 
        });

    </script>
    
</asp:Content>
