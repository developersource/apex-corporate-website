﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditHistoricalNavFn.aspx.cs" Inherits="AdminApex.ApexWeb.EditHistoricalNavFn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <div class="modal-header">
        <h5 class="modal-title" id="editModalTitle">Edit Historical NAV</h5>
        </div>
        <div id="editModalBody" class="modal-body">
            <form id="editHistoricalNAVForm" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="fs-14 mt-2">Fund Name:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                                <input type="hidden" name="newPrice" value="0" id="newPrice" runat="server" clientidmode="static" />
                                <div>
                                    <input type="text" name="FundName" id="FundNameTxt" runat="server" clientidmode="static" class="form-control" placeholder="Fund Name" readonly="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="fs-14 mt-2">NAV Date:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="NAVDate" id="NAVDateTxt" runat="server" clientidmode="static" class="form-control" placeholder="NAV Date" readonly="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="fs-14 mt-2">NAV Price:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="NAVPrice" id="NAVPriceTxt" runat="server" clientidmode="static" class="form-control" placeholder="NAV Price"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <div class="modal-footer">
        <button type="button" class="editButton btn-warning" data-dismiss="modal">Close</button>
        <button type="button" class="editButton btn-success" id="btnModalSubmit">Save changes</button>
    </div>

    <script>
        $(document).ready(function () {
            
                $("#NAVPriceTxt").on("input", function(evt) {
                   var self = $(this);
                   self.val(self.val().replace(/[^0-9\.]/g, ''));
                   if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
                   {
                     evt.preventDefault();
                   }
                 });

                $('#btnModalSubmit').unbind('click');
                $('#btnModalSubmit').click(function () {
                    var currFundId = $('#Id').val();
                    var newPriceVal = $('#NAVPriceTxt').val();
                    var postData = { fundId: JSON.stringify(currFundId), newNavPrice: JSON.stringify(newPriceVal)}
                    $.ajax({
                        url: "EditHistoricalNav.aspx/Add",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: postData,
                        success: function (data) {
                            var response = data.d;
                            console.log(response);
                            if (response.IsSuccess != undefined) {
                                if (response.IsSuccess) {
                                    $('#commonEditModal').modal('hide');
                                    //setTimeout($('#tablePrice1').load('EditHistoricalNav.aspx'), 100);

                                    //async function reload(){
                                    //    await showSnackBar(response.Message);
                                    //    window.location.reload();
                                    //};
                                    showSnackBar(response.Message);
                                    checkAnimationTimer();
                                    //$.when(showSnackBar(response.Message)).done(function () {
                                    //    window.location.reload();
                                    //    console.log("triggered");
                                    //});
                                    //window.location.reload();
                                    
                                }
                            }
                            else {
                                setTimeout($('#tablePrice1').load('EditHistoricalNav.aspx'),100);
                                if (response) {

                                    
                                }
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            //showSnackBar(xhr.Message);
                            showSnackBar('Please check that you have entered valid information!');
                            //$('.loadingDivAdmin').hide();
                        }
                    });
                });

            function checkAnimationTimer () {
                // Test if ANY/ALL page animations are currently active

                var testAnimationInterval = setInterval(function () {
                    if (! $.timers.length) { // any page animations finished
                        clearInterval(testAnimationInterval);
                        window.location.reload();
                    }
                }, 3000);
            };

            function showSnackBar(message) {
                var x = $("#snackbar");
                x.html(message);
                x.addClass('show');
                setTimeout(function () {
                    x.className = x.removeClass('show');
                }, 3000); // 3000 = 3 seconds
            }
        });       
    </script>
</body>
</html>
