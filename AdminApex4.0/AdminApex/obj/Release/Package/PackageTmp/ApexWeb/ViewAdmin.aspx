﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewAdmin.aspx.cs" Inherits="AdminApex.ApexWeb.ViewAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30 table-responsive ">
        <h2>Admins</h2>

        <table class="table table-hover table-striped table-bordered nowrap display pb-30">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Email</th>
                    <th style="text-align: center">Status</th>
                    <th style="text-align: center">Action</th>
                </tr>
            </thead>
            <tbody id="tableManageAdmin" runat="server" clientidmode="Static">
            </tbody>
        </table>
        <asp:HiddenField ID="hdnUserID" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnAction" runat="server" ClientIDMode="Static" />
        <asp:Button ID="btnAction" runat="server" OnClick="btnAction_Click" ClientIDMode="Static" CssClass="hide" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="../Content/js/dataTables-data.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableManageAdmin a').click(function () {
                var id = $(this).parents('tr').attr('data-id');
                $('#hdnUserID').val(id);
                var title = $(this).attr('title');
                if (title == "")
                    title = $(this).attr('data-original-title');
                $('#hdnAction').val(title);
                $('#btnAction').click();
            });
        });
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
    </script>
</asp:Content>
