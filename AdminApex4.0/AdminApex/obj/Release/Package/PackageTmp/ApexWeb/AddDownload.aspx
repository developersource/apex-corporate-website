﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddDownload.aspx.cs" Inherits="AdminApex.ApexWeb.AddDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdntype" runat="server" />
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Add File</h2>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>File Type</label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlFileType" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged"></asp:DropDownList>
                        <br />
                        <asp:RadioButtonList ID="rblFund" runat="server"></asp:RadioButtonList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>File Name</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtFileName" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:DropDownList ID="ddlFileName" runat="server" CssClass="form-control" Visible="False" AutoPostBack="True" ></asp:DropDownList>
                    </div>
                    
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <asp:Label ID="lblFileAuthor" runat="server" Text="File Author" Visible="False"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtFileAuthor" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>
                    </div>
                </div>
                   <div class="row mt-10">
                    <div class="col-md-3">
                        <asp:Label ID="lblDate" runat="server" Text="Date" Visible="False"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False" TextMode="Date" AutoPostBack="True"></asp:TextBox>
                          <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDate"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:FileUpload ID="fuFileType" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="lblSubmit" runat="server" Text="Save" CssClass="btn" OnClick="lblSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
