﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUnitSplit.aspx.cs" Inherits="AdminApex.ApexWeb.AddUnitSplit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Add Unit Split</h2>
                 <div class="row mt-20">
                    <div class="col-md-3">
                        <label>Fund</label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlFund" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-md-3">
                        <label>Ex Date</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtDate" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                           <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDate"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Split Ratio</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtSplitRatio" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>               
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-9 col-md-offset-3">
                        <asp:Button ID="lblSubmit" runat="server" Text="Save" CssClass="btn" OnClick="lblSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
