﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditFund.aspx.cs" Inherits="AdminApex.ApexWeb.EditFund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Edit Fund</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Fund Logo</label>
                    </div>
                    <div class="col-md-6">
                        <asp:Image ID="ImgPhoto" runat="server" Width="200px" Height="200px" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label></label>
                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="fundlogo" runat="server" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Fund Code</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_fund_code" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Fund Name</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_fundname" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Fund Type</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlFileType" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>EPF Approved Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_epf_approved" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Potential Investors</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="txt_pot_inves" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Investment strategy</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="txt_inves_stra" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Lanch date</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_launch_date" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                           <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txt_launch_date"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Trustee</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_trustee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Fund category/type</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_fund_category" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Sales charge</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_sales_charge" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Management fee</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_manage_fee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Trustee fee</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_trustee_fee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Minimum initial investment</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_min_ini_inves" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Minimum Subsquent investment</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_min_sub_inves" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Redemption fee</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_redemp_fee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Status</label>
                    </div>
                    <div class="col-md-6">
                        <asp:CheckBox ID="chkact" runat="server" Text="Activate" AutoPostBack="True" OnCheckedChanged="chkact_CheckedChanged" />
                        <asp:CheckBox ID="chksus" runat="server" Text="Suspended" AutoPostBack="True" OnCheckedChanged="chksus_CheckedChanged" />
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnsubmit1" runat="server" Text="Save" CssClass="btn" OnClick="btnsubmit1_Click" />
                        &nbsp
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
