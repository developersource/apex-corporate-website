﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditDownload.aspx.cs" Inherits="AdminApex.ApexWeb.EditDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdntype" runat="server" />
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Edit Download</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>File Type</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlFileType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                        <br />
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>File Name</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtFileName" runat="server" CssClass="form-control"></asp:TextBox><br />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <asp:Label ID="lblFileAuthor" runat="server" Text="File Author"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtFileAuthor" runat="server" CssClass="form-control"></asp:TextBox><br />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDate"     
                                ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" />
                       <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                            ControlToValidate="txtDate" ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.](19|20)\d\d$" ForeColor="black" ErrorMessage="invalid date"></asp:RegularExpressionValidator>--%>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:HyperLink ID="hlFile" runat="server" Target="_blank">Click here to view</asp:HyperLink>
                        <asp:FileUpload ID="fuFile" runat="server" />
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                        &nbsp
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
