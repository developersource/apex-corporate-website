﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditNews.aspx.cs" Inherits="AdminApex.ApexWeb.EditNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Edit News</h2>
                <div class="row mt-20">
                    <div class="col-md-5">
                        <label>Title</label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txt_title" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Fund Logo</label>
                    </div>
                    <div class="col-md-6">
                        <asp:Image ID="ImgPhoto" runat="server" Width="200px" Height="200px" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label></label>
                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="newslogo" runat="server" />
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-5">
                        <label>Content</label>
                    </div>
                    <div class="col-md-7">
                        <textarea id="txt_Content" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-7 col-md-offset-5">
                        <asp:CheckBoxList ID="chkboxfundname" runat="server"></asp:CheckBoxList>
                    </div>
                </div>
                <%--<div class="row mt-10">
                          <div class="col-md-5">
                        <label>Annoucement Status</label>
                        </div>
                    <div class="col-md-7">
                        <asp:CheckBox id="chkboxAnnoucement" runat="server" text="Activate"/>
                    </div>
                </div>--%>
                <div class="row mt-10" id="annoucement_txt"  style="display:none;">
                    <div class="col-md-5">
                        <label>Description</label>
                    </div>
                    <div class="col-md-7">
                        <textarea id="txt_Desc" cols="30" rows="10" runat="server" class="border" cssclass="form-control"></textarea>
                    </div>
                </div>
                <br />
                <div class="row mt-10 mb-30">
                    <div class="col-md-7 col-md-offset-5">
                        <asp:Button ID="btnsubmit1" runat="server" Text="Save" OnClick="btnsubmit1_Click" CssClass="btn"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
        <script type="text/javascript">
            <%--$(document).ready(function () {
                if ($('#<%=chkboxAnnoucement.ClientID %>').is(':checked')){
                    $('#annoucement_txt').show();
                }

            $('#<%=chkboxAnnoucement.ClientID %>').change(function () {
                if (this.checked) {
                    $('#annoucement_txt').show();
                }
                else {
                    $('#annoucement_txt').hide();
                }
            });--%>
        });


        </script>
</asp:Content>
