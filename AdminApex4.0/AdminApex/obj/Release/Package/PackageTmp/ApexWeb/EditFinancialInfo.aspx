﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditFinancialInfo.aspx.cs" Inherits="AdminApex.ApexWeb.EditFinancialInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="kfi container-fluid mt-30 mb-30">
        <div class="row">
            <div class="col-md-6">
                <h2>Key Financial Information</h2>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <label>Year</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlyear" runat="server" class="border" CssClass="ddlbox1">
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem>2016</asp:ListItem>
                            <asp:ListItem>2017</asp:ListItem>
                            <asp:ListItem>2018</asp:ListItem>
                            <asp:ListItem>2019</asp:ListItem>
                            <asp:ListItem>2020</asp:ListItem>
                            <asp:ListItem>2021</asp:ListItem>
                            <asp:ListItem>2022</asp:ListItem>
                            <asp:ListItem>2023</asp:ListItem>
                            <asp:ListItem>2024</asp:ListItem>
                            <asp:ListItem>2025</asp:ListItem>
                            <asp:ListItem>2026</asp:ListItem>
                            <asp:ListItem>2027</asp:ListItem>
                            <asp:ListItem>2028</asp:ListItem>
                            <asp:ListItem>2029</asp:ListItem>
                            <asp:ListItem>2030</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Revenue</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_revenue" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Profit or Loss Before Tax</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_plbeforetax" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Profit or Loss After Tax</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_plaftertax" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Paid-up Capital</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_ipcap" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Shareholders’ Fund</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_shareholder" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Cash & Bank Deposits</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_cbdeposits" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-6">
                        <label>Bank Borrowings</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txt_bankborrow" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row mt-10 mb-30">
                    <div class="col-md-6 col-md-offset-6">
                        <asp:Button ID="btnsubmit1" runat="server" Text="Save" CssClass="btn" OnClick="btnsubmit1_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
