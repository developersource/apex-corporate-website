﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="people.aspx.cs" Inherits="AdminApex.people" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="people">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image">
                    <img src="/ICONPIC/APEX_Header_aboutus.jpg" />
                    <div class="header-copy">
                        <h2>20 years strong.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content narrow">
            <h3 class="inner-content">Our People</h3>
        </div>
        <div class="subnav bg-gray">
            <div class="bg-gray-container subnav-wrapper">
                <div class="special_select desktop-hidden">
                    <ul>
                        <li>
                            <a href="javascript:;" id="mobile_page"></a>
                            <img class="" src="http://staging.apexis.com.my/img/down-arrow.png" /><img src="../Picture/arrow-down.png.png" />
                        </li>
                    </ul>
                </div>
                <div class="inner-content">
                    <ul>
                        <li><a class="active" page="directors" href="javascript:;">Board of Directors & Investment Committee</a></li>
                        <li><a class="" page="management" href="javascript:;">Management Team</a></li>
                        <li><a class="" page="shariah" href="javascript:;">Shariah Advisers</a></li>
                        <li><a class="" page="trustee" href="javascript:;">Trustee</a></li>
                        <li><a class="" page="compliance" href="javascript:;">Compliance</a></li>
                        <li><a class="" page="chart" href="javascript:;">Organisation Chart</a></li>
                        <li><a class="" page="internal_audit" href="javascript:;">Internal Audit</a></li>
                    </ul>
                    <div class="subnav active-bar"></div>
                </div>
            </div>
        </div>
        <div class="content narrow subpages">
            <div class="about inner-content subnav-pages" id="directors">
                <div class="description">
                    <!-- <h4>Board of Directors</h4> -->

                    <p><strong>Roles and Functions of the Board of Directors and Investment Committee:</strong></p>
                    <p>The Board of Directors of the Manager is responsible for overseeing the activities of the Manager. The Investment Committee is responsible for formulating, implementing and monitoring the investment management policies of the Funds in accordance with its objectives and the provisions of the respective Deed. Board and Investment Committee meetings are held every three months.</p>

                    <p><strong>Remuneration Policy For Directors And Senior Management</strong></p>
                    <p>The Company has remuneration policies in place for its directors and senior management that take into account the demands, complexities and performance of the company, as well as skills and experience required.</p>

                </div>
                <ul class="tiles">
                    <li data-pname=" Y.M. Dato’ Tunku Ahmad Zahir bin Tunku Ibrahim">
                        <div class="info">
                            <p>Y.M. Dato’ Tunku Ahmad Zahir bin Tunku Ibrahim</p>
                            <p>Non-Executive and Independent Director/Chairman</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0515.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Y.M. Dato' Tunku Ahmad Zahir bin Tunku Ibrahim</h4>
                                            <p class="font-black">
                                                <p>Y.M. Dato' Tunku has been a Director of the Company since 21 February 1997. After graduation in 1987, he joined KAF Astley &amp; Pearce Sdn. Bhd. as a broker. In 1994, he joined PYEMAS Sdn. Bhd. as a Senior Manager until 1995 when he led a management buyout exercise of the company. He has significant experience in the trading of money market instruments such as MGS, Treasury bills, SWAP, IRS, Private Debts Securities (PDS), Cagamas Notes &amp; Bonds and interbank money market. Y.M. Dato' Tunku is the Executive Chairman of First TAZ Capital Sdn. Bhd., Chairman of First TAZ Tradition Sdn. Bhd. and sits on the board of several private companies. He is active in the development of several property projects. Dato' Tunku holds a Bachelor of Arts degree in Economics from Carlton University in Ottawa, Canada.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0515.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li data-pname="Clement Chew Kuan Hock">
                        <div class="info">
                            <p>Clement Chew Kuan Hock</p>
                            <p>Chief Executive Officer</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0548.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Clement Chew Kuan Hock</h4>
                                            <p class="font-black">
                                                <p>Clement has been the CEO of Astute Fund Management Berhad since 2015. Clement worked for J.P. Morgan for 19+ years and Merrill Lynch for 4+ years. He was Senior Country Officer of J.P. Morgan in Malaysia between 2007 and 2010. Clement also held the positions of Chairman/Executive Director of J.P. Morgan Securities Malaysia Sdn Bhd (2005 to 2014), Director of J.P. Morgan Chase Bank Bhd (2008 to 2010) and head of Malaysia equity product (1998 to 2005). In 2005, he was voted No. 1 Malaysia equity sales person by Asiamoney. Clement started his investment career as an equity analyst for Merrill Lynch in 1991 before moving to New York to cover Asia ex-Japan sales for Merrill Lynch and J.P. Morgan (1993 to 1998). Prior to joining the securities industry, he worked for property developer IGB Corporation where his last position was corporate affairs manager. Clement holds a MSc in Management Science from Imperial College London and a bachelor of economics (upper second honours) from La Trobe University in Melbourne. He is a CMSRL holder for Fund Management (eCMSRL/B6806/2016) and a member of Federation of Investment Managers Malaysia (FIMM).  In 2018, Clement was appointed to the Board of Lembaga Totalisator Malaysia, a statutory entity under the Ministry of Finance (MoF). He was awarded the Pingat Mahkota Perlis (PMP) by DYTM Raja of Perlis, Tuanku Syed Sirajuddin in 2009.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0548.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li data-pname="Wong Fay Lee">
                        <div class="info">
                            <p>Wong Fay Lee</p>
                            <p>Non-Independent and Non-Executive Director</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0751.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Wong Fay Lee</h4>
                                            <p class="font-black">
                                                <p>Wong Fay Lee is an Executive Director of MBMR and has been the Group General Counsel to MBM Resources Bhd. (MBMR) since 2011. She started her career in the corporate commercial and corporate finance practice of Mallesons Stephens Jaques in Sydney and later joined Mallesons practice in Singapore. Her former positions include Manager in the Research &amp; Development Division (responsible for bond and derivatives markets) with the Malaysian Securities Commission, Chief Executive/Managing Director of Malaysia Derivatives Clearing House (now Bursa Malaysia Derivatives Berhad) and Adviser to the Clearing Division of the Hong Kong Exchanges and Clearing Limited. She was an Independent Director of KFH Asset Management Sdn. Bhd. from 2008 to 2014. She has a Bachelor's degree in Law from the University of Sydney and a Graduate Diploma in Applied Finance and Investments from the Securities Institute of Australia. She was admitted as a Solicitor to the NSW Supreme Court and the High Court of Malaya.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0751.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li data-pname="Azran bin Osman Rani">
                        <div class="info">
                            <p>Azran bin Osman Rani</p>
                            <p>Non-Executive and Independent Director</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0528.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Azran bin Osman Rani</h4>
                                            <p class="font-black">
                                                <p>Azran is currently the CEO of iflix Malaysia - an internet technology company delivering video-on-demand services across Southeast Asia and emerging markets. He was the former CEO of AirAsia X. He led the start-up team that developed the business plan, raised capital, secured relevant licenses and approvals, acquired aircraft and launched the airline's inaugural flight to the Gold Coast in 2007. Before joining Air Asia X, Azran was the Senior Director of Business Development of Astro and Senior VP, Special Projects of Bursa Malaysia Berhad. He was also an Associate Partner of McKinsey &amp; Company. Azran started his management consultancy career with Booz Allen &amp; Hamilton where his last position was a Lead Consultant in 2000. He graduated from Stanford University with a Master degree in Management Science &amp; Engineering and a Bachelor of Science degree in Electrical Engineering. Azran is a Fellow of CPA Australia. He is a Non-Executive Commissioner of PT XL Axiata Tbk.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0528.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li data-pname="Azran bin Osman Rani">
                        <div class="info">
                            <p>Asgari bin Mohd Fuad Stephens</p>
                            <p>Non-Independent and Non-Executive Director</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0620.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Asgari bin Mohd Fuad Stephens</h4>
                                            <p class="font-black">
                                                <p>Asgari is a Malaysian originally from Sabah now residing in Kuala Lumpur. Asgari holds a Bachelor of Commerce (Honours) from University of Melbourne, Australia and a Master of Business Administration degree from Cranfield University, UK. He has extensive experience in both public and private equity investing in Malaysia. He is the co-founder of Kumpulan Sentiasa Cemerlang Sdn Bhd (“KSC”), an investment advisory and fund management group. He started two venture capital firms, iSpring Venture Management Sdn Bhd and Intelligent Capital Sdn Bhd while continuing to work with KSC. He was previously the Chairman of the Malaysian Venture Capital Association. Prior to starting his own company in 1996 he worked at Usaha Tegas Sdn Berhad establishing their fund investment division between 1988-1990 and 1992-1995. In 1990-1992 he worked as an associate director at a stock broking firm.In the years 1982-1988 he worked for a small family company in Sabah which was involved with Oil Palm plantations and property investments.Asgari is a director of Innoprise Plantations Berhad. He has never been convicted of any offences (other than traffic offences, if any) within the past 5 years and has not been imposed of any public sanction or penalty by the relevant regulatory bodies during the financial years.
                                                </p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0620.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <%--<li data-pname="Tan Keah Huat">
                        <div class="info">
                            <p>Tan Keah Huat</p>
                            <p>Executive and Non-Independent Director</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0538.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Tan Keah Huat</h4>
                                            <p class="font-black">
                                                <p>Tan Keah Huat holds a Bachelor of Science (Honours) degree in Actuarial Science from the University of Western Ontario, Canada. He is also a Certified Financial Planner (CFP). Prior to this, he was the Chief Executive Officer of AMMB Investment Services Berhad. He has been involved in the fund management industry since 1995 and was a council member of the Federation of Investment Managers Malaysia. He has been a director of the Company since 3 May 2001.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0538.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>--%>
                </ul>
            </div>
            <div class="about inner-content subnav-pages" id="management">
                <div class="description">
                    <p>The Manager has a staff force of 27 employees, comprising 24 executives and 3 non-executives as at 30 April 2018.</p>
                    <p>The qualifications and experience of the key management staff are as follows:</p>
                </div>
                <ul class="tiles">
                    <li>
                        <div class="info">
                            <p>Clement Chew Kuan Hock</p>
                            <p>Chief Executive Officer</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0548.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Clement Chew Kuan Hock</h4>
                                            <p class="font-black">
                                                <p>Clement Chew joined the Company in December 2014. He worked for J.P. Morgan for 19 years and Merrill Lynch for 4 years. At J.P. Morgan, he was Chairman/Executive Director of J.P. Morgan Securities Malaysia Sdn. Bhd. (2005 to 2014), Senior Country Officer for Malaysia (2007 to 2010), Non-independent director of J.P. Morgan Chase Bank Bhd. (2008 to 2010) and Head of Malaysia Equity Product (1998 to 2005). He started his career as an equities analyst at Merrill Lynch in 1991 before being transferred to New York in 1993 to cover Asia ex-Japan sales. Prior to joining the finance industry, Clement worked for IGB Corporation where his last position was Corporate Affairs Manager. He holds a MSc in Management Science from Imperial College in London and a Bachelor of Economics (Upper Second Honours) from La Trobe University in Melbourne.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0548.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <%--<li>
                        <div class="info">
                            <p>Tan Keah Huat</p>
                            <p>Executive Director</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0538.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Tan Keah Huat</h4>
                                            <p class="font-black">
                                                <p>Tan Keah Huat holds a Bachelor of Science (Honours) degree in Actuarial Science from the University of Western Ontario, Canada. He is also a Certified Financial Planner (CFP). Prior to this, he was the Chief Executive Officer of AMMB Investment Services Berhad. He has been involved in the fund management industry since 1995 and was a council member of the Federation of Investment Managers Malaysia. He has been a director of the Company since 3 May 2001.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0538.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>--%>
                    <%--<li>
                        <div class="info noPic">
                            <p>Stephanie Claire Rodrigues</p>
                            <p>Compliance Officer</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Stephanie Claire Rodrigues</h4>
                                            <p class="font-black">Stephanie Claire Rodrigues joined the company in May 2018, having served 8 years within the financial industry and capital market intermediaries. She is a graduate in law from the University of London (LLB) as well as a certified Compliance Officer having passed Module 9, 10 and 11 respectively with the Securities Commission. Her key functions include overseeing corporate compliance programs and function as a neutral body to review and evaluate compliance issues. Her function also includes acting as liaison between company and all Financial Regulators.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>--%>
                    <%--<li>
                        <div class="info noPic">
                            <p>Steven Matthew B. Nambirajan</p>
                            <p>Risk Officer</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Steven Matthew B. Nambirajan</h4>
                                            <p class="font-black">Steven joined the company in December 2015. Prior to this, he spent more than 8 years in banking and finance covering Back Office functions and Client Risk Management. He holds a Diploma In Business Studies (Accounting) from Tunku Abdul Rahman College (KTAR) and an International Executive Master In Business Administration (Strategic Management) from Paris Graduate School of Management (PGSM).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>--%>
                    <li>
                        <div class="info">
                            <p>Nancy Rioh</p>
                            <p>Head of Compliance and Risk Management</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0610.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Nancy Rioh - Head of Compliance and Risk Management</h4>
                                            <p class="font-black">
                                                Nancy Rioh is the designated person responsible for Compliance and Risk Management matters for Astute Fund Management Berhad (“AFMB”). </br></br> 
Nancy Rioh has a Master`s in Business Administration major in International Business from University of Greenwich, UK, and a Bachelor Degree in Business Administration from the University of Abertay Dundee, UK.  She also an Advanced Diploma holder from Association of Business Executive (ABE,UK), as well as  a Diploma holder in Information Technology from SAL Group of Colleges, Malaysia. </br></br> 
Prior to join AISB, she was a registered Compliance and Risk Management Manager at Pheim Asset Management SdnBhd and Pheim Unit Trusts Bhd for five years.  She also has nearly seven years` experience in the stock broking industry, having worked in both the Settlements and the Credit Control Departments at Hong Leong Investment Bank where she attained the position of Manager.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0610.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                </ul>
                <div class="description">
                    <h4>Investment Team</h4>
                    <p><b>Roles and Responsibilities:</b></p>
                    <!--
        <p>The investment committee is responsible for formulating, implementing and monitoring the investment management policies of the Funds in accordance with its objectives and the provisions of the respective Deed. Investment committee meetings are held formally every month, or more frequently if required.</p>
        <p>The main functions of the investment committee are to: </p>
        <ul>
            <li>Select appropriate strategies to achieve the proper performance of the Funds in line with its investment objectives; </li>
            <li>Ensure that strategies are implemented properly and efficiently by the Manager and any delegate thereof; </li>
            <li>Actively monitor, measure and evaluate the investment management performance of the Manager and any delegate; and </li>
            <li>Ensure that the investment management of the Funds complies with the provisions of its respective Deed, the Act, the Guidelines, securities law and internal investment restrictions and policies. </li>
        </ul>
        -->
                    <p>
                        Astute Fund Management Berhad’s fund managers are authorised to manage the Funds in accordance with the Funds’ stated investment objective. This authority is subject to the requirements of this Master Prospectus, the respective Deeds, Guidelines and relevant laws, acceptable and efficacious business practice within the unit trust industry, the policies and internal controls in place of the Management Company. The fund managers will report to the investment committee of the Fund and will implement the investment strategies selected by this committee. 
                    </p>
                </div>
                <ul class="tiles">
                    <li>
                        <div class="info">
                            <p>Christopher Chan</p>
                            <p>Head of Fund Management</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" src="../ICONPIC/EDM_0600.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" />
                                                <h4 class="font-black">Christopher Chan</h4>
                                            <p class="font-black">
                                                    <%--Gary Lim has 20 years of experience in investments covering the Asia region. Prior to joining Apex Investment Services, he spent almost nine years at Absolute Asia Asset Management and four and a half years at Nomura Asset Management where he managed largely institutional money, both from American and European pensions. At Absolute Asia and Nomura, Gary has served in various research functions but largely with responsibilities for the North Asian Markets. At Absolute Asia, Gary was also involved in asset allocation while he was the Deputy Chairman of the Stock Selection Committee at Nomura. Prior to Nomura Asset Management, Gary was with OCBC Asset Management for five and a half years where he started off as an analyst covering the Asean markets before he progressed to become a fund manager with research responsibilities for North Asia. His previous experience includes working a year at Sun Hung Kai Securities as an investment analyst covering Singapore and Malaysia listed companies and two years with Neptune Orient Lines as a Corporate Planning Executive. He is also the designated fund manager for AAEJF, ADF and AMGT.--%>
                                                    Christopher joined Astute Fund Management in January 2022 as Head of Fund Management. Over the last 30 years, he has worked primarily in the asset management, insurance and investment banking industries. He has held various roles in fund management, product development, branding, marketing and communications, and sales. In fund management, he has managed domestic and regional assets for institutional, private wealth and retail portfolios. In investment banking, he has covered domestic and regional equities, local and global fixed income, currencies and alternative investments. He holds a Master's degree in Banking and Finance from the University of Hull, UK.                                                    
                                                    <br />
                                                    <br />
                                                    <%--Gary graduated from The University of Michigan, Ann Arbor with a Bachelor in Business Administration degree majoring in Accounting &amp; Finance. Gary is also a CFA' charterholder.--%>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM_0600.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li>
                        <div class="info">
                            <p>Kelvin Chan</p>
                            <p>Head Of Fixed Income Investments</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-pic-holder">
                                            <img class="face" style="left: 0px" src="../ICONPIC/EDM-5556.png" />
                                            <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                                        </div>
                                        <div class="profile-bio-holder">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" />
                                            </a>
                                            <h4 class="font-black">Kelvin Chan</h4>
                                            <p class="font-black">
                                                <p>
                                                    <%--Gary Lim has 20 years of experience in investments covering the Asia region. Prior to joining Apex Investment Services, he spent almost nine years at Absolute Asia Asset Management and four and a half years at Nomura Asset Management where he managed largely institutional money, both from American and European pensions. At Absolute Asia and Nomura, Gary has served in various research functions but largely with responsibilities for the North Asian Markets. At Absolute Asia, Gary was also involved in asset allocation while he was the Deputy Chairman of the Stock Selection Committee at Nomura. Prior to Nomura Asset Management, Gary was with OCBC Asset Management for five and a half years where he started off as an analyst covering the Asean markets before he progressed to become a fund manager with research responsibilities for North Asia. His previous experience includes working a year at Sun Hung Kai Securities as an investment analyst covering Singapore and Malaysia listed companies and two years with Neptune Orient Lines as a Corporate Planning Executive. He is also the designated fund manager for AAEJF, ADF and AMGT.--%>
                                                    Kevin has over 17 years of experience in fixed income investments specializing in the domestic fixed income market. He started his career with KAF Investment Bank in 1997 as an analyst and later as a fixed income dealer where he focused on trading propriety fixed-income securities with emphasis on corporate bonds as well as assisting the sales and distribution team with pricing and distribution of primary issuances originated by the bank. In 1999, he joined Intrinsic Capital Management as a fixed income fund manager and was appointed as Executive Director of the firm from 2005  to 2009. He was responsible for the investment strategy and performance of the entire fixed income portfolio across both institutional and private client accounts covering a broad spectrum  of the fixed-income market.
                                                    <br />
                                                    <br />
                                                    <%--Gary graduated from The University of Michigan, Ann Arbor with a Bachelor in Business Administration degree majoring in Accounting &amp; Finance. Gary is also a CFA' charterholder.--%>
                                                   He is a licensed fund manager and holds a Masters in Business Administration degree from SCU, Australia. 
                                                </p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="face" src="../ICONPIC/EDM-5556.png" />
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                </ul>
            </div>
            <div class="about inner-content subnav-pages" id="shariah">
                <div class="description">
                    <p><strong>The role of a Shariah committee is to:</strong></p>
                    <ul>
                        <li>Ensure that the Fund is managed and administered in accordance with the Shariah principles;</li>
                        <li>Provide expertise and guidance for the Fund in all matters relating to the Shariah principles, including on the Fund&rsquo;s deed and prospectus, its structure and investment, and other operational and administrative matters;</li>
                        <li>Consult the Securities Commission who may consult the Shariah Advisory Council where there is any ambiguity or uncertainty as to an investment, instrument, system, procedure and/or process;</li>
                        <li>Ensure that the Fund complies with any guideline, ruling or decision issued by the Securities Commission;</li>
                        <li>Be responsible for scrutinising the Fund&rsquo;s compliance report as provided by the compliance officer and transaction report provided by or duly approved by the trustee to ensure that the Fund&rsquo;s investments are in line with the Shariah principles; and</li>
                        <li>Prepare a report to be included in the Fund&rsquo;s interim and annual report certifying whether the Fund has been managed and administered in accordance with Shariah principles for the period concerned.</li>
                    </ul>
                    <p>The Shariah committee meetings are held formally twice in a year, or more frequently if required to review on the Fund&rsquo;s asset.</p>
                    <p>The number of funds in which it acts as adviser: 5 (as at 30 April 2018)</p>
                </div>
                <ul class="tiles">
                    <li>
                        <div class="info noPic">
                            <p>Prof. Madya Dr. Mohamad Sabri bin Haron</p>
                            <p>Chairman and Independent Member</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Prof. Madya Dr. Mohamad Sabri bin Haron</h4>
                                            <p class="font-black">Prof. Madya Dr. Mohamad Sabri bin Haron is a lecturer at the Pusat Citra Universiti, Universiti Kebangsaan Malaysia. He is also an Associate Senior Fellow at Institute of Malaysian and International Studies (IKMAS). He obtained a Diploma in Islamic Studies from Kolej Sultan Zainal Abidin in 1985 and Bachelor of Islamic Studies (al-Quran and al-Sunnah) from National University of Malaysia in 1988. He completed his Master of Comparative Law at International Islamic University of Malaysia in 1993. He succeeded in obtaining his Ph.D. in Islamic Law (Fiqh and Usul Fiqh) in 1998 from University of Jordan. His specialisation areas are in Islamic Economics and Islamic Civilisation. He also was seconded to the Securities Commission as the Senior Manager in Islamic Capital market starting from 1 June 2009 until 31 May 2010.    </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li>
                        <div class="info noPic">
                            <p>Dr. Ab. Halim bin Muhammad</p>
                            <p>Independent Member</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Dr. Ab. Halim bin Muhammad</h4>
                                            <p class="font-black">Dr. Ab Halim bin Muhammad graduated in 1972 with a Bachelor's Degree of Shari'ah from Al-Azhar University, Cairo, Egypt and completed his studies in Ph. D. of Shari'ah at St. Andrew, Scotland University in 1977. He served as a lecturer and became the Head of Department of Quran & Sunnah, Faculty of Islamic Studies Universiti Kebangsaan Malaysia. Some of the subjects that he taught were Islamic Jurisprudence (Muamalat, Islamic Banking & Islamic Finance and Takaful), Principles of Islamic Jurisprudence and Islamic Criminal Laws. However, he has retired as a lecturer now. He used to be the first Chairman of Shariah Committee of BMMB prior to joining National Shariah Advisory Council of Bank Negara Malaysia in 2004. He was also a member of Shariah Committee of Securities Commission. He has been re-appointed as a member of the Bank's Shariah Committee since 30 November 2009.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                    <li>
                        <div class="info noPic">
                            <p>Mohd Fadhly bin Md. Yusoff</p>
                            <p>Independent Member</p>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <h4 class="font-black">Mohd Fadhly bin Md. Yusoff</h4>
                                            <p class="font-black">
                                                <p>Encik Mohd Fadhly Md. Yusoff (Encik Mohd Fadhly) has more than 13 years of experience in Islamic Capital Market during his tenure as manager with Islamic capital market department of Securities Commission Malaysia from 1995 to 2008. During this period, he was involved in Shariah compliance supervision in relation to submissions for the issuances of sukuk, structured products, collective investment schemes and Islamic REITs. In addition, he has also undertaken in-depth research for the development of new Islamic capital market instruments as well as providing technical inputs for the preparation of various guidelines issued by Securities Commission Malaysia. Currently, Encik Mohd Fadhly also serves as a member of Shariah committee of RHB Islamic Bank Berhad, Bank Pembangunan Malaysia Berhad, Sun Life Malaysia Takaful Bhd and Opus Asset Management Sdn Bhd. He has actively participated in various industry development initiatives namely the International Organization of Securities Commission (IOSCO) Task Force on Islamic Capital Market, Islamic Financial Services Board's (IFSB) Governance of Islamic Investment Funds Working Group, technical member for the publication of Resolutions of the Securities Commission Shariah Advisory Council and Islamic capital market educational and/or promotional programmes. He obtained his Bachelor of Shariah (1st Class Honours) from University of Malaya, Malaysia in 1995.</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                </ul>
            </div>
            <div class="about inner-content subnav-pages" id="trustee">
                <ul class="tiles">

                    <li>
                        <div class="info noPic">
                            <h2>Maybank Trustees Berhad</h2>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <!-- 						<div class="profile-pic-holder">
								<img class="bg" src="http://staging.apexis.com.my/img/about/trustee/maybank.png"/>
							</div> -->
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <div class="copy">
                                                <h4 class="font-black">Maybank Trustees Berhad</h4>
                                                <p><b>Corporate Information</b></p>
                                                <p>Maybank Trustees Berhad (5004-P) is the Trustee of the Fund with its registered office at 8th Floor, Menara Maybank, 100 Jalan Tun Perak, 50050 Kuala Lumpur.</p>
                                                <p>Maybank Trustees Berhad (“MTB”) was incorporated on 12 April 1963 and registered as a Trust Company under the Trust Companies Act 1949 on 11 November 1963. It was one of the first local trust companies to provide trustee services with the objective of meeting the financial needs of both individual and corporate clients.</p>
                                                <p><b>Experience in Trustee Business</b></p>
                                                <p>Maybank Trustees Berhad has acquired experience in the administration of unit trust funds/ schemes since 1991.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>

                    <li>
                        <div class="info noPic">
                            <h2>CIMB Commerce Trustee Berhad</h2>
                        </div>
                        <div class="expandable-div-holder">
                            <div class="expandable-div-cover">
                                <div class="expandable-div-content">
                                    <div class="expandable-div-center">
                                        <div class="expandable-div-bg bg-white"></div>
                                        <!-- 						<div class="profile-pic-holder">
								<img class="bg" src="http://staging.apexis.com.my/img/about/trustee/cimb.png"/>
							</div> -->
                                        <div class="profile-bio-holder noPic">
                                            <a href="javascript:;">
                                                <img class="close-btn" src="../ICONPIC/close-btn-dark.png" /></a>
                                            <div class="copy">
                                                <h4 class="font-black">CIMB Commerce Trustee Berhad</h4>
                                                <p><b>Corporate Information</b></p>
                                                <p>CIMB Commerce Trustee Berhad was incorporated on 25 August 1994 and registered as a trust company under the Trust Companies Act, 1949 and having its registered office at Level 13, Menara CIMB, Jalan Stesen Sentral 2, Kuala Lumpur Sentral, 50470 Kuala Lumpur, Malaysia.   The Trustee is qualified to act as a trustee for collective investment schemes approved under the Capital Markets and Services Act 2007.</p>
                                                <p><b>Experience as Trustee to Unit Trust Funds</b></p>
                                                <p>CIMB Commerce Trustee Berhad has been involved in unit trust industry as trustee  since 1996. It acts as Trustee to various unit trust funds, real estate investment trust fund,  wholesale funds and  private retirement schemes.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="bg" src="/ICONPIC/profile-bg.jpg" />
                    </li>
                </ul>
            </div>
            <div class="about inner-content subnav-pages" id="compliance">
                <div class="description">
                    <!-- <h4>Compliance</h4> -->
                    <p>Astute Fund Management Berhad (&ldquo;AFMB&rdquo;) is committed to Compliance with the highest standards of ethics and integrity in all its business operations and expects that all its employees, officers and directors will comply with the word and spirit of every applicable law or regulation in the countries and localities in which we operate. AFMB has a comprehensive Compliance Unit which has established robust Compliance program and designed to ensure that its employees are aware of and adhere to applicable laws and ethical standards.</p>
                    <p><b>Statement on Code of Ethics and Conduct</b></p>
                    <p>We are committed to a high standard of integrity, fairness, ethical behaviour and excellence in our business dealings.</p>
                    <p>At the core of our value system is the belief that our clients are the bedrock of our business. Serving their needs effectively lies in the heart of everything that we do.</p>
                    <p>We believe that trust must be earned. This is fundamental to all lasting and successful partnerships. There is a requirement for us to know our clients well in order to meet their changing needs and objectives.</p>
                    <p>The principles and ethical standards which govern our conduct are contained in the Company’s Code of Ethics & Conduct for Employees (&ldquo;Code of Ethics&rdquo;).</p>
                    <p>The Code was developed with the following objectives in mind:</p>
                    <ol type="i" style="margin:auto; padding:20px;">
                        <li style="list-style:lower-roman;">To instil a performance based culture among our employees and to deliver excellent performance and service to our clients.</li>
                        <li style="list-style:lower-roman;">As a Capital Market Services Licence (&ldquo;CMSL&rdquo;) holder under the Securities Commission, to ensure that our employees across all business activities act with integrity, honesty and accountability.</li>
                        <li style="list-style:lower-roman;">To provide growth and development for our staff, serve the communities in which we operate and continuously build our reputation as a trusted partner among our clients, regulators and stakeholders.</li>
                    </ol>
                    <p>The Company takes compliance and risk seriously. Our employees are encouraged to stay abreast with the changing requirements under the industry’s laws and regulations. In addition, they are required to adhere to our internal policies and procedures. Incidents of non-compliance by our staff are met with appropriate penalties including dismissal for serious breaches or misconduct.</p>
                    <p>Employees are also required to confirm their adherence to the Code of Ethics on an annual basis.</p>
                    <p>Please email <a class="font-light-blue" href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a> should you require more information on our Code of Ethics & Conduct.</p>
                    <p>Please Find Contact Details:</p>
                    <p>
                        Tel:&nbsp;<a class="font-light-blue" href="tel:+60387361118">(603) 2095 9999</a><br />
                        Email:&nbsp;<a class="font-light-blue" href="mailto:aisb.comp@apexis.com.my">aisb.comp@apexis.com.my</a>
                    </p>
                </div>
            </div>
            <div class="about inner-content subnav-pages" id="chart">
                <div class="description">
                    <!-- <h4>Organisation Chart</h4> -->
                </div>
                <div class="chart_holder">
                    <p>
                        <img src="../ICONPIC/org_chart_april_2022.jpg" />
                    </p>
                </div>
            </div>
            <div class="about inner-content subnav-pages" id="internal_audit">
                <div class="description">
                    <!-- <h4>Internal Audit</h4> -->
                    <p>Astute Fund Management Berhad outsourced its Internal Audit function to Total International Associates (“TIA”), an independent professional firm in order to provide the Audit Committee and Board of Directors with adequate assurance by leveraging on specialist expertise not available internally. We believe that TIA, will be able to bring a systematic and disciplined approach in evaluating and improving the effectivess of the Company’s internal risk management, control and governance processes.</p>
                    <p>Established in 2004, Total International Associates (TIA) is a boutique firm approved by Malaysian Institute of Accountants (MIA) to carry out our duties which specializing in assurance and providing business solutions that are customised to meet our client’s financial needs.</p>
                    <p>Total International Associates takes a holistic approach towards risk and controls management with a view to offer comprehensive solutions, taking into account the dynamics of the business environment.</p>
                    <p>Members of the team come from diverse backgrounds, thus offering new insights to ensure efficient and effective solutions. We have members on the team who are very experienced in the areas of corporate governance, enterprise risk management, and internal controls framework, and have undertaken many assignments in Public Listed Companies.</p>
                    <%--<p>Astute Fund Management Berhad’s Internal Audit function is outsourced to Messers Moore Stephens Associate PLT (“Moore Stephens Malaysia”), an independent professional firm, which adopts the International Professional Practices Framework (”IPPF”) in carrying out internal audit assignments on the company.</p>
                    <p>Moore Stephens is a global accountancy and advisory organization headquartered in London, United Kingdom, and has more than 650 offices in 106 countries, Malaysia being one of which.</p>
                    <p>Moore Stephens Malaysia offers a flexible and customized approach to the engagement based on client’s requirements, and is well organized for keeping abreast of the rapidly changing business environment.</p>
                    <p>Having worked with businesses in different sectors, Moore Stephens Malaysia advises clients on their governance structure, providing a range of services designed to support them in maintaining a robust governance and assurance framework, as well as assisting on managing risks effectively whilst embedding transparency and accountability.</p>
                    <p>In addition to internal audit services, Moore Stephens Malaysia also provides the following professional services:-</p>
                    <p>
                        &nbsp&nbsp&nbsp&nbsp&#8226;Risk Management<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Risk management framework development<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Enterprise wide risk assessments<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Risk and performance monitoring
                    </p>
                    <p>
                        &nbsp&nbsp&nbsp&nbsp&#8226;Strategic Advisory<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Performance management<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Succession planning Corporate governance ‘health checks’<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Business continuity planning<br />
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&#9900 Crisis management planning
                    </p>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
