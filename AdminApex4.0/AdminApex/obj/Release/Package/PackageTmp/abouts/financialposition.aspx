﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="financialposition.aspx.cs" Inherits="AdminApex.FinancialPosition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <div class="financialposition">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image">
                    <img src="/ICONPIC/APEX_Header_aboutus.jpg" />
                    <div class="header-copy">
                        <h2>20 years strong.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="content narrow">
            <h3>Financial Position</h3>
            <table class="key-table" cellpadding="0" cellspacing="0">
                <tr>
                    <th colspan="12">Key Financial Information</th>
                </tr>
                <tr class="table-desktop-hide">
                    <td class="bold text-center mobile-years" colspan="2" data-year="3"><asp:Label id="myear1" runat="server"></asp:Label></td>
                    <td class="bold text-center mobile-years" colspan="2" data-year="2"><asp:Label id="myear2" runat="server"></asp:Label></td>
                    <td class="bold text-center mobile-years" colspan="2" data-year="1"><asp:Label id="myear3" runat="server"></asp:Label></td>
                </tr>
                <tr class="table-mobile-row-hide">
                    <td colspan="3"></td>
                    <td colspan="9" class="bold text-center">Financial Year Ended</td>
                </tr>
                <tr>
                    <td rowspan="2" colspan="3"></td>
                    <td class="bold text-center year-3 table-mobile-cell-hide" colspan="3"><asp:Label id="year1" runat="server"></asp:Label></td>
                    <td class="bold text-center year-2 table-mobile-cell-hide" colspan="3"><asp:Label id="year2" runat="server"></asp:Label></td>
                    <td class="bold text-center year-1 table-mobile-cell-hide" colspan="3"><asp:Label id="year3" runat="server"></asp:Label></td
                </tr>

                <tr>
                    <td class="bold text-center year-3" colspan="3">(MYR 'mil) Audited</td>
                    <td class="bold text-center year-2" colspan="3">(MYR 'mil) Audited</td>
                    <td class="bold text-center year-1" colspan="3">(MYR 'mil) Audited</td>
                </tr>

                <tr>
                    <td class="" colspan="3">Revenue</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="revenue1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="revenue2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="revenue3" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="" colspan="3">Profit or Loss Before Tax</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="before1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="before2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="before3" runat="server"></asp:Label></td>
                </tr>

                <tr>
                    <td class="" colspan="3">Profit or Loss After Tax</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="after1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="after2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="after3" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="" colspan="3">Paid-up Capital</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="issue1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="issue2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="issue3" runat="server"></asp:Label></td>

                <tr>
                    <td class="" colspan="3">Shareholders’ Fund</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="share1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="share2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="share3" runat="server"></asp:Label></td>
                </tr>

                <tr>
                    <td class="" colspan="3">Cash & Bank Deposits</td>
                    <td align="center" class="year-3" colspan="3"><asp:Label id="cash1" runat="server"></asp:Label></td>
                    <td align="center" class="year-2" colspan="3"><asp:Label id="cash2" runat="server"></asp:Label></td>
                    <td align="center" class="year-1" colspan="3"><asp:Label id="cash3" runat="server"></asp:Label></td>
                </tr>
                
                <tr>
                    <td class="" colspan="3">Bank Borrowings</td>
                    <td align="center" class="year-3" colspan="3">Nil</td>
                    <td align="center" class="year-2" colspan="3">Nil</td>
                    <td align="center" class="year-1" colspan="3">Nil</td>
                </tr>
            </table>

            <table class="fund-table" cellpadding="0" cellspacing="0">
                <tr>
                    <th colspan="3">Fund Summary</th>
                </tr>

                <tr>
                    <td class="bold">Funds</td>
                    <td class="bold text-center">No. of Funds</td>
                    <td class="bold text-center">Total Value of Funds (MYR)*</td>
                </tr>
                <tr>
                    <td class=""><asp:Label id="fund1" runat="server"></asp:Label></td>
                    <td class="text-center"><asp:Label id="no1" runat="server"></asp:Label></td>
                    <td class="text-center"><asp:Label id="value1" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class=""><asp:Label id="fund2" runat="server"></asp:Label></td>
                    <td class="text-center"><asp:Label id="no2" runat="server"></asp:Label></td>
                    <td class="text-center"><asp:Label id="value2" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="">Total</td>
                    <td class="text-center"><asp:Label id="totalno" runat="server"></asp:Label></td>
                    <td class="text-center"><asp:Label id="totalvalue" runat="server"></asp:Label></td>
                </tr>
            </table>
           

            <div class="footnote">
                <p>* NAV as at <asp:Label id="navdate" runat="server"></asp:Label> (this excludes Private Mandates)</p>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
