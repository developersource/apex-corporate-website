﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="security.aspx.cs" Inherits="AdminApex.security" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="single docs security">
    <div class="content pages">
        <div class="about header bg-white">
            <div class="header-content bg-dark-blue">
                <div class="header-copy">
                    <h2>NOTICE CONCERNING YOUR PERSONAL PRIVACY AND PERSONAL DATA POLICY</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="content narrow">
        <div class="narrow-copy">
        <p>We are committed to protecting the privacy of the investors in conducting our unit trust management business. “Personal Data” is information that identifies and relates to you or other individuals (such as your joint account holder). In this form, we describe to you how we handle your Personal Data that we collect through this Application Form (the “MasterAccount Application Form”) and through other means (for example, from your written instructions, telephone calls, e-mails and other communications or correspondences with us, as well as from our unit trust agents, business partners, other unit trust management companies, or other third parties involved in our business dealings with you). “YOU” IN THIS NOTICE REFERS TO YOU AS PRINCIPAL HOLDER AND/OR YOUR JOINT ACCOUNT HOLDER, IF APPLICABLE.</p>

        <h4>1.  About this Notice:</h4>
        <p>1.1 This <b>“Notice Concerning Your Personal Data”</b> (“Notice”) is issued pursuant to <b>Section 7 of Malaysian Personal Data Protection Act 2010</b> (“Act”) under the “Notice and Choice Principle”. Please refer to Section 2 of this Notice for definition of Personal Data.</p><br>
        <p>1.2 You may request to be issued a free copy of the Notice by sending an email to our Customer Service at <a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a> or calling our Customer Service Hotline at <span class="phonetext">03- 2095 9999</span>, or writing to “Customer Service, Astute Fund Management Berhad” at 3rd Floor, Menara MBSB, 46 Jalan Dungun, Damansara Heights, 50490 Kuala Lumpur. This Notice is also available on our Website “(<a href="//www.apexis.com.my" target="_blank">www.apexis.com.my</a>)”.</p><br>
        <p>1.3 Please note that the act is only applicable in respect of data provided by individual applicant(s) (i.e. individual data subjects) only and not applicable to corporate applicants.</p>

        <h4>2.  What is Your Personal Data (Definition of Personal Data):</h4>
        <p>2.1 Your Personal Data means any information given by you (“the Data Subject”) that relates directly or indirectly to you in our business dealings with you, which is:</p>
        <ul>
            <li>being processed wholly or partly using electronic medium (e.g. computer) or any equipment operating automatically or manually.</li>
            <li>being recorded by us with the intention that it should wholly or partly be processed by means of such equipment referred to above.</li>
            <li>being recorded as part of relevant filing system or with the intention that it should form part of our filing system.</li>
        </ul><br>
        <p>2.2 Personal Data also includes “sensitive” personal data (<b>“Sensitive Personal Data”</b>), defined as any personal data consisting of information as to your physical or mental health or conditions, your political opinions, your religious beliefs or other beliefs of a similar nature, the commission or alleged commission by you of any offence, or any other sensitive personal data as determined under the Act.</p><br>
        <p>2.3 Description of the Personal Data collected from you is set out in <b>Section 8</b> below.</p><br>
        <p><b>2.4 SENSITIVE PERSONAL DATA STATEMENT: In general, we will never ask for any of your Sensitive Personal Data as it is not relevant to our business dealings with you. If it becomes necessary to ask for such Sensitive Personal Data, we will only process the same after receiving your express written consent.</b></p>

        <h4>3.  The purposes for which your Personal Data is being or collected and further processed:</h4>
        <p>We collect and process your Personal Data for the following purposes:</p>
        <ol>
            <li>To solicit contributions to Units in the Unit Trust Fund(s).</li>
            <li>To aid us to make decisions on whether and how to provide our products and services to you</li>
            <li>To enter into business transaction with you.</li>
            <li>To deliver the necessary notice, services and/or products in accordance with our agreement with you.</li>
            <li>To execute business process and operations such as client relationship management.</li>
            <li>To aid in our planning in connection with our service and products.</li>
            <li>To communicate with you as part of our client-business relationship.</li>
            <li>To send you important information regarding changes to our policies, other terms and conditions and other administrative information.</li>
            <li>To assess your eligibility for suitable investment plans, and process your funds and other payments.</li>
            <li>To improve the quality of our training and security (for example, with respect to recorded or monitored phone calls to our Customer Service contact numbers).</li>
            <li>To prevent, detect and investigate crime, including fraud and money laundering, and analyse and manage other commercial risks.</li>
            <li>To carry out market research and analysis, including satisfaction surveys, where applicable.</li>
            <li>To provide marketing information to you (including information about other products and services offered by companies within Apex Services Investment Services Berhad and selected third-party partners) in accordance with preferences you have expressed.</li>
            <li>To personalise your experience on our Website by presenting information to you via our Website.</li>
            <li>To allow you to participate in contests, prize draws and similar promotions, and to administer these activities. Some of these activities have additional terms and conditions, which could contain additional information about how we use and disclose your Personal Data, which you must read carefully.</li>
            <li>To manage our IT infrastructure and business operations.</li>
            <li>To comply with internal policies and procedures such as for auditing; finance and accounting; IT systems; data and Website hosting; business continuity; and records, document and print management. </li>
            <li>To resolve complaints, and handle requests for data access or correction. </li>
            <li>To comply with applicable Malaysian laws and regulatory obligations (such as those relating to anti-money laundering, anti-terrorism and Proceeds of Unlawful Activities).</li>
            <li>To comply with legal process; and respond to requests from public regulatory and governmental authorities.</li>
            <li>To establish and defend the legal rights privacy, safety or property of our company and/or related companies, and pursue available remedies or limit our damages.</li>
        </ol>

        <h4>4.  How we collect your Personal Data (Source of your Personal Data):</h4>
        <p>We collect your Personal Data from various sources such as from the internet and social media, from publicly available information, from forms submitted by you, and through telephone calls, telephone recordings, camera and security footage (CCTV), your communication and correspondences with us (via electronic or written media), from our unit trust agents, financial planners, business partners, other unit trust management companies, or other third parties involved in our business dealings with you.</p>

        <h4>5.  Your right to access your Personal Data and make correction requests, raise questions and concerns:</h4>
        <p>5.1 You shall be given access to your Personal Data held by us and you shall be able to correct that Personal Data where the Personal Data is inaccurate, incomplete, misleading or not up-to-date, except where compliance with a request to such access or correction is refused under the Act.</p><br>
        <p>5.2 Your Personal Data shall be processed by us or by a third party (“Service Provider”) on our behalf.</p><br>
        <p>5.3 You may, upon payment of a prescribed fee, make a request in writing to us, for (1) information of your Personal Data that is being processed by or on our behalf, and (2) for a copy of your Personal Data to be provided to you in a legible format.</p><br>
        <p>5.4 If you found your Personal Data to be inaccurate, incomplete, misleading or not up-to-date, you have the right to access, correct, object to the use of, or request deletion or suppression of your Personal Data. Please contact us as set out in <b>Section 7</b> below with any such requests or if you have any questions or concerns about how we process Personal Data.</p><br>
        <p>5.5 We will ensure compliance with your request not later than twenty-one (21) days from the date of receipt of such request, subject to <b>Section 5.6</b> below.</p><br>
        <p>5.6 Please note that some Personal Data may be exempt from access, correction, objection, deletion or suppression rights in accordance with the Act. We will notify you when certain circumstances arise as permitted under the Act where we may refuse to comply with your request not later than twenty- one (21) days from the date of receipt of your request.</p>

        <h4>6.  Sharing of Your Personal Data (the class of third parties to whom we disclose or may disclose your Personal Data):</h4>
        <p>6.1  We may make your Personal Data available to:</p>
        <p><b>a)    Our company and partners</b><br>
            For a list of Astute Fund Management Berhad ("AFMB") and its partners that may have access to and use of your Personal Data, please refer to: (<a href="//www.apexis.com.my" target="_blank">www.apexis.com.my</a>). AFMB is responsible for the management and security of jointly used Personal Data. Access to Personal Data within AFMB is restricted to those individuals who have a need to access the information for our business purposes.</p><br>
        <p><b>b)    Other marketing and distribution parties</b><br>
            In the course of marketing and distribution of unit trust funds, we may make Personal Data available to third parties such as other financial planners; service providers; regulators and employees and other intermediaries and agents; appointed representatives; distributors; affinity marketing partners; and financial institutions, securities firms and other business partners.</p><br>
        <p><b>c)    Our service providers</b><br>
            External third-party service providers, such as medical professionals, accountants, auditors, experts, lawyers and other outside professional advisors; call center service providers; IT systems, support and hosting service providers; printing, advertising, marketing and market research and analysis service providers; banks and financial institutions that service our accounts; third-party back office service providers and administrators; document and records management providers; construction consultants; engineers; examiners; administrators of justice; translators; and similar third-party vendors and outsourced service providers that assist us in carrying out business activities.</p><br>
        <p><b>d)    Governmental authorities and third parties involved in court action</b><br>
            We may also share Personal Data with governmental or other public authorities (including, but not limited to, workers’ compensation boards, courts, law enforcement, tax authorities and criminal investigations agencies); and third-party civil legal process participants and their accountants, auditors, lawyers and other advisors and representatives as we believe to be necessary or appropriate: (a) to comply with applicable law, including laws outside Malaysia; (b) to comply with legal process; (c) to respond to requests from public and government authorities including public and government authorities outside Malaysia; (d) to enforce our terms and conditions; (e) to protect our operations or those of any of AFMB affiliated companies; (f) to protect our rights, privacy, safety or property, and/or that of our group companies, you or others; and (g) to allow us to pursue available remedies or limit our damages.</p><br>
        <p><b>e)    Other Third Parties</b><br>
            We may share Personal Data with payees; emergency providers (fire, police and medical emergency services); retailers; medical networks, organisations and providers; travel carriers; credit bureaus; credit reporting agencies; and other people involved in an incident that is the subject of a dispute; as well as purchasers and prospective purchasers or other parties in any actual or proposed reorganisation, merger, sale, joint venture, assignment, transfer or other transaction relating to all or any portion of our business, assets or stock.</p><br>
            <p>Personal Data may also be shared by you, on message boards, chat, profile pages and blogs, and other services on our Website to which you are able to post information and materials. Please note that any information you post or disclose through Website services will become public information, and may be available to visitors to the Website and to the general public. We urge you to be very careful when deciding to disclose your Personal Data, or any other information, on our Website.
            </p><br>
        <p>6.2  We shall ensure that there is a procedure, policy, process or control when vendors, contractors, suppliers or any third party are allowed to have access to your Personal Data in the course of data maintenance.</p>

        <h4>7.  Who to contact about your Personal Data (for any inquiries or complaints in respect of your Personal Data):</h4>
        <p>If you have any questions about our use of your Personal Data you can e-mail to our Customer Service at <a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a>, calling our Customer Service Hotline at <span class="phonetext">03-2095 9999</span> or writing to “Customer Service, Astute Fund Management Berhad” at 3rd Floor, Menara MBSB, 46 Jalan Dungun, Damansara Heights, 50490 Kuala Lumpur.  </p>

        <h4>8.  Your Personal Data that we collect (Description of the Personal Data collected from you):</h4>
        <p>The Personal Data collected about you may include:</p>
        <ul>
            <li><b>General identification and contact information</b><br>
                Your name; address; e-mail and telephone details; gender; race, nationality status; identification card number; date of birth; passwords; educational background; photos; employment history, skills and experience; professional licenses and affiliations and relationship to the joint account holder (if applicable).</li><br>
            <li><b>identification numbers issued by government bodies or agencies</b><br>
                National registration identification number; passport number; tax identification number; military identification number; or driver’s or other license number.</li><br>
            <li><b>Financial information and account details</b><br>
                Payment card number; bank account number and account details; credit history and credit score; assets; income; and other financial information.</li><br>
            <li><b>Other sensitive information</b><br>
                In rare cases, we may receive sensitive information about your religious beliefs, political opinions, family medical history or genetic information (for example, if you apply for insurance through a third- party marketing partner that is a trade, religious or political organisation). In addition, we may obtain information about your criminal record or civil litigation history in the process of preventing, detecting and investigating fraud. We may also obtain sensitive information if you voluntarily provide it to us (for example, if you express preferences regarding medical treatment based on your religious beliefs).</li><br>
            <li><b>Telephone recordings</b><br>
                Recordings of telephone calls to our representatives and customer service call centers.</li><br>
            <li><b>Information enabling us to provide products and services</b><br>
                Location and identification of your property to send your statement (for example, property address); your status as director or partner; and other ownership or management interest in an organisation.</li><br>
            <li><b>Marketing preferences and customer feedback</b><br>
                You may let us know your marketing preferences, enter a contest or prize draw or other sales promotion, or respond to a voluntary customer satisfaction survey.</li><br>
        </ul>

        <h4>9.  The choices and means available for limiting the processing of Personal Data, including Personal Data relating to other persons who may be identified from that personal data:</h4>
        <p>If you wish to limit the processing of your Personal Data, including Personal Data relating to other persons who may be identified from that personal data, you may contact us at the address set out in <b>Section 7</b> above.</p>

        <h4>10. Whether it is obligatory or voluntary for you to supply your Personal Data</h4>
        <p>It is obligatory for you to supply your Personal Data to us to enable us to use it as described in Section 3 above. Your failure to provide your Personal Data may cause us to be unable to provide you any of the services described herein.</p>

        <h4>11. Withdrawal of consent</h4>
        <p>You may by notice in writing withdraw your consent to the processing of your Personal Data. We shall, upon receiving your notice, cease the processing of the Personal Data, whereupon our relationship may be terminated.</p>

        <h4>12. Your right to prevent processing of Personal Data likely to cause damage or distress</h4>
        <p>12.1 Subject to <b>Section 12.1</b>, you may at any time by notice in writing to us, require us, at the end of such period as is reasonable in the circumstances, to, in respect of any of your Personal Data, either</p>
        <ol>
            <li>cease the processing of or processing for a specified purpose or in a specified manner; or</li>
            <li>not begin the processing of or processing for a specified purpose or in a specified manner, if, based on reasons to be stated by you,</li>
            <li>the processing of that Personal Data or the processing of personal data for that purpose or in that manner is causing or is likely to cause substantial damage or substantial distress to you or to another person; and</li>
            <li>the damage or distress is or would be unwarranted.</li>
        </ol>
        <p>12.2 <b>Section 12.1</b> shall not apply where:</p>
        <ol class="lower-alpha">
            <li>you have given your consent;</li>
            <li>the processing of Personal Data is necessary;
                <ol class="lower-roman">
                    <li>for the performance of a contract to which you are a party;</li>
                    <li>for the taking of steps at your request with a view to entering a contract;</li>
                    <li>for compliance with any legal obligation to which we are the subject, other than an obligation imposed by contract; or</li>
                    <li>in order to protect the vital interests of the data subject; or</li>
                </ol>
            </li>
            <li>in such other cases as may be prescribed under the Act.</li>
        </ol>

        <h4>13. Disclosure of your Personal Data</h4>
        <p>13.1 Subject to <b>Section 13.1</b> below, no personal data shall, without your consent, be disclosed:</p>
        <ol class="lower-alpha">
            <li>for any purpose other than (i) the purpose for which the Personal Data was to be disclosed at the time of collection, or (ii) a purpose directly related to the aforementioned purpose , or</li>
            <li>to any party other than a third party of the class of third parties as specified in <b>Section 6</b> above.</li>
        </ol>
        <p>13.2 Notwithstanding <b>Section 13.1</b>, your Personal Data may be disclosed by us for any purpose other than the purpose for which the Personal Data was to be disclosed at the time of its collection or any other purpose directly related to that purpose, only under the following circumstances:</p>
        <ol class="lower-alpha">
            <li>you have given your consent to the disclosure;</li>
            <li>the disclosure:-
                <ol class="lower-roman">
                    <li>is necessary for the purpose of preventing or detecting a crime, or for the purpose of investigations; or</li>
                    <li>was required or authorised by or under any law or by the order of a court;</li>
                </ol>
            </li>
            <li>We have acted in the reasonable belief that we had in law the right to disclose the personal data to the other person;</li>
            <li>We have acted in the reasonable belief that we would have had your consent if you had known of the disclosing of the Personal Data and the circumstances of such disclosure; or the disclosure was justified as being in the public interest in circumstances as determined by the Government.</li>
        </ol>

        <h4>14. Security</h4>
        <p>14.1    We shall, when processing your Personal Data, take practical steps to protect your Personal Data from any loss, misuse, modification, unauthorised or accidental access or disclosure, alteration or destruction having considered: (a) to the nature of the Personal Data and the harm that would result from such loss, misuse, modification, unauthorised or accidental access or disclosure, alteration or destruction; (b) to the place or location where the Personal Data is stored; (c) to any security measures incorporated into any equipment in which the Personal Data is stored; (d) to the measures taken for ensuring the reliability, integrity and competence of personnel having access to the Personal Data; and (e) to the measures taken for ensuring the secure transfer of the Personal Data.</p><br>
        <p>14.2    <b>Where processing of your Personal Data is carried out by a data processor on our behalf, we shall, for the purpose of protecting your Personal Data as set out in Section 14.1 ensure that the data  processor</b></p>
        <ol class="lower-alpha">
            <li>give sufficient guarantees in respect of the technical and organisational security measures governing the processing to be carried out and </li>
            <li>take reasonable steps to ensure compliance with those measures.</li>
        </ol><br>
        <p>14.3    We will take appropriate technical, physical, legal and organisational measures, which are consistent with applicable privacy and data security laws. Unfortunately, no data transmission over the Internet or data storage system can be guaranteed to be 100% secure. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of any Personal Data you might have with us has been compromised), please immediately notify us. (See the <b>“Who to Contact About Your Personal Data”</b> section above.)</p><br>
        <p>14.4    When we provide Personal Data to a service provider, the service provider will be selected carefully and required to use appropriate measures to protect the confidentiality and security of the Personal Data. </p><br>
        <p>14.5    We shall ensure that your Personal Data is being held securely, either in electronic form, on paper or in any other medium.</p><br>
        <p>14.6    <b>Our responsibility to train our employees and Unit Trust Consultants:</b> Since we are responsible for the processing of your Personal Data processed by our employees, we strive to ensure our employees and Unit Trust Consultant are aware of their responsibilities when processing of the your Personal Data and Sensitive Personal Data (where applicable) to ensure the reliability, integrity and competence of the employees having access to your Personal Data. Our employees and Unit Trust Consultant will be required to undergo training to understand their duties and responsibilities under the Personal Data Protection Act 2010 at least once. We shall restrict access to your Personal Data to those employees, in the strict need to know only</p>

        <h4>15. Retention of Personal Data</h4>
        <p>15.1    Your Personal Data processed for the purposes here shall not be kept longer than is necessary for the fulfillment of that purpose. We shall take all reasonable steps to ensure that all Personal Data is destroyed or permanently deleted if it is no longer required for the purpose for which it was to be processed.</p><br>
        <p>15.2    We take reasonable steps to ensure that the Personal Data we process is reliable for its intended use and as accurate and complete as is necessary to carry out the purposes described in this Notice.</p><br>
        <p>15.3    We shall keep and maintain a record of any application, notice, request or any other information relating to your Personal Data that has been or is being processed by us or any third Party. For this purpose, we will retain your Personal Data for the period necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law.</p><br>
        <p>15.4    Additionally, we have developed a <b>Document Retention Policy</b> to specify the retention period of your Personal Data and when to dispose any document containing your Personal Data when we no longer require to process it or when you refuse to give your consent pursuant to this Notice. Your Personal Data shall not be kept longer than is necessary for the fulfillment of this purpose and the permanent deletion or destruction of your Personal Data is necessary as soon as there is no more need for this purpose. Similarly, we shall have a policy for <b>Dealing with Data Protection Issues</b>.</p><br>
        <p>15.5    We shall keep and maintain a record of any application, notice, request or any other information relating to your Personal Data that has been or is being processed by us as required by Section 108 of the Capital Markets and Services Act 2007 for a period of not less than seven (7) years.</p>

        <h4>16. Data Integrity:</h4>
        <p>We shall take reasonable steps to ensure that the Personal Data is accurate, complete, not misleading and kept up-to-date by having regard to the purpose, including any directly related purpose, for which the Personal Data was collected and further processed.</p>

        <h4>17. Repeated collection of personal data in same circumstances:</h4>
        <p>17.1    Where we have complied with requirement to give this Notice pursuant to the <b><u>Section 7 of the Act</u></b> in respect of the collection of Personal Data from you, referred to as the “first collection”; and, where on any subsequent occasion again we collect Personal Data from you, referred to as the “subsequent collection”, we are not required to comply with section 7 of the act if (a) to comply in respect of that subsequent collection would be to repeat, in the same circumstances, what was done to comply in respect of the first collection; and (b) not more than twelve (12) months have elapsed between the first collection and the subsequent collection.</p><br>
        <p>17.2    For the avoidance of doubt, it is declared that subsection (1) shall not operate to prevent a subsequent collection from becoming a first collection if we have complied with the provisions of the Notice pursuant to Section 7 of the Act.</p>

        <h4>18. Personal Data of other individuals:</h4>
        <p>If you provide Personal Data to us regarding other individuals, you <b>undertake</b>:</p>
        <ol class="lower-alpha">
            <li>to inform the individual about the content of this Privacy and Personal Data Policy; and</li>
            <li>to obtain any legally-required consent for the collection, use, disclosure, and transfer (including cross- border transfer) of Personal Data about the individual in accordance with this Privacy and Personal Data Policy. </li>
        </ol>

        <h4>19. International transfer of Personal Data:</h4>
        <p>19.1    Due to the global nature of our business, for the purposes set out above we may only transfer Personal Data to parties located in other countries when it is necessary (including the United States and other countries that have a different data protection regime than is found in the country where you are based). For example, we may transfer Personal Data in order to process international wire transfer for payment settlement. We may transfer information internationally to our group companies, service providers, business partners and governmental or public authorities.</p><br>
        <p>19.2    If we do so, we shall ensure that your Personal Data transferred out of Malaysia is secure and protected.</p>

        <h4>20. Use of Fund Master Form by minors:</h4>
        <p>20.1    This Fund Master Form is not directed to individuals under the age of 18 and we request that these individuals do not provide Personal Data through this Fund Master Form.</p><br>
        <p>20.2    You are not allowed to provide any Personal Data to us regarding the other individuals who are minors (“said minors”), through this Form, unless you are the parent/legal guardian of the said minors. If you are, you agree to (i) to inform the individual about the content of this Privacy and Personal Data Policy; and give consent on their behalf by executing a <b>Parent/Guardian Consent form</b>, allowing for the collection, use, disclosure, and transfer (including cross-border transfer) of Personal Data of the said minors.</p>

        <h4>21. Solicitation of Direct Marketing:</h4>
        <p>21.1    We invite you to be in our mailing list for the purposes of Direct Marketing from us. “Direct Marketing” means the communication from us by whatever means of any advertising or marketing material from us which is directed to you. Alternatively, we may cease or not to begin processing your personal data for purposes of direct marketing. <b>Therefore, if you <u>do not wish</u> to be included in</b> our future unit trust products campaign, new unit trust products launches and events including promotional events with business partners in our service platform, please <a href="http://staging.apexis.com.my/contact" target="_blank">Contact Us</a>.</p><br>
        <p>21.2    Notwithstanding the foregoing, you may at any time by notice in writing to us requesting us at the end of such period as is reasonable in the circumstances to cease or not to begin processing your Personal Data for the above purposes. If you are dissatisfied with our failure to comply with your notice, whether in whole or in part, you may submit an application to the Commissioner of Personal Data Protection Board, to require us to comply with the Notice.</p>

        <h4>22. Your marketing preferences:</h4>
        <p>We will provide you with regular opportunities to tell us your marketing preferences, including in our communications to you. You can also contact us by e-mail at <a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a> or call us at our Customer Service Hotline: <b><span class="phonetext">03- 2095 9999</span></b>, or write to Customer Service, Astute Fund Management Berhad at <b>3rd Floor, Menara MBSB, 46 Jalan Dungun, Damansara Heights, 50490 Kuala Lumpur</b> to tell us your marketing preferences and to opt-out.</p><br>
        <p>22.1    If you no longer want to receive marketing-related e-mails from us on a going-forward basis, you may opt-out of receiving these marketing-related emails by clicking on the link to “unsubscribe” provided in each e- mail or by contacting us at the above addresses.</p><br>
        <p>22.2    We aim to comply with your opt-out request(s) within a reasonable time period. Please note that if you opt-out as described above, we will not be able to remove your Personal Data from the databases of third parties with whom we have already shared your Personal Data (i.e., to those to whom we have already provided your Personal Data as of the date on which we respond to your opt-out request). Please also note that if you do opt-out of receiving marketing communications from us, we may still send you other important administrative communications from which you cannot opt-out.</p>

        <h4>23. International transfer of Personal Data:</h4>
        <p>23.1    We shall not transfer any of your Personal Data to a place outside Malaysia unless to such place as specified pursuant to the Act.</p><br>
        <p>23.2    Notwithstanding <b><u>Section 23.1</u></b>, we may transfer your Personal Data to a place outside Malaysia, for example to our group companies, service providers, business partners and governmental or public authorities, under the following circumstances:</p>
        <ul>
            <li>Where you have given your consent to the transfer;</li>
            <li>the transfer is necessary for the performance of a contract between you and us;</li>
            <li>the transfer is necessary for the conclusion or performance of a contract between us and a third party which:</li>
            <li>is entered into at your request;</li>
            <li>is in your interests.</li>
            <li>the transfer is for the purpose of any legal proceedings or for the purpose of obtaining legal advice or for establishing, exercising or defending legal rights;</li>
            <li>we have reasonable grounds for believing that in all circumstances of the case;</li>
            <li>the transfer is for the avoidance or mitigation of adverse action against you;</li>
            <li>it is not practicable to obtain your consent in writing to that transfer; and</li>
            <li>if it was practicable to obtain such consent, you would have given his consent.</li>
            <li>We have taken all reasonable precautions and exercised all due diligence to ensure that the Personal Data will not in that place be processed in any manner which, if that place is Malaysia, would be a contravention of this Act;</li>
            <li>the transfer is necessary in order to protect your vital interests ;</li>
            <li>the transfer is necessary as being in the public interest in circumstances as determined under the Act.</li>
        </ul><br>
        <p>23.3    Due to the global nature of our business, it may become necessary to transfer your Personal Data outside of Malaysia. </p>


        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
