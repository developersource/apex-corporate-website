﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdminApex.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="css/liMarquee.css" rel="stylesheet" />

    <style>
        .modal-container {
            position: fixed;
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: rgba(0,0,0,0.3);
            top: 0;
            left: 0;
            height: 100vh;
            width: 100vw;
            z-index: 1000;
        }

        .popup-annoucement-box {
            background-color: #fff;
            width: 700px;
            max-width: 100%;
            padding: 30px 50px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.2);
            text-align: center;
        }

            .popup-annoucement-box h3 {
                margin: 15px;
                text-align: center;
                font-weight: 600;
                font-size: 20px;
                text-transform: none;
            }

            .popup-annoucement-box p {
                margin: 15px 0px 0px 0px;
            }

        .popup-annoucement-box-copy {
            padding: 30px 50px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            background: #475976;
            text-align: center;
            vertical-align: middle;
            box-sizing: border-box;
        }
    </style>


    <div id="showAnnoucement" runat="server" clientidmode="static">
    </div>

    <div class="header-content home">

        <div class="tint">
        </div>

        <div class="first-slider slider-item">

            <div class="one segmenter" style="background-image: url('../ICONPIC/EDM17882.jpg'); background-size: cover;"></div>

            <!-- <div class="tint"></div> -->

            <div class="home header">

                <div class="header-copy">

                    <h2 class="font-white" style="width: 100%">CREATE WEALTH BY INVESTING.<br />
                        Begin with the<br />
                        right fund manager.</h2>
                    <!--<h3 class="font-white h3_value" style="width: 100%">Dear valued clients,<br /></h3> -->
                    <div style="width:100%; height:100%; border: 3px solid white;">
                        <div style="padding-left:10px; padding-right:10px;">
                            <p style="margin-top:15px">Pursuant to the change in our Company name from Apex Investment Services Berhad to Astute Fund Management Berhad, we are pleased to inform you that we have changed the fund name for the following funds with the issuance of a Third Supplementary Master Prospectus (“Prospectus”) and Product Highlights Sheet (“PHS”). </p>
                            <p style="margin-top:15px">Please take note that there may be other changes made on the disclosure of the funds, kindly refer to the Prospectus and PHS of the respective funds for further details. Please contact your sales representative if you have any queries or require further clarification on this matter. Alternatively, we can also be reached via phone or email at 03-2095 9999 / enquiry@astutefm.com.my. Thank You.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="second-slider slider-item">

            <div class="two segmenter" style="background-image: url('../ICONPIC/EDM27228.jpg'); background-size: cover;"></div>
            <!-- <div class="tint"></div> -->
            <div class="home header">

                <div class="header-copy">

                    <h2 class="font-white">PROVIDING MARKET EXPERTISE
                    <br />
                        FOR OVER TWO DECADES.</h2>

                    <%--                <a href="http://staging.apexis.com.my/products/unit_trust" class="rounded-btn bg-gray font-black">Our Products</a>--%>
                </div>

            </div>

        </div>

        <div class="third-slider slider-item">

            <div class="three segmenter" style="background-image: url('../ICONPIC/3_the_right_thing.jpg'); background-size: cover;"></div>

            <!-- <div class="tint"></div> -->

            <div class="home header">

                <div class="header-copy">

                    <h2 class="font-white">THE RIGHT THING,
                        <br />
                        IN THE RIGHT WAY,
                        <br />
                        AT THE RIGHT TIME.
                    </h2>

                    <%--                <a href="ourvalues.aspx" class="rounded-btn bg-gray font-black">
                    Apex</a>--%>
                </div>
                <b style="color: red"></b>

            </div>

        </div>

    </div>
    <div class="container">
        <div class="row-sm-1">
            <div class="col-sm-12">
                <div class="tickertape">


                    <div class="goDown">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div runat="server" id="spanA" class="str3-2 str_wrap mt-10" onmouseover="this.stop()" onmouseout="this.start()">
                                        <span>
                                            <b runat="server" id="fund1"></b>
                                            <img runat="server" id="img1" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund2"></b>
                                            <img runat="server" id="img2" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund3"></b>
                                            <img runat="server" id="img3" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund4"></b>
                                            <img runat="server" id="img4" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund5"></b>
                                            <img runat="server" id="img5" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund6"></b>
                                            <img runat="server" id="img6" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund7"></b>
                                            <img runat="server" id="img7" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund8"></b>
                                            <img runat="server" id="img8" src="Picture/same.png" width="10" height="10" />
                                        </span>
                                        <span>
                                            <b runat="server" id="fund9"></b>
                                            <img runat="server" id="img9" src="Picture/same.png" width="10" height="10" />
                                        </span>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small">
                        <small runat="server" id="dateA"></small>
                        <small>| 
                        <a href="resources/fundinfo.aspx">
                            <small>
                                <u class="ViewHistory">View History</u>
                            </small>
                        </a>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/jquery.liMarquee.js"></script>
    <link href="Content/pdmargin.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('.str3-2').liMarquee({
                direction: 'left',
                loop: -1,
                scrolldelay: 0,
                scrollamount: 50,
                circular: true,
                drag: true
            });
        })
    </script>
</asp:Content>
