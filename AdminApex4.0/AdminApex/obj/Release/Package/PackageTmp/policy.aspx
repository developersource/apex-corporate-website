﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="policy.aspx.cs" Inherits="AdminApex.policy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <div class="single docs privacypolicy">
    <div class="content pages">
    	<div class="about header bg-white">
            <div class="header-content bg-dark-blue">
                <div class="header-copy">
                    <h2>Privacy Policy</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="content narrow">
    	<div class="narrow-copy">
            <p>Astute Fund Management Berhad ("AFMB") (“the Company”, “our”, “us”, or “we”) is a holder of a Capital Markets Services Licence issued under the Capital Market and Services Act 2007 permitting the Company to carry out regulated activities of fund management and dealings in securities (restricted to unit trust).</p>

            <h4>This means, most importantly, that we do not sell client information-whether it is your personal information or the fact that you are an Astute Fund Management Berhad ("AFMB") client-to anyone.</h4>
            <p>In the course of the Company carrying out the aforesaid permitted activities, the Company may collect, record, hold, store or process your Personal Data (as defined in the Personal Data Protection Act 2010). We have always (and will continue to do so) respected the privacy and confidentiality of all the personal information we have received and collected in the course of the provision of our services to and/or our dealings with you and taken all reasonable steps to ensure the proper safeguard of such information.</p>

            <h4>Your Consent</h4>
            <p>By proceeding to access and use this Site, you are deemed to have consented to the collection and use of your personal information including for the disclosure of such personal information to the relevant entities and/or regulatory bodies.</p>

            <h4>Safeguarding of your Personal Data</h4>
            <p>Please take note that your Personal Data may be stored or processed to or in locations or systems in jurisdictions outside Malaysia (where necessary to facilitate the provision of our services and products to you) subject to those jurisdictions having similar data protection laws in place and/or our securing reciprocal confidentiality undertakings.</p><br />
            <p>Please be assured that we will take all necessary practical steps including but not limited to incorporating reasonable security measures into any equipment in which your Personal Data is stored, to protect your Personal Data from any loss, misuse or unauthorised access or disclosure.</p>

            <h4>Description of the Personal Data We Collect and Process</h4>
            <p>You provide personal information when you complete an AFMB account application. (If you enter information in an online application, we may store the information even if you don't complete or submit the application.). You also required providing personal information when you request a transaction that involves AFMB or one of the AFMB affiliated companies.</p><br />
            <p>In addition to personal information you provide to us, we may receive information about you that you have authorised third parties to provide to us. We also may obtain personal information from third parties in order for us to verify your identity, prevent fraud, and to help us identify products and services that may benefit you.</p><br />
            <p>Personal information collected from any source may include your: </p>
            <ul>
                <li>Name and address</li>
                <li>identification Card Number/Passport Number</li>
                <li>Assets</li>
                <li>Income</li>
                <li>Account balance</li>
                <li>Investment activity</li>
                <li>Accounts at other institutions</li>
            </ul><br />
            <p>We will not disclose any of your personal information except where required by regulatory bodies, regulation of legal process, judicial order or as required or permitted by law or for lawful purposes only. Further, we may use your personal information within AFMB, professional advisors, consultants and/or third party's performing services on our behalf and such confidential information will only be used by the said parties for the purpose for which it is provided whereby the said parties have undertaken to treat such information confidential.</p>

            <h4>Security of Your Personal Data</h4>
            <ul>
                <li>AFMB protects your Personal Data by ensuring we have sufficient security measures in place and shall ensure that your Personal Data is stored and handled in such a way as to prevent any unauthorised disclosure.</li>
                <li>AFMB shall take all reasonable action to prevent unauthorised use, access or disclosure of and to protect the confidentiality of your Personal Data in connection with the purpose for which the Personal Data, has been disclosed to, or has been collected by us.</li>
                <li>In order to alert you to other financial products and services that AFMB offers or sponsors, we may share your information within the AFMB affiliated companies. This would include, for example, sharing your information within AFMB to make you aware of new AFMB funds or other investment offerings and trust services offered through AFMB’s trust companies and registered investment advisors.</li>
                <li>In certain instances, we may contract with non-affiliated companies to perform services for us. Where necessary, we will disclose information we have about you to these third parties. In all such cases, we provide the third party with only the information necessary to carry out its assigned responsibilities and only for that purpose. And, we require these third parties to treat your private information with the same high degree of confidentiality that we do.</li>
                <li>Finally, we will release information about you if you direct us to do so, if we are compelled by law to do so, or in other legally limited circumstances (for example, to prevent fraud).</li>
            </ul>

            <h4>How we protect privacy online</h4>
            <p>Our concern for the privacy of our shareholders naturally extends to those who use our Site, <a href="ourvalues.aspx" target="_blank">www.apexis.com.my</a></p>
            <p>a)&nbsp;Our Site uses some of the most secure forms of online communication available, including data encryption, Secure Sockets Layer (SSL) protocol, and user names and passwords. These technologies provide a high level of security and privacy when you access your account information, initiate online transactions, or send secure messages;</p><br />
            <p>b)&nbsp;<a href="ourvalues.aspx" target="_blank">www.apexis.com.my</a>&nbsp;offers customised features that require our use of "HTTP cookies" - tiny pieces of information that we ask your browser to store. However, we make very limited use of these cookies. We don't use them to pull data from your hard drive, to learn your e-mail address, or to view data in cookies created by other Sites. We won't share the information in our cookies or give others access to it-except to help us better serve your investment needs;
            </p><br />
            <p>c)&nbsp;When you visit our Site, we may collect certain technical and navigational information, such as computer browser type, Internet protocol address, and pages visited and average time spent on our Site. This information may be used, for example, to alert you to software compatibility issues or to resolve technical or service problems, or it may be analysed to improve our Web design and functionality and our ability to service you and your accounts.
            </p>

            <h4>What you can do</h4>
            <p>For your protection, if you have an online account with us, we advise you NOT to reveal any information relating to your account to anyone. You are responsible to maintain the confidentiality of your account information, user names, login ids, password, security questions and answers to access your account in this Site. DO NOT reveal your account information, user names, login ids, password, security questions and answers to access your account in this Site, to anyone at any time under any circumstances. You acknowledge that you are fully responsible for your account and activities and should you become aware of any suspicious and unauthorised activities or any other breach of security, please act immediately and notify us as soon as possible. You should also aware that there shall be risk factors involved in sending confidential information over the Internet Sites, e-mails or other electronic means.</p><br />  
            <p>By using this Site, you consent and authorise for such transfer of information and you agree that we may contact you through any of the means stated herein. You further consent and authorise that you will receive information from us through the electronic means from time to time and/or other means as determined by us and/or as required by the regulatory bodies and/or by the law.</p><br /> 
            <p>Online safety tips:-</p>
            <ol>
                <li>You should keep your confidential information in a safe place;</li>
                <li>Always protect your PINs and other passwords. You should not reveal the PINs and passwords to anyone unless it's for a service or transaction you request;</li>
                <li>Check your account regularly. Contact us immediately should there any error or dispute a transaction in your account.</li>
                <li>
                    If you believe you are a victim of identity theft, take immediate action and keep records of your conversations and correspondence.</li>

            </ol><br /> 
            <p>Do contact us immediately. File a report with your local police station and keep a copy of the police report for records.</p>

            <h4>Rights to Modify</h4>
            <p>We reserve the right to modify this policy at any time without prior notice to you. Any such amendment shall be effective once the revised changes have been posted on the Site. Since we may update the Site from time to time, you should constantly check the contents herein from time to time so that you are aware of any changes or amendments to the policy.</p>

            <h4>You need to update your current information</h4>
            <p>You should always ensure that you provide us with your most updated and current personal particulars and information in order to ensure that your records with us are kept up to date, complete and accurate. If any information supplied by you changes during the course of your account with us, you should notify us immediately to enable us to update your information.</p>

            <h4>Any enquiries?</h4>
            <p>If you have any enquiries or concerns on the Privacy Policy, please <a href="http://staging.apexis.com.my/contact" target="_blank">contact us</a> and we will respond to your enquiries as soon as possible.</p>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
