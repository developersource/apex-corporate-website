var thisBox;



$(document).ready(function () {

    //---------------------------------------------------------------------------------

    //dont load subnav engine on certain pages

    //---------------------------------------------------------------------------------

    // if(window.location.pathname === "/" 

    // 	|| window.location.pathname === "/about/profile" 

    // 	|| window.location.pathname === "/about/ourvalues"

    // 	|| window.location.pathname === "/about/financialposition"){

    // 	//dont init

    // } else {

    // 	initSubnav();

    // }

    initSubnav();



    //---------------------------------------------------------------------------------

    //init price ticker

    //---------------------------------------------------------------------------------

    // $("#priceTicker").endlessRiver();

    //get width of ticker children



    //---------------------------------------------------------------------------------

    //hover price ticker

    //---------------------------------------------------------------------------------

    // $("#priceTicker").mouseenter(function(){

    // 	var newLeft = $("#priceTicker li").position().left;

    // 	var speed = 1;

    // 	var interval = 32;

    // 	// console.log(cycled)

    // 	loopEngine(newLeft, cycled, elements, speed, interval)

    // })



    // $("#priceTicker").mouseleave(function(){

    // 	var newLeft = $("#priceTicker li").position().left;

    // 	var speed = 2;

    // 	var interval = 32;

    // 	// console.log(cycled)

    // 	loopEngine(newLeft, cycled, elements, speed, interval)

    // })



    //---------------------------------------------------------------------------------

    //hamburger button

    //---------------------------------------------------------------------------------

    $(".hamburger").click(function () {

        if ($(this).hasClass("open")) {

            showNav(0);

            showExpandedNav(false)

            $(this).removeClass("open")



        } else {

            showNav(1);

            $(this).addClass("open")



        }

    })



    //---------------------------------------------------------------------------------

    //disclaimer popup

    //---------------------------------------------------------------------------------

    $(".popup-tnc-box-copy").mCustomScrollbar({

        scrollbarPosition: "outside",

        theme: "dark"

    })

    $(".popup-agree").click(function () {

        $("body").mCustomScrollbar("update");

        $(".popup-tnc").fadeOut(500, globalEasing);
        document.cookie = "popup=0"
    });


    //---------------------------------------------------------------------------------

    //annoucement popup 

    //---------------------------------------------------------------------------------

    $(".popup-close").click(function () {

        $(".modal-container").fadeOut(500, globalEasing);
    });

    //---------------------------------------------------------------------------------

    //on window resize

    //---------------------------------------------------------------------------------

    $(window).resize(function () {

        // console.log("resize")

        resetTable();

        initSubnav();

        $("#annual_statement").hide();

        $("#other_services").hide();

        $(".monitor-expand").hide()

        $(".monitor-expand .bubble-pin").hide()

        if ($(".expandable-div-holder").is(":visible")) {

            thisBox.find(".expandable-div-cover").addClass("animate").css({ "clip": "rect(0," + $(window).width() + "px," + $(window).height() + "px,0" })

        }

        if (mDevice()) {

            $(".nav-right").stop().css({ "right": "-100%" })

            $(".expanded-nav").stop().css({ "top": "80px", "right": "-100%" })

            $(".nav-left-bg.bg-transparent").css({ "top": "-80px" })

            showNav(0);

            $(".hamburger").removeClass("open")

            showExpandedNav(0)

        } else {

            $(".nav-right").stop().animate({ "right": "0" }, 400, globalEasing)

            $(".expanded-nav").stop().css({ "top": "-560px", "right": "0" })

            $(".nav-left-bg.bg-transparent").css({ "top": "0" })

        }

        if ($(".expandable-div-holder").is(":visible")) {

            thisBox1 = thisBox.offset().top;

            thisBox2 = thisBox.offset().left + thisBoxWidth;

            thisBox3 = thisBox.offset().top + thisBoxHeight;

            thisBox4 = thisBox.offset().left;

            $(".expandable-div-holder").children(".expandable-div-cover").css({ "clip": "rect(0," + $(window).width() + "," + $(window).height() + ",0" })

        }

    });



    //---------------------------------------------------------------------------------

    //init landing slider

    //---------------------------------------------------------------------------------

    // $('.home-slider').slick({

    // 	autoplay: true,

    // 	autoplaySpeed: 5000,

    // 	arrows: false,

    // 	easing: globalEasing,

    // 	dots: false,

    // 	infinite: false,

    // 	speed: 300,

    // 	slidesToShow: 1,

    // 	adaptiveHeight: true,

    // });



    //---------------------------------------------------------------------------------

    //init landing segmenter

    //---------------------------------------------------------------------------------

    //console.log(window.location.pathname);

    if (window.location.pathname == "/") {

        // animLand(1)

        $(".one.segmenter").addClass("animate")

        setTimeout(function () {

            animLand(2)

            setTimeout(function () {

                animLand(3)

            }, 5000)

        }, 5000)

        setInterval(function () {

            animLand(1)

            setTimeout(function () {

                animLand(2)

                setTimeout(function () {

                    animLand(3)

                }, 5000)

            }, 5000)

        }, 15000);



    }



    if (window.location.pathname == "/Invest") {

        $('.close-btn').hide();

    }



    //---------------------------------------------------------------------------------

    //Initialize mcustomscrollbar and trigger hide/show of navbar

    //---------------------------------------------------------------------------------

    var lastPos = 0;

    $("body").mCustomScrollbar({

        scrollbarPosition: "inside",

        theme: "dark",

        // scrollInertia: 0,

        // mouseWheel:{

        // 	scrollAmount: 1

        // },

        callbacks: {

            onCreate: function () {



            },

            onInit: function () {

                // $(".loader").fadeOut(1000,globalEasing)
                //for disclaimer popup
                if ((window.location.pathname.toLowerCase().indexOf("resources") > -1 || window.location.pathname.toLowerCase().indexOf("invest") > -1) && popup == 1 && window.location.pathname.toLowerCase().indexOf("news") == -1) {

                    setTimeout(function () {
                        $("body").mCustomScrollbar("disable", false);

                        console.log("popup")

                    }, 200)

                }


                //---------------------------------------------------------------------------------

                //onpage jump to function

                //---------------------------------------------------------------------------------

                if (window.location.pathname.indexOf("Invest") > 0 && popup == 0) {

                    var goTo = $("#get_link").attr("data-page");

                    if (goTo == "risk") {

                        $("body").mCustomScrollbar("scrollTo", ".risks");

                    }

                }

            },

            whileScrolling: function () {

                //---------------------------------------------------------------------------------

                //control nav bar background opacity based on scroll

                //---------------------------------------------------------------------------------

                // console.log(-this.mcs.top)

                var x = (-this.mcs.top) / ($(".header-content").height() - 80)

                // console.log(x)

                if (-this.mcs.top >= ($(".header-content").height() - 80)) {

                    $(".nav-bg").css({ "opacity": x })

                } else {

                    $(".nav-bg").css({ "opacity": x })

                }

                if (lastPos > this.mcs.top) {

                    // showNav(false);

                    // showExpandedNav(false);

                    $(".login-nav").hide();

                    // console.log("down")

                } else if ((lastPos < this.mcs.top)) {

                    if (!mDevice()) {

                        showNav(true);

                    }

                    // console.log("up")

                }

                lastPos = this.mcs.top;


                if (window.location.pathname.indexOf("abouts/timeline") > 0) {
                    // console.log($(window).height()+" : "+$("#shareholdChart").offset().top)

                    //---------------------------------------------------------------------------------

                    //init chart when reach the chart

                    //---------------------------------------------------------------------------------
                    if ($("#shareholdChart").offset().top < ($(window).height() - 300) && initChart === 0) {

                        //---------------------------------------------------------------------------------

                        //Initialize chartjs as it comes into window

                        //---------------------------------------------------------------------------------

                        initChart = 1;

                        var ctx = document.getElementById("shareholdChart");

                        var myChart = new Chart(ctx, {

                            type: 'doughnut',

                            data: {

                                labels: [/*"JF Apex Securities Berhad",*/ "Med-BumikarMara Sdn Bhd", "Management & Others"],

                                datasets: [

                                    {

                                        data: [ 43, 57],

                                        backgroundColor: [

                                            /*"#6c6c6c",*/

                                            "#375f92",

                                            "#b7dee8"

                                        ],

                                        hoverBackgroundColor: [

                                            /*"#FF6384",*/

                                            "#36A2EB",

                                            "#FFCE56"

                                        ]

                                    }]

                            },

                            options: {

                                legend: {

                                    display: false

                                },

                                rotation: 5.14 * Math.PI

                            }

                        });

                    }

                }

                //---------------------------------------------------------------------------------

                //change url when scroll to private mandate

                //---------------------------------------------------------------------------------

                //         if(window.location.pathname.indexOf("products") > 0){

                //         	var a = $(".private_mandate").offset().top

                //         	// console.log(($(window).height()/2)+" : "+a)

                // var thisURL1 = window.location.pathname.split("/")[1];

                //         	var thisURL2;

                //         	if(a <= ($(window).height()/2)){

                //         		thisURL2 = "private_mandate"

                //         	} else {

                //         		var curPage = $(".subnav ul li").find(".active").attr("page")

                //         		thisURL2 = "unit_trust/"+curPage;

                //         	}

                //         	window.history.replaceState("object or string", "products", "/"+thisURL1+"/"+thisURL2+"/");

                //         }

            }

        }

    });

    //---------------------------------------------------------------------------------

    //init scrollbar for popup

    //---------------------------------------------------------------------------------

    // $(".profile-bio-holder .copy").mCustomScrollbar({

    // 	scrollbarPosition: "outside",

    // 	theme:"dark",

    // })



    //---------------------------------------------------------------------------------

    //Hover nav-bar to expand nav-bar

    //---------------------------------------------------------------------------------

    $(".nav-right ul li a").not($("#nav-login")).mouseenter(function () {

        if (!mDevice()) {

            triggerExpandedNav(this);

        }

    })



    $(".nav-right ul li a").not($("#nav-login")).click(function () {

        if (mDevice()) {

            triggerExpandedNav(this);

        }

    })



    //---------------------------------------------------------------------------------

    //Close nav bar when leaving the header container

    //---------------------------------------------------------------------------------

    $(".main-nav-container").mouseleave(function () {

        showExpandedNav(false)

    })



    //---------------------------------------------------------------------------------

    //Making selection of input clickable area bigger than input area

    //---------------------------------------------------------------------------------

    $(".login-nav-fields").click(function () {

        $(".login-nav-fields").removeClass("selected")

        $(this).addClass("selected").children("input").focus();

    })



    //---------------------------------------------------------------------------------

    //Making selection of input selectable with keyboard tabs

    //---------------------------------------------------------------------------------

    $(".login-nav-fields input").focusin(function () {

        $(".login-nav-fields").removeClass("selected")

        $(this).parent().addClass("selected").focus();

    })



    //---------------------------------------------------------------------------------

    //reset selectors on change of selections

    //---------------------------------------------------------------------------------

    $(".login-nav-fields input").focusout(function () {

        $(".login-nav-fields").removeClass("selected")

    })



    //---------------------------------------------------------------------------------

    //Show/hide login bubble

    //---------------------------------------------------------------------------------

    // $("#nav-login").click(function(){

    // 	showExpandedNav(false)

    // 	if(!$(".login-nav").is(":animated")){

    //  	if($(".login-nav").is(":visible")){

    //  		$(".login-nav").fadeOut(400, globalEasing, function(){

    //  			$(".login-nav").removeClass("open")

    //  		})

    //  	} else {

    //  		$(".login-nav").fadeIn(400, globalEasing, function(){

    //  			$(".login-nav").addClass("open")

    //  		})

    //  	}

    //  }

    // })



    //---------------------------------------------------------------------------------

    //Our values slant engine

    //---------------------------------------------------------------------------------

    // var a = $(".pointers-info ul li").length;

    // var b = 0;

    // $(".pointers-info ul li:nth-child(1)").css({"margin-left":b})

    // b = -20;

    // for(i = 2; i <= a; i++){

    // 	$(".pointers-info ul li:nth-child("+i+")").css({"margin-left":b})

    // 	b = b-17

    // }



    //---------------------------------------------------------------------------------

    //Show/hide footer sitemap

    //---------------------------------------------------------------------------------

    $("#sitemap").click(function () {

        if (!$(".footer.lower .footer-content").is(":visible")) {

            // $(".footer-control img").removeClass("flipV")

            // $(".footer-control a span").html("Close Site Map")

            var oriHeight = $('body div')[0].scrollHeight - 80 //80 is height of navbar 

            var sitemapy = $("#mCSB_1_container").outerHeight();
            if (sitemapy == 0)
                var sitemapy = $("#mCSB_2_container").outerHeight();
            console.log('>> ' + sitemapy);

            $(".footer.lower").show(function () {


                $("body").mCustomScrollbar("scrollTo", sitemapy);

            })



            console.log("This is If");


        } else {

            // $(".footer-control img").addClass("flipV")

            // $(".footer-control a span").html("Open Site Map")

            var a = $('.footer.upper').offset().top

            // var a = $(".footer.lower").outerHeight()

            var b = $(document).height() - a - $('.footer.upper').outerHeight();

            // var c = a - b;

            console.log(b)

            //$("body").mCustomScrollbar("scrollTo", b);

            setTimeout(function () {

                $(".footer.lower").hide()

            }, 1500)
            console.log("This is Else");
        }


    })



    //---------------------------------------------------------------------------------

    //Big overlay engine

    //---------------------------------------------------------------------------------



    var thisBox1;

    var thisBox2;

    var thisBox3;

    var thisBox4;

    var thisBoxWidth;

    var thisBoxHeight;

    var activeTile_

    $(".tiles li").click(function () {

        thisBox = $(this);



        console.log(thisBox.hasClass("open"))



        if (!!document.documentMode) {//ie fallback



            if (!thisBox.hasClass("open")) {

                //console.log(thisBox)

                thisBox.children(".expandable-div-holder").fadeIn()

                thisBox.addClass("open")

                $("body").mCustomScrollbar("disable", false);

            }

        } else {

            if ($(this).attr("data-js") != "disabled") {

                thisBoxWidth = thisBox.width();

                thisBoxHeight = thisBox.height();

                thisBox1 = thisBox.offset().top;

                thisBox2 = thisBox.offset().left + thisBoxWidth;

                thisBox3 = thisBox.offset().top + thisBoxHeight;

                thisBox4 = thisBox.offset().left;

                console.log(thisBox1)

                console.log(thisBox2)

                console.log(thisBox3)

                console.log(thisBox4)

                if (!$(this).children(".expandable-div-holder").is(":visible")) {

                    $("body").mCustomScrollbar("disable", false);

                    thisBox.children(".expandable-div-holder").fadeIn()

                    thisBox.children(".expandable-div-holder").children(".expandable-div-cover").css({ "clip": "rect(" + thisBox1 + "px," + thisBox2 + "px," + thisBox3 + "px," + thisBox4 + "px)" })

                    setTimeout(function () {

                        // thisBox.children(".expandable-div-holder").children(".expandable-div-cover").addClass("animate").css({"clip":"rect(0,"+$(window).width()+","+$(window).height()+",0 !important"})

                        console.log(thisBox.find(".expandable-div-cover"))

                        thisBox.find(".expandable-div-cover").addClass("animate").css({ "clip": "rect(0," + $(window).width() + "px," + $(window).height() + "px,0" })

                    }, 600);

                }

            }

        }

    })



    $(".expandable-div-content .close-btn").click(function (e) {

        if (!!document.documentMode) {//ie fallback

            if (thisBox.hasClass("open")) {

                thisBox.find(".expandable-div-holder").stop(true, true).fadeOut('fast', function () {

                    $(".expandable-div-holder").removeClass("open");

                    console.log('-- closed - ' + thisBox.data('pname'))

                    thisBox.removeClass("open");

                })

                $(".expandable-div-holder").removeClass("open");

                //activeTile_.removeClass("open");

            }

        } else {

            $(this).parent().parent().parent().parent().parent(".expandable-div-cover").css({ "clip": "rect(" + thisBox1 + "px," + thisBox2 + "px," + thisBox3 + "px," + thisBox4 + "px)" })

            setTimeout(function () {

                $(".expandable-div-holder").fadeOut()

                $(".expandable-div-cover").removeClass("animate")



            }, 600);

        }

        $("body").mCustomScrollbar("update");

    })



    //---------------------------------------------------------------------------------

    //Invest in apex accorion engine

    //---------------------------------------------------------------------------------

    //benefits

    $("#accord_benefits").click(function () {

        if ($("#benefits").is(":visible")) {

            $("#benefits").slideUp(400, globalEasing);

            $("#accord_benefits img").attr("src", "ICONPIC/plus-icon.png")

            $("#accord_benefits span").html("Learn More")

            $("#benefits .lower-li").slideUp(400, globalEasing);

            $("#benefits .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $("#benefits .upper-li").removeClass("bold")

        } else {

            $("#benefits").slideDown(400, globalEasing);

            $("#accord_benefits img").attr("src", "ICONPIC/minus-icon.png")

            $("#accord_benefits span").html("Hide")

        }

    })

    $("#benefits .upper-li").click(function () {

        if ($(this).next().is(":visible")) {

            $(this).next().slideUp(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/plus-icon.png")

            $(this).removeClass("bold")

        } else {

            $("#benefits .lower-li").slideUp(400, globalEasing);

            $("#benefits .upper-li").removeClass("bold")

            $("#benefits .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $(this).next().slideDown(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/minus-icon.png")

            $(this).addClass("bold")

        }

    })

    //how to get started

    $("#accord_how").click(function () {

        if ($("#get-started-list").is(":visible")) {

            $("#get-started-list").slideUp(400, globalEasing);

            $("#accord_how img").attr("src", "ICONPIC/plus-icon-light.png")

            $("#accord_how span").html("Learn More")

            $("#get-started-list .lower-li").slideUp(400, globalEasing);

            $("#get-started-list .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $("#get-started-list .upper-li").removeClass("bold")

        } else {

            $("#get-started-list").slideDown(400, globalEasing);

            $("#accord_how img").attr("src", "ICONPIC/minus-icon-light.png")

            $("#accord_how span").html("Hide")

        }

    })

    $("#get-started-list .upper-li").click(function () {

        if ($(this).next().is(":visible")) {

            $(this).next().slideUp(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/plus-icon.png")

            $(this).removeClass("bold")

        } else {

            $("#get-started-list .lower-li").slideUp(400, globalEasing);

            $("#get-started-list .upper-li").removeClass("bold")

            $("#get-started-list .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $(this).next().slideDown(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/minus-icon.png")

            $(this).addClass("bold")

        }

    })

    //how to monitor

    $(".monitor .close-btn").click(function () {

        $(".monitor .monitor-expand").fadeOut(400, globalEasing, function () {

            $("#annual_statement").hide();

            $("#other_services").hide();

        })

        $("body").mCustomScrollbar("update");

    })



    $("#annual_statement_btn").click(function () {

        if ($("#annual_statement").is(":visible")) {

            $("#annual_statement").hide();

            $("#other_services").hide();

            $(".monitor-expand .bubble-pin").hide()

        } else {

            $(".monitor .monitor-expand .close-btn").fadeIn(400, globalEasing)

            if (mDevice()) {

                $("#annual_statement").show();

                $(".monitor .monitor-expand").fadeIn(400, globalEasing)

                $("body").mCustomScrollbar("disable", false);

            } else {

                $("#annual_statement").show();

                $("#other_services").hide();

                $(".monitor-expand").show()

                $(".monitor-expand .bubble-pin").show().css({ "left": "5%" })

            }

        }

    })

    $("#other_services_btn").click(function () {

        if ($("#other_services").is(":visible")) {

            $("#annual_statement").hide();

            $("#other_services").hide();

            $(".monitor-expand .bubble-pin").hide()

        } else {

            $(".monitor .monitor-expand .close-btn").fadeIn(400, globalEasing)

            if (mDevice()) {

                $("#other_services").show();

                $(".monitor .monitor-expand").fadeIn(400, globalEasing)

                $("body").mCustomScrollbar("disable", false);

            } else {

                $("#annual_statement").hide();

                $("#other_services").show();

                $(".monitor-expand").show()

                $(".monitor-expand .bubble-pin").show().css({ "left": "95%" })

            }

        }

    })

    //risks

    $("#accord_risks").click(function () {

        if ($("#all_risks").is(":visible")) {

            $("#all_risks").slideUp(400, globalEasing);

            $("#accord_risks img").attr("src", "ICONPIC/plus-icon.png")

            $("#accord_risks span").html("Learn More")

            $("#all_risks .special-lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-upper-li img").attr("src", "ICONPIC/round-down-arrow.png")

            $("#all_risks .special-lower-li .lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-lower-li .upper-li").removeClass("bold")

            $("#all_risks .special-lower-li .upper-li img").attr("src", "ICONPIC/plus-icon.png")

        } else {

            $("#all_risks").slideDown(400, globalEasing);

            $("#accord_risks img").attr("src", "ICONPIC/minus-icon.png")

            $("#accord_risks span").html("Hide")

        }

    })

    $("#all_risks .special-upper-li").click(function () {

        if ($(this).next().is(":visible")) {

            $(this).next().slideUp(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/round-down-arrow.png")

            $("#all_risks .special-lower-li .lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-lower-li .upper-li").removeClass("bold")

            $("#all_risks .special-lower-li .upper-li img").attr("src", "ICONPIC/plus-icon.png")

        } else {

            $("#all_risks .special-lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-upper-li img").attr("src", "ICONPIC/round-down-arrow.png")

            $("#all_risks .special-lower-li .lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-lower-li .upper-li").removeClass("bold")

            $("#all_risks .special-lower-li .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $(this).next().slideDown(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/round-up-arrow.png")

        }

    })

    $("#all_risks .special-lower-li .upper-li").click(function () {

        if ($(this).next().is(":visible")) {

            $(this).next().slideUp(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/plus-icon.png")

            $(this).removeClass("bold")

        } else {

            $("#all_risks .special-lower-li .lower-li").slideUp(400, globalEasing);

            $("#all_risks .special-lower-li .upper-li").removeClass("bold")

            $("#all_risks .special-lower-li .upper-li img").attr("src", "ICONPIC/plus-icon.png")

            $(this).next().slideDown(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/minus-icon.png")

            $(this).addClass("bold")

        }

    })



    //---------------------------------------------------------------------------------

    //Datepicker

    //---------------------------------------------------------------------------------

    $("#datepicker").datepicker({

        showAnim: "fadeIn",

    });

    $("#datepicker").datepicker('setDate', new Date());

    $("#datepicker").datepicker("option", "dateFormat", "d MM yy");

    $("#datepicker2").datepicker({

        showAnim: "fadeIn",

    });

    $("#datepicker2").datepicker('setDate', new Date());

    $("#datepicker2").datepicker("option", "dateFormat", "d MM yy");



    //---------------------------------------------------------------------------------

    //Query historical fund prices

    //---------------------------------------------------------------------------------

    //init on load

    var historical_url = $("#prices_form").attr("action");

    if (window.location.pathname.indexOf("fundinfo") > 0) {

        getPrices(0, new Date(), new Date(), historical_url, 1, "resources");

    } else if (window.location.pathname == "/") {

        historical_url = $("#priceTicker").attr("data-url")

        getPrices(0, new Date(), new Date(), historical_url, 1, "home");

    }

    $("#prices_form").submit(function (e) {

        e.preventDefault();

        var fund_picker = $("#fund_picker option:selected").attr("system_id"),

            sDate = $("#datepicker").datepicker("getDate"),

            eDate = $("#datepicker2").datepicker("getDate");

        getPrices(fund_picker, sDate, eDate, historical_url, 0, "resources");

    })



    //---------------------------------------------------------------------------------

    //Query distribution

    //---------------------------------------------------------------------------------

    //init on load

    var dist_url = $("#distribution_form").attr("action");

    $("#distribution_form").submit(function (e) {

        e.preventDefault();

        var fund_picker = $("#dist_picker option:selected").attr("system_id")

        getDist(fund_picker, dist_url);

    })



    //---------------------------------------------------------------------------------

    //Funds download accordion engine

    //---------------------------------------------------------------------------------

    $('.fund_cat_btn').click(function () {

        if ($(this).next().is(":visible")) {

            $(this).next().slideUp(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/round-down-arrow.png")

        } else {

            // $(".category_contents").slideUp(400, globalEasing);

            // $("#products_highlight_sheet .func_cat_btn img").attr("src","../../img/round-down-arrow.png")

            $(this).next().slideDown(400, globalEasing);

            $(this).children("img").attr("src", "ICONPIC/round-up-arrow.png")

        }

    })



    //---------------------------------------------------------------------------------

    //pagination engine

    //---------------------------------------------------------------------------------

    $(".pagination a").click(function () {

        var thisTable = $(this).parent().attr("data-table");

        var url = $(this).parent().attr("data-url");

        var goToPage = $(this).attr("data-page");

    })



    //---------------------------------------------------------------------------------

    //feedback button

    //---------------------------------------------------------------------------------

    $(".qtype").click(function () {

        var qtype = $(this).attr("data-type")

        if (qtype == "enquiry") {

            typevalue = "Ask us anything and we will try our best to help you...";

        } else if (qtype == "feedback") {

            typevalue = "Leave any feedback or tip regarding our products and services...";

        } else {

            typevalue = "Have you encountered any problems? Raise your concerns to us...";

        }

        $(".custom_textarea textarea").attr("placeholder", typevalue);

    })



    //---------------------------------------------------------------------------------

    //send feedback

    //---------------------------------------------------------------------------------

    $(".custom_textarea p a").click(function () {

        $(".custom_textarea p a").removeClass("active");

        $(this).addClass("active");

    });

    $(document).on('input', '#contact_form textarea[id="txtcomment"]', function () {

        var l = $("#contact_form textarea[id='txtcomment']").val().length;

        $("#comment_length span").html(l);

        $(".error.comment").hide()

        if (l >= 980 && l < 1000) {

            $("#comment_length").css({ "color": "#fa7a15" })

        } else if (l >= 1000) {

            $("#comment_length").css({ "color": "red" })

        } else {

            $("#comment_length").css({ "color": "black" })

        }

    })

    $("#send_feedback").click(function () {

        $(".error").hide();

        var pass = 0;

        var atpos = $("#contact_form input[id='txtemail']").val().indexOf("@");

        var dotpos = $("#contact_form input[id='txtemail']").val().lastIndexOf(".");

        if ($("#contact_form input[id='txtname']").val() == "" || $("#contact_form input[id='txtname']").val().length <= 0) {

            $(".error.fullname").html("Mandatory field")

            $(".error.fullname").show();

        } else {

            pass++;

        }

        if ($("#contact_form input[id='txttel']").val() == "" || $("#contact_form input[id='txttel']").val().length <= 0) {

            $(".error.contact_num").html("Mandatory field")

            $(".error.contact_num").show()

        } else if ($("#contact_form input[id='txttel']").val().match(/[a-z]/i) || $("#contact_form input[id='txttel']").val().length < 9) {

            $(".error.contact_num").html("This is an invalid entry. Please try again.")

            $(".error.contact_num").show()

        } else {

            pass++;

        }

        if ($("#contact_form input[id='txtemail']").val() == "" || $("#contact_form input[id='txtemail']").val().length <= 0) {

            $(".error.email").html("Mandatory field")

            $(".error.email").show()

        } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= $("#contact_form input[id='txtemail']").val().length) {

            $(".error.email").html("This is an invalid entry. Please try again.")

            $(".error.email").show()

        } else {

            pass++;

        }

        if ($("#contact_form textarea[id='txtcomment']").val() == "" || $("#contact_form textarea[id='txtcomment']").val().length <= 0) {

            $(".error.comment").html("Mandatory field")

            $(".error.comment").show()

        } else {

            pass++;

        }

        if (pass == 4) {
            $("#loading_feedback").show();
            $.ajax({
                type: "POST",
                url: "Contact.aspx/SendEmail",
                data: '{name: "' + $("#txtname").val() + '", tel: "' + $("#txttel").val() + '",  email: "' + $("#txtemail").val() + '",  comment: "' + $("#txtcomment").val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#success_holder").fadeIn(500);
                    $("#loading_feedback").hide();
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        else {
            return false;
        }

    })


    $("#feedback_ok").click(function () {

        $("textarea").val("")

        $("input").val("")

        $("#comment_length span").html("0")

        $("#success_holder").fadeOut(500);

    })



    //---------------------------------------------------------------------------------

    //mobile table engine

    //---------------------------------------------------------------------------------

    $(".mobile-years:nth-child(1)").addClass("active")

    $(".mobile-years").click(function () {

        $(".mobile-years").removeClass("active")

        $(this).addClass("active")

        var column = $(this).attr("data-year")

        $(".year-1, .year-2, .year-3").hide()

        $(".year-" + column).show()

    })



    //---------------------------------------------------------------------------------

    //close expanded nav

    //---------------------------------------------------------------------------------

    $(".close-expanded-nav").click(function () {

        showExpandedNav(0)

    })

});