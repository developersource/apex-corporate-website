﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="AdminApex.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="single contact contactpage">
        <div class="content pages">
            <div class="about header bg-white">
                <div class="header-content wide-image" style="background: #273148;">
                    <div class="header-copy">
                        <h2>Your trust,our duty.</h2>
                        <!-- <img class="apex-line" src="http://staging.apexis.com.my/img/apex-line.png"/> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="content narrow">
            <h3 class="first">Get in touch</h3>
            <div class="left map-title">
                <h4>Business Office</h4>
                <div class="map-container">
                    <div class="left">
                        <p>
                            Astute Fund Management Berhad<br>
                            (formerly known as Apex Investment Services Berhad)<br>
                            3rd Floor, Menara MBSB,<br>
                            46 Jalan Dungun,
                            <br>
                            Damansara Heights,<br>
                            50490 Kuala Lumpur, Malaysia.
                        </p>
                        <p>
                            Tel: <a href="tel:+60320959999">(603) 2095 9999</a><br>Fax: <a href="tel:+60320950693">(603) 2095 0693</a></br>
                            Website: <a href="//www.astutefm.com.my" target="_blank">www.astutefm.com.my</a><br>
                            Email: <a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a>
                        </p>
                    </div>
                    <div class="right">
                        <a href="https://goo.gl/maps/8CFpH4pgSuN2" target="_blank">
                             <img src="Picture/apexMap.jpg" alt="Astute Fund Management Berhad" />
                        </a>
                       
                    </div>
                </div>
            </div>
            <div class="right map-title" style="display: none;">
                <h4>Head Office</h4>
                <div class="map-container">
                    <div class="left">
                        <p>
                            Astute Fund Management Berhad
                            <br>
                            Suite 7.02, 7th Floor, Menara Apex,<br>
                            Off Jalan Semenyih, Bukit Mewah,<br>
                            43000 Kajang,<br>
                            Selangor Darul Ehsan, Malaysia.
                        </p>
                        <p>
                            Tel: <a href="tel:+60387361118">(603) 8736 1118</a>
                            <br>
                            Fax: <a href="tel:+60387377924">(603) 8737 7924</a><br>
                            Website: <a href="//www.apexis.com.my" target="_blank">www.apexis.com.my</a><br>
                            Email: <a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a>
                        </p>
                    </div>
                    <div class="right">
                        <a href="https://goo.gl/maps/JWxfk34pjaC2" target="_blank">
                            <img src="http://staging.apexis.com.my/img/contact/registered-map.jpg" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content bg-gray" id="form_wrapper">
            <div class="content narrow" id="form_holder" runat="server">
                <form id="contact_form" runat="server" clientidmode="static" data-url="">
                    <div class="left">
                        <p>Name<span class="error fullname"></span></p>
                        <asp:TextBox ID="txtname" runat="server" placeholder="Full Name" ClientIDMode="Static"></asp:TextBox>
                        <p>Contact number<span class="error contact_num"></span></p>
                        <asp:TextBox ID="txttel" runat="server" placeholder="e.g. +60123456789" ClientIDMode="Static"></asp:TextBox>
                        <p>Email address<span class="error email"></span></p>
                        <asp:TextBox ID="txtemail" runat="server" placeholder="name@email.com" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <div class="right">
                        <p>Enquire or leave a comment:<span class="error comment"></span></p>
                        <div class="custom_textarea">
                            <!-- Do not change the tiltes of Enquiry, Feedback and Complaints! -->
                            <p><a class="qtype active" data-type="enquiry">Enquiry</a></p>
                            <textarea id="txtcomment" name="comment" clientidmode="static" runat="server" placeholder="Ask us anything and we will try our best to help you..." maxlength="1000" wrap="hard"></textarea>
                        </div>
                        <p id="comment_length"><span>0</span>/1000</p>
                    </div>
                    <a class="rounded-btn bg-light-blue" id="send_feedback">Submit</a>
                    <img id="loading_feedback" src="ICONPIC/loading.gif" />
                </form>
            </div>
            <div class="content narrow" id="success_holder" runat="server" clientidmode="static">
                <div class="content-holder">
                    <h4>Thank you for your submission.</h4>
                    <p>Our team will evaluate your entry shortly and get back to you soon. </p>
                    <a href="javascript:;" class="rounded-btn bg-light-blue" id="feedback_ok">Close</a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="narrow-copy contact_faq">
                <p><b>Who should I contact for further information or to lodge a complaint?</b></p>
                <p>
                    <!-- <img src="http://staging.apexis.com.my/img/contact/bubble1.png"/> -->
                    For internal dispute resolution, you may contact our Customer Service personnel:
                </p>
                <table class="bg-gray">
                    <tr>
                        <td>a) via phone to:</td>
                        <td><a href="tel:+60320959999">03-2095 9999</a></td>
                    </tr>
                    <tr>
                        <td>b) via fax to:</td>
                        <td><a href="tel:+60320950693">03-2095 0693</a></td>
                    </tr>
                    <tr>
                        <td>c) via email to:</td>
                        <td><a href="mailto:enquiry@astutefm.com.my">enquiry@astutefm.com.my</a></td>
                    </tr>
                    <tr>
                        <td>d) via letter to:</td>
                        <td>Astute Fund Management Berhad<br>
                            (formerly known as Apex Investment Services Berhad)<br>
                            3rd Floor, Menara MBSB, 46, Jalan Dungun,
                            <br>
                            Damansara Heights, 50490 Kuala Lumpur<br>
                        </td>
                    </tr>
                </table>
                <p>
                    <!-- <img src="http://staging.apexis.com.my/img/contact/bubble2.png"/> -->
                    If you are dissatisfied with the outcome of the internal dispute resolution process, please refer your dispute to the Securities Industries Dispute Resolution Corporation (SidREC):
                </p>
                <table class="bg-gray">
                    <tr>
                        <td>a) via phone to:</td>
                        <td><a href="tel:+60322822280">03-2282 2280</a></td>
                    </tr>
                    <tr>
                        <td>b) via fax to:</td>
                        <td><a href="tel:+60322823855">03-2282 3855</a></td>
                    </tr>
                    <tr>
                        <td>c) via email to:</td>
                        <td><a href="mailto:info@sidrec.com.my">info@sidrec.com.my</a></td>
                    </tr>
                    <tr>
                        <td>d) via letter to:</td>
                        <td>Securities Industry Dispute Resolution Center (SidREC)<br>
                            Unit A-9-1, Level 9, Tower A, Menara UOA Bangsar,<br>
                            No 5, Jalan Bangsar Utama 1, 59000 Kuala Lumpur.
                        </td>
                    </tr>
                </table>
                <p>
                    <!-- <img src="http://staging.apexis.com.my/img/contact/bubble3.png"/> -->
                    You can also direct your complaint to the SC even if you have initiated a dispute resolution process with SidREC. To make a complaint, please contact the SC’s Investor Affairs &amp; Complaints Department:
                </p>
                <table class="bg-gray">
                    <tr>
                        <td>a) via phone to:</td>
                        <td><a href="tel:+60362048999">03-6204 8999</a></td>
                    </tr>
                    <tr>
                        <td>b) via fax to:</td>
                        <td><a href="tel:+60362048997">03-6204 8997</a></td>
                    </tr>
                    <tr>
                        <td>c) via email to:</td>
                        <td><a href="mailto:aduan@seccom.com.my">aduan@seccom.com.my</a></td>
                    </tr>
                    <tr>
                        <td>d) via online complaint form available at:</td>
                        <td><a href="//www.sc.com.my" target="_blank">www.sc.com.my</a></td>
                    </tr>
                </table>
                <p>Alternatively, you can also escalate your complaint to the Federation of Investment Managers Malaysia (FIMM):</p>
                <table class="bg-gray" style="padding: 20px 0;">
                    <tr>
                        <td>a) via phone to:</td>
                        <td><a href="tel:+60320923800">03-2092 3800</a></td>
                    </tr>
                    <tr>
                        <td>b) via fax to:</td>
                        <td><a href="tel:+60320932700">03-2093 2700</a></td>
                    </tr>
                    <tr>
                        <td>c) via email to:</td>
                        <td><a href="mailto:complaints@fimm.com.my">complaints@fimm.com.my</a></td>
                    </tr>
                    <tr>
                        <td>d) via online complaint form available at:</td>
                        <td><a href="//www.fimm.com.my" target="_blank">www.fimm.com.my</a></td>
                    </tr>
                    <tr>
                        <td>e) via letter to:</td>
                        <td>Legal, Secretarial &amp; Regulatory Affairs,
                            <br>
                            Federation of Investment Managers Malaysia,
                            <br>
                            19-06-1, 6th Floor Wisma Tune,
                            <br>
                            No. 19, Lorong Dungun,<br>
                            Damansara Heights,<br>
                            50490 Kuala Lumpur.</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
