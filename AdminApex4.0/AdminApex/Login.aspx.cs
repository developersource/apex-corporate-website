﻿using AdminApex.ApexUtility;
using AdminApex.ApexData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminApex.ApexService;

namespace AdminApex
{
    public partial class Login : System.Web.UI.Page
    {
        static int attemptcount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void login_Click(object sender, EventArgs e)
        {
            if (txtusername.Text != "" || txtpassword.Text != "")
            {
                string username = txtusername.Text;
                string password = txtpassword.Text;
                List<ApexUtility.Admin> admins = AdminData.GetAllAdmin();
                ApexUtility.Admin admin = new ApexUtility.Admin();

                if(admins.Where(x => (x.username == username)).FirstOrDefault() == null) {
                    Response.Write("<script>window.alert('Account not found');</script>");
                }
                else
                {
                    admin = admins.Where(x => (x.Status == 1 && x.username == username) || (x.Status == 0 && x.username == username)).FirstOrDefault();

                    if (admin.Status == 1)
                    {
                        admin = admins.Where(x => x.Status == 1 && x.username == username).FirstOrDefault();

                        if (admin.locked == 0 )
                        {
                            //admin = admins.Where(x => x.username == username && x.Password == password && x.Status == 1).FirstOrDefault();
                            if (admin.username == username && admin.Password == password)
                            {
                                Session["admin"] = admin;

                                //user log                    
                                WriteTextFile wtf = new WriteTextFile();
                                if (DateTime.Now.Day.ToString().Equals("1"))
                                {
                                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();

                                    if (userLog == null)
                                    {
                                        //create new text file
                                        string path = Server.MapPath("/Log/");
                                        string[] data = { DateTime.Now.ToString(), admin.name, "Login", "Success" };
                                        string url = wtf.Create(path, data);

                                        //update db
                                        UserLogService.Update();
                                        UserLog newUserLog = new UserLog();
                                        newUserLog.Title = "UserLog-" + DateTime.Now.ToString("MMMMyyyy");
                                        newUserLog.UrlPath = "/Log/" + url;
                                        newUserLog.Month = DateTime.Now.Month.ToString();
                                        newUserLog.Year = DateTime.Now.Year.ToString();
                                        newUserLog.CreatedDate = DateTime.Now;
                                        newUserLog.Status = 1;
                                        newUserLog = UserLogService.Insert(newUserLog);
                                    }
                                    else
                                    {
                                        //write user log
                                        string[] data = { DateTime.Now.ToString(), admin.name, "Login", "Success" };
                                        wtf.Write(Server.MapPath(userLog.UrlPath), data);
                                    }
                                }
                                else
                                {
                                    //try
                                    //{
                                    //write userlog
                                    UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Status == 1).FirstOrDefault();
                                    string[] data = { DateTime.Now.ToString(), admin.name, "Login", "Success" };
                                    wtf.Write(Server.MapPath(userLog.UrlPath), data);
                                    //}
                                    //catch
                                    //{

                                    //}
                                }

                                Response.Redirect("/ApexWeb/ViewFinancialInfo.aspx");
                            }
                            else
                            {
                                locked.Text = "Invalid Username or Password - Remaining : " + (4 - attemptcount);
                                //Response.Write("<script>window.alert('Invalid Username or Password - '" + (2 - attemptcount) + "' remaining');</script>");
                                attemptcount = attemptcount + 1;
                            }
                        }
                        else
                        {
                            Response.Write("<script>window.alert('Your Account Locked Already : Contact Administrator');</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>window.alert('Your Account Blocked Already');</script>");
                    }
                }
                
            }
            else
            {
                Response.Write("<script>window.alert('<i class='fa fa-close icon'></i> Please enter (<sup>*</sup>) required fields.');</script>");
            }
            if (attemptcount == 5)
            {
                Response.Write("<script>window.alert('Your Account Has Been Locked Due to Three Invalid Attempts - Contact Administrator');</script>");
                string username = txtusername.Text;
                List<ApexUtility.Admin> admins = AdminData.GetAllAdmin();
                ApexUtility.Admin admin = new ApexUtility.Admin();

                admin = admins.Where(x => x.username == username).FirstOrDefault();
                admin.locked = 1;
                AdminData.UpdateLOCK(admin);
                attemptcount = 0;

                //write user log
                string[] data = { DateTime.Now.ToString(), admin.name, "Login", "Failed" };
                WriteTextFile wtf = new WriteTextFile();
                UserLog userLog = UserLogService.GetAllUserLog().Where(x => x.Month == (DateTime.Now.Month.ToString()) && x.Year == (DateTime.Now.Year.ToString()) && x.Status == 1).FirstOrDefault();
                wtf.Write(Server.MapPath(userLog.UrlPath), data);
            }
        }
    }
}